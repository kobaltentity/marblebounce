﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

public class GameController : SingletonGeneric<GameController>
{
    [Header("Prefabs")]
    [SerializeField] Platform startPlatformPrefab;
    [SerializeField] Platform endPlatformPrefab;
    [SerializeField] List<Platform> platformPrefabs;
    [SerializeField] public SlimeController slimePrefab;
    [SerializeField] public Coin coinPrefab;
    [SerializeField] public Hoop hoopPrefab;

    [Header("Settings")]
    [SerializeField] float touchSensitivity = .65f;
    [SerializeField] float touchSensitivityMobile = .65f;

    [SerializeField] List<LevelSetting> availableSettings;
    [SerializeField] Material groundSharedMaterial;
    [SerializeField] Material hoopsSharedMaterial;

    public float coinSpawnChance = 0.1f;
    public float swipeThreshold = 1f;
    public Transform platformsRoot;


    public GameSave gameSave;
    List<Coin> levelCoins;
    List<Hoop> levelHoops;
    List<Platform> levelPlatforms;
    SlimeController currentSlime;
    Vector3 lastPos;
    Tween fogDensityTween;
    Tween timeScaleTween;
    LevelSetting currentLevelSettings;
    EndPlatform endPlatform;

    [HideInInspector] public bool isGameOver = true;
    bool isGameStarted = false;
    bool isEndReached = false;
    bool isInTutorial = false;


    int maxJump;
    int currentJump;
    float startFogDensity;

    private void Awake()
    {
        Feedback.InitAndroidFeedback();
    }

    void Start()
    {
        InputManager.Instance.OnTouchBegan += OnTouchBegan;
        InputManager.Instance.OnTouchUpdate += OnTouchUpdate;
        InputManager.Instance.OnTouchEnded += OnTouchEnded;

        gameSave = GameSave.LoadSave();
        //GameSave.ResetSave(ref gameSave);

        startFogDensity = RenderSettings.fogDensity;
        UIController.Instance.SetCurrentLevel(gameSave.currentLevel);
        UIController.Instance.SetCoinAmount(gameSave.coins);

        InitLevel();
        SpawnPlayer();
    }

    private void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
#endif
    }

    public void SlimeJump()
    {
        AddJump(1);
    }

    public void CollectCoin(Coin coin)
    {
        gameSave.coins++;
        Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.ImpactLight);
    }

    public void CoinSpawned(Coin newCoin)
    {
        levelCoins.Add(newCoin);
    }

    public void HoopSpawned(Hoop hoop)
    {
        levelHoops.Add(hoop);
    }

    public void AddJump(int amount)
    {
        currentJump += amount;
        UIController.Instance.SetJumpAmount(currentJump, maxJump);
    }

    public void PlayerOnPlatform(Platform platform)
    {
        if (isEndReached) return;

        int idx = levelPlatforms.IndexOf(platform);
        int jumpLeft = 0;
        for (int i = idx+1; i < levelPlatforms.Count; i++)
        {
            jumpLeft += Mathf.FloorToInt(levelPlatforms[i].Length * .48f);
        }

        if(jumpLeft + currentJump > maxJump)
        {
            Platform newPlat = ContinueLevel(endPlatformPrefab, false);
            endPlatform = newPlat.GetComponent<EndPlatform>();

            isEndReached = true;
        }
        else if (idx >= levelPlatforms.Count - 6)
        {
            ContinueLevel(platformPrefabs[Random.Range(0, platformPrefabs.Count)], true);
        }
    }

    public void GameOver(bool victory)
    {
        if (isGameOver) return;
        isGameOver = true;
        CameraController.Instance.Follow(null);

        if (victory)
            Victory();
        else
            Defeat();
    }

    void Victory()
    {
        //endParticle = Instantiate(endParticlePrefab, currentSlime.transform.position + Vector3.forward * 6f, Quaternion.identity);
        //endParticle.transform.SetParent(CameraController.Instance.transform, true);
        //Destroy(endParticle, 6f);
        endPlatform.FireParticles();
        Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.Success);

        DOVirtual.DelayedCall(2f, () =>
        {
            ChangeFog(1f, 4f);
            UIController.Instance.EndScreenCompletion(Mathf.Clamp01(currentJump / (float)maxJump), true);
        });

        gameSave.currentLevel++;
        gameSave.currentLevelSetting++;
        gameSave.currentLevelSetting %= availableSettings.Count;

        GameSave.Save(gameSave);
    }
    
    void Defeat()
    {
        Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.Failure);
        UIController.Instance.EndScreenCompletion(Mathf.Clamp01(currentJump / (float)maxJump), false);
        GameSave.Save(gameSave);
    }

    public float GetDifficulty()
    {
        float ratio = Mathf.Clamp01(currentJump / (float)maxJump);
        float difficulty = 0 + ratio * 0.1f; // from 0 to .1

        difficulty += ratio * Mathf.Clamp(gameSave.currentLevel * 0.05f, 0, .25f); // adds 0 to .2 max

        return difficulty;
    }

    public void EnterTutorial()
    {
        if (isInTutorial) return;
        isInTutorial = true;

        UIController.Instance.ToggleTutoDashPanel(true);
        ChangeTimeScale(0f, .05f);
    }
    public void ExitTutorial()
    {
        isInTutorial = false;
        gameSave.tutorialSeen = true;

        UIController.Instance.ToggleTutoDashPanel(false);
        ChangeTimeScale(1f, .8f);
    }

    void ApplyLevelSettings(LevelSetting currentSettings)
    {
        groundSharedMaterial.color = currentLevelSettings.groundColor;
        hoopsSharedMaterial.color = currentLevelSettings.hoopsColor;
        groundSharedMaterial.SetTexture("_MainTex", currentSettings.groundTexture);
    }

    Platform ContinueLevel(Platform prefab, bool deletePrevious)
    {
        Platform newPlat = CreatePlatform(ref lastPos, prefab);
        GameObject obj = levelPlatforms[0].gameObject;
        if(deletePrevious)
        {
            levelPlatforms.RemoveAt(0);
            Destroy(obj);
        }
        return newPlat;
    }


    void InitLevel()
    {
        levelHoops = new List<Hoop>();
        levelCoins = new List<Coin>();
        levelPlatforms = new List<Platform>();
        lastPos = Vector3.zero;
        maxJump = 100 + gameSave.currentLevel * 5;
        currentJump = 0;
        ChangeFog(startFogDensity, 0f);
        isEndReached = false;
        Hoop.rewardCombo = 0;
        UIController.Instance.SetCurrentLevel(gameSave.currentLevel);

        currentLevelSettings = availableSettings[gameSave.currentLevelSetting];
        ApplyLevelSettings(currentLevelSettings);


        CreatePlatform(ref lastPos, startPlatformPrefab);

        for (int i = 0; i < 8; i++)
        {
            Platform prefab = platformPrefabs[Random.Range(0, platformPrefabs.Count)];
            CreatePlatform(ref lastPos, prefab);
        }
    }

    void ClearLevel()
    {
        for (int i = 0; i < levelPlatforms.Count;)
        {
            Platform plat = levelPlatforms[0];
            Destroy(plat.gameObject);
            levelPlatforms.RemoveAt(0);
        }

        for (int i = 0; i < levelCoins.Count;)
        {
            Coin coin = levelCoins[0];
            Destroy(coin.gameObject);
            levelCoins.RemoveAt(0);
        }

        for (int i = 0; i < levelHoops.Count;)
        {
            Hoop hoop = levelHoops[0];
            Destroy(hoop.gameObject);
            levelHoops.RemoveAt(0);
        }

        Destroy(currentSlime.gameObject);
    }

    Platform CreatePlatform(ref Vector3 localPos, Platform prefab)
    {
        Platform plat = Instantiate(prefab);

        plat.transform.SetParent(platformsRoot, false);
        plat.transform.localPosition = localPos;

        localPos += new Vector3(0, 0, plat.Length);
        levelPlatforms.Add(plat);

        return plat;
    }

    void ChangeFog(float goalDensity, float duration)
    {
        fogDensityTween?.Kill();
        fogDensityTween = DOVirtual.Float(RenderSettings.fogDensity, goalDensity, duration, (f) =>
        {
            RenderSettings.fogDensity = f;
        }).SetEase(Ease.Linear);
    }

    void ChangeTimeScale(float goalTimeScale, float realTimeDuration)
    {
        timeScaleTween?.Kill();
        timeScaleTween = DOVirtual.Float(Time.timeScale, goalTimeScale, realTimeDuration, (f) =>
        {
            Time.timeScale = f;
        }).SetEase(Ease.Linear).SetUpdate(true);
    }

    void SpawnPlayer()
    {
        currentSlime = Instantiate(slimePrefab, levelPlatforms[0].GetMarbleSpawnPos(), Quaternion.identity);
        CameraController.Instance.Follow(currentSlime.cubeScaleParent.transform);
    }

    void StartGame()
    {
        isGameOver = false;
        isGameStarted = true;

        currentSlime.StartBouncing();
        UIController.Instance.OnPlay();
    }
    public void ResetGame()
    {
        isGameStarted = false;
        UIController.Instance.BackToMainMenu();
        ClearLevel();
        InitLevel();
        SpawnPlayer();
    }

    void OnTouchBegan()
    {   
        if(!isGameStarted)
        {
            StartGame();
        }
    }
    void OnTouchUpdate()
    {
        Vector2 touchDir = InputManager.Instance.GetTouchDifference();

#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        currentSlime.InputDirection(touchDir.x * touchSensitivityMobile);
#else
        currentSlime.InputDirection(touchDir.x * touchSensitivity);
#endif

    }
    void OnTouchEnded()
    {
        currentSlime.ApplyLateralPosition();
    }
}
