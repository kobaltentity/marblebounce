﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] Transform marbleSpawnPos;
    public int Length;

    [HideInInspector] public int angleID;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetMarbleSpawnPos()
    {
        return marbleSpawnPos.position + Vector3.up * marbleSpawnPos.localScale.y / 2f;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 0, 1, 0.35f);
        Gizmos.DrawCube(marbleSpawnPos.position, marbleSpawnPos.localScale + Vector3.one * 0.01f);
    }
}
