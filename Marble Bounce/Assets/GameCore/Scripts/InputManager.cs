﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : SingletonGeneric<InputManager>
{
	public delegate void TouchDelegate();
	public TouchDelegate OnTouchBegan;
	public TouchDelegate OnTouchUpdate;
	public TouchDelegate OnTouchEnded;

    Vector3 lastTouchPosition;
    Vector3 currentTouchPos;
	Vector3 touchesInitialPosition;
	bool isTouching;
    float touchDuration;

	void Start()
	{
        touchDuration = 0;
        isTouching = false;
		Input.multiTouchEnabled = false;
        Application.targetFrameRate = 60;
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0) && EventSystem.current.currentSelectedGameObject == null)
		{
			isTouching = true;
			touchesInitialPosition = currentTouchPos  = lastTouchPosition = Input.mousePosition;

            touchDuration = 0;

            OnTouchBegan?.Invoke();
		}

		if (Input.GetMouseButton(0) && EventSystem.current.currentSelectedGameObject == null)
		{
            if(isTouching == false)
            {
                isTouching = true;
                touchesInitialPosition = currentTouchPos = lastTouchPosition = Input.mousePosition;
                touchDuration = 0;
            }

            lastTouchPosition = currentTouchPos;
            currentTouchPos = Input.mousePosition;
            touchDuration += Time.deltaTime;
            OnTouchUpdate?.Invoke();
		}

		if (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == null)
		{
            OnTouchEnded?.Invoke();
            currentTouchPos = Input.mousePosition;
			isTouching = false;
        }

	}

	public float GetTouchMagnitude()
	{
		return GetTouchVector().magnitude;
	}

    public float GetTouchDuration()
    {
        return touchDuration;
    }

	public Vector2 GetTouchVector()
	{
		if (!isTouching)
		{
			return Vector2.zero;
		}

		return (Input.mousePosition - touchesInitialPosition) / Screen.dpi;
	}

    public Vector2 GetTouchDifference()
    {
        if (!isTouching)
        {
            return Vector2.zero;
        }

        return (currentTouchPos - lastTouchPosition) / Screen.dpi;
    }


    public void ResetTouchVector()
    {
        isTouching = false;
    }
}
