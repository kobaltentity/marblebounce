﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update
    bool CanBeCollected = true;
    MeshRenderer meshRenderer;
    ParticleSystem particles;

    void Start()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        particles = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!CanBeCollected) return;
        CanBeCollected = false;

        GameController.Instance.CollectCoin(this);

        meshRenderer.enabled = false;
        particles.Emit(20);

        //Destroy(gameObject, 1f) ;
    }
}
