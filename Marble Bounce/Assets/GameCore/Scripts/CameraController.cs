﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : SingletonGeneric<CameraController>
{
    private static Camera currentCamera;
    public static Camera CurrentCamera {
        get {
            if(currentCamera == null)
            {
                currentCamera = CameraController.Instance.GetComponent<Camera>();
            }
            return currentCamera;
        }
    }

    [SerializeField] float cameraSpeed;
    [SerializeField] Vector3 offset;

    Vector3 initalCameraPos;
    Transform followTarget;

    void Start()
    {
        
    }

    void Update()
    {
        UpdateFollow();
    }

    void UpdateFollow()
    {
        if (followTarget == null) return;

        float xDiff = followTarget.transform.position.x - initalCameraPos.x;
        Vector3 goalPos = new Vector3(initalCameraPos.x + xDiff * .5f, offset.y, followTarget.position.z + offset.z);

        if(followTarget.position.y > 3)
        {
            goalPos += new Vector3(0, followTarget.position.y - 1, 0);
        }


        transform.position = Vector3.Lerp(transform.position,
           goalPos,
           cameraSpeed * Time.deltaTime);
    }

    public void Follow(Transform newTarget)
    {
        followTarget = newTarget;

        if (newTarget == null)
            return;

        transform.position = newTarget.position + offset;
        initalCameraPos = transform.position;
    }
}
