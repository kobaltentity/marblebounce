﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Hoop : MonoBehaviour
{
    [SerializeField] GameObject popupPrefab;
    [SerializeField] ParticleSystem particle;
    [SerializeField] ParticleSystem particle2;

    bool activated = false;
    public static int rewardCombo = 0;
    Tween rotateTween;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnHitSide(Vector3 worldPos)
    {
        if (activated) return;
        activated = true;

        rewardCombo = 0;
        Vector3 localPos = worldPos - transform.position;

        rotateTween = transform.DORotate(new Vector3(0, 1800 * -Mathf.Sign(localPos.x), 0), 270f, RotateMode.FastBeyond360).SetSpeedBased(true).SetEase(Ease.OutQuad);
    }

    public void OnHitCenter()
    {
        if (activated) return;
        activated = true;
        rewardCombo++;

        GameObject newPopup = Instantiate(popupPrefab, transform.position + new Vector3(0,0.35f,-0.2f), Quaternion.identity);
        Sequence newSeq = DOTween.Sequence();
        newPopup.GetComponentInChildren<Text>().text = "+" + rewardCombo.ToString();

        newPopup.transform.localScale = Vector3.zero;

        newSeq.Append(newPopup.transform.DOScale(1, 1f).SetEase(Ease.OutElastic));
        newSeq.AppendInterval(.3f);
        newSeq.Append(newPopup.transform.DOScale(0, .5f).SetEase(Ease.InBack));
        newSeq.OnComplete(()=> {
            Destroy(newPopup);
        });

        GameController.Instance.AddJump(rewardCombo);
        particle.Emit(4 + rewardCombo * 2);
        particle2.Emit(6 + rewardCombo * 3);
    }


}
