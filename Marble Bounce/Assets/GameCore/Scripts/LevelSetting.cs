﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSetting",  menuName = "Level Setting")]
public class LevelSetting : ScriptableObject
{
    public Color hoopsColor;
    public Color groundColor;
    public Texture groundTexture;
}
