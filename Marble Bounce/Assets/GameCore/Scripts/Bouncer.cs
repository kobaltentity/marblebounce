﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bouncer : MonoBehaviour
{
    public iOSHapticFeedback.iOSFeedbackType feedbackType;
    public Transform scaleParent;
    public float bounceHeight = 5f;
    public float bounceDistance = 4f;

    Sequence squeezeSequence;
    Vector3 originalScale;
    void Start()
    {
        originalScale = scaleParent.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        SqueezeAnimation();
    }

    void SqueezeAnimation()
    {
        squeezeSequence?.Kill();

        squeezeSequence = DOTween.Sequence();
        squeezeSequence.Insert(0, scaleParent.DOScale(new Vector3(originalScale.x + .3f, .4f, originalScale.z + .3f), .15f));
        squeezeSequence.AppendInterval(.1f);
        squeezeSequence.Append(scaleParent.DOScale(new Vector3(originalScale.x, 1f, originalScale.z), .8f).SetEase(Ease.OutElastic));
        squeezeSequence.SetEase(Ease.Linear);
        squeezeSequence.SetUpdate(false);
        squeezeSequence.Play();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position + Vector3.forward * bounceDistance, 0.25f);
        int steps = 10;
        for (int i = 1; i <= steps; i++)
        {
            float prevT = (i-1) / (float)steps;
            float currT = i / (float)steps;

            Vector3 lastPos = GetPosOnJump(bounceDistance, bounceHeight, prevT);
            Vector3 currPos = GetPosOnJump(bounceDistance, bounceHeight, currT);

            Gizmos.DrawLine(transform.position + lastPos, transform.position + currPos);
        }
    }

    Vector3 GetPosOnJump(float jumpDist, float jumpHeight, float t)
    {
        float x = t * 2 - 1;
        float y = -(x * x) + 1;
        return new Vector3(0, y * jumpHeight, t * jumpDist);
    }
}
