﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIController : SingletonGeneric<UIController>
{
    [SerializeField] UIScreen mainMenuScreen;
    [SerializeField] UIScreen ingameScreen;
    [SerializeField] UIScreen retryScreen;
    [SerializeField] UIScreen victoryScreen;

    [SerializeField] GameObject tutoDashPanel;

    [SerializeField] Slider levelProgression;
    [SerializeField] Text levelText;
    [SerializeField] Text coinText;
    [SerializeField] Text completionText;
    [SerializeField] Text maxJumpText;
    [SerializeField] Text jumpText;
    void Start()
    {
        mainMenuScreen.SetEnable(true, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPlay()
    {
        mainMenuScreen.SetEnable(false);
        ingameScreen.SetEnable(true);
        levelProgression.value = 0f;
        maxJumpText.text = "100";
        jumpText.text = "0";
    }

    public void SetJumpAmount(int jump, int maxJump)
    {
        jump = Mathf.Min(jump, maxJump);

        levelProgression.DOValue(Mathf.Clamp01(jump / (float)maxJump), .1f);
        maxJumpText.text = maxJump.ToString();
        jumpText.text = jump.ToString();
    }
    public void ToggleTutoDashPanel(bool value)
    {
        tutoDashPanel.SetActive(value);
    }

    public void EndScreenCompletion(float completion, bool victory)
    {
        if(victory)
        {
            victoryScreen.SetEnable(true);
        }
        else
        {
            retryScreen.SetEnable(true);
        }
        completionText.text = (completion * 100f).ToString("0.0") + "% Completed";
        completionText.gameObject.SetActive(true);
    }
    public void SetCurrentLevel(int Level)
    {
        levelText.text = "Level " + Level.ToString();
    }

    public void SetCoinAmount(int amount)
    {
        coinText.text = amount.ToString();
    }

    public void BackToMainMenu()
    {
        SetCoinAmount(GameController.Instance.gameSave.coins);

        retryScreen.SetEnable(false);
        ingameScreen.SetEnable(false);
        victoryScreen.SetEnable(false);
        mainMenuScreen.SetEnable(true);

        completionText.gameObject.SetActive(false);
    }
}
