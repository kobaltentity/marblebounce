﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EndPlatform : MonoBehaviour
{
    [SerializeField] List<ParticleSystem> particleSystems;
    [SerializeField] List<float> fireDelay;

    public void FireParticles()
    {
        for (int i = 0; i < particleSystems.Count; i++)
        {
            int unref = i;
            DOVirtual.DelayedCall(fireDelay[i], () =>
            {
                particleSystems[unref].Emit(60);
            });
        }
    }

}
