﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeController : MonoBehaviour
{
    [SerializeField] public GameObject cubeScaleParent;
    [SerializeField] GameObject cubeRotateParent;
    [SerializeField] Rigidbody ballRigidbody;
    [SerializeField] TrailRenderer trailRenderer;
    [SerializeField] ParticleSystem particles;
    //[SerializeField] float bounceDuration = .5f;
    [SerializeField] float landingDuration = .1f;

    [SerializeField] public float hopHeight = .35f;
    [SerializeField] float hopDistance = 2f;

    Quaternion finalSlimeRot;
    Vector3 originalPosition;
    Vector3 stickOffset;

    Sequence jumpTween;
    Sequence scaleBounceTween;
    Tween slimeRotateTween;
    Tween moveTween;

    Vector3 peakScale = new Vector3(.9f, 1.1f, .9f);
    Vector3 landScale = new Vector3(1.2f, .8f, 1.2f);

    bool isJumping = false; // is in jumping anim
    bool isSticked = false; // is sticked on a wall
    bool isMovingLateraly = false; // is moving lateraly
    bool isTransitioning = false; // is transitioning between a dash or a release
    bool inputApplied = true; // is ready for next input

    int stickDirection;
    float initialLateralPosition = 0;
    float diffLatPos;

    private void Awake()
    {
        finalSlimeRot = Quaternion.identity;
        originalPosition = cubeScaleParent.transform.localPosition;
    }

    void Start()
    {
        //DOVirtual.DelayedCall(1, StartBouncing);
        //CameraController.Instance.Follow(transform);
    }

    void Update()
    {
        CheckHoops();
        CheckTutorial();
    }

    void CheckTutorial()
    {
        if (GameController.Instance.isGameOver) return;
        if(!GameController.Instance.gameSave.tutorialSeen && isJumping && (CanDash(1) || CanDash(-1))) 
        {
            GameController.Instance.EnterTutorial();
        }
    }

   

    public void InputDirection(float xDirection)
    {
        if (GameController.Instance.isGameOver) return;
        if (isTransitioning) return;
        inputApplied = false;

        if(!isMovingLateraly && isJumping) // If already moving lateraly
        {
            if(Mathf.Abs(xDirection) > GameController.Instance.swipeThreshold)
                Dash(xDirection > 0 ? 1 : -1);
        }
        else if(isSticked)
        {
            bool stickDirOpposite = Mathf.RoundToInt(Mathf.Sign(stickDirection)) != Mathf.RoundToInt(Mathf.Sign(xDirection));
            if (Mathf.Abs(xDirection) > GameController.Instance.swipeThreshold && stickDirOpposite)
                UnStick();
        }
        else
        {
            SetLateralPosition(xDirection);
        }
    }

    void SetLateralPosition(float xDirection)
    {
        isMovingLateraly = true;
        diffLatPos = xDirection;
        transform.localPosition += new Vector3(xDirection,0,0);
    }

    public void ApplyLateralPosition()
    {
        isMovingLateraly = false;
        inputApplied = true;
    }

    void UnStick()
    {
        Vector3 offset = new Vector3(-stickDirection * 2f, .1f, 1f);
        Ray newRay = new Ray(cubeRotateParent.transform.position + offset, Vector3.down);
        RaycastHit hitInfo;
        if (!Physics.Raycast(newRay, out hitInfo, 15f, 1 << LayerMask.NameToLayer("Ground"))) // If ground not found
        {
            //GameOver();
            isTransitioning = true;
            CeaseMovement();
            finalSlimeRot = Quaternion.AngleAxis(-180 * stickDirection, Vector3.forward) * finalSlimeRot;
            AnimationRotateSlime(.5f - 0.1f);

            Vector3 pos = transform.position + offset;
            jumpTween = cubeScaleParent.transform.DOJump(pos, 2f, 1, .5f).SetEase(Ease.Linear).SetUpdate(false).OnComplete(() =>
            {
                Fall();
            });
            AnimationScale(.5f, null);
        }
        else // else if ground found
        {
            Transform hitTransform = hitInfo.collider.transform;
            float duration = GetDurationFromJumpHeight(hitInfo.distance) * .38f;
            Vector3 pos = hitTransform.position + Vector3.up * hitTransform.localScale.y / 2f;

            cubeScaleParent.transform.localRotation = Quaternion.identity;
            cubeScaleParent.transform.localPosition -= stickOffset;
            stickOffset = Vector3.zero;

            isTransitioning = true;
            CeaseMovement();
            finalSlimeRot = Quaternion.AngleAxis(-180 * stickDirection, Vector3.forward) * finalSlimeRot;
            AnimationRotateSlime(duration-0.1f);

            moveTween = transform.DOMove(pos, duration).SetUpdate(false);
            jumpTween = cubeScaleParent.transform.DOJump(pos, 2f, 1, duration).SetEase(Ease.Linear).SetUpdate(false).OnComplete(() =>
            {
                isSticked = false;
                isTransitioning = false;
                OnLanded();
            });

            AnimationScale(duration, NextMove);
        }
    }

    void Dash(int xDirection)
    {
        Vector3 dir = new Vector3(xDirection, 0, 0);
        Ray newRay = new Ray(cubeRotateParent.transform.position, dir);
        RaycastHit hitInfo;

        stickDirection = xDirection;

        if (Physics.Raycast(newRay, out hitInfo, 15, 1 << LayerMask.NameToLayer("Ground")))
        {
            CeaseMovement();
            isSticked = true;
            isTransitioning = true;

            stickOffset = new Vector3(0, .5f, 0);

            cubeScaleParent.transform.localPosition += stickOffset;
            cubeScaleParent.transform.localRotation = Quaternion.Euler(0, 0, 90 * xDirection);
            trailRenderer.emitting = true;
            Vector3 projPos = hitInfo.transform.position;
            projPos.y = transform.position.y;
            projPos -= dir * hitInfo.transform.localScale.x/2f;

            GameController.Instance.ExitTutorial();

            transform.DOLocalMove(projPos, hitInfo.distance * 0.03f).SetEase(Ease.Linear).SetUpdate(false).OnComplete(() =>
            {
                trailRenderer.emitting = false;
                OnLanded();
                isTransitioning = false;
                NextMove();
            });
        }
        else
        {

        }
    }

    bool CanDash(int xDirection)
    {
        Vector3 dir = new Vector3(xDirection, 0, 0);
        Ray newRay = new Ray(cubeRotateParent.transform.position, dir);
        RaycastHit hitInfo;

        return Physics.Raycast(newRay, out hitInfo, 15, 1 << LayerMask.NameToLayer("Ground"));
    }

    //public void Jump(float jumpHeight, float distance, bool forced = false)
    //{
    //    if (gameOver) return;

    //    nextJumpHeight = jumpHeight;
    //    nextJumpDistance = distance;
    //}


    void OnLanded()
    {
        particles.Emit(30);
        //Feedback.Vibrate(new long[] {100, 30, 100, 30 }, 0);
    }


    public void StartBouncing()
    {
        if (GameController.Instance.isGameOver) return;
        NextMove();
    }

    void NextMove()
    {
        isJumping = false;
        Bouncer bumper = GetBumper();

        GameController.Instance.SlimeJump();
        float SpeedModifier = GameController.Instance.GetDifficulty() * .25f;
        //Debug.Log(SpeedModifier);

        if (bumper != null)
        {
            isJumping = true;
            float duration = GetDurationFromJumpHeight(bumper.bounceHeight);

            Feedback.SendFeedback(bumper.feedbackType);

            AnimationJump(bumper.bounceHeight, duration, NextMove);
            MoveForward(bumper.bounceDistance, duration + landingDuration * 0.1f);
        }
        else if(isSticked)
        {
            float duration = .15f;
            finalSlimeRot = Quaternion.AngleAxis(90, Vector3.right) * finalSlimeRot;

            if(!CheckCollision())
            {
                UnStick();
            }
            else
            {
                OnLanded();
                Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.SelectionChange);

                MoveForward(1, duration - 0.02f, false);
                AnimationScale(duration, NextMove); // NextMove
                AnimationRotateSlime(duration);
            }
        }
        else
        {
            float duration = GetDurationFromJumpHeight(hopHeight) * .75f;
            finalSlimeRot = Quaternion.AngleAxis(90, Vector3.right) * finalSlimeRot;

            MoveForward(hopDistance, duration + landingDuration * 0.1f  - SpeedModifier);
            AnimationJump(hopHeight, duration - SpeedModifier, NextMove);
            AnimationRotateSlime(duration * .9f - SpeedModifier);
            Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.SelectionChange);

        }
    }

    Bouncer GetBumper()
    {
        Collider[] colliders = Physics.OverlapSphere(cubeScaleParent.transform.position, .15f, 1 << LayerMask.NameToLayer("Bumper"));
        for (int i = 0; i < colliders.Length; i++)
        {
            Bouncer bouncer = colliders[i].GetComponent<Bouncer>();
            if(bouncer)
            {
                return bouncer;
            }
        }
        return null;
    }

        

    void AnimationJump(float jumpHeight, float jumpDuration, TweenCallback onLanded = null)
    {
        float duration =  jumpDuration; 

        Transform targetTransform = cubeScaleParent.transform;
        Vector3 previousPos = targetTransform.position;

        jumpTween?.Kill();
        jumpTween = targetTransform.DOLocalJump(originalPosition + stickOffset, jumpHeight, 1, duration).SetEase(Ease.Linear);
        jumpTween.SetUpdate(false);
        jumpTween.OnComplete(() =>
        {
            OnLanded();
        });
        //jumpTween.OnUpdate(() => {
        //    ballRigidbody.MovePosition(ballRigidbody.transform.position);//updates physics
        //});
        AnimationScale(duration, onLanded);
    }

    void AnimationScale(float duration, TweenCallback onLanded)
    {
        Transform targetTransform = cubeScaleParent.transform;
        Vector3 rPeakScale;
        Vector3 rLandScale;


        scaleBounceTween?.Kill();
        scaleBounceTween = DOTween.Sequence();
        scaleBounceTween.Insert(0, targetTransform.DOScale(peakScale, landingDuration).SetEase(Ease.InCubic)); // Landing to Peak
        scaleBounceTween.Insert(Mathf.Min(duration * .2f, landingDuration), targetTransform.DOScale(Vector3.one, duration * .15f).SetEase(Ease.OutQuad)); // Peak to default scale
        scaleBounceTween.Insert(duration * .65f, targetTransform.DOScale(peakScale, duration * .15f).SetEase(Ease.OutQuad)); // default scale to Peak
        if(duration > .2f)
        {
            scaleBounceTween.Insert(duration, targetTransform.DOScale(landScale, landingDuration).SetEase(Ease.OutCubic)); // Peak to Landing
        }
        scaleBounceTween.SetUpdate(false);
        scaleBounceTween.OnComplete(onLanded);
    }

    float GetDurationFromJumpHeight(float jumpHeight)
    {
        return Mathf.Sqrt(jumpHeight) * .5f;
    }

    void AnimationRotateSlime(float duration)
    {
        slimeRotateTween?.Kill();
        slimeRotateTween = cubeRotateParent.transform.DOLocalRotateQuaternion(
           finalSlimeRot, duration);
        slimeRotateTween.SetEase(Ease.Linear);
        slimeRotateTween.SetUpdate(false);
    }

    void MoveForward(float distance, float duration, bool checkGameOver = true)
    {
        moveTween?.Kill();
        moveTween = transform.DOBlendableLocalMoveBy(new Vector3(0, 0, distance), duration).SetEase(Ease.Linear).OnComplete(() =>
        {
            if(checkGameOver)
                CheckGameOver();
        });
        moveTween.SetUpdate(false);
    }

    void CheckGameOver()
    {
        if(!CheckCollision())
        {
            Fall();
        }
    }

    bool CheckCollision()
    {
        //GameController.Instance.SlimeJump();

        Collider[] colliders = Physics.OverlapSphere(cubeScaleParent.transform.position + Vector3.down * .05f, .2f, 1 << LayerMask.NameToLayer("Ground"));
        Platform parentPlatform;
        for (int i = 0; i < colliders.Length; i++)
        {
            parentPlatform = colliders[i].GetComponentInParent<Platform>();
            if(colliders[i].CompareTag("FinishLine"))
            {
                GameController.Instance.GameOver(true);
                transform.DOBlendableLocalMoveBy(new Vector3(-transform.position.x, 0, 0), .2f);

                DOVirtual.DelayedCall(2f, () => {
                    CeaseMovement();
                });
            }

            if(parentPlatform != null)
            {
                GameController.Instance.PlayerOnPlatform(parentPlatform);
                return true;
            }
        }

        if (colliders.Length == 0)
        {
            return false;
        }
        return true;
    }

    void CheckHoops()
    {
        if (GameController.Instance.isGameOver) return;

        Collider[] colliders = Physics.OverlapBox(cubeScaleParent.transform.position, cubeScaleParent.transform.localScale*.5f *.6f, Quaternion.identity, 1 << LayerMask.NameToLayer("Default"));
        bool hitSide = false;
        Hoop hoop = null;

        for (int i = 0; i < colliders.Length; i++)
        {
            hoop = colliders[i].GetComponentInParent<Hoop>();
            if (hoop != null)
            {
                if (colliders[i].CompareTag("HoopSide"))
                {
                    hitSide = true;
                    break;
                }
            }
        }

        if(hoop != null)
        {
            if (hitSide)
            {
                hoop.OnHitSide(transform.position);
                Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.SelectionChange);
            }
            else
            {
                hoop.OnHitCenter();
                Feedback.SendFeedback(iOSHapticFeedback.iOSFeedbackType.ImpactLight);
            }
        }
        
    }

    void Fall()
    {
        CeaseMovement();
        if (GameController.Instance.isGameOver) return;
        GameController.Instance.GameOver(false);

        cubeScaleParent.transform.localScale = peakScale;
        ballRigidbody.isKinematic = false;
        ballRigidbody.angularVelocity = Random.insideUnitSphere * 5f;
        ballRigidbody.velocity = (isSticked ? new Vector3(-stickDirection,0,0) : Vector3.down) * 5f;
    }

    void CeaseMovement()
    {
        moveTween.Kill();
        jumpTween.Kill();
        scaleBounceTween.Kill();
    }

}
