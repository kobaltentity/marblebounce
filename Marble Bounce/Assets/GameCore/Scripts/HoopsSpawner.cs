﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoopsSpawner : MonoBehaviour
{
    [SerializeField] Bouncer connectedBouncer;
    [SerializeField] float spawnChance;

    private void Start()
    {
        RollSpawn();
    }

    void RollSpawn()
    {
        if(Random.value < spawnChance)
        {
            SpawnHoop();
        }
    }

    void SpawnHoop()
    {
        Vector3 spawnPos = transform.position;
        if(connectedBouncer != null)
        {
            spawnPos.y = connectedBouncer.bounceHeight;
        }
        else
        {
            spawnPos.y = GameController.Instance.slimePrefab.hopHeight;
        }

        if(transform.localScale.x > 1.1f && Random.value < .4f ) // 40% chance of spawning with an offset;
        {
            spawnPos.x += Random.Range(-transform.localScale.x * .5f, transform.localScale.x * .5f);
        }

        Hoop newHoop = Instantiate(GameController.Instance.hoopPrefab, spawnPos, Quaternion.identity);
        GameController.Instance.HoopSpawned(newHoop);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position, transform.localScale);

        if(connectedBouncer)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, connectedBouncer.transform.position);
        }
    }
}
