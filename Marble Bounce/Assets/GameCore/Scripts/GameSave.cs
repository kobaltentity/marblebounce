﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSave
{
    public int currentLevel = 0;
    public int coins = 0;
    public bool tutorialSeen = false;
    public int currentLevelSetting = 0;
    public GameSave()
    {
    }

    public static void ResetSave(ref GameSave save)
    {
        save = new GameSave();
        Save(save);
    }

    public static GameSave LoadSave()
    {
        GameSave save = JSONUtils.LoadJson<GameSave>("save.json");
        if(save == null)
        {
            save = new GameSave();
        }
        Save(save);
        return save;
    }

    public static void Save(GameSave save)
    {
        JSONUtils.SaveJson(save, "save.json");
    }
}
