﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;


public class JSONUtils
{
    public static void SaveJson(object obj, string filePath)
    {
        JsonSerializer ser = JsonSerializer.Create();

        using (StreamWriter wr = new StreamWriter(Path.Combine(Application.persistentDataPath, filePath)))
        {
            ser.Serialize(wr, obj);
        }
    }

    public static T LoadJson<T>(string filePath)
    {
        Debug.Log("JSON> Loading " + filePath);

        string path = Path.Combine(Application.persistentDataPath, filePath);

        if (!File.Exists(path))
        {
            Debug.Log("JSON> File not found " + filePath);
            return default(T);
        }

        //string json = File.ReadAllText(path);
        //Debug.Log ("JSON> " + filePath + ":  " + json);

        using (StreamReader sr = new StreamReader(path))
        {
            JsonSerializer ser = JsonSerializer.Create(new JsonSerializerSettings());
            T result = (T)ser.Deserialize(sr, typeof(T));
            return (result);
        }
    }

    public static T FromJson<T>(string json)
    {
        using (StringReader sr = new StringReader(json))
        {
            JsonSerializer ser = JsonSerializer.Create(new JsonSerializerSettings());
            T result = (T)ser.Deserialize(sr, typeof(T));
            return (result);
        }
    }
}
