﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Feedback
{
#if UNITY_ANDROID && !UNITY_EDITOR
    public static AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    public static AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
    public static AndroidJavaObject vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#elif UNITY_ANDROID
    public static AndroidJavaClass unityPlayer ;
    public static AndroidJavaObject currentActivity ;
    public static AndroidJavaObject vibrator ;
#endif

    static Dictionary<iOSHapticFeedback.iOSFeedbackType, long[]> androidFeedbacks;

    public static void InitAndroidFeedback()
    {
        androidFeedbacks = new Dictionary<iOSHapticFeedback.iOSFeedbackType, long[]>();

        androidFeedbacks.Add(iOSHapticFeedback.iOSFeedbackType.Failure, new long[] {0, 60, 100, 60, 100, 80});
        androidFeedbacks.Add(iOSHapticFeedback.iOSFeedbackType.Success, new long[] {0, 50, 100, 90});
        androidFeedbacks.Add(iOSHapticFeedback.iOSFeedbackType.SelectionChange, new long[] {0, 40});
        androidFeedbacks.Add(iOSHapticFeedback.iOSFeedbackType.ImpactLight, new long[] {0, 60});
        androidFeedbacks.Add(iOSHapticFeedback.iOSFeedbackType.ImpactMedium, new long[] {0, 80});
        androidFeedbacks.Add(iOSHapticFeedback.iOSFeedbackType.ImpactHeavy, new long[] {0, 100});
    }


    public static void SendFeedback(iOSHapticFeedback.iOSFeedbackType feedbackType)
    {
#if UNITY_ANDROID
        try
        {
            if (androidFeedbacks.ContainsKey(feedbackType))
                Vibrate(androidFeedbacks[feedbackType], -1);
        }
        catch (Exception e)
        {
            Debug.Log("Couldn't vibrate");
        }
#elif UNITY_IOS
        iOSHapticFeedback.Instance.Trigger(feedbackType);
#endif
        }


    static void Vibrate(long[] pattern, int repeat)
    {
#if UNITY_ANDROID
        if (isAndroid())
            vibrator.Call("vibrate", pattern, repeat);
#endif
    }

    static bool HasVibrator()
    {
        return isAndroid();
    }

    public static void Cancel()
    {
#if UNITY_ANDROID

        try
        {
            if (isAndroid())
                vibrator.Call("cancel");
        }
        catch (Exception e)
        {
            Debug.Log("Couldn't cancel");
        }
#endif

    }

    private static bool isAndroid()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        return true ;
#else
        return false;
#endif
    }

}
