﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIScreen : MonoBehaviour
{
    [SerializeField] GameObject header;
    [SerializeField] GameObject footer;

    Vector3 headerOriginalPos;
    Vector3 footerOriginalPos;
    RectTransform headerTransform;
    RectTransform footerTransform;

    Tween headerTween;
    Tween footerTween;

    void Start()
    {
        headerTransform = header.GetComponent<RectTransform>();
        footerTransform = footer.GetComponent<RectTransform>();

        headerOriginalPos = headerTransform.anchoredPosition;
        footerOriginalPos = footerTransform.anchoredPosition;

        SetEnable(false, 0);
    }

    public void SetEnable(bool show, float duration = .8f)
    {
        KillTweens();

        if (!show)
        {
            
            headerTween = headerTransform.DOAnchorPos(headerOriginalPos + new Vector3(0, Screen.height / 2f, 0), duration);
            footerTween = footerTransform.DOAnchorPos(footerOriginalPos + new Vector3(0, -Screen.height / 2f, 0), duration);
        }
        else
        {
            headerTween = headerTransform.DOAnchorPos(headerOriginalPos, duration);
            footerTween = footerTransform.DOAnchorPos(footerOriginalPos, duration);
        }

    }

    void KillTweens()
    {
        headerTween?.Kill();
        footerTween?.Kill();
    }
}
