﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    void Start()
    {
        RollSpawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void RollSpawn()
    {
        if (Random.value < GameController.Instance.coinSpawnChance)
        {
            SpawnCoin();
        }
    }

    void SpawnCoin()
    {
        Coin newCoin = Instantiate(GameController.Instance.coinPrefab, transform.position, Quaternion.identity);
        GameController.Instance.CoinSpawned(newCoin);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, transform.localScale);
    }
}
