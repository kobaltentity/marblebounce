﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Bouncer
struct Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A;
// CameraController
struct CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB;
// Coin
struct Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59;
// DG.Tweening.Sequence
struct Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2;
// DG.Tweening.Tween
struct Tween_t119487E0AB84EF563521F1043116BDBAE68AC437;
// DG.Tweening.TweenCallback`1<System.Single>
struct TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2;
// EndPlatform
struct EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD;
// GameController
struct GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3;
// GameSave
struct GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8;
// Hoop
struct Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9;
// IngameDebugConsole.CircularBuffer`1<System.String>
struct CircularBuffer_1_t2F11E738091D4E0A93B448F8BD736E9C207CC0E6;
// IngameDebugConsole.DebugLogEntry
struct DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8;
// IngameDebugConsole.DebugLogIndexList
struct DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC;
// IngameDebugConsole.DebugLogItem
struct DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51;
// IngameDebugConsole.DebugLogManager
struct DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974;
// IngameDebugConsole.DebugLogPopup
struct DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656;
// IngameDebugConsole.DebugLogRecycledListView
struct DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928;
// InputManager
struct InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE;
// InputManager/TouchDelegate
struct TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322;
// LevelSetting
struct LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773;
// Platform
struct Platform_t32A27B4C009E21E1D2383495043A2E49A919F337;
// SlimeController
struct SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<IngameDebugConsole.DebugLogEntry,System.Int32>
struct Dictionary_2_t26860259EC2C47041B29D8B5CC0507DE43F9D5BF;
// System.Collections.Generic.Dictionary`2<System.Int32,IngameDebugConsole.DebugLogItem>
struct Dictionary_2_t777E1429B40024DB728AEB28AFEBFDB1B7A79081;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t9F401E8FAE13945DFBD264E317248AB9CC9C36CA;
// System.Collections.Generic.Dictionary`2<System.String,IngameDebugConsole.ConsoleMethodInfo>
struct Dictionary_2_tE3CF9D253B289480299CCBA6F6E05B5B4093009D;
// System.Collections.Generic.Dictionary`2<System.Type,IngameDebugConsole.DebugLogConsole/ParseFunction>
struct Dictionary_2_t381A1EAFB646ADF3823E3116F68C41840AC63FAA;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Sprite>
struct Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D;
// System.Collections.Generic.Dictionary`2<iOSHapticFeedback/iOSFeedbackType,System.Int64[]>
struct Dictionary_2_tEE87A52E57E461C67CC749CAEAD4764CA00FAB61;
// System.Collections.Generic.HashSet`1<UnityEngine.UI.IClippable>
struct HashSet_1_tAAE962DCA7E1BD56AD7B2C079CD4DBA3D0B231AD;
// System.Collections.Generic.HashSet`1<UnityEngine.UI.MaskableGraphic>
struct HashSet_1_tF035B0C2C7E1925B6966D73DB277DF70D4C48408;
// System.Collections.Generic.List`1<Coin>
struct List_1_tB7E3B3F1DAA95D6323FBDC0C4B3A8AB00EAF2D2F;
// System.Collections.Generic.List`1<Hoop>
struct List_1_t4616D0C08FB7C88EEC62F447A5F661C4CE8ACF26;
// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry>
struct List_1_t3A4F894E1990F66813E80863CBC0815538439C01;
// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogItem>
struct List_1_t42A70AE83EB6E2E98262558DE80CA745A3F48370;
// System.Collections.Generic.List`1<IngameDebugConsole.QueuedDebugLogEntry>
struct List_1_t0795B1BC835CECF56CA584A8380D9A2A34DE9A64;
// System.Collections.Generic.List`1<LevelSetting>
struct List_1_tB9C3B2978F60CC833FA7D172B1F33BC2EE26A672;
// System.Collections.Generic.List`1<Platform>
struct List_1_tBA87FE584CEE77F9910FC9239505F539594377ED;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t4FB5BF302DAD74D690156A022C4FA4D4081E9B26;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_tED9ECFD90851157EFC14F5BE7FAA8124613D0C97;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t882412D5BE0B5BFC1900366319F8B2EB544BDD8B;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t3D4AF004AB30F72D5A06525187E5F50A875DE9FA;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_tF0A983C549D4719BB6731A650E4E4F1BB16AADAA;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t1BB31D71CB8736DC35CCD9FFB9228FAFD427EB52;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t0087C02D52C7E5CFF8C0C55FC0453A28FD5F055B;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_tE2895D6ED3A7C02005A89712BECBA7812B6CCC91;
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
struct List_1_t2BA894CDA977BF8C8AC7BD397B1F6602A63B92C4;
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t7984FB74D3877329C67F707EE61308104862C691;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t5603D617415C67B322F898ABF659C3E2C8BD668C;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t32541D3F4C935BBA3800256BD21A7CA8148AAC13;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t4632D9FB0A696C184FCADAE4820EEF1BD4A57AD8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_tE56FA3D824428369D16F4315471FA8CF76553941;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UIController
struct UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE;
// UIScreen
struct UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t7521247C87411935E8A2CA38683533083459473F;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.TrailRenderer
struct TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t68ABA103C2150D63C7C1D7CE1621CDDA297C9588;
// UnityEngine.UI.Outline
struct Outline_tB750E496976B072E79142D51C0A991AC20183095;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.RectangularVertexClipper
struct RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// iOSHapticFeedback/iOSFeedbackTypeSettings
struct iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};


// System.Object


// DG.Tweening.DOTweenCYInstruction
struct  DOTweenCYInstruction_t99180052E9B15DB8365A25440477F7BE9B4D6847  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleAudio
struct  DOTweenModuleAudio_t769A1BF9E3697BFACC9815179E7A39085E56CE4D  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::target
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F, ___target_0)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_target_0() const { return ___target_0; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::target
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD, ___target_0)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_target_0() const { return ___target_0; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC  : public RuntimeObject
{
public:
	// UnityEngine.Audio.AudioMixer DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::target
	AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * ___target_0;
	// System.String DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::floatName
	String_t* ___floatName_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC, ___target_0)); }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * get_target_0() const { return ___target_0; }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_floatName_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC, ___floatName_1)); }
	inline String_t* get_floatName_1() const { return ___floatName_1; }
	inline String_t** get_address_of_floatName_1() { return &___floatName_1; }
	inline void set_floatName_1(String_t* value)
	{
		___floatName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___floatName_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics
struct  DOTweenModulePhysics_t54AF484E9A4CEC236EE03303DDE8634FC383938E  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::trans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F, ___trans_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_trans_0() const { return ___trans_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F, ___target_1)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_1() const { return ___target_1; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::trans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1, ___trans_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_trans_0() const { return ___trans_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1, ___target_1)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_1() const { return ___target_1; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D
struct  DOTweenModulePhysics2D_t31D337CBA76C3DFD0CF747008022FBBAB46343BF  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleSprite
struct  DOTweenModuleSprite_tA07D646AA9234481CF1BA55D046BC7F824A818F2  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::target
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998, ___target_0)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::target
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7, ___target_0)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI
struct  DOTweenModuleUI_t39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::target
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7, ___target_0)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_target_0() const { return ___target_0; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::target
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA, ___target_0)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get_target_0() const { return ___target_0; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::target
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647, ___target_0)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get_target_0() const { return ___target_0; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::target
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B, ___target_0)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get_target_0() const { return ___target_0; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A, ___target_0)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_0() const { return ___target_0; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0
struct  U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF, ___target_0)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_0() const { return ___target_0; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::target
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642, ___target_0)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_target_0() const { return ___target_0; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::target
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79, ___target_0)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_target_0() const { return ___target_0; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::target
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF, ___target_0)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_target_0() const { return ___target_0; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187  : public RuntimeObject
{
public:
	// UnityEngine.UI.Slider DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::target
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187, ___target_0)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_target_0() const { return ___target_0; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D, ___target_0)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_0() const { return ___target_0; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8, ___target_0)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_0() const { return ___target_0; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649, ___target_0)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_0() const { return ___target_0; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813, ___target_0)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_0() const { return ___target_0; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::target
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A, ___target_0)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_target_0() const { return ___target_0; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_Utils
struct  Utils_tF7D730835163762D9317B6FB65E30C704BD3BF7F  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUnityVersion
struct  DOTweenModuleUnityVersion_tD051D14A3CC840A947AF4E411FDA4A3E19E40CCE  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::target
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::propertyID
	int32_t ___propertyID_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943, ___target_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_target_0() const { return ___target_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_propertyID_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943, ___propertyID_1)); }
	inline int32_t get_propertyID_1() const { return ___propertyID_1; }
	inline int32_t* get_address_of_propertyID_1() { return &___propertyID_1; }
	inline void set_propertyID_1(int32_t value)
	{
		___propertyID_1 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::target
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::propertyID
	int32_t ___propertyID_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6, ___target_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_target_0() const { return ___target_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_propertyID_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6, ___propertyID_1)); }
	inline int32_t get_propertyID_1() const { return ___propertyID_1; }
	inline int32_t* get_address_of_propertyID_1() { return &___propertyID_1; }
	inline void set_propertyID_1(int32_t value)
	{
		___propertyID_1 = value;
	}
};


// DG.Tweening.DOTweenModuleUtils
struct  DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF  : public RuntimeObject
{
public:

public:
};

struct DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields
{
public:
	// System.Boolean DG.Tweening.DOTweenModuleUtils::_initialized
	bool ____initialized_0;

public:
	inline static int32_t get_offset_of__initialized_0() { return static_cast<int32_t>(offsetof(DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields, ____initialized_0)); }
	inline bool get__initialized_0() const { return ____initialized_0; }
	inline bool* get_address_of__initialized_0() { return &____initialized_0; }
	inline void set__initialized_0(bool value)
	{
		____initialized_0 = value;
	}
};


// DG.Tweening.DOTweenModuleUtils_Physics
struct  Physics_tFF3A360D9F8EF2286D1343AA0E89E67FC909D59D  : public RuntimeObject
{
public:

public:
};


// EndPlatform_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t664CF92BE2A9F7882339520E787A188AA72FFC14  : public RuntimeObject
{
public:
	// System.Int32 EndPlatform_<>c__DisplayClass2_0::unref
	int32_t ___unref_0;
	// EndPlatform EndPlatform_<>c__DisplayClass2_0::<>4__this
	EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_unref_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t664CF92BE2A9F7882339520E787A188AA72FFC14, ___unref_0)); }
	inline int32_t get_unref_0() const { return ___unref_0; }
	inline int32_t* get_address_of_unref_0() { return &___unref_0; }
	inline void set_unref_0(int32_t value)
	{
		___unref_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t664CF92BE2A9F7882339520E787A188AA72FFC14, ___U3CU3E4__this_1)); }
	inline EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// Feedback
struct  Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF  : public RuntimeObject
{
public:

public:
};

struct Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass Feedback::unityPlayer
	AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * ___unityPlayer_0;
	// UnityEngine.AndroidJavaObject Feedback::currentActivity
	AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * ___currentActivity_1;
	// UnityEngine.AndroidJavaObject Feedback::vibrator
	AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * ___vibrator_2;
	// System.Collections.Generic.Dictionary`2<iOSHapticFeedback_iOSFeedbackType,System.Int64[]> Feedback::androidFeedbacks
	Dictionary_2_tEE87A52E57E461C67CC749CAEAD4764CA00FAB61 * ___androidFeedbacks_3;

public:
	inline static int32_t get_offset_of_unityPlayer_0() { return static_cast<int32_t>(offsetof(Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields, ___unityPlayer_0)); }
	inline AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * get_unityPlayer_0() const { return ___unityPlayer_0; }
	inline AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE ** get_address_of_unityPlayer_0() { return &___unityPlayer_0; }
	inline void set_unityPlayer_0(AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * value)
	{
		___unityPlayer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unityPlayer_0), (void*)value);
	}

	inline static int32_t get_offset_of_currentActivity_1() { return static_cast<int32_t>(offsetof(Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields, ___currentActivity_1)); }
	inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * get_currentActivity_1() const { return ___currentActivity_1; }
	inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D ** get_address_of_currentActivity_1() { return &___currentActivity_1; }
	inline void set_currentActivity_1(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * value)
	{
		___currentActivity_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentActivity_1), (void*)value);
	}

	inline static int32_t get_offset_of_vibrator_2() { return static_cast<int32_t>(offsetof(Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields, ___vibrator_2)); }
	inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * get_vibrator_2() const { return ___vibrator_2; }
	inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D ** get_address_of_vibrator_2() { return &___vibrator_2; }
	inline void set_vibrator_2(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * value)
	{
		___vibrator_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vibrator_2), (void*)value);
	}

	inline static int32_t get_offset_of_androidFeedbacks_3() { return static_cast<int32_t>(offsetof(Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields, ___androidFeedbacks_3)); }
	inline Dictionary_2_tEE87A52E57E461C67CC749CAEAD4764CA00FAB61 * get_androidFeedbacks_3() const { return ___androidFeedbacks_3; }
	inline Dictionary_2_tEE87A52E57E461C67CC749CAEAD4764CA00FAB61 ** get_address_of_androidFeedbacks_3() { return &___androidFeedbacks_3; }
	inline void set_androidFeedbacks_3(Dictionary_2_tEE87A52E57E461C67CC749CAEAD4764CA00FAB61 * value)
	{
		___androidFeedbacks_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___androidFeedbacks_3), (void*)value);
	}
};


// GameController_<>c
struct  U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields
{
public:
	// GameController_<>c GameController_<>c::<>9
	U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928 * ___U3CU3E9_0;
	// DG.Tweening.TweenCallback`1<System.Single> GameController_<>c::<>9__51_0
	TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 * ___U3CU3E9__51_0_1;
	// DG.Tweening.TweenCallback`1<System.Single> GameController_<>c::<>9__52_0
	TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 * ___U3CU3E9__52_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__51_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields, ___U3CU3E9__51_0_1)); }
	inline TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 * get_U3CU3E9__51_0_1() const { return ___U3CU3E9__51_0_1; }
	inline TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 ** get_address_of_U3CU3E9__51_0_1() { return &___U3CU3E9__51_0_1; }
	inline void set_U3CU3E9__51_0_1(TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 * value)
	{
		___U3CU3E9__51_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__51_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__52_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields, ___U3CU3E9__52_0_2)); }
	inline TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 * get_U3CU3E9__52_0_2() const { return ___U3CU3E9__52_0_2; }
	inline TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 ** get_address_of_U3CU3E9__52_0_2() { return &___U3CU3E9__52_0_2; }
	inline void set_U3CU3E9__52_0_2(TweenCallback_1_t83D64A51B58CC04A091C96EC07C2614E8DCF6CB2 * value)
	{
		___U3CU3E9__52_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__52_0_2), (void*)value);
	}
};


// GameSave
struct  GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8  : public RuntimeObject
{
public:
	// System.Int32 GameSave::currentLevel
	int32_t ___currentLevel_0;
	// System.Int32 GameSave::coins
	int32_t ___coins_1;
	// System.Boolean GameSave::tutorialSeen
	bool ___tutorialSeen_2;
	// System.Int32 GameSave::currentLevelSetting
	int32_t ___currentLevelSetting_3;

public:
	inline static int32_t get_offset_of_currentLevel_0() { return static_cast<int32_t>(offsetof(GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8, ___currentLevel_0)); }
	inline int32_t get_currentLevel_0() const { return ___currentLevel_0; }
	inline int32_t* get_address_of_currentLevel_0() { return &___currentLevel_0; }
	inline void set_currentLevel_0(int32_t value)
	{
		___currentLevel_0 = value;
	}

	inline static int32_t get_offset_of_coins_1() { return static_cast<int32_t>(offsetof(GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8, ___coins_1)); }
	inline int32_t get_coins_1() const { return ___coins_1; }
	inline int32_t* get_address_of_coins_1() { return &___coins_1; }
	inline void set_coins_1(int32_t value)
	{
		___coins_1 = value;
	}

	inline static int32_t get_offset_of_tutorialSeen_2() { return static_cast<int32_t>(offsetof(GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8, ___tutorialSeen_2)); }
	inline bool get_tutorialSeen_2() const { return ___tutorialSeen_2; }
	inline bool* get_address_of_tutorialSeen_2() { return &___tutorialSeen_2; }
	inline void set_tutorialSeen_2(bool value)
	{
		___tutorialSeen_2 = value;
	}

	inline static int32_t get_offset_of_currentLevelSetting_3() { return static_cast<int32_t>(offsetof(GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8, ___currentLevelSetting_3)); }
	inline int32_t get_currentLevelSetting_3() const { return ___currentLevelSetting_3; }
	inline int32_t* get_address_of_currentLevelSetting_3() { return &___currentLevelSetting_3; }
	inline void set_currentLevelSetting_3(int32_t value)
	{
		___currentLevelSetting_3 = value;
	}
};


// Hoop_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t57ACCCEDEA84808D0BDDE4C7D54AA49E19430A59  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Hoop_<>c__DisplayClass9_0::newPopup
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___newPopup_0;

public:
	inline static int32_t get_offset_of_newPopup_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t57ACCCEDEA84808D0BDDE4C7D54AA49E19430A59, ___newPopup_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_newPopup_0() const { return ___newPopup_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_newPopup_0() { return &___newPopup_0; }
	inline void set_newPopup_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___newPopup_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newPopup_0), (void*)value);
	}
};


// IngameDebugConsole.ConsoleMethodInfo
struct  ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo IngameDebugConsole.ConsoleMethodInfo::method
	MethodInfo_t * ___method_0;
	// System.Type[] IngameDebugConsole.ConsoleMethodInfo::parameterTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___parameterTypes_1;
	// System.Object IngameDebugConsole.ConsoleMethodInfo::instance
	RuntimeObject * ___instance_2;
	// System.String IngameDebugConsole.ConsoleMethodInfo::signature
	String_t* ___signature_3;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_0), (void*)value);
	}

	inline static int32_t get_offset_of_parameterTypes_1() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65, ___parameterTypes_1)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_parameterTypes_1() const { return ___parameterTypes_1; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_parameterTypes_1() { return &___parameterTypes_1; }
	inline void set_parameterTypes_1(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___parameterTypes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parameterTypes_1), (void*)value);
	}

	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65, ___instance_2)); }
	inline RuntimeObject * get_instance_2() const { return ___instance_2; }
	inline RuntimeObject ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RuntimeObject * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_2), (void*)value);
	}

	inline static int32_t get_offset_of_signature_3() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65, ___signature_3)); }
	inline String_t* get_signature_3() const { return ___signature_3; }
	inline String_t** get_address_of_signature_3() { return &___signature_3; }
	inline void set_signature_3(String_t* value)
	{
		___signature_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___signature_3), (void*)value);
	}
};


// IngameDebugConsole.DebugLogConsole
struct  DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488  : public RuntimeObject
{
public:

public:
};

struct DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,IngameDebugConsole.ConsoleMethodInfo> IngameDebugConsole.DebugLogConsole::methods
	Dictionary_2_tE3CF9D253B289480299CCBA6F6E05B5B4093009D * ___methods_0;
	// System.Collections.Generic.Dictionary`2<System.Type,IngameDebugConsole.DebugLogConsole_ParseFunction> IngameDebugConsole.DebugLogConsole::parseFunctions
	Dictionary_2_t381A1EAFB646ADF3823E3116F68C41840AC63FAA * ___parseFunctions_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> IngameDebugConsole.DebugLogConsole::typeReadableNames
	Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * ___typeReadableNames_2;
	// System.Collections.Generic.List`1<System.String> IngameDebugConsole.DebugLogConsole::commandArguments
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___commandArguments_3;
	// System.String[] IngameDebugConsole.DebugLogConsole::inputDelimiters
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___inputDelimiters_4;

public:
	inline static int32_t get_offset_of_methods_0() { return static_cast<int32_t>(offsetof(DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields, ___methods_0)); }
	inline Dictionary_2_tE3CF9D253B289480299CCBA6F6E05B5B4093009D * get_methods_0() const { return ___methods_0; }
	inline Dictionary_2_tE3CF9D253B289480299CCBA6F6E05B5B4093009D ** get_address_of_methods_0() { return &___methods_0; }
	inline void set_methods_0(Dictionary_2_tE3CF9D253B289480299CCBA6F6E05B5B4093009D * value)
	{
		___methods_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___methods_0), (void*)value);
	}

	inline static int32_t get_offset_of_parseFunctions_1() { return static_cast<int32_t>(offsetof(DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields, ___parseFunctions_1)); }
	inline Dictionary_2_t381A1EAFB646ADF3823E3116F68C41840AC63FAA * get_parseFunctions_1() const { return ___parseFunctions_1; }
	inline Dictionary_2_t381A1EAFB646ADF3823E3116F68C41840AC63FAA ** get_address_of_parseFunctions_1() { return &___parseFunctions_1; }
	inline void set_parseFunctions_1(Dictionary_2_t381A1EAFB646ADF3823E3116F68C41840AC63FAA * value)
	{
		___parseFunctions_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parseFunctions_1), (void*)value);
	}

	inline static int32_t get_offset_of_typeReadableNames_2() { return static_cast<int32_t>(offsetof(DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields, ___typeReadableNames_2)); }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * get_typeReadableNames_2() const { return ___typeReadableNames_2; }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 ** get_address_of_typeReadableNames_2() { return &___typeReadableNames_2; }
	inline void set_typeReadableNames_2(Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * value)
	{
		___typeReadableNames_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeReadableNames_2), (void*)value);
	}

	inline static int32_t get_offset_of_commandArguments_3() { return static_cast<int32_t>(offsetof(DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields, ___commandArguments_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_commandArguments_3() const { return ___commandArguments_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_commandArguments_3() { return &___commandArguments_3; }
	inline void set_commandArguments_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___commandArguments_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___commandArguments_3), (void*)value);
	}

	inline static int32_t get_offset_of_inputDelimiters_4() { return static_cast<int32_t>(offsetof(DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields, ___inputDelimiters_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_inputDelimiters_4() const { return ___inputDelimiters_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_inputDelimiters_4() { return &___inputDelimiters_4; }
	inline void set_inputDelimiters_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___inputDelimiters_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputDelimiters_4), (void*)value);
	}
};


// IngameDebugConsole.DebugLogEntry
struct  DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8  : public RuntimeObject
{
public:
	// System.String IngameDebugConsole.DebugLogEntry::logString
	String_t* ___logString_1;
	// System.String IngameDebugConsole.DebugLogEntry::stackTrace
	String_t* ___stackTrace_2;
	// System.String IngameDebugConsole.DebugLogEntry::completeLog
	String_t* ___completeLog_3;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogEntry::logTypeSpriteRepresentation
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___logTypeSpriteRepresentation_4;
	// System.Int32 IngameDebugConsole.DebugLogEntry::count
	int32_t ___count_5;
	// System.Int32 IngameDebugConsole.DebugLogEntry::hashValue
	int32_t ___hashValue_6;

public:
	inline static int32_t get_offset_of_logString_1() { return static_cast<int32_t>(offsetof(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8, ___logString_1)); }
	inline String_t* get_logString_1() const { return ___logString_1; }
	inline String_t** get_address_of_logString_1() { return &___logString_1; }
	inline void set_logString_1(String_t* value)
	{
		___logString_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logString_1), (void*)value);
	}

	inline static int32_t get_offset_of_stackTrace_2() { return static_cast<int32_t>(offsetof(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8, ___stackTrace_2)); }
	inline String_t* get_stackTrace_2() const { return ___stackTrace_2; }
	inline String_t** get_address_of_stackTrace_2() { return &___stackTrace_2; }
	inline void set_stackTrace_2(String_t* value)
	{
		___stackTrace_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stackTrace_2), (void*)value);
	}

	inline static int32_t get_offset_of_completeLog_3() { return static_cast<int32_t>(offsetof(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8, ___completeLog_3)); }
	inline String_t* get_completeLog_3() const { return ___completeLog_3; }
	inline String_t** get_address_of_completeLog_3() { return &___completeLog_3; }
	inline void set_completeLog_3(String_t* value)
	{
		___completeLog_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___completeLog_3), (void*)value);
	}

	inline static int32_t get_offset_of_logTypeSpriteRepresentation_4() { return static_cast<int32_t>(offsetof(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8, ___logTypeSpriteRepresentation_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_logTypeSpriteRepresentation_4() const { return ___logTypeSpriteRepresentation_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_logTypeSpriteRepresentation_4() { return &___logTypeSpriteRepresentation_4; }
	inline void set_logTypeSpriteRepresentation_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___logTypeSpriteRepresentation_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logTypeSpriteRepresentation_4), (void*)value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_hashValue_6() { return static_cast<int32_t>(offsetof(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8, ___hashValue_6)); }
	inline int32_t get_hashValue_6() const { return ___hashValue_6; }
	inline int32_t* get_address_of_hashValue_6() { return &___hashValue_6; }
	inline void set_hashValue_6(int32_t value)
	{
		___hashValue_6 = value;
	}
};


// IngameDebugConsole.DebugLogIndexList
struct  DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC  : public RuntimeObject
{
public:
	// System.Int32[] IngameDebugConsole.DebugLogIndexList::indices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___indices_0;
	// System.Int32 IngameDebugConsole.DebugLogIndexList::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_indices_0() { return static_cast<int32_t>(offsetof(DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC, ___indices_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_indices_0() const { return ___indices_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_indices_0() { return &___indices_0; }
	inline void set_indices_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___indices_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indices_0), (void*)value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};


// JSONUtils
struct  JSONUtils_t5A3C370C1B54A62088180AB38FE12D8AAB21616D  : public RuntimeObject
{
public:

public:
};


// SlimeController_<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t057190362D7BBE749EBD4B3F6325A76A58995F51  : public RuntimeObject
{
public:
	// System.Boolean SlimeController_<>c__DisplayClass43_0::checkGameOver
	bool ___checkGameOver_0;
	// SlimeController SlimeController_<>c__DisplayClass43_0::<>4__this
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_checkGameOver_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t057190362D7BBE749EBD4B3F6325A76A58995F51, ___checkGameOver_0)); }
	inline bool get_checkGameOver_0() const { return ___checkGameOver_0; }
	inline bool* get_address_of_checkGameOver_0() { return &___checkGameOver_0; }
	inline void set_checkGameOver_0(bool value)
	{
		___checkGameOver_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t057190362D7BBE749EBD4B3F6325A76A58995F51, ___U3CU3E4__this_1)); }
	inline SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D  : public RuntimeObject
{
public:

public:
};


// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};


// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_t68ABA103C2150D63C7C1D7CE1621CDDA297C9588 * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_tE2895D6ED3A7C02005A89712BECBA7812B6CCC91 * ___s_InternalTransformList_18;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerEnterHandler_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerExitHandler_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerDownHandler_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerUpHandler_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerClickHandler_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InitializePotentialDragHandler_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_BeginDragHandler_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DragHandler_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EndDragHandler_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DropHandler_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ScrollHandler_10), (void*)value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UpdateSelectedHandler_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SelectHandler_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DeselectHandler_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_MoveHandler_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SubmitHandler_15), (void*)value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CancelHandler_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_t68ABA103C2150D63C7C1D7CE1621CDDA297C9588 * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_t68ABA103C2150D63C7C1D7CE1621CDDA297C9588 ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_t68ABA103C2150D63C7C1D7CE1621CDDA297C9588 * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_HandlerListPool_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_tE2895D6ED3A7C02005A89712BECBA7812B6CCC91 * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_tE2895D6ED3A7C02005A89712BECBA7812B6CCC91 ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_tE2895D6ED3A7C02005A89712BECBA7812B6CCC91 * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalTransformList_18), (void*)value);
	}
};


// UnityEngine.EventSystems.ExecuteEvents_<>c
struct  U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents_<>c UnityEngine.EventSystems.ExecuteEvents_<>c::<>9
	U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// UnityEngine.EventSystems.PhysicsRaycaster_RaycastHitComparer
struct  RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2  : public RuntimeObject
{
public:

public:
};

struct RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2_StaticFields
{
public:
	// UnityEngine.EventSystems.PhysicsRaycaster_RaycastHitComparer UnityEngine.EventSystems.PhysicsRaycaster_RaycastHitComparer::instance
	RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2_StaticFields, ___instance_0)); }
	inline RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2 * get_instance_0() const { return ___instance_0; }
	inline RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_0), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerInputModule_MouseState
struct  MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule_ButtonState> UnityEngine.EventSystems.PointerInputModule_MouseState::m_TrackedButtons
	List_1_tF0A983C549D4719BB6731A650E4E4F1BB16AADAA * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7, ___m_TrackedButtons_0)); }
	inline List_1_tF0A983C549D4719BB6731A650E4E4F1BB16AADAA * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_tF0A983C549D4719BB6731A650E4E4F1BB16AADAA ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_tF0A983C549D4719BB6731A650E4E4F1BB16AADAA * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedButtons_0), (void*)value);
	}
};


// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A  : public RuntimeObject
{
public:

public:
};

struct RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_tED9ECFD90851157EFC14F5BE7FAA8124613D0C97 * ___s_Raycasters_0;

public:
	inline static int32_t get_offset_of_s_Raycasters_0() { return static_cast<int32_t>(offsetof(RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields, ___s_Raycasters_0)); }
	inline List_1_tED9ECFD90851157EFC14F5BE7FAA8124613D0C97 * get_s_Raycasters_0() const { return ___s_Raycasters_0; }
	inline List_1_tED9ECFD90851157EFC14F5BE7FAA8124613D0C97 ** get_address_of_s_Raycasters_0() { return &___s_Raycasters_0; }
	inline void set_s_Raycasters_0(List_1_tED9ECFD90851157EFC14F5BE7FAA8124613D0C97 * value)
	{
		___s_Raycasters_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Raycasters_0), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};


// UnityEngine.UI.MaskUtilities
struct  MaskUtilities_t28395C0AF1B83B3A798D76DC69B012BB303D9683  : public RuntimeObject
{
public:

public:
};


// UnityEngine.UI.Misc
struct  Misc_t87057804A6479127307E42B6C83A4F3244521315  : public RuntimeObject
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycast3D_0), (void*)value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycast3DAll_1), (void*)value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycast2D_2), (void*)value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getRayIntersectionAll_3), (void*)value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getRayIntersectionAllNonAlloc_4), (void*)value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getRaycastNonAlloc_5), (void*)value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ReflectionMethodsCache_6), (void*)value);
	}
};


// UnityEngine.UI.Scrollbar_<ClickRepeat>d__57
struct  U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar_<ClickRepeat>d__57::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.UI.Scrollbar_<ClickRepeat>d__57::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.Scrollbar_<ClickRepeat>d__57::<>4__this
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ___U3CU3E4__this_2;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Scrollbar_<ClickRepeat>d__57::eventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074, ___U3CU3E4__this_2)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_eventData_3() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074, ___eventData_3)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_eventData_3() const { return ___eventData_3; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_eventData_3() { return &___eventData_3; }
	inline void set_eventData_3(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___eventData_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventData_3), (void*)value);
	}
};


// UnityEngine.UI.SetPropertyUtility
struct  SetPropertyUtility_t20B3FC057E91FD49F7F71279C2DFAAD263E32DEC  : public RuntimeObject
{
public:

public:
};


// UnityEngine.UI.StencilMaterial
struct  StencilMaterial_t1DC5D1CDE53AF67E74925AC2F4083FD29810D974  : public RuntimeObject
{
public:

public:
};

struct StencilMaterial_t1DC5D1CDE53AF67E74925AC2F4083FD29810D974_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial_MatEntry> UnityEngine.UI.StencilMaterial::m_List
	List_1_t7984FB74D3877329C67F707EE61308104862C691 * ___m_List_0;

public:
	inline static int32_t get_offset_of_m_List_0() { return static_cast<int32_t>(offsetof(StencilMaterial_t1DC5D1CDE53AF67E74925AC2F4083FD29810D974_StaticFields, ___m_List_0)); }
	inline List_1_t7984FB74D3877329C67F707EE61308104862C691 * get_m_List_0() const { return ___m_List_0; }
	inline List_1_t7984FB74D3877329C67F707EE61308104862C691 ** get_address_of_m_List_0() { return &___m_List_0; }
	inline void set_m_List_0(List_1_t7984FB74D3877329C67F707EE61308104862C691 * value)
	{
		___m_List_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_List_0), (void*)value);
	}
};


// UnityEngine.UI.ToggleGroup_<>c
struct  U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields
{
public:
	// UnityEngine.UI.ToggleGroup_<>c UnityEngine.UI.ToggleGroup_<>c::<>9
	U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E * ___U3CU3E9_0;
	// System.Predicate`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup_<>c::<>9__12_0
	Predicate_1_tE56FA3D824428369D16F4315471FA8CF76553941 * ___U3CU3E9__12_0_1;
	// System.Func`2<UnityEngine.UI.Toggle,System.Boolean> UnityEngine.UI.ToggleGroup_<>c::<>9__13_0
	Func_2_t4632D9FB0A696C184FCADAE4820EEF1BD4A57AD8 * ___U3CU3E9__13_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Predicate_1_tE56FA3D824428369D16F4315471FA8CF76553941 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Predicate_1_tE56FA3D824428369D16F4315471FA8CF76553941 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Predicate_1_tE56FA3D824428369D16F4315471FA8CF76553941 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields, ___U3CU3E9__13_0_2)); }
	inline Func_2_t4632D9FB0A696C184FCADAE4820EEF1BD4A57AD8 * get_U3CU3E9__13_0_2() const { return ___U3CU3E9__13_0_2; }
	inline Func_2_t4632D9FB0A696C184FCADAE4820EEF1BD4A57AD8 ** get_address_of_U3CU3E9__13_0_2() { return &___U3CU3E9__13_0_2; }
	inline void set_U3CU3E9__13_0_2(Func_2_t4632D9FB0A696C184FCADAE4820EEF1BD4A57AD8 * value)
	{
		___U3CU3E9__13_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__13_0_2), (void*)value);
	}
};


// iOSHapticFeedback_iOSFeedbackTypeSettings
struct  iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1  : public RuntimeObject
{
public:
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::SelectionChange
	bool ___SelectionChange_0;
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::ImpactLight
	bool ___ImpactLight_1;
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::ImpactMedium
	bool ___ImpactMedium_2;
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::ImpactHeavy
	bool ___ImpactHeavy_3;
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::NotificationSuccess
	bool ___NotificationSuccess_4;
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::NotificationWarning
	bool ___NotificationWarning_5;
	// System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::NotificationFailure
	bool ___NotificationFailure_6;

public:
	inline static int32_t get_offset_of_SelectionChange_0() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___SelectionChange_0)); }
	inline bool get_SelectionChange_0() const { return ___SelectionChange_0; }
	inline bool* get_address_of_SelectionChange_0() { return &___SelectionChange_0; }
	inline void set_SelectionChange_0(bool value)
	{
		___SelectionChange_0 = value;
	}

	inline static int32_t get_offset_of_ImpactLight_1() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___ImpactLight_1)); }
	inline bool get_ImpactLight_1() const { return ___ImpactLight_1; }
	inline bool* get_address_of_ImpactLight_1() { return &___ImpactLight_1; }
	inline void set_ImpactLight_1(bool value)
	{
		___ImpactLight_1 = value;
	}

	inline static int32_t get_offset_of_ImpactMedium_2() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___ImpactMedium_2)); }
	inline bool get_ImpactMedium_2() const { return ___ImpactMedium_2; }
	inline bool* get_address_of_ImpactMedium_2() { return &___ImpactMedium_2; }
	inline void set_ImpactMedium_2(bool value)
	{
		___ImpactMedium_2 = value;
	}

	inline static int32_t get_offset_of_ImpactHeavy_3() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___ImpactHeavy_3)); }
	inline bool get_ImpactHeavy_3() const { return ___ImpactHeavy_3; }
	inline bool* get_address_of_ImpactHeavy_3() { return &___ImpactHeavy_3; }
	inline void set_ImpactHeavy_3(bool value)
	{
		___ImpactHeavy_3 = value;
	}

	inline static int32_t get_offset_of_NotificationSuccess_4() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___NotificationSuccess_4)); }
	inline bool get_NotificationSuccess_4() const { return ___NotificationSuccess_4; }
	inline bool* get_address_of_NotificationSuccess_4() { return &___NotificationSuccess_4; }
	inline void set_NotificationSuccess_4(bool value)
	{
		___NotificationSuccess_4 = value;
	}

	inline static int32_t get_offset_of_NotificationWarning_5() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___NotificationWarning_5)); }
	inline bool get_NotificationWarning_5() const { return ___NotificationWarning_5; }
	inline bool* get_address_of_NotificationWarning_5() { return &___NotificationWarning_5; }
	inline void set_NotificationWarning_5(bool value)
	{
		___NotificationWarning_5 = value;
	}

	inline static int32_t get_offset_of_NotificationFailure_6() { return static_cast<int32_t>(offsetof(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1, ___NotificationFailure_6)); }
	inline bool get_NotificationFailure_6() const { return ___NotificationFailure_6; }
	inline bool* get_address_of_NotificationFailure_6() { return &___NotificationFailure_6; }
	inline void set_NotificationFailure_6(bool value)
	{
		___NotificationFailure_6 = value;
	}
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8__padding[12];
	};

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32
struct  __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315__padding[32];
	};

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48
struct  __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6__padding[48];
	};

public:
};


// DG.Tweening.DOTweenCYInstruction_WaitForCompletion
struct  WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForCompletion::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops
struct  WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;
	// System.Int32 DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::elapsedLoops
	int32_t ___elapsedLoops_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}

	inline static int32_t get_offset_of_elapsedLoops_1() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83, ___elapsedLoops_1)); }
	inline int32_t get_elapsedLoops_1() const { return ___elapsedLoops_1; }
	inline int32_t* get_address_of_elapsedLoops_1() { return &___elapsedLoops_1; }
	inline void set_elapsedLoops_1(int32_t value)
	{
		___elapsedLoops_1 = value;
	}
};


// DG.Tweening.DOTweenCYInstruction_WaitForKill
struct  WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForKill::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// DG.Tweening.DOTweenCYInstruction_WaitForPosition
struct  WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForPosition::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;
	// System.Single DG.Tweening.DOTweenCYInstruction_WaitForPosition::position
	float ___position_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3, ___position_1)); }
	inline float get_position_1() const { return ___position_1; }
	inline float* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(float value)
	{
		___position_1 = value;
	}
};


// DG.Tweening.DOTweenCYInstruction_WaitForRewind
struct  WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForRewind::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// DG.Tweening.DOTweenCYInstruction_WaitForStart
struct  WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C  : public CustomYieldInstruction_t819BB0973AFF22766749FF087B8AEFEAF3C2CB7D
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction_WaitForStart::t
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C, ___t_0)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_t_0() const { return ___t_0; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// IngameDebugConsole.ConsoleMethodAttribute
struct  ConsoleMethodAttribute_t985BF70FAEB63DF31809C507803CD727E03ADE59  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String IngameDebugConsole.ConsoleMethodAttribute::m_command
	String_t* ___m_command_0;
	// System.String IngameDebugConsole.ConsoleMethodAttribute::m_description
	String_t* ___m_description_1;

public:
	inline static int32_t get_offset_of_m_command_0() { return static_cast<int32_t>(offsetof(ConsoleMethodAttribute_t985BF70FAEB63DF31809C507803CD727E03ADE59, ___m_command_0)); }
	inline String_t* get_m_command_0() const { return ___m_command_0; }
	inline String_t** get_address_of_m_command_0() { return &___m_command_0; }
	inline void set_m_command_0(String_t* value)
	{
		___m_command_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_command_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_description_1() { return static_cast<int32_t>(offsetof(ConsoleMethodAttribute_t985BF70FAEB63DF31809C507803CD727E03ADE59, ___m_description_1)); }
	inline String_t* get_m_description_1() const { return ___m_description_1; }
	inline String_t** get_address_of_m_description_1() { return &___m_description_1; }
	inline void set_m_description_1(String_t* value)
	{
		___m_description_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_1), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};


// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5  : public AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5, ___m_EventSystem_1)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_tE040CE348097B63925504E9E6AFCD89D46798FE3  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tE040CE348097B63925504E9E6AFCD89D46798FE3, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_tFB475F569CC8852B004B3F2DE7536E67324C2AF8  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tFB475F569CC8852B004B3F2DE7536E67324C2AF8, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t55DE148B605149DF84E469388B37672EE507573E  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t55DE148B605149DF84E469388B37672EE507573E, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t60552D56FE9D9EC7B17849EE5D16EF96110E69A0  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t60552D56FE9D9EC7B17849EE5D16EF96110E69A0, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween_FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Target_0)); }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_pinvoke
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_com
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_SelectedSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_3)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48 <PrivateImplementationDetails>::31DCC8673FADE08890F573E7E9B161FFFC255F84
	__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  ___31DCC8673FADE08890F573E7E9B161FFFC255F84_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::4EF93FCF8C8343821BE623E829DCE20851E64BD5
	__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  ___4EF93FCF8C8343821BE623E829DCE20851E64BD5_1;

public:
	inline static int32_t get_offset_of_U331DCC8673FADE08890F573E7E9B161FFFC255F84_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___31DCC8673FADE08890F573E7E9B161FFFC255F84_0)); }
	inline __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  get_U331DCC8673FADE08890F573E7E9B161FFFC255F84_0() const { return ___31DCC8673FADE08890F573E7E9B161FFFC255F84_0; }
	inline __StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 * get_address_of_U331DCC8673FADE08890F573E7E9B161FFFC255F84_0() { return &___31DCC8673FADE08890F573E7E9B161FFFC255F84_0; }
	inline void set_U331DCC8673FADE08890F573E7E9B161FFFC255F84_0(__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6  value)
	{
		___31DCC8673FADE08890F573E7E9B161FFFC255F84_0 = value;
	}

	inline static int32_t get_offset_of_U34EF93FCF8C8343821BE623E829DCE20851E64BD5_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___4EF93FCF8C8343821BE623E829DCE20851E64BD5_1)); }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  get_U34EF93FCF8C8343821BE623E829DCE20851E64BD5_1() const { return ___4EF93FCF8C8343821BE623E829DCE20851E64BD5_1; }
	inline __StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 * get_address_of_U34EF93FCF8C8343821BE623E829DCE20851E64BD5_1() { return &___4EF93FCF8C8343821BE623E829DCE20851E64BD5_1; }
	inline void set_U34EF93FCF8C8343821BE623E829DCE20851E64BD5_1(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315  value)
	{
		___4EF93FCF8C8343821BE623E829DCE20851E64BD5_1 = value;
	}
};


// DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::target
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::s
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___s_4;
	// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::endValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::yTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___target_0)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_target_0() const { return ___target_0; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___s_4)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_s_4() const { return ___s_4; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_4), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___endValue_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endValue_5() const { return ___endValue_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endValue_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5, ___yTween_6)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___yTween_6), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::target
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::s
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::endValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::yTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___target_0)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___s_4)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_s_4() const { return ___s_4; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_4), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___endValue_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_endValue_5() const { return ___endValue_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___endValue_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7, ___yTween_6)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___yTween_6), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::target
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B, ___target_1)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_target_1() const { return ___target_1; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::target
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___target_0;
	// System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::s
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::endValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___endValue_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___target_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_target_0() const { return ___target_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___s_4)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_s_4() const { return ___s_4; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_4), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417, ___endValue_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_endValue_5() const { return ___endValue_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___endValue_5 = value;
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::target
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7, ___target_1)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_target_1() const { return ___target_1; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::target
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6, ___target_1)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_target_1() const { return ___target_1; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::to
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___to_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::target
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B, ___to_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_to_0() const { return ___to_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B, ___target_1)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_target_1() const { return ___target_1; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// IngameDebugConsole.DebugLogFilter
struct  DebugLogFilter_tF5E3C266EB271F87811192CBD62A660BAD0A66C9 
{
public:
	// System.Int32 IngameDebugConsole.DebugLogFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLogFilter_tF5E3C266EB271F87811192CBD62A660BAD0A66C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24
struct  U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548  : public RuntimeObject
{
public:
	// System.Int32 IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// IngameDebugConsole.DebugLogPopup IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<>4__this
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::targetPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPos_3;
	// System.Single IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<modifier>5__2
	float ___U3CmodifierU3E5__2_4;
	// UnityEngine.Vector3 IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::<initialPos>5__3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CinitialPosU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548, ___U3CU3E4__this_2)); }
	inline DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_targetPos_3() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548, ___targetPos_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPos_3() const { return ___targetPos_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPos_3() { return &___targetPos_3; }
	inline void set_targetPos_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CmodifierU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548, ___U3CmodifierU3E5__2_4)); }
	inline float get_U3CmodifierU3E5__2_4() const { return ___U3CmodifierU3E5__2_4; }
	inline float* get_address_of_U3CmodifierU3E5__2_4() { return &___U3CmodifierU3E5__2_4; }
	inline void set_U3CmodifierU3E5__2_4(float value)
	{
		___U3CmodifierU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CinitialPosU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548, ___U3CinitialPosU3E5__3_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CinitialPosU3E5__3_5() const { return ___U3CinitialPosU3E5__3_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CinitialPosU3E5__3_5() { return &___U3CinitialPosU3E5__3_5; }
	inline void set_U3CinitialPosU3E5__3_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CinitialPosU3E5__3_5 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.EventSystems.EventHandle
struct  EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.EventTrigger_TriggerEvent
struct  TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793  : public UnityEvent_1_t55DE148B605149DF84E469388B37672EE507573E
{
public:

public:
};


// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData_FramePressState
struct  FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_FramePressState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData_InputButton
struct  InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GameObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___module_1), (void*)value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};

// UnityEngine.EventSystems.StandaloneInputModule_InputMode
struct  InputMode_t6C81C4F84B743FC877C53380040470BE273BA79D 
{
public:
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule_InputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputMode_t6C81C4F84B743FC877C53380040470BE273BA79D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.LogType
struct  LogType_t6B6C6234E8B44B73937581ACFBE15DE28227849D 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogType_t6B6C6234E8B44B73937581ACFBE15DE28227849D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.Rendering.ColorWriteMask
struct  ColorWriteMask_t5DC00042EAC46AEEB06A7E0D51EA00C26F076E70 
{
public:
	// System.Int32 UnityEngine.Rendering.ColorWriteMask::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorWriteMask_t5DC00042EAC46AEEB06A7E0D51EA00C26F076E70, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_t217BE827C5994EDCA3FE70CE73578C2F729F9E69 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompareFunction_t217BE827C5994EDCA3FE70CE73578C2F729F9E69, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.StencilOp
struct  StencilOp_t39C53F937E65AEB59181772222564CEE34A3A48A 
{
public:
	// System.Int32 UnityEngine.Rendering.StencilOp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StencilOp_t39C53F937E65AEB59181772222564CEE34A3A48A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_SelectedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenCallback
struct  ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0  : public UnityEvent_1_tFB475F569CC8852B004B3F2DE7536E67324C2AF8
{
public:

public:
};


// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode
struct  ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.CoroutineTween.FloatTween_FloatTweenCallback
struct  FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502  : public UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6
{
public:

public:
};


// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent
struct  CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4  : public UnityEvent_1_tE040CE348097B63925504E9E6AFCD89D46798FE3
{
public:

public:
};


// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ScrollRect_MovementType
struct  MovementType_t78F2436465C40CA3C70631E1E5F088EA7A15C97A 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect_MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_t78F2436465C40CA3C70631E1E5F088EA7A15C97A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ScrollRect_ScrollRectEvent
struct  ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB  : public UnityEvent_1_t60552D56FE9D9EC7B17849EE5D16EF96110E69A0
{
public:

public:
};


// UnityEngine.UI.ScrollRect_ScrollbarVisibility
struct  ScrollbarVisibility_t4D6A5D8EF1681A91CED9F04283D0C882DCE1531F 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect_ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t4D6A5D8EF1681A91CED9F04283D0C882DCE1531F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar_Axis
struct  Axis_t5CC6D92E75113BD2F2816AFC44EF728126921DF7 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar_Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t5CC6D92E75113BD2F2816AFC44EF728126921DF7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar_Direction
struct  Direction_t7DC57FCC1DB6C12E88B2227EEEE2FCEF3F1483FF 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar_Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t7DC57FCC1DB6C12E88B2227EEEE2FCEF3F1483FF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar_ScrollEvent
struct  ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7  : public UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6
{
public:

public:
};


// UnityEngine.UI.Selectable_SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider_Axis
struct  Axis_t5D4CE8029AAE120D6F7C8AC3FE1B1F46B2623830 
{
public:
	// System.Int32 UnityEngine.UI.Slider_Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t5D4CE8029AAE120D6F7C8AC3FE1B1F46B2623830, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider_Direction
struct  Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E 
{
public:
	// System.Int32 UnityEngine.UI.Slider_Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider_SliderEvent
struct  SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C  : public UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6
{
public:

public:
};


// UnityEngine.UI.Toggle_ToggleEvent
struct  ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43  : public UnityEvent_1_tE040CE348097B63925504E9E6AFCD89D46798FE3
{
public:

public:
};


// UnityEngine.UI.Toggle_ToggleTransition
struct  ToggleTransition_t45980EB1352FF47B2D8D8EBC90385AB68939046D 
{
public:
	// System.Int32 UnityEngine.UI.Toggle_ToggleTransition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleTransition_t45980EB1352FF47B2D8D8EBC90385AB68939046D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv0S_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv1S_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv2S_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv3S_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tangents_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Indices_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};


// iOSHapticFeedback_iOSFeedbackType
struct  iOSFeedbackType_tCBEE4B633688224F2D9773B2AB0AE54C55D73A27 
{
public:
	// System.Int32 iOSHapticFeedback_iOSFeedbackType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(iOSFeedbackType_tCBEE4B633688224F2D9773B2AB0AE54C55D73A27, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// IngameDebugConsole.QueuedDebugLogEntry
struct  QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96 
{
public:
	// System.String IngameDebugConsole.QueuedDebugLogEntry::logString
	String_t* ___logString_0;
	// System.String IngameDebugConsole.QueuedDebugLogEntry::stackTrace
	String_t* ___stackTrace_1;
	// UnityEngine.LogType IngameDebugConsole.QueuedDebugLogEntry::logType
	int32_t ___logType_2;

public:
	inline static int32_t get_offset_of_logString_0() { return static_cast<int32_t>(offsetof(QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96, ___logString_0)); }
	inline String_t* get_logString_0() const { return ___logString_0; }
	inline String_t** get_address_of_logString_0() { return &___logString_0; }
	inline void set_logString_0(String_t* value)
	{
		___logString_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logString_0), (void*)value);
	}

	inline static int32_t get_offset_of_stackTrace_1() { return static_cast<int32_t>(offsetof(QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96, ___stackTrace_1)); }
	inline String_t* get_stackTrace_1() const { return ___stackTrace_1; }
	inline String_t** get_address_of_stackTrace_1() { return &___stackTrace_1; }
	inline void set_stackTrace_1(String_t* value)
	{
		___stackTrace_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stackTrace_1), (void*)value);
	}

	inline static int32_t get_offset_of_logType_2() { return static_cast<int32_t>(offsetof(QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96, ___logType_2)); }
	inline int32_t get_logType_2() const { return ___logType_2; }
	inline int32_t* get_address_of_logType_2() { return &___logType_2; }
	inline void set_logType_2(int32_t value)
	{
		___logType_2 = value;
	}
};

// Native definition for P/Invoke marshalling of IngameDebugConsole.QueuedDebugLogEntry
struct QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96_marshaled_pinvoke
{
	char* ___logString_0;
	char* ___stackTrace_1;
	int32_t ___logType_2;
};
// Native definition for COM marshalling of IngameDebugConsole.QueuedDebugLogEntry
struct QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96_marshaled_com
{
	Il2CppChar* ___logString_0;
	Il2CppChar* ___stackTrace_1;
	int32_t ___logType_2;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.EventSystems.EventTrigger_Entry
struct  Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger_Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger_TriggerEvent UnityEngine.EventSystems.EventTrigger_Entry::callback
	TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E, ___callback_1)); }
	inline TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_1), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t0087C02D52C7E5CFF8C0C55FC0453A28FD5F055B * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData_InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerEnterU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___m_PointerPress_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerPress_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClastPressU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrawPointerPressU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerDragU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_7))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_7))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_8))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_8))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___hovered_9)); }
	inline List_1_t0087C02D52C7E5CFF8C0C55FC0453A28FD5F055B * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t0087C02D52C7E5CFF8C0C55FC0453A28FD5F055B ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t0087C02D52C7E5CFF8C0C55FC0453A28FD5F055B * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hovered_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};


// UnityEngine.EventSystems.PointerInputModule_ButtonState
struct  ButtonState_tCF0544E1131CD058FABBEE56FA1D0A4716A17F9D  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData_InputButton UnityEngine.EventSystems.PointerInputModule_ButtonState::m_Button
	int32_t ___m_Button_0;
	// UnityEngine.EventSystems.PointerInputModule_MouseButtonEventData UnityEngine.EventSystems.PointerInputModule_ButtonState::m_EventData
	MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988 * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_tCF0544E1131CD058FABBEE56FA1D0A4716A17F9D, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_tCF0544E1131CD058FABBEE56FA1D0A4716A17F9D, ___m_EventData_1)); }
	inline MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventData_1), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerInputModule_MouseButtonEventData
struct  MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData_FramePressState UnityEngine.EventSystems.PointerInputModule_MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.PointerInputModule_MouseButtonEventData::buttonData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988, ___buttonData_1)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_buttonData_1() const { return ___buttonData_1; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonData_1), (void*)value);
	}
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Target_0)); }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_StartColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TargetColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_pinvoke
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_com
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};

// UnityEngine.UI.StencilMaterial_MatEntry
struct  MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityEngine.UI.StencilMaterial_MatEntry::baseMat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___baseMat_0;
	// UnityEngine.Material UnityEngine.UI.StencilMaterial_MatEntry::customMat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___customMat_1;
	// System.Int32 UnityEngine.UI.StencilMaterial_MatEntry::count
	int32_t ___count_2;
	// System.Int32 UnityEngine.UI.StencilMaterial_MatEntry::stencilId
	int32_t ___stencilId_3;
	// UnityEngine.Rendering.StencilOp UnityEngine.UI.StencilMaterial_MatEntry::operation
	int32_t ___operation_4;
	// UnityEngine.Rendering.CompareFunction UnityEngine.UI.StencilMaterial_MatEntry::compareFunction
	int32_t ___compareFunction_5;
	// System.Int32 UnityEngine.UI.StencilMaterial_MatEntry::readMask
	int32_t ___readMask_6;
	// System.Int32 UnityEngine.UI.StencilMaterial_MatEntry::writeMask
	int32_t ___writeMask_7;
	// System.Boolean UnityEngine.UI.StencilMaterial_MatEntry::useAlphaClip
	bool ___useAlphaClip_8;
	// UnityEngine.Rendering.ColorWriteMask UnityEngine.UI.StencilMaterial_MatEntry::colorMask
	int32_t ___colorMask_9;

public:
	inline static int32_t get_offset_of_baseMat_0() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___baseMat_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_baseMat_0() const { return ___baseMat_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_baseMat_0() { return &___baseMat_0; }
	inline void set_baseMat_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___baseMat_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseMat_0), (void*)value);
	}

	inline static int32_t get_offset_of_customMat_1() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___customMat_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_customMat_1() const { return ___customMat_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_customMat_1() { return &___customMat_1; }
	inline void set_customMat_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___customMat_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customMat_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilId_3() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___stencilId_3)); }
	inline int32_t get_stencilId_3() const { return ___stencilId_3; }
	inline int32_t* get_address_of_stencilId_3() { return &___stencilId_3; }
	inline void set_stencilId_3(int32_t value)
	{
		___stencilId_3 = value;
	}

	inline static int32_t get_offset_of_operation_4() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___operation_4)); }
	inline int32_t get_operation_4() const { return ___operation_4; }
	inline int32_t* get_address_of_operation_4() { return &___operation_4; }
	inline void set_operation_4(int32_t value)
	{
		___operation_4 = value;
	}

	inline static int32_t get_offset_of_compareFunction_5() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___compareFunction_5)); }
	inline int32_t get_compareFunction_5() const { return ___compareFunction_5; }
	inline int32_t* get_address_of_compareFunction_5() { return &___compareFunction_5; }
	inline void set_compareFunction_5(int32_t value)
	{
		___compareFunction_5 = value;
	}

	inline static int32_t get_offset_of_readMask_6() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___readMask_6)); }
	inline int32_t get_readMask_6() const { return ___readMask_6; }
	inline int32_t* get_address_of_readMask_6() { return &___readMask_6; }
	inline void set_readMask_6(int32_t value)
	{
		___readMask_6 = value;
	}

	inline static int32_t get_offset_of_writeMask_7() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___writeMask_7)); }
	inline int32_t get_writeMask_7() const { return ___writeMask_7; }
	inline int32_t* get_address_of_writeMask_7() { return &___writeMask_7; }
	inline void set_writeMask_7(int32_t value)
	{
		___writeMask_7 = value;
	}

	inline static int32_t get_offset_of_useAlphaClip_8() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___useAlphaClip_8)); }
	inline bool get_useAlphaClip_8() const { return ___useAlphaClip_8; }
	inline bool* get_address_of_useAlphaClip_8() { return &___useAlphaClip_8; }
	inline void set_useAlphaClip_8(bool value)
	{
		___useAlphaClip_8 = value;
	}

	inline static int32_t get_offset_of_colorMask_9() { return static_cast<int32_t>(offsetof(MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701, ___colorMask_9)); }
	inline int32_t get_colorMask_9() const { return ___colorMask_9; }
	inline int32_t* get_address_of_colorMask_9() { return &___colorMask_9; }
	inline void set_colorMask_9(int32_t value)
	{
		___colorMask_9 = value;
	}
};


// IngameDebugConsole.DebugLogConsole_ParseFunction
struct  ParseFunction_tF3BA66F5AA42C4E5C2C99F8F4F6457E5BDFDFC00  : public MulticastDelegate_t
{
public:

public:
};


// InputManager_TouchDelegate
struct  TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322  : public MulticastDelegate_t
{
public:

public:
};


// LevelSetting
struct  LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Color LevelSetting::hoopsColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___hoopsColor_4;
	// UnityEngine.Color LevelSetting::groundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___groundColor_5;
	// UnityEngine.Texture LevelSetting::groundTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___groundTexture_6;

public:
	inline static int32_t get_offset_of_hoopsColor_4() { return static_cast<int32_t>(offsetof(LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773, ___hoopsColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_hoopsColor_4() const { return ___hoopsColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_hoopsColor_4() { return &___hoopsColor_4; }
	inline void set_hoopsColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___hoopsColor_4 = value;
	}

	inline static int32_t get_offset_of_groundColor_5() { return static_cast<int32_t>(offsetof(LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773, ___groundColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_groundColor_5() const { return ___groundColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_groundColor_5() { return &___groundColor_5; }
	inline void set_groundColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___groundColor_5 = value;
	}

	inline static int32_t get_offset_of_groundTexture_6() { return static_cast<int32_t>(offsetof(LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773, ___groundTexture_6)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_groundTexture_6() const { return ___groundTexture_6; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_groundTexture_6() { return &___groundTexture_6; }
	inline void set_groundTexture_6(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___groundTexture_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groundTexture_6), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Bouncer
struct  Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// iOSHapticFeedback_iOSFeedbackType Bouncer::feedbackType
	int32_t ___feedbackType_4;
	// UnityEngine.Transform Bouncer::scaleParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___scaleParent_5;
	// System.Single Bouncer::bounceHeight
	float ___bounceHeight_6;
	// System.Single Bouncer::bounceDistance
	float ___bounceDistance_7;
	// DG.Tweening.Sequence Bouncer::squeezeSequence
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___squeezeSequence_8;
	// UnityEngine.Vector3 Bouncer::originalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___originalScale_9;

public:
	inline static int32_t get_offset_of_feedbackType_4() { return static_cast<int32_t>(offsetof(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A, ___feedbackType_4)); }
	inline int32_t get_feedbackType_4() const { return ___feedbackType_4; }
	inline int32_t* get_address_of_feedbackType_4() { return &___feedbackType_4; }
	inline void set_feedbackType_4(int32_t value)
	{
		___feedbackType_4 = value;
	}

	inline static int32_t get_offset_of_scaleParent_5() { return static_cast<int32_t>(offsetof(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A, ___scaleParent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_scaleParent_5() const { return ___scaleParent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_scaleParent_5() { return &___scaleParent_5; }
	inline void set_scaleParent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___scaleParent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scaleParent_5), (void*)value);
	}

	inline static int32_t get_offset_of_bounceHeight_6() { return static_cast<int32_t>(offsetof(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A, ___bounceHeight_6)); }
	inline float get_bounceHeight_6() const { return ___bounceHeight_6; }
	inline float* get_address_of_bounceHeight_6() { return &___bounceHeight_6; }
	inline void set_bounceHeight_6(float value)
	{
		___bounceHeight_6 = value;
	}

	inline static int32_t get_offset_of_bounceDistance_7() { return static_cast<int32_t>(offsetof(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A, ___bounceDistance_7)); }
	inline float get_bounceDistance_7() const { return ___bounceDistance_7; }
	inline float* get_address_of_bounceDistance_7() { return &___bounceDistance_7; }
	inline void set_bounceDistance_7(float value)
	{
		___bounceDistance_7 = value;
	}

	inline static int32_t get_offset_of_squeezeSequence_8() { return static_cast<int32_t>(offsetof(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A, ___squeezeSequence_8)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_squeezeSequence_8() const { return ___squeezeSequence_8; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_squeezeSequence_8() { return &___squeezeSequence_8; }
	inline void set_squeezeSequence_8(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___squeezeSequence_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___squeezeSequence_8), (void*)value);
	}

	inline static int32_t get_offset_of_originalScale_9() { return static_cast<int32_t>(offsetof(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A, ___originalScale_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_originalScale_9() const { return ___originalScale_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_originalScale_9() { return &___originalScale_9; }
	inline void set_originalScale_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___originalScale_9 = value;
	}
};


// Coin
struct  Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Coin::CanBeCollected
	bool ___CanBeCollected_4;
	// UnityEngine.MeshRenderer Coin::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_5;
	// UnityEngine.ParticleSystem Coin::particles
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particles_6;

public:
	inline static int32_t get_offset_of_CanBeCollected_4() { return static_cast<int32_t>(offsetof(Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59, ___CanBeCollected_4)); }
	inline bool get_CanBeCollected_4() const { return ___CanBeCollected_4; }
	inline bool* get_address_of_CanBeCollected_4() { return &___CanBeCollected_4; }
	inline void set_CanBeCollected_4(bool value)
	{
		___CanBeCollected_4 = value;
	}

	inline static int32_t get_offset_of_meshRenderer_5() { return static_cast<int32_t>(offsetof(Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59, ___meshRenderer_5)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_5() const { return ___meshRenderer_5; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_5() { return &___meshRenderer_5; }
	inline void set_meshRenderer_5(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___meshRenderer_5), (void*)value);
	}

	inline static int32_t get_offset_of_particles_6() { return static_cast<int32_t>(offsetof(Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59, ___particles_6)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particles_6() const { return ___particles_6; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particles_6() { return &___particles_6; }
	inline void set_particles_6(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particles_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particles_6), (void*)value);
	}
};


// CoinSpawn
struct  CoinSpawn_t2DF1091628ECF009A19AD074E884F22370447DED  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// EndPlatform
struct  EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> EndPlatform::particleSystems
	List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * ___particleSystems_4;
	// System.Collections.Generic.List`1<System.Single> EndPlatform::fireDelay
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___fireDelay_5;

public:
	inline static int32_t get_offset_of_particleSystems_4() { return static_cast<int32_t>(offsetof(EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD, ___particleSystems_4)); }
	inline List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * get_particleSystems_4() const { return ___particleSystems_4; }
	inline List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE ** get_address_of_particleSystems_4() { return &___particleSystems_4; }
	inline void set_particleSystems_4(List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * value)
	{
		___particleSystems_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particleSystems_4), (void*)value);
	}

	inline static int32_t get_offset_of_fireDelay_5() { return static_cast<int32_t>(offsetof(EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD, ___fireDelay_5)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_fireDelay_5() const { return ___fireDelay_5; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_fireDelay_5() { return &___fireDelay_5; }
	inline void set_fireDelay_5(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___fireDelay_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fireDelay_5), (void*)value);
	}
};


// Hoop
struct  Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Hoop::popupPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___popupPrefab_4;
	// UnityEngine.ParticleSystem Hoop::particle
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particle_5;
	// UnityEngine.ParticleSystem Hoop::particle2
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particle2_6;
	// System.Boolean Hoop::activated
	bool ___activated_7;
	// DG.Tweening.Tween Hoop::rotateTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___rotateTween_9;

public:
	inline static int32_t get_offset_of_popupPrefab_4() { return static_cast<int32_t>(offsetof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9, ___popupPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_popupPrefab_4() const { return ___popupPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_popupPrefab_4() { return &___popupPrefab_4; }
	inline void set_popupPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___popupPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___popupPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_particle_5() { return static_cast<int32_t>(offsetof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9, ___particle_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particle_5() const { return ___particle_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particle_5() { return &___particle_5; }
	inline void set_particle_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particle_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particle_5), (void*)value);
	}

	inline static int32_t get_offset_of_particle2_6() { return static_cast<int32_t>(offsetof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9, ___particle2_6)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particle2_6() const { return ___particle2_6; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particle2_6() { return &___particle2_6; }
	inline void set_particle2_6(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particle2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particle2_6), (void*)value);
	}

	inline static int32_t get_offset_of_activated_7() { return static_cast<int32_t>(offsetof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9, ___activated_7)); }
	inline bool get_activated_7() const { return ___activated_7; }
	inline bool* get_address_of_activated_7() { return &___activated_7; }
	inline void set_activated_7(bool value)
	{
		___activated_7 = value;
	}

	inline static int32_t get_offset_of_rotateTween_9() { return static_cast<int32_t>(offsetof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9, ___rotateTween_9)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_rotateTween_9() const { return ___rotateTween_9; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_rotateTween_9() { return &___rotateTween_9; }
	inline void set_rotateTween_9(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___rotateTween_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotateTween_9), (void*)value);
	}
};

struct Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9_StaticFields
{
public:
	// System.Int32 Hoop::rewardCombo
	int32_t ___rewardCombo_8;

public:
	inline static int32_t get_offset_of_rewardCombo_8() { return static_cast<int32_t>(offsetof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9_StaticFields, ___rewardCombo_8)); }
	inline int32_t get_rewardCombo_8() const { return ___rewardCombo_8; }
	inline int32_t* get_address_of_rewardCombo_8() { return &___rewardCombo_8; }
	inline void set_rewardCombo_8(int32_t value)
	{
		___rewardCombo_8 = value;
	}
};


// HoopsSpawner
struct  HoopsSpawner_tDB8A7B4D583F9D023EFD65E42D9DAAF5AADE3B18  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Bouncer HoopsSpawner::connectedBouncer
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A * ___connectedBouncer_4;
	// System.Single HoopsSpawner::spawnChance
	float ___spawnChance_5;

public:
	inline static int32_t get_offset_of_connectedBouncer_4() { return static_cast<int32_t>(offsetof(HoopsSpawner_tDB8A7B4D583F9D023EFD65E42D9DAAF5AADE3B18, ___connectedBouncer_4)); }
	inline Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A * get_connectedBouncer_4() const { return ___connectedBouncer_4; }
	inline Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A ** get_address_of_connectedBouncer_4() { return &___connectedBouncer_4; }
	inline void set_connectedBouncer_4(Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A * value)
	{
		___connectedBouncer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___connectedBouncer_4), (void*)value);
	}

	inline static int32_t get_offset_of_spawnChance_5() { return static_cast<int32_t>(offsetof(HoopsSpawner_tDB8A7B4D583F9D023EFD65E42D9DAAF5AADE3B18, ___spawnChance_5)); }
	inline float get_spawnChance_5() const { return ___spawnChance_5; }
	inline float* get_address_of_spawnChance_5() { return &___spawnChance_5; }
	inline void set_spawnChance_5(float value)
	{
		___spawnChance_5 = value;
	}
};


// IngameDebugConsole.DebugLogItem
struct  DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogItem::transformComponent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___transformComponent_4;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::imageComponent
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___imageComponent_5;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogItem::logText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___logText_6;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::logTypeImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___logTypeImage_7;
	// UnityEngine.GameObject IngameDebugConsole.DebugLogItem::logCountParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___logCountParent_8;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogItem::logCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___logCountText_9;
	// IngameDebugConsole.DebugLogEntry IngameDebugConsole.DebugLogItem::logEntry
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8 * ___logEntry_10;
	// System.Int32 IngameDebugConsole.DebugLogItem::entryIndex
	int32_t ___entryIndex_11;
	// IngameDebugConsole.DebugLogRecycledListView IngameDebugConsole.DebugLogItem::manager
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 * ___manager_12;

public:
	inline static int32_t get_offset_of_transformComponent_4() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___transformComponent_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_transformComponent_4() const { return ___transformComponent_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_transformComponent_4() { return &___transformComponent_4; }
	inline void set_transformComponent_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___transformComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___transformComponent_4), (void*)value);
	}

	inline static int32_t get_offset_of_imageComponent_5() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___imageComponent_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_imageComponent_5() const { return ___imageComponent_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_imageComponent_5() { return &___imageComponent_5; }
	inline void set_imageComponent_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___imageComponent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imageComponent_5), (void*)value);
	}

	inline static int32_t get_offset_of_logText_6() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___logText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_logText_6() const { return ___logText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_logText_6() { return &___logText_6; }
	inline void set_logText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___logText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logText_6), (void*)value);
	}

	inline static int32_t get_offset_of_logTypeImage_7() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___logTypeImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_logTypeImage_7() const { return ___logTypeImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_logTypeImage_7() { return &___logTypeImage_7; }
	inline void set_logTypeImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___logTypeImage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logTypeImage_7), (void*)value);
	}

	inline static int32_t get_offset_of_logCountParent_8() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___logCountParent_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_logCountParent_8() const { return ___logCountParent_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_logCountParent_8() { return &___logCountParent_8; }
	inline void set_logCountParent_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___logCountParent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logCountParent_8), (void*)value);
	}

	inline static int32_t get_offset_of_logCountText_9() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___logCountText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_logCountText_9() const { return ___logCountText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_logCountText_9() { return &___logCountText_9; }
	inline void set_logCountText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___logCountText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logCountText_9), (void*)value);
	}

	inline static int32_t get_offset_of_logEntry_10() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___logEntry_10)); }
	inline DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8 * get_logEntry_10() const { return ___logEntry_10; }
	inline DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8 ** get_address_of_logEntry_10() { return &___logEntry_10; }
	inline void set_logEntry_10(DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8 * value)
	{
		___logEntry_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logEntry_10), (void*)value);
	}

	inline static int32_t get_offset_of_entryIndex_11() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___entryIndex_11)); }
	inline int32_t get_entryIndex_11() const { return ___entryIndex_11; }
	inline int32_t* get_address_of_entryIndex_11() { return &___entryIndex_11; }
	inline void set_entryIndex_11(int32_t value)
	{
		___entryIndex_11 = value;
	}

	inline static int32_t get_offset_of_manager_12() { return static_cast<int32_t>(offsetof(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51, ___manager_12)); }
	inline DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 * get_manager_12() const { return ___manager_12; }
	inline DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 ** get_address_of_manager_12() { return &___manager_12; }
	inline void set_manager_12(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 * value)
	{
		___manager_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___manager_12), (void*)value);
	}
};


// IngameDebugConsole.DebugLogManager
struct  DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean IngameDebugConsole.DebugLogManager::singleton
	bool ___singleton_5;
	// System.Single IngameDebugConsole.DebugLogManager::minimumHeight
	float ___minimumHeight_6;
	// System.Boolean IngameDebugConsole.DebugLogManager::enablePopup
	bool ___enablePopup_7;
	// System.Boolean IngameDebugConsole.DebugLogManager::startInPopupMode
	bool ___startInPopupMode_8;
	// System.Boolean IngameDebugConsole.DebugLogManager::toggleWithKey
	bool ___toggleWithKey_9;
	// UnityEngine.KeyCode IngameDebugConsole.DebugLogManager::toggleKey
	int32_t ___toggleKey_10;
	// System.Boolean IngameDebugConsole.DebugLogManager::clearCommandAfterExecution
	bool ___clearCommandAfterExecution_11;
	// System.Int32 IngameDebugConsole.DebugLogManager::commandHistorySize
	int32_t ___commandHistorySize_12;
	// System.Boolean IngameDebugConsole.DebugLogManager::receiveLogcatLogsInAndroid
	bool ___receiveLogcatLogsInAndroid_13;
	// System.String IngameDebugConsole.DebugLogManager::logcatArguments
	String_t* ___logcatArguments_14;
	// IngameDebugConsole.DebugLogItem IngameDebugConsole.DebugLogManager::logItemPrefab
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51 * ___logItemPrefab_15;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::infoLog
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___infoLog_16;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::warningLog
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___warningLog_17;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::errorLog
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___errorLog_18;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Sprite> IngameDebugConsole.DebugLogManager::logSpriteRepresentations
	Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D * ___logSpriteRepresentations_19;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::collapseButtonNormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___collapseButtonNormalColor_20;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::collapseButtonSelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___collapseButtonSelectedColor_21;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::filterButtonsNormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___filterButtonsNormalColor_22;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::filterButtonsSelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___filterButtonsSelectedColor_23;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::logWindowTR
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___logWindowTR_24;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::canvasTR
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___canvasTR_25;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::logItemsContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___logItemsContainer_26;
	// UnityEngine.UI.InputField IngameDebugConsole.DebugLogManager::commandInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___commandInputField_27;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::collapseButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___collapseButton_28;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterInfoButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___filterInfoButton_29;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterWarningButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___filterWarningButton_30;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterErrorButton
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___filterErrorButton_31;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::infoEntryCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___infoEntryCountText_32;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::warningEntryCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___warningEntryCountText_33;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::errorEntryCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___errorEntryCountText_34;
	// UnityEngine.GameObject IngameDebugConsole.DebugLogManager::snapToBottomButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___snapToBottomButton_35;
	// UnityEngine.CanvasGroup IngameDebugConsole.DebugLogManager::logWindowCanvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___logWindowCanvasGroup_36;
	// IngameDebugConsole.DebugLogPopup IngameDebugConsole.DebugLogManager::popupManager
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 * ___popupManager_37;
	// UnityEngine.UI.ScrollRect IngameDebugConsole.DebugLogManager::logItemsScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___logItemsScrollRect_38;
	// IngameDebugConsole.DebugLogRecycledListView IngameDebugConsole.DebugLogManager::recycledListView
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 * ___recycledListView_39;
	// System.Int32 IngameDebugConsole.DebugLogManager::infoEntryCount
	int32_t ___infoEntryCount_40;
	// System.Int32 IngameDebugConsole.DebugLogManager::warningEntryCount
	int32_t ___warningEntryCount_41;
	// System.Int32 IngameDebugConsole.DebugLogManager::errorEntryCount
	int32_t ___errorEntryCount_42;
	// System.Boolean IngameDebugConsole.DebugLogManager::isLogWindowVisible
	bool ___isLogWindowVisible_43;
	// System.Boolean IngameDebugConsole.DebugLogManager::screenDimensionsChanged
	bool ___screenDimensionsChanged_44;
	// System.Boolean IngameDebugConsole.DebugLogManager::isCollapseOn
	bool ___isCollapseOn_45;
	// IngameDebugConsole.DebugLogFilter IngameDebugConsole.DebugLogManager::logFilter
	int32_t ___logFilter_46;
	// System.Boolean IngameDebugConsole.DebugLogManager::snapToBottom
	bool ___snapToBottom_47;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry> IngameDebugConsole.DebugLogManager::collapsedLogEntries
	List_1_t3A4F894E1990F66813E80863CBC0815538439C01 * ___collapsedLogEntries_48;
	// System.Collections.Generic.Dictionary`2<IngameDebugConsole.DebugLogEntry,System.Int32> IngameDebugConsole.DebugLogManager::collapsedLogEntriesMap
	Dictionary_2_t26860259EC2C47041B29D8B5CC0507DE43F9D5BF * ___collapsedLogEntriesMap_49;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogManager::uncollapsedLogEntriesIndices
	DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * ___uncollapsedLogEntriesIndices_50;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogManager::indicesOfListEntriesToShow
	DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * ___indicesOfListEntriesToShow_51;
	// System.Collections.Generic.List`1<IngameDebugConsole.QueuedDebugLogEntry> IngameDebugConsole.DebugLogManager::queuedLogs
	List_1_t0795B1BC835CECF56CA584A8380D9A2A34DE9A64 * ___queuedLogs_52;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogItem> IngameDebugConsole.DebugLogManager::pooledLogItems
	List_1_t42A70AE83EB6E2E98262558DE80CA745A3F48370 * ___pooledLogItems_53;
	// IngameDebugConsole.CircularBuffer`1<System.String> IngameDebugConsole.DebugLogManager::commandHistory
	CircularBuffer_1_t2F11E738091D4E0A93B448F8BD736E9C207CC0E6 * ___commandHistory_54;
	// System.Int32 IngameDebugConsole.DebugLogManager::commandHistoryIndex
	int32_t ___commandHistoryIndex_55;
	// UnityEngine.EventSystems.PointerEventData IngameDebugConsole.DebugLogManager::nullPointerEventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___nullPointerEventData_56;

public:
	inline static int32_t get_offset_of_singleton_5() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___singleton_5)); }
	inline bool get_singleton_5() const { return ___singleton_5; }
	inline bool* get_address_of_singleton_5() { return &___singleton_5; }
	inline void set_singleton_5(bool value)
	{
		___singleton_5 = value;
	}

	inline static int32_t get_offset_of_minimumHeight_6() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___minimumHeight_6)); }
	inline float get_minimumHeight_6() const { return ___minimumHeight_6; }
	inline float* get_address_of_minimumHeight_6() { return &___minimumHeight_6; }
	inline void set_minimumHeight_6(float value)
	{
		___minimumHeight_6 = value;
	}

	inline static int32_t get_offset_of_enablePopup_7() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___enablePopup_7)); }
	inline bool get_enablePopup_7() const { return ___enablePopup_7; }
	inline bool* get_address_of_enablePopup_7() { return &___enablePopup_7; }
	inline void set_enablePopup_7(bool value)
	{
		___enablePopup_7 = value;
	}

	inline static int32_t get_offset_of_startInPopupMode_8() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___startInPopupMode_8)); }
	inline bool get_startInPopupMode_8() const { return ___startInPopupMode_8; }
	inline bool* get_address_of_startInPopupMode_8() { return &___startInPopupMode_8; }
	inline void set_startInPopupMode_8(bool value)
	{
		___startInPopupMode_8 = value;
	}

	inline static int32_t get_offset_of_toggleWithKey_9() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___toggleWithKey_9)); }
	inline bool get_toggleWithKey_9() const { return ___toggleWithKey_9; }
	inline bool* get_address_of_toggleWithKey_9() { return &___toggleWithKey_9; }
	inline void set_toggleWithKey_9(bool value)
	{
		___toggleWithKey_9 = value;
	}

	inline static int32_t get_offset_of_toggleKey_10() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___toggleKey_10)); }
	inline int32_t get_toggleKey_10() const { return ___toggleKey_10; }
	inline int32_t* get_address_of_toggleKey_10() { return &___toggleKey_10; }
	inline void set_toggleKey_10(int32_t value)
	{
		___toggleKey_10 = value;
	}

	inline static int32_t get_offset_of_clearCommandAfterExecution_11() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___clearCommandAfterExecution_11)); }
	inline bool get_clearCommandAfterExecution_11() const { return ___clearCommandAfterExecution_11; }
	inline bool* get_address_of_clearCommandAfterExecution_11() { return &___clearCommandAfterExecution_11; }
	inline void set_clearCommandAfterExecution_11(bool value)
	{
		___clearCommandAfterExecution_11 = value;
	}

	inline static int32_t get_offset_of_commandHistorySize_12() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___commandHistorySize_12)); }
	inline int32_t get_commandHistorySize_12() const { return ___commandHistorySize_12; }
	inline int32_t* get_address_of_commandHistorySize_12() { return &___commandHistorySize_12; }
	inline void set_commandHistorySize_12(int32_t value)
	{
		___commandHistorySize_12 = value;
	}

	inline static int32_t get_offset_of_receiveLogcatLogsInAndroid_13() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___receiveLogcatLogsInAndroid_13)); }
	inline bool get_receiveLogcatLogsInAndroid_13() const { return ___receiveLogcatLogsInAndroid_13; }
	inline bool* get_address_of_receiveLogcatLogsInAndroid_13() { return &___receiveLogcatLogsInAndroid_13; }
	inline void set_receiveLogcatLogsInAndroid_13(bool value)
	{
		___receiveLogcatLogsInAndroid_13 = value;
	}

	inline static int32_t get_offset_of_logcatArguments_14() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logcatArguments_14)); }
	inline String_t* get_logcatArguments_14() const { return ___logcatArguments_14; }
	inline String_t** get_address_of_logcatArguments_14() { return &___logcatArguments_14; }
	inline void set_logcatArguments_14(String_t* value)
	{
		___logcatArguments_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logcatArguments_14), (void*)value);
	}

	inline static int32_t get_offset_of_logItemPrefab_15() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logItemPrefab_15)); }
	inline DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51 * get_logItemPrefab_15() const { return ___logItemPrefab_15; }
	inline DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51 ** get_address_of_logItemPrefab_15() { return &___logItemPrefab_15; }
	inline void set_logItemPrefab_15(DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51 * value)
	{
		___logItemPrefab_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logItemPrefab_15), (void*)value);
	}

	inline static int32_t get_offset_of_infoLog_16() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___infoLog_16)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_infoLog_16() const { return ___infoLog_16; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_infoLog_16() { return &___infoLog_16; }
	inline void set_infoLog_16(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___infoLog_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___infoLog_16), (void*)value);
	}

	inline static int32_t get_offset_of_warningLog_17() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___warningLog_17)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_warningLog_17() const { return ___warningLog_17; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_warningLog_17() { return &___warningLog_17; }
	inline void set_warningLog_17(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___warningLog_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___warningLog_17), (void*)value);
	}

	inline static int32_t get_offset_of_errorLog_18() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___errorLog_18)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_errorLog_18() const { return ___errorLog_18; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_errorLog_18() { return &___errorLog_18; }
	inline void set_errorLog_18(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___errorLog_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorLog_18), (void*)value);
	}

	inline static int32_t get_offset_of_logSpriteRepresentations_19() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logSpriteRepresentations_19)); }
	inline Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D * get_logSpriteRepresentations_19() const { return ___logSpriteRepresentations_19; }
	inline Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D ** get_address_of_logSpriteRepresentations_19() { return &___logSpriteRepresentations_19; }
	inline void set_logSpriteRepresentations_19(Dictionary_2_t48903AD17CC355C8DD3F85A6DC8A554A7258618D * value)
	{
		___logSpriteRepresentations_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logSpriteRepresentations_19), (void*)value);
	}

	inline static int32_t get_offset_of_collapseButtonNormalColor_20() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___collapseButtonNormalColor_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_collapseButtonNormalColor_20() const { return ___collapseButtonNormalColor_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_collapseButtonNormalColor_20() { return &___collapseButtonNormalColor_20; }
	inline void set_collapseButtonNormalColor_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___collapseButtonNormalColor_20 = value;
	}

	inline static int32_t get_offset_of_collapseButtonSelectedColor_21() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___collapseButtonSelectedColor_21)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_collapseButtonSelectedColor_21() const { return ___collapseButtonSelectedColor_21; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_collapseButtonSelectedColor_21() { return &___collapseButtonSelectedColor_21; }
	inline void set_collapseButtonSelectedColor_21(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___collapseButtonSelectedColor_21 = value;
	}

	inline static int32_t get_offset_of_filterButtonsNormalColor_22() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___filterButtonsNormalColor_22)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_filterButtonsNormalColor_22() const { return ___filterButtonsNormalColor_22; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_filterButtonsNormalColor_22() { return &___filterButtonsNormalColor_22; }
	inline void set_filterButtonsNormalColor_22(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___filterButtonsNormalColor_22 = value;
	}

	inline static int32_t get_offset_of_filterButtonsSelectedColor_23() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___filterButtonsSelectedColor_23)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_filterButtonsSelectedColor_23() const { return ___filterButtonsSelectedColor_23; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_filterButtonsSelectedColor_23() { return &___filterButtonsSelectedColor_23; }
	inline void set_filterButtonsSelectedColor_23(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___filterButtonsSelectedColor_23 = value;
	}

	inline static int32_t get_offset_of_logWindowTR_24() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logWindowTR_24)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_logWindowTR_24() const { return ___logWindowTR_24; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_logWindowTR_24() { return &___logWindowTR_24; }
	inline void set_logWindowTR_24(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___logWindowTR_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logWindowTR_24), (void*)value);
	}

	inline static int32_t get_offset_of_canvasTR_25() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___canvasTR_25)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_canvasTR_25() const { return ___canvasTR_25; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_canvasTR_25() { return &___canvasTR_25; }
	inline void set_canvasTR_25(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___canvasTR_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasTR_25), (void*)value);
	}

	inline static int32_t get_offset_of_logItemsContainer_26() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logItemsContainer_26)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_logItemsContainer_26() const { return ___logItemsContainer_26; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_logItemsContainer_26() { return &___logItemsContainer_26; }
	inline void set_logItemsContainer_26(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___logItemsContainer_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logItemsContainer_26), (void*)value);
	}

	inline static int32_t get_offset_of_commandInputField_27() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___commandInputField_27)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_commandInputField_27() const { return ___commandInputField_27; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_commandInputField_27() { return &___commandInputField_27; }
	inline void set_commandInputField_27(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___commandInputField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___commandInputField_27), (void*)value);
	}

	inline static int32_t get_offset_of_collapseButton_28() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___collapseButton_28)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_collapseButton_28() const { return ___collapseButton_28; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_collapseButton_28() { return &___collapseButton_28; }
	inline void set_collapseButton_28(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___collapseButton_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collapseButton_28), (void*)value);
	}

	inline static int32_t get_offset_of_filterInfoButton_29() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___filterInfoButton_29)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_filterInfoButton_29() const { return ___filterInfoButton_29; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_filterInfoButton_29() { return &___filterInfoButton_29; }
	inline void set_filterInfoButton_29(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___filterInfoButton_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filterInfoButton_29), (void*)value);
	}

	inline static int32_t get_offset_of_filterWarningButton_30() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___filterWarningButton_30)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_filterWarningButton_30() const { return ___filterWarningButton_30; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_filterWarningButton_30() { return &___filterWarningButton_30; }
	inline void set_filterWarningButton_30(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___filterWarningButton_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filterWarningButton_30), (void*)value);
	}

	inline static int32_t get_offset_of_filterErrorButton_31() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___filterErrorButton_31)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_filterErrorButton_31() const { return ___filterErrorButton_31; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_filterErrorButton_31() { return &___filterErrorButton_31; }
	inline void set_filterErrorButton_31(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___filterErrorButton_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filterErrorButton_31), (void*)value);
	}

	inline static int32_t get_offset_of_infoEntryCountText_32() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___infoEntryCountText_32)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_infoEntryCountText_32() const { return ___infoEntryCountText_32; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_infoEntryCountText_32() { return &___infoEntryCountText_32; }
	inline void set_infoEntryCountText_32(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___infoEntryCountText_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___infoEntryCountText_32), (void*)value);
	}

	inline static int32_t get_offset_of_warningEntryCountText_33() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___warningEntryCountText_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_warningEntryCountText_33() const { return ___warningEntryCountText_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_warningEntryCountText_33() { return &___warningEntryCountText_33; }
	inline void set_warningEntryCountText_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___warningEntryCountText_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___warningEntryCountText_33), (void*)value);
	}

	inline static int32_t get_offset_of_errorEntryCountText_34() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___errorEntryCountText_34)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_errorEntryCountText_34() const { return ___errorEntryCountText_34; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_errorEntryCountText_34() { return &___errorEntryCountText_34; }
	inline void set_errorEntryCountText_34(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___errorEntryCountText_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorEntryCountText_34), (void*)value);
	}

	inline static int32_t get_offset_of_snapToBottomButton_35() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___snapToBottomButton_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_snapToBottomButton_35() const { return ___snapToBottomButton_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_snapToBottomButton_35() { return &___snapToBottomButton_35; }
	inline void set_snapToBottomButton_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___snapToBottomButton_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___snapToBottomButton_35), (void*)value);
	}

	inline static int32_t get_offset_of_logWindowCanvasGroup_36() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logWindowCanvasGroup_36)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_logWindowCanvasGroup_36() const { return ___logWindowCanvasGroup_36; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_logWindowCanvasGroup_36() { return &___logWindowCanvasGroup_36; }
	inline void set_logWindowCanvasGroup_36(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___logWindowCanvasGroup_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logWindowCanvasGroup_36), (void*)value);
	}

	inline static int32_t get_offset_of_popupManager_37() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___popupManager_37)); }
	inline DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 * get_popupManager_37() const { return ___popupManager_37; }
	inline DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 ** get_address_of_popupManager_37() { return &___popupManager_37; }
	inline void set_popupManager_37(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656 * value)
	{
		___popupManager_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___popupManager_37), (void*)value);
	}

	inline static int32_t get_offset_of_logItemsScrollRect_38() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logItemsScrollRect_38)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_logItemsScrollRect_38() const { return ___logItemsScrollRect_38; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_logItemsScrollRect_38() { return &___logItemsScrollRect_38; }
	inline void set_logItemsScrollRect_38(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___logItemsScrollRect_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logItemsScrollRect_38), (void*)value);
	}

	inline static int32_t get_offset_of_recycledListView_39() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___recycledListView_39)); }
	inline DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 * get_recycledListView_39() const { return ___recycledListView_39; }
	inline DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 ** get_address_of_recycledListView_39() { return &___recycledListView_39; }
	inline void set_recycledListView_39(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928 * value)
	{
		___recycledListView_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recycledListView_39), (void*)value);
	}

	inline static int32_t get_offset_of_infoEntryCount_40() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___infoEntryCount_40)); }
	inline int32_t get_infoEntryCount_40() const { return ___infoEntryCount_40; }
	inline int32_t* get_address_of_infoEntryCount_40() { return &___infoEntryCount_40; }
	inline void set_infoEntryCount_40(int32_t value)
	{
		___infoEntryCount_40 = value;
	}

	inline static int32_t get_offset_of_warningEntryCount_41() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___warningEntryCount_41)); }
	inline int32_t get_warningEntryCount_41() const { return ___warningEntryCount_41; }
	inline int32_t* get_address_of_warningEntryCount_41() { return &___warningEntryCount_41; }
	inline void set_warningEntryCount_41(int32_t value)
	{
		___warningEntryCount_41 = value;
	}

	inline static int32_t get_offset_of_errorEntryCount_42() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___errorEntryCount_42)); }
	inline int32_t get_errorEntryCount_42() const { return ___errorEntryCount_42; }
	inline int32_t* get_address_of_errorEntryCount_42() { return &___errorEntryCount_42; }
	inline void set_errorEntryCount_42(int32_t value)
	{
		___errorEntryCount_42 = value;
	}

	inline static int32_t get_offset_of_isLogWindowVisible_43() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___isLogWindowVisible_43)); }
	inline bool get_isLogWindowVisible_43() const { return ___isLogWindowVisible_43; }
	inline bool* get_address_of_isLogWindowVisible_43() { return &___isLogWindowVisible_43; }
	inline void set_isLogWindowVisible_43(bool value)
	{
		___isLogWindowVisible_43 = value;
	}

	inline static int32_t get_offset_of_screenDimensionsChanged_44() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___screenDimensionsChanged_44)); }
	inline bool get_screenDimensionsChanged_44() const { return ___screenDimensionsChanged_44; }
	inline bool* get_address_of_screenDimensionsChanged_44() { return &___screenDimensionsChanged_44; }
	inline void set_screenDimensionsChanged_44(bool value)
	{
		___screenDimensionsChanged_44 = value;
	}

	inline static int32_t get_offset_of_isCollapseOn_45() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___isCollapseOn_45)); }
	inline bool get_isCollapseOn_45() const { return ___isCollapseOn_45; }
	inline bool* get_address_of_isCollapseOn_45() { return &___isCollapseOn_45; }
	inline void set_isCollapseOn_45(bool value)
	{
		___isCollapseOn_45 = value;
	}

	inline static int32_t get_offset_of_logFilter_46() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___logFilter_46)); }
	inline int32_t get_logFilter_46() const { return ___logFilter_46; }
	inline int32_t* get_address_of_logFilter_46() { return &___logFilter_46; }
	inline void set_logFilter_46(int32_t value)
	{
		___logFilter_46 = value;
	}

	inline static int32_t get_offset_of_snapToBottom_47() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___snapToBottom_47)); }
	inline bool get_snapToBottom_47() const { return ___snapToBottom_47; }
	inline bool* get_address_of_snapToBottom_47() { return &___snapToBottom_47; }
	inline void set_snapToBottom_47(bool value)
	{
		___snapToBottom_47 = value;
	}

	inline static int32_t get_offset_of_collapsedLogEntries_48() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___collapsedLogEntries_48)); }
	inline List_1_t3A4F894E1990F66813E80863CBC0815538439C01 * get_collapsedLogEntries_48() const { return ___collapsedLogEntries_48; }
	inline List_1_t3A4F894E1990F66813E80863CBC0815538439C01 ** get_address_of_collapsedLogEntries_48() { return &___collapsedLogEntries_48; }
	inline void set_collapsedLogEntries_48(List_1_t3A4F894E1990F66813E80863CBC0815538439C01 * value)
	{
		___collapsedLogEntries_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collapsedLogEntries_48), (void*)value);
	}

	inline static int32_t get_offset_of_collapsedLogEntriesMap_49() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___collapsedLogEntriesMap_49)); }
	inline Dictionary_2_t26860259EC2C47041B29D8B5CC0507DE43F9D5BF * get_collapsedLogEntriesMap_49() const { return ___collapsedLogEntriesMap_49; }
	inline Dictionary_2_t26860259EC2C47041B29D8B5CC0507DE43F9D5BF ** get_address_of_collapsedLogEntriesMap_49() { return &___collapsedLogEntriesMap_49; }
	inline void set_collapsedLogEntriesMap_49(Dictionary_2_t26860259EC2C47041B29D8B5CC0507DE43F9D5BF * value)
	{
		___collapsedLogEntriesMap_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collapsedLogEntriesMap_49), (void*)value);
	}

	inline static int32_t get_offset_of_uncollapsedLogEntriesIndices_50() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___uncollapsedLogEntriesIndices_50)); }
	inline DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * get_uncollapsedLogEntriesIndices_50() const { return ___uncollapsedLogEntriesIndices_50; }
	inline DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC ** get_address_of_uncollapsedLogEntriesIndices_50() { return &___uncollapsedLogEntriesIndices_50; }
	inline void set_uncollapsedLogEntriesIndices_50(DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * value)
	{
		___uncollapsedLogEntriesIndices_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uncollapsedLogEntriesIndices_50), (void*)value);
	}

	inline static int32_t get_offset_of_indicesOfListEntriesToShow_51() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___indicesOfListEntriesToShow_51)); }
	inline DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * get_indicesOfListEntriesToShow_51() const { return ___indicesOfListEntriesToShow_51; }
	inline DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC ** get_address_of_indicesOfListEntriesToShow_51() { return &___indicesOfListEntriesToShow_51; }
	inline void set_indicesOfListEntriesToShow_51(DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * value)
	{
		___indicesOfListEntriesToShow_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indicesOfListEntriesToShow_51), (void*)value);
	}

	inline static int32_t get_offset_of_queuedLogs_52() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___queuedLogs_52)); }
	inline List_1_t0795B1BC835CECF56CA584A8380D9A2A34DE9A64 * get_queuedLogs_52() const { return ___queuedLogs_52; }
	inline List_1_t0795B1BC835CECF56CA584A8380D9A2A34DE9A64 ** get_address_of_queuedLogs_52() { return &___queuedLogs_52; }
	inline void set_queuedLogs_52(List_1_t0795B1BC835CECF56CA584A8380D9A2A34DE9A64 * value)
	{
		___queuedLogs_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___queuedLogs_52), (void*)value);
	}

	inline static int32_t get_offset_of_pooledLogItems_53() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___pooledLogItems_53)); }
	inline List_1_t42A70AE83EB6E2E98262558DE80CA745A3F48370 * get_pooledLogItems_53() const { return ___pooledLogItems_53; }
	inline List_1_t42A70AE83EB6E2E98262558DE80CA745A3F48370 ** get_address_of_pooledLogItems_53() { return &___pooledLogItems_53; }
	inline void set_pooledLogItems_53(List_1_t42A70AE83EB6E2E98262558DE80CA745A3F48370 * value)
	{
		___pooledLogItems_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pooledLogItems_53), (void*)value);
	}

	inline static int32_t get_offset_of_commandHistory_54() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___commandHistory_54)); }
	inline CircularBuffer_1_t2F11E738091D4E0A93B448F8BD736E9C207CC0E6 * get_commandHistory_54() const { return ___commandHistory_54; }
	inline CircularBuffer_1_t2F11E738091D4E0A93B448F8BD736E9C207CC0E6 ** get_address_of_commandHistory_54() { return &___commandHistory_54; }
	inline void set_commandHistory_54(CircularBuffer_1_t2F11E738091D4E0A93B448F8BD736E9C207CC0E6 * value)
	{
		___commandHistory_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___commandHistory_54), (void*)value);
	}

	inline static int32_t get_offset_of_commandHistoryIndex_55() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___commandHistoryIndex_55)); }
	inline int32_t get_commandHistoryIndex_55() const { return ___commandHistoryIndex_55; }
	inline int32_t* get_address_of_commandHistoryIndex_55() { return &___commandHistoryIndex_55; }
	inline void set_commandHistoryIndex_55(int32_t value)
	{
		___commandHistoryIndex_55 = value;
	}

	inline static int32_t get_offset_of_nullPointerEventData_56() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974, ___nullPointerEventData_56)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_nullPointerEventData_56() const { return ___nullPointerEventData_56; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_nullPointerEventData_56() { return &___nullPointerEventData_56; }
	inline void set_nullPointerEventData_56(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___nullPointerEventData_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nullPointerEventData_56), (void*)value);
	}
};

struct DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974_StaticFields
{
public:
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogManager::instance
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974_StaticFields, ___instance_4)); }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * get_instance_4() const { return ___instance_4; }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// IngameDebugConsole.DebugLogPopup
struct  DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogPopup::popupTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___popupTransform_4;
	// UnityEngine.Vector2 IngameDebugConsole.DebugLogPopup::halfSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___halfSize_5;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogPopup::backgroundImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___backgroundImage_6;
	// UnityEngine.CanvasGroup IngameDebugConsole.DebugLogPopup::canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___canvasGroup_7;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogPopup::debugManager
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * ___debugManager_8;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newInfoCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___newInfoCountText_9;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newWarningCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___newWarningCountText_10;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newErrorCountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___newErrorCountText_11;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorInfo
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___alertColorInfo_12;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorWarning
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___alertColorWarning_13;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorError
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___alertColorError_14;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newInfoCount
	int32_t ___newInfoCount_15;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newWarningCount
	int32_t ___newWarningCount_16;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newErrorCount
	int32_t ___newErrorCount_17;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::normalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___normalColor_18;
	// System.Boolean IngameDebugConsole.DebugLogPopup::isPopupBeingDragged
	bool ___isPopupBeingDragged_19;
	// System.Collections.IEnumerator IngameDebugConsole.DebugLogPopup::moveToPosCoroutine
	RuntimeObject* ___moveToPosCoroutine_20;

public:
	inline static int32_t get_offset_of_popupTransform_4() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___popupTransform_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_popupTransform_4() const { return ___popupTransform_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_popupTransform_4() { return &___popupTransform_4; }
	inline void set_popupTransform_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___popupTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___popupTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_halfSize_5() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___halfSize_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_halfSize_5() const { return ___halfSize_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_halfSize_5() { return &___halfSize_5; }
	inline void set_halfSize_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___halfSize_5 = value;
	}

	inline static int32_t get_offset_of_backgroundImage_6() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___backgroundImage_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_backgroundImage_6() const { return ___backgroundImage_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_backgroundImage_6() { return &___backgroundImage_6; }
	inline void set_backgroundImage_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___backgroundImage_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backgroundImage_6), (void*)value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___canvasGroup_7)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasGroup_7), (void*)value);
	}

	inline static int32_t get_offset_of_debugManager_8() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___debugManager_8)); }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * get_debugManager_8() const { return ___debugManager_8; }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 ** get_address_of_debugManager_8() { return &___debugManager_8; }
	inline void set_debugManager_8(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * value)
	{
		___debugManager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugManager_8), (void*)value);
	}

	inline static int32_t get_offset_of_newInfoCountText_9() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___newInfoCountText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_newInfoCountText_9() const { return ___newInfoCountText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_newInfoCountText_9() { return &___newInfoCountText_9; }
	inline void set_newInfoCountText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___newInfoCountText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newInfoCountText_9), (void*)value);
	}

	inline static int32_t get_offset_of_newWarningCountText_10() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___newWarningCountText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_newWarningCountText_10() const { return ___newWarningCountText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_newWarningCountText_10() { return &___newWarningCountText_10; }
	inline void set_newWarningCountText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___newWarningCountText_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newWarningCountText_10), (void*)value);
	}

	inline static int32_t get_offset_of_newErrorCountText_11() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___newErrorCountText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_newErrorCountText_11() const { return ___newErrorCountText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_newErrorCountText_11() { return &___newErrorCountText_11; }
	inline void set_newErrorCountText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___newErrorCountText_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newErrorCountText_11), (void*)value);
	}

	inline static int32_t get_offset_of_alertColorInfo_12() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___alertColorInfo_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_alertColorInfo_12() const { return ___alertColorInfo_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_alertColorInfo_12() { return &___alertColorInfo_12; }
	inline void set_alertColorInfo_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___alertColorInfo_12 = value;
	}

	inline static int32_t get_offset_of_alertColorWarning_13() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___alertColorWarning_13)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_alertColorWarning_13() const { return ___alertColorWarning_13; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_alertColorWarning_13() { return &___alertColorWarning_13; }
	inline void set_alertColorWarning_13(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___alertColorWarning_13 = value;
	}

	inline static int32_t get_offset_of_alertColorError_14() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___alertColorError_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_alertColorError_14() const { return ___alertColorError_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_alertColorError_14() { return &___alertColorError_14; }
	inline void set_alertColorError_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___alertColorError_14 = value;
	}

	inline static int32_t get_offset_of_newInfoCount_15() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___newInfoCount_15)); }
	inline int32_t get_newInfoCount_15() const { return ___newInfoCount_15; }
	inline int32_t* get_address_of_newInfoCount_15() { return &___newInfoCount_15; }
	inline void set_newInfoCount_15(int32_t value)
	{
		___newInfoCount_15 = value;
	}

	inline static int32_t get_offset_of_newWarningCount_16() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___newWarningCount_16)); }
	inline int32_t get_newWarningCount_16() const { return ___newWarningCount_16; }
	inline int32_t* get_address_of_newWarningCount_16() { return &___newWarningCount_16; }
	inline void set_newWarningCount_16(int32_t value)
	{
		___newWarningCount_16 = value;
	}

	inline static int32_t get_offset_of_newErrorCount_17() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___newErrorCount_17)); }
	inline int32_t get_newErrorCount_17() const { return ___newErrorCount_17; }
	inline int32_t* get_address_of_newErrorCount_17() { return &___newErrorCount_17; }
	inline void set_newErrorCount_17(int32_t value)
	{
		___newErrorCount_17 = value;
	}

	inline static int32_t get_offset_of_normalColor_18() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___normalColor_18)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_normalColor_18() const { return ___normalColor_18; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_normalColor_18() { return &___normalColor_18; }
	inline void set_normalColor_18(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___normalColor_18 = value;
	}

	inline static int32_t get_offset_of_isPopupBeingDragged_19() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___isPopupBeingDragged_19)); }
	inline bool get_isPopupBeingDragged_19() const { return ___isPopupBeingDragged_19; }
	inline bool* get_address_of_isPopupBeingDragged_19() { return &___isPopupBeingDragged_19; }
	inline void set_isPopupBeingDragged_19(bool value)
	{
		___isPopupBeingDragged_19 = value;
	}

	inline static int32_t get_offset_of_moveToPosCoroutine_20() { return static_cast<int32_t>(offsetof(DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656, ___moveToPosCoroutine_20)); }
	inline RuntimeObject* get_moveToPosCoroutine_20() const { return ___moveToPosCoroutine_20; }
	inline RuntimeObject** get_address_of_moveToPosCoroutine_20() { return &___moveToPosCoroutine_20; }
	inline void set_moveToPosCoroutine_20(RuntimeObject* value)
	{
		___moveToPosCoroutine_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moveToPosCoroutine_20), (void*)value);
	}
};


// IngameDebugConsole.DebugLogRecycledListView
struct  DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogRecycledListView::transformComponent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___transformComponent_4;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogRecycledListView::viewportTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___viewportTransform_5;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogRecycledListView::debugManager
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * ___debugManager_6;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemNormalColor1
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___logItemNormalColor1_7;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemNormalColor2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___logItemNormalColor2_8;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemSelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___logItemSelectedColor_9;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogRecycledListView::manager
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * ___manager_10;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::logItemHeight
	float ___logItemHeight_11;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::_1OverLogItemHeight
	float ____1OverLogItemHeight_12;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::viewportHeight
	float ___viewportHeight_13;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry> IngameDebugConsole.DebugLogRecycledListView::collapsedLogEntries
	List_1_t3A4F894E1990F66813E80863CBC0815538439C01 * ___collapsedLogEntries_14;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogRecycledListView::indicesOfEntriesToShow
	DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * ___indicesOfEntriesToShow_15;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::indexOfSelectedLogEntry
	int32_t ___indexOfSelectedLogEntry_16;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::positionOfSelectedLogEntry
	float ___positionOfSelectedLogEntry_17;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::heightOfSelectedLogEntry
	float ___heightOfSelectedLogEntry_18;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::deltaHeightOfSelectedLogEntry
	float ___deltaHeightOfSelectedLogEntry_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,IngameDebugConsole.DebugLogItem> IngameDebugConsole.DebugLogRecycledListView::logItemsAtIndices
	Dictionary_2_t777E1429B40024DB728AEB28AFEBFDB1B7A79081 * ___logItemsAtIndices_20;
	// System.Boolean IngameDebugConsole.DebugLogRecycledListView::isCollapseOn
	bool ___isCollapseOn_21;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::currentTopIndex
	int32_t ___currentTopIndex_22;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::currentBottomIndex
	int32_t ___currentBottomIndex_23;

public:
	inline static int32_t get_offset_of_transformComponent_4() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___transformComponent_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_transformComponent_4() const { return ___transformComponent_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_transformComponent_4() { return &___transformComponent_4; }
	inline void set_transformComponent_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___transformComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___transformComponent_4), (void*)value);
	}

	inline static int32_t get_offset_of_viewportTransform_5() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___viewportTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_viewportTransform_5() const { return ___viewportTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_viewportTransform_5() { return &___viewportTransform_5; }
	inline void set_viewportTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___viewportTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewportTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_debugManager_6() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___debugManager_6)); }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * get_debugManager_6() const { return ___debugManager_6; }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 ** get_address_of_debugManager_6() { return &___debugManager_6; }
	inline void set_debugManager_6(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * value)
	{
		___debugManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_logItemNormalColor1_7() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___logItemNormalColor1_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_logItemNormalColor1_7() const { return ___logItemNormalColor1_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_logItemNormalColor1_7() { return &___logItemNormalColor1_7; }
	inline void set_logItemNormalColor1_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___logItemNormalColor1_7 = value;
	}

	inline static int32_t get_offset_of_logItemNormalColor2_8() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___logItemNormalColor2_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_logItemNormalColor2_8() const { return ___logItemNormalColor2_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_logItemNormalColor2_8() { return &___logItemNormalColor2_8; }
	inline void set_logItemNormalColor2_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___logItemNormalColor2_8 = value;
	}

	inline static int32_t get_offset_of_logItemSelectedColor_9() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___logItemSelectedColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_logItemSelectedColor_9() const { return ___logItemSelectedColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_logItemSelectedColor_9() { return &___logItemSelectedColor_9; }
	inline void set_logItemSelectedColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___logItemSelectedColor_9 = value;
	}

	inline static int32_t get_offset_of_manager_10() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___manager_10)); }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * get_manager_10() const { return ___manager_10; }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 ** get_address_of_manager_10() { return &___manager_10; }
	inline void set_manager_10(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * value)
	{
		___manager_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___manager_10), (void*)value);
	}

	inline static int32_t get_offset_of_logItemHeight_11() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___logItemHeight_11)); }
	inline float get_logItemHeight_11() const { return ___logItemHeight_11; }
	inline float* get_address_of_logItemHeight_11() { return &___logItemHeight_11; }
	inline void set_logItemHeight_11(float value)
	{
		___logItemHeight_11 = value;
	}

	inline static int32_t get_offset_of__1OverLogItemHeight_12() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ____1OverLogItemHeight_12)); }
	inline float get__1OverLogItemHeight_12() const { return ____1OverLogItemHeight_12; }
	inline float* get_address_of__1OverLogItemHeight_12() { return &____1OverLogItemHeight_12; }
	inline void set__1OverLogItemHeight_12(float value)
	{
		____1OverLogItemHeight_12 = value;
	}

	inline static int32_t get_offset_of_viewportHeight_13() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___viewportHeight_13)); }
	inline float get_viewportHeight_13() const { return ___viewportHeight_13; }
	inline float* get_address_of_viewportHeight_13() { return &___viewportHeight_13; }
	inline void set_viewportHeight_13(float value)
	{
		___viewportHeight_13 = value;
	}

	inline static int32_t get_offset_of_collapsedLogEntries_14() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___collapsedLogEntries_14)); }
	inline List_1_t3A4F894E1990F66813E80863CBC0815538439C01 * get_collapsedLogEntries_14() const { return ___collapsedLogEntries_14; }
	inline List_1_t3A4F894E1990F66813E80863CBC0815538439C01 ** get_address_of_collapsedLogEntries_14() { return &___collapsedLogEntries_14; }
	inline void set_collapsedLogEntries_14(List_1_t3A4F894E1990F66813E80863CBC0815538439C01 * value)
	{
		___collapsedLogEntries_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collapsedLogEntries_14), (void*)value);
	}

	inline static int32_t get_offset_of_indicesOfEntriesToShow_15() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___indicesOfEntriesToShow_15)); }
	inline DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * get_indicesOfEntriesToShow_15() const { return ___indicesOfEntriesToShow_15; }
	inline DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC ** get_address_of_indicesOfEntriesToShow_15() { return &___indicesOfEntriesToShow_15; }
	inline void set_indicesOfEntriesToShow_15(DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC * value)
	{
		___indicesOfEntriesToShow_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indicesOfEntriesToShow_15), (void*)value);
	}

	inline static int32_t get_offset_of_indexOfSelectedLogEntry_16() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___indexOfSelectedLogEntry_16)); }
	inline int32_t get_indexOfSelectedLogEntry_16() const { return ___indexOfSelectedLogEntry_16; }
	inline int32_t* get_address_of_indexOfSelectedLogEntry_16() { return &___indexOfSelectedLogEntry_16; }
	inline void set_indexOfSelectedLogEntry_16(int32_t value)
	{
		___indexOfSelectedLogEntry_16 = value;
	}

	inline static int32_t get_offset_of_positionOfSelectedLogEntry_17() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___positionOfSelectedLogEntry_17)); }
	inline float get_positionOfSelectedLogEntry_17() const { return ___positionOfSelectedLogEntry_17; }
	inline float* get_address_of_positionOfSelectedLogEntry_17() { return &___positionOfSelectedLogEntry_17; }
	inline void set_positionOfSelectedLogEntry_17(float value)
	{
		___positionOfSelectedLogEntry_17 = value;
	}

	inline static int32_t get_offset_of_heightOfSelectedLogEntry_18() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___heightOfSelectedLogEntry_18)); }
	inline float get_heightOfSelectedLogEntry_18() const { return ___heightOfSelectedLogEntry_18; }
	inline float* get_address_of_heightOfSelectedLogEntry_18() { return &___heightOfSelectedLogEntry_18; }
	inline void set_heightOfSelectedLogEntry_18(float value)
	{
		___heightOfSelectedLogEntry_18 = value;
	}

	inline static int32_t get_offset_of_deltaHeightOfSelectedLogEntry_19() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___deltaHeightOfSelectedLogEntry_19)); }
	inline float get_deltaHeightOfSelectedLogEntry_19() const { return ___deltaHeightOfSelectedLogEntry_19; }
	inline float* get_address_of_deltaHeightOfSelectedLogEntry_19() { return &___deltaHeightOfSelectedLogEntry_19; }
	inline void set_deltaHeightOfSelectedLogEntry_19(float value)
	{
		___deltaHeightOfSelectedLogEntry_19 = value;
	}

	inline static int32_t get_offset_of_logItemsAtIndices_20() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___logItemsAtIndices_20)); }
	inline Dictionary_2_t777E1429B40024DB728AEB28AFEBFDB1B7A79081 * get_logItemsAtIndices_20() const { return ___logItemsAtIndices_20; }
	inline Dictionary_2_t777E1429B40024DB728AEB28AFEBFDB1B7A79081 ** get_address_of_logItemsAtIndices_20() { return &___logItemsAtIndices_20; }
	inline void set_logItemsAtIndices_20(Dictionary_2_t777E1429B40024DB728AEB28AFEBFDB1B7A79081 * value)
	{
		___logItemsAtIndices_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logItemsAtIndices_20), (void*)value);
	}

	inline static int32_t get_offset_of_isCollapseOn_21() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___isCollapseOn_21)); }
	inline bool get_isCollapseOn_21() const { return ___isCollapseOn_21; }
	inline bool* get_address_of_isCollapseOn_21() { return &___isCollapseOn_21; }
	inline void set_isCollapseOn_21(bool value)
	{
		___isCollapseOn_21 = value;
	}

	inline static int32_t get_offset_of_currentTopIndex_22() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___currentTopIndex_22)); }
	inline int32_t get_currentTopIndex_22() const { return ___currentTopIndex_22; }
	inline int32_t* get_address_of_currentTopIndex_22() { return &___currentTopIndex_22; }
	inline void set_currentTopIndex_22(int32_t value)
	{
		___currentTopIndex_22 = value;
	}

	inline static int32_t get_offset_of_currentBottomIndex_23() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928, ___currentBottomIndex_23)); }
	inline int32_t get_currentBottomIndex_23() const { return ___currentBottomIndex_23; }
	inline int32_t* get_address_of_currentBottomIndex_23() { return &___currentBottomIndex_23; }
	inline void set_currentBottomIndex_23(int32_t value)
	{
		___currentBottomIndex_23 = value;
	}
};


// IngameDebugConsole.DebugsOnScrollListener
struct  DebugsOnScrollListener_tF2CF1E9D609286A379D31C9A269461007847308E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.ScrollRect IngameDebugConsole.DebugsOnScrollListener::debugsScrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___debugsScrollRect_4;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugsOnScrollListener::debugLogManager
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * ___debugLogManager_5;

public:
	inline static int32_t get_offset_of_debugsScrollRect_4() { return static_cast<int32_t>(offsetof(DebugsOnScrollListener_tF2CF1E9D609286A379D31C9A269461007847308E, ___debugsScrollRect_4)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_debugsScrollRect_4() const { return ___debugsScrollRect_4; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_debugsScrollRect_4() { return &___debugsScrollRect_4; }
	inline void set_debugsScrollRect_4(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___debugsScrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugsScrollRect_4), (void*)value);
	}

	inline static int32_t get_offset_of_debugLogManager_5() { return static_cast<int32_t>(offsetof(DebugsOnScrollListener_tF2CF1E9D609286A379D31C9A269461007847308E, ___debugLogManager_5)); }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * get_debugLogManager_5() const { return ___debugLogManager_5; }
	inline DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 ** get_address_of_debugLogManager_5() { return &___debugLogManager_5; }
	inline void set_debugLogManager_5(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974 * value)
	{
		___debugLogManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugLogManager_5), (void*)value);
	}
};


// Platform
struct  Platform_t32A27B4C009E21E1D2383495043A2E49A919F337  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform Platform::marbleSpawnPos
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___marbleSpawnPos_4;
	// System.Int32 Platform::Length
	int32_t ___Length_5;
	// System.Int32 Platform::angleID
	int32_t ___angleID_6;

public:
	inline static int32_t get_offset_of_marbleSpawnPos_4() { return static_cast<int32_t>(offsetof(Platform_t32A27B4C009E21E1D2383495043A2E49A919F337, ___marbleSpawnPos_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_marbleSpawnPos_4() const { return ___marbleSpawnPos_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_marbleSpawnPos_4() { return &___marbleSpawnPos_4; }
	inline void set_marbleSpawnPos_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___marbleSpawnPos_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___marbleSpawnPos_4), (void*)value);
	}

	inline static int32_t get_offset_of_Length_5() { return static_cast<int32_t>(offsetof(Platform_t32A27B4C009E21E1D2383495043A2E49A919F337, ___Length_5)); }
	inline int32_t get_Length_5() const { return ___Length_5; }
	inline int32_t* get_address_of_Length_5() { return &___Length_5; }
	inline void set_Length_5(int32_t value)
	{
		___Length_5 = value;
	}

	inline static int32_t get_offset_of_angleID_6() { return static_cast<int32_t>(offsetof(Platform_t32A27B4C009E21E1D2383495043A2E49A919F337, ___angleID_6)); }
	inline int32_t get_angleID_6() const { return ___angleID_6; }
	inline int32_t* get_address_of_angleID_6() { return &___angleID_6; }
	inline void set_angleID_6(int32_t value)
	{
		___angleID_6 = value;
	}
};


// SingletonGeneric`1<CameraController>
struct  SingletonGeneric_1_tBD5B031E200694D9280EC6CE4CBAA6ADFADC10C5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SingletonGeneric_1_tBD5B031E200694D9280EC6CE4CBAA6ADFADC10C5_StaticFields
{
public:
	// T SingletonGeneric`1::_instance
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(SingletonGeneric_1_tBD5B031E200694D9280EC6CE4CBAA6ADFADC10C5_StaticFields, ____instance_4)); }
	inline CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB * get__instance_4() const { return ____instance_4; }
	inline CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// SingletonGeneric`1<GameController>
struct  SingletonGeneric_1_t7EEF725D67921FC52227FB7318245CB577E68B3C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SingletonGeneric_1_t7EEF725D67921FC52227FB7318245CB577E68B3C_StaticFields
{
public:
	// T SingletonGeneric`1::_instance
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(SingletonGeneric_1_t7EEF725D67921FC52227FB7318245CB577E68B3C_StaticFields, ____instance_4)); }
	inline GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * get__instance_4() const { return ____instance_4; }
	inline GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// SingletonGeneric`1<InputManager>
struct  SingletonGeneric_1_tAF48E5EF01B2CB89DBA790CE0CC7008F5458256E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SingletonGeneric_1_tAF48E5EF01B2CB89DBA790CE0CC7008F5458256E_StaticFields
{
public:
	// T SingletonGeneric`1::_instance
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(SingletonGeneric_1_tAF48E5EF01B2CB89DBA790CE0CC7008F5458256E_StaticFields, ____instance_4)); }
	inline InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE * get__instance_4() const { return ____instance_4; }
	inline InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// SingletonGeneric`1<UIController>
struct  SingletonGeneric_1_t2BD3193D28CB06B653FFB3ACC786BA907C4D4556  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SingletonGeneric_1_t2BD3193D28CB06B653FFB3ACC786BA907C4D4556_StaticFields
{
public:
	// T SingletonGeneric`1::_instance
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(SingletonGeneric_1_t2BD3193D28CB06B653FFB3ACC786BA907C4D4556_StaticFields, ____instance_4)); }
	inline UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * get__instance_4() const { return ____instance_4; }
	inline UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// SlimeController
struct  SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject SlimeController::cubeScaleParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeScaleParent_4;
	// UnityEngine.GameObject SlimeController::cubeRotateParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cubeRotateParent_5;
	// UnityEngine.Rigidbody SlimeController::ballRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___ballRigidbody_6;
	// UnityEngine.TrailRenderer SlimeController::trailRenderer
	TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D * ___trailRenderer_7;
	// UnityEngine.ParticleSystem SlimeController::particles
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particles_8;
	// System.Single SlimeController::landingDuration
	float ___landingDuration_9;
	// System.Single SlimeController::hopHeight
	float ___hopHeight_10;
	// System.Single SlimeController::hopDistance
	float ___hopDistance_11;
	// UnityEngine.Quaternion SlimeController::finalSlimeRot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___finalSlimeRot_12;
	// UnityEngine.Vector3 SlimeController::originalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___originalPosition_13;
	// UnityEngine.Vector3 SlimeController::stickOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stickOffset_14;
	// DG.Tweening.Sequence SlimeController::jumpTween
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___jumpTween_15;
	// DG.Tweening.Sequence SlimeController::scaleBounceTween
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___scaleBounceTween_16;
	// DG.Tweening.Tween SlimeController::slimeRotateTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___slimeRotateTween_17;
	// DG.Tweening.Tween SlimeController::moveTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___moveTween_18;
	// UnityEngine.Vector3 SlimeController::peakScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___peakScale_19;
	// UnityEngine.Vector3 SlimeController::landScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___landScale_20;
	// System.Boolean SlimeController::isJumping
	bool ___isJumping_21;
	// System.Boolean SlimeController::isSticked
	bool ___isSticked_22;
	// System.Boolean SlimeController::isMovingLateraly
	bool ___isMovingLateraly_23;
	// System.Boolean SlimeController::isTransitioning
	bool ___isTransitioning_24;
	// System.Boolean SlimeController::inputApplied
	bool ___inputApplied_25;
	// System.Int32 SlimeController::stickDirection
	int32_t ___stickDirection_26;
	// System.Single SlimeController::initialLateralPosition
	float ___initialLateralPosition_27;
	// System.Single SlimeController::diffLatPos
	float ___diffLatPos_28;

public:
	inline static int32_t get_offset_of_cubeScaleParent_4() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___cubeScaleParent_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeScaleParent_4() const { return ___cubeScaleParent_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeScaleParent_4() { return &___cubeScaleParent_4; }
	inline void set_cubeScaleParent_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeScaleParent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeScaleParent_4), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateParent_5() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___cubeRotateParent_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cubeRotateParent_5() const { return ___cubeRotateParent_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cubeRotateParent_5() { return &___cubeRotateParent_5; }
	inline void set_cubeRotateParent_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cubeRotateParent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateParent_5), (void*)value);
	}

	inline static int32_t get_offset_of_ballRigidbody_6() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___ballRigidbody_6)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_ballRigidbody_6() const { return ___ballRigidbody_6; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_ballRigidbody_6() { return &___ballRigidbody_6; }
	inline void set_ballRigidbody_6(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___ballRigidbody_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ballRigidbody_6), (void*)value);
	}

	inline static int32_t get_offset_of_trailRenderer_7() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___trailRenderer_7)); }
	inline TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D * get_trailRenderer_7() const { return ___trailRenderer_7; }
	inline TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D ** get_address_of_trailRenderer_7() { return &___trailRenderer_7; }
	inline void set_trailRenderer_7(TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D * value)
	{
		___trailRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trailRenderer_7), (void*)value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___particles_8)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particles_8() const { return ___particles_8; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particles_8), (void*)value);
	}

	inline static int32_t get_offset_of_landingDuration_9() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___landingDuration_9)); }
	inline float get_landingDuration_9() const { return ___landingDuration_9; }
	inline float* get_address_of_landingDuration_9() { return &___landingDuration_9; }
	inline void set_landingDuration_9(float value)
	{
		___landingDuration_9 = value;
	}

	inline static int32_t get_offset_of_hopHeight_10() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___hopHeight_10)); }
	inline float get_hopHeight_10() const { return ___hopHeight_10; }
	inline float* get_address_of_hopHeight_10() { return &___hopHeight_10; }
	inline void set_hopHeight_10(float value)
	{
		___hopHeight_10 = value;
	}

	inline static int32_t get_offset_of_hopDistance_11() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___hopDistance_11)); }
	inline float get_hopDistance_11() const { return ___hopDistance_11; }
	inline float* get_address_of_hopDistance_11() { return &___hopDistance_11; }
	inline void set_hopDistance_11(float value)
	{
		___hopDistance_11 = value;
	}

	inline static int32_t get_offset_of_finalSlimeRot_12() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___finalSlimeRot_12)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_finalSlimeRot_12() const { return ___finalSlimeRot_12; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_finalSlimeRot_12() { return &___finalSlimeRot_12; }
	inline void set_finalSlimeRot_12(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___finalSlimeRot_12 = value;
	}

	inline static int32_t get_offset_of_originalPosition_13() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___originalPosition_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_originalPosition_13() const { return ___originalPosition_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_originalPosition_13() { return &___originalPosition_13; }
	inline void set_originalPosition_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___originalPosition_13 = value;
	}

	inline static int32_t get_offset_of_stickOffset_14() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___stickOffset_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stickOffset_14() const { return ___stickOffset_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stickOffset_14() { return &___stickOffset_14; }
	inline void set_stickOffset_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stickOffset_14 = value;
	}

	inline static int32_t get_offset_of_jumpTween_15() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___jumpTween_15)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_jumpTween_15() const { return ___jumpTween_15; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_jumpTween_15() { return &___jumpTween_15; }
	inline void set_jumpTween_15(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___jumpTween_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jumpTween_15), (void*)value);
	}

	inline static int32_t get_offset_of_scaleBounceTween_16() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___scaleBounceTween_16)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_scaleBounceTween_16() const { return ___scaleBounceTween_16; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_scaleBounceTween_16() { return &___scaleBounceTween_16; }
	inline void set_scaleBounceTween_16(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___scaleBounceTween_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scaleBounceTween_16), (void*)value);
	}

	inline static int32_t get_offset_of_slimeRotateTween_17() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___slimeRotateTween_17)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_slimeRotateTween_17() const { return ___slimeRotateTween_17; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_slimeRotateTween_17() { return &___slimeRotateTween_17; }
	inline void set_slimeRotateTween_17(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___slimeRotateTween_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slimeRotateTween_17), (void*)value);
	}

	inline static int32_t get_offset_of_moveTween_18() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___moveTween_18)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_moveTween_18() const { return ___moveTween_18; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_moveTween_18() { return &___moveTween_18; }
	inline void set_moveTween_18(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___moveTween_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moveTween_18), (void*)value);
	}

	inline static int32_t get_offset_of_peakScale_19() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___peakScale_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_peakScale_19() const { return ___peakScale_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_peakScale_19() { return &___peakScale_19; }
	inline void set_peakScale_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___peakScale_19 = value;
	}

	inline static int32_t get_offset_of_landScale_20() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___landScale_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_landScale_20() const { return ___landScale_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_landScale_20() { return &___landScale_20; }
	inline void set_landScale_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___landScale_20 = value;
	}

	inline static int32_t get_offset_of_isJumping_21() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___isJumping_21)); }
	inline bool get_isJumping_21() const { return ___isJumping_21; }
	inline bool* get_address_of_isJumping_21() { return &___isJumping_21; }
	inline void set_isJumping_21(bool value)
	{
		___isJumping_21 = value;
	}

	inline static int32_t get_offset_of_isSticked_22() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___isSticked_22)); }
	inline bool get_isSticked_22() const { return ___isSticked_22; }
	inline bool* get_address_of_isSticked_22() { return &___isSticked_22; }
	inline void set_isSticked_22(bool value)
	{
		___isSticked_22 = value;
	}

	inline static int32_t get_offset_of_isMovingLateraly_23() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___isMovingLateraly_23)); }
	inline bool get_isMovingLateraly_23() const { return ___isMovingLateraly_23; }
	inline bool* get_address_of_isMovingLateraly_23() { return &___isMovingLateraly_23; }
	inline void set_isMovingLateraly_23(bool value)
	{
		___isMovingLateraly_23 = value;
	}

	inline static int32_t get_offset_of_isTransitioning_24() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___isTransitioning_24)); }
	inline bool get_isTransitioning_24() const { return ___isTransitioning_24; }
	inline bool* get_address_of_isTransitioning_24() { return &___isTransitioning_24; }
	inline void set_isTransitioning_24(bool value)
	{
		___isTransitioning_24 = value;
	}

	inline static int32_t get_offset_of_inputApplied_25() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___inputApplied_25)); }
	inline bool get_inputApplied_25() const { return ___inputApplied_25; }
	inline bool* get_address_of_inputApplied_25() { return &___inputApplied_25; }
	inline void set_inputApplied_25(bool value)
	{
		___inputApplied_25 = value;
	}

	inline static int32_t get_offset_of_stickDirection_26() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___stickDirection_26)); }
	inline int32_t get_stickDirection_26() const { return ___stickDirection_26; }
	inline int32_t* get_address_of_stickDirection_26() { return &___stickDirection_26; }
	inline void set_stickDirection_26(int32_t value)
	{
		___stickDirection_26 = value;
	}

	inline static int32_t get_offset_of_initialLateralPosition_27() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___initialLateralPosition_27)); }
	inline float get_initialLateralPosition_27() const { return ___initialLateralPosition_27; }
	inline float* get_address_of_initialLateralPosition_27() { return &___initialLateralPosition_27; }
	inline void set_initialLateralPosition_27(float value)
	{
		___initialLateralPosition_27 = value;
	}

	inline static int32_t get_offset_of_diffLatPos_28() { return static_cast<int32_t>(offsetof(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421, ___diffLatPos_28)); }
	inline float get_diffLatPos_28() const { return ___diffLatPos_28; }
	inline float* get_address_of_diffLatPos_28() { return &___diffLatPos_28; }
	inline void set_diffLatPos_28(float value)
	{
		___diffLatPos_28 = value;
	}
};


// UIScreen
struct  UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIScreen::header
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___header_4;
	// UnityEngine.GameObject UIScreen::footer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___footer_5;
	// UnityEngine.Vector3 UIScreen::headerOriginalPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___headerOriginalPos_6;
	// UnityEngine.Vector3 UIScreen::footerOriginalPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___footerOriginalPos_7;
	// UnityEngine.RectTransform UIScreen::headerTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___headerTransform_8;
	// UnityEngine.RectTransform UIScreen::footerTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___footerTransform_9;
	// DG.Tweening.Tween UIScreen::headerTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___headerTween_10;
	// DG.Tweening.Tween UIScreen::footerTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___footerTween_11;

public:
	inline static int32_t get_offset_of_header_4() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___header_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_header_4() const { return ___header_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_header_4() { return &___header_4; }
	inline void set_header_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___header_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_4), (void*)value);
	}

	inline static int32_t get_offset_of_footer_5() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___footer_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_footer_5() const { return ___footer_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_footer_5() { return &___footer_5; }
	inline void set_footer_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___footer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___footer_5), (void*)value);
	}

	inline static int32_t get_offset_of_headerOriginalPos_6() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___headerOriginalPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_headerOriginalPos_6() const { return ___headerOriginalPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_headerOriginalPos_6() { return &___headerOriginalPos_6; }
	inline void set_headerOriginalPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___headerOriginalPos_6 = value;
	}

	inline static int32_t get_offset_of_footerOriginalPos_7() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___footerOriginalPos_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_footerOriginalPos_7() const { return ___footerOriginalPos_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_footerOriginalPos_7() { return &___footerOriginalPos_7; }
	inline void set_footerOriginalPos_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___footerOriginalPos_7 = value;
	}

	inline static int32_t get_offset_of_headerTransform_8() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___headerTransform_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_headerTransform_8() const { return ___headerTransform_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_headerTransform_8() { return &___headerTransform_8; }
	inline void set_headerTransform_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___headerTransform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headerTransform_8), (void*)value);
	}

	inline static int32_t get_offset_of_footerTransform_9() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___footerTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_footerTransform_9() const { return ___footerTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_footerTransform_9() { return &___footerTransform_9; }
	inline void set_footerTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___footerTransform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___footerTransform_9), (void*)value);
	}

	inline static int32_t get_offset_of_headerTween_10() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___headerTween_10)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_headerTween_10() const { return ___headerTween_10; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_headerTween_10() { return &___headerTween_10; }
	inline void set_headerTween_10(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___headerTween_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headerTween_10), (void*)value);
	}

	inline static int32_t get_offset_of_footerTween_11() { return static_cast<int32_t>(offsetof(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557, ___footerTween_11)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_footerTween_11() const { return ___footerTween_11; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_footerTween_11() { return &___footerTween_11; }
	inline void set_footerTween_11(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___footerTween_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___footerTween_11), (void*)value);
	}
};


// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger_Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t3D4AF004AB30F72D5A06525187E5F50A875DE9FA * ___m_Delegates_4;

public:
	inline static int32_t get_offset_of_m_Delegates_4() { return static_cast<int32_t>(offsetof(EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298, ___m_Delegates_4)); }
	inline List_1_t3D4AF004AB30F72D5A06525187E5F50A875DE9FA * get_m_Delegates_4() const { return ___m_Delegates_4; }
	inline List_1_t3D4AF004AB30F72D5A06525187E5F50A875DE9FA ** get_address_of_m_Delegates_4() { return &___m_Delegates_4; }
	inline void set_m_Delegates_4(List_1_t3D4AF004AB30F72D5A06525187E5F50A875DE9FA * value)
	{
		___m_Delegates_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Delegates_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// iOSHapticFeedback
struct  iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// iOSHapticFeedback_iOSFeedbackTypeSettings iOSHapticFeedback::usedFeedbackTypes
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1 * ___usedFeedbackTypes_5;
	// System.Boolean iOSHapticFeedback::feedbackGeneratorsSetUp
	bool ___feedbackGeneratorsSetUp_6;
	// System.Boolean iOSHapticFeedback::debug
	bool ___debug_7;
	// System.Boolean iOSHapticFeedback::_isEnabled
	bool ____isEnabled_8;

public:
	inline static int32_t get_offset_of_usedFeedbackTypes_5() { return static_cast<int32_t>(offsetof(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898, ___usedFeedbackTypes_5)); }
	inline iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1 * get_usedFeedbackTypes_5() const { return ___usedFeedbackTypes_5; }
	inline iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1 ** get_address_of_usedFeedbackTypes_5() { return &___usedFeedbackTypes_5; }
	inline void set_usedFeedbackTypes_5(iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1 * value)
	{
		___usedFeedbackTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___usedFeedbackTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_feedbackGeneratorsSetUp_6() { return static_cast<int32_t>(offsetof(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898, ___feedbackGeneratorsSetUp_6)); }
	inline bool get_feedbackGeneratorsSetUp_6() const { return ___feedbackGeneratorsSetUp_6; }
	inline bool* get_address_of_feedbackGeneratorsSetUp_6() { return &___feedbackGeneratorsSetUp_6; }
	inline void set_feedbackGeneratorsSetUp_6(bool value)
	{
		___feedbackGeneratorsSetUp_6 = value;
	}

	inline static int32_t get_offset_of_debug_7() { return static_cast<int32_t>(offsetof(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898, ___debug_7)); }
	inline bool get_debug_7() const { return ___debug_7; }
	inline bool* get_address_of_debug_7() { return &___debug_7; }
	inline void set_debug_7(bool value)
	{
		___debug_7 = value;
	}

	inline static int32_t get_offset_of__isEnabled_8() { return static_cast<int32_t>(offsetof(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898, ____isEnabled_8)); }
	inline bool get__isEnabled_8() const { return ____isEnabled_8; }
	inline bool* get_address_of__isEnabled_8() { return &____isEnabled_8; }
	inline void set__isEnabled_8(bool value)
	{
		____isEnabled_8 = value;
	}
};

struct iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898_StaticFields
{
public:
	// iOSHapticFeedback iOSHapticFeedback::_instance
	iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898_StaticFields, ____instance_4)); }
	inline iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898 * get__instance_4() const { return ____instance_4; }
	inline iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// iOSHapticFeedbackExample
struct  iOSHapticFeedbackExample_t53A765AEC3D85A09166BCE723079D28A37238970  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean iOSHapticFeedbackExample::supported
	bool ___supported_4;

public:
	inline static int32_t get_offset_of_supported_4() { return static_cast<int32_t>(offsetof(iOSHapticFeedbackExample_t53A765AEC3D85A09166BCE723079D28A37238970, ___supported_4)); }
	inline bool get_supported_4() const { return ___supported_4; }
	inline bool* get_address_of_supported_4() { return &___supported_4; }
	inline void set_supported_4(bool value)
	{
		___supported_4 = value;
	}
};


// CameraController
struct  CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB  : public SingletonGeneric_1_tBD5B031E200694D9280EC6CE4CBAA6ADFADC10C5
{
public:
	// System.Single CameraController::cameraSpeed
	float ___cameraSpeed_6;
	// UnityEngine.Vector3 CameraController::offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_7;
	// UnityEngine.Vector3 CameraController::initalCameraPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initalCameraPos_8;
	// UnityEngine.Transform CameraController::followTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___followTarget_9;

public:
	inline static int32_t get_offset_of_cameraSpeed_6() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB, ___cameraSpeed_6)); }
	inline float get_cameraSpeed_6() const { return ___cameraSpeed_6; }
	inline float* get_address_of_cameraSpeed_6() { return &___cameraSpeed_6; }
	inline void set_cameraSpeed_6(float value)
	{
		___cameraSpeed_6 = value;
	}

	inline static int32_t get_offset_of_offset_7() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB, ___offset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_offset_7() const { return ___offset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_offset_7() { return &___offset_7; }
	inline void set_offset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___offset_7 = value;
	}

	inline static int32_t get_offset_of_initalCameraPos_8() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB, ___initalCameraPos_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initalCameraPos_8() const { return ___initalCameraPos_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initalCameraPos_8() { return &___initalCameraPos_8; }
	inline void set_initalCameraPos_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initalCameraPos_8 = value;
	}

	inline static int32_t get_offset_of_followTarget_9() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB, ___followTarget_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_followTarget_9() const { return ___followTarget_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_followTarget_9() { return &___followTarget_9; }
	inline void set_followTarget_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___followTarget_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___followTarget_9), (void*)value);
	}
};

struct CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB_StaticFields
{
public:
	// UnityEngine.Camera CameraController::currentCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___currentCamera_5;

public:
	inline static int32_t get_offset_of_currentCamera_5() { return static_cast<int32_t>(offsetof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB_StaticFields, ___currentCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_currentCamera_5() const { return ___currentCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_currentCamera_5() { return &___currentCamera_5; }
	inline void set_currentCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___currentCamera_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentCamera_5), (void*)value);
	}
};


// GameController
struct  GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3  : public SingletonGeneric_1_t7EEF725D67921FC52227FB7318245CB577E68B3C
{
public:
	// Platform GameController::startPlatformPrefab
	Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 * ___startPlatformPrefab_5;
	// Platform GameController::endPlatformPrefab
	Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 * ___endPlatformPrefab_6;
	// System.Collections.Generic.List`1<Platform> GameController::platformPrefabs
	List_1_tBA87FE584CEE77F9910FC9239505F539594377ED * ___platformPrefabs_7;
	// SlimeController GameController::slimePrefab
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * ___slimePrefab_8;
	// Coin GameController::coinPrefab
	Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59 * ___coinPrefab_9;
	// Hoop GameController::hoopPrefab
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9 * ___hoopPrefab_10;
	// System.Single GameController::touchSensitivity
	float ___touchSensitivity_11;
	// System.Single GameController::touchSensitivityMobile
	float ___touchSensitivityMobile_12;
	// System.Collections.Generic.List`1<LevelSetting> GameController::availableSettings
	List_1_tB9C3B2978F60CC833FA7D172B1F33BC2EE26A672 * ___availableSettings_13;
	// UnityEngine.Material GameController::groundSharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___groundSharedMaterial_14;
	// UnityEngine.Material GameController::hoopsSharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___hoopsSharedMaterial_15;
	// System.Single GameController::coinSpawnChance
	float ___coinSpawnChance_16;
	// System.Single GameController::swipeThreshold
	float ___swipeThreshold_17;
	// UnityEngine.Transform GameController::platformsRoot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___platformsRoot_18;
	// GameSave GameController::gameSave
	GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8 * ___gameSave_19;
	// System.Collections.Generic.List`1<Coin> GameController::levelCoins
	List_1_tB7E3B3F1DAA95D6323FBDC0C4B3A8AB00EAF2D2F * ___levelCoins_20;
	// System.Collections.Generic.List`1<Hoop> GameController::levelHoops
	List_1_t4616D0C08FB7C88EEC62F447A5F661C4CE8ACF26 * ___levelHoops_21;
	// System.Collections.Generic.List`1<Platform> GameController::levelPlatforms
	List_1_tBA87FE584CEE77F9910FC9239505F539594377ED * ___levelPlatforms_22;
	// SlimeController GameController::currentSlime
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * ___currentSlime_23;
	// UnityEngine.Vector3 GameController::lastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastPos_24;
	// DG.Tweening.Tween GameController::fogDensityTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___fogDensityTween_25;
	// DG.Tweening.Tween GameController::timeScaleTween
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * ___timeScaleTween_26;
	// LevelSetting GameController::currentLevelSettings
	LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773 * ___currentLevelSettings_27;
	// EndPlatform GameController::endPlatform
	EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD * ___endPlatform_28;
	// System.Boolean GameController::isGameOver
	bool ___isGameOver_29;
	// System.Boolean GameController::isGameStarted
	bool ___isGameStarted_30;
	// System.Boolean GameController::isEndReached
	bool ___isEndReached_31;
	// System.Boolean GameController::isInTutorial
	bool ___isInTutorial_32;
	// System.Int32 GameController::maxJump
	int32_t ___maxJump_33;
	// System.Int32 GameController::currentJump
	int32_t ___currentJump_34;
	// System.Single GameController::startFogDensity
	float ___startFogDensity_35;

public:
	inline static int32_t get_offset_of_startPlatformPrefab_5() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___startPlatformPrefab_5)); }
	inline Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 * get_startPlatformPrefab_5() const { return ___startPlatformPrefab_5; }
	inline Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 ** get_address_of_startPlatformPrefab_5() { return &___startPlatformPrefab_5; }
	inline void set_startPlatformPrefab_5(Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 * value)
	{
		___startPlatformPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPlatformPrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of_endPlatformPrefab_6() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___endPlatformPrefab_6)); }
	inline Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 * get_endPlatformPrefab_6() const { return ___endPlatformPrefab_6; }
	inline Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 ** get_address_of_endPlatformPrefab_6() { return &___endPlatformPrefab_6; }
	inline void set_endPlatformPrefab_6(Platform_t32A27B4C009E21E1D2383495043A2E49A919F337 * value)
	{
		___endPlatformPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endPlatformPrefab_6), (void*)value);
	}

	inline static int32_t get_offset_of_platformPrefabs_7() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___platformPrefabs_7)); }
	inline List_1_tBA87FE584CEE77F9910FC9239505F539594377ED * get_platformPrefabs_7() const { return ___platformPrefabs_7; }
	inline List_1_tBA87FE584CEE77F9910FC9239505F539594377ED ** get_address_of_platformPrefabs_7() { return &___platformPrefabs_7; }
	inline void set_platformPrefabs_7(List_1_tBA87FE584CEE77F9910FC9239505F539594377ED * value)
	{
		___platformPrefabs_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___platformPrefabs_7), (void*)value);
	}

	inline static int32_t get_offset_of_slimePrefab_8() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___slimePrefab_8)); }
	inline SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * get_slimePrefab_8() const { return ___slimePrefab_8; }
	inline SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 ** get_address_of_slimePrefab_8() { return &___slimePrefab_8; }
	inline void set_slimePrefab_8(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * value)
	{
		___slimePrefab_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slimePrefab_8), (void*)value);
	}

	inline static int32_t get_offset_of_coinPrefab_9() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___coinPrefab_9)); }
	inline Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59 * get_coinPrefab_9() const { return ___coinPrefab_9; }
	inline Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59 ** get_address_of_coinPrefab_9() { return &___coinPrefab_9; }
	inline void set_coinPrefab_9(Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59 * value)
	{
		___coinPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinPrefab_9), (void*)value);
	}

	inline static int32_t get_offset_of_hoopPrefab_10() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___hoopPrefab_10)); }
	inline Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9 * get_hoopPrefab_10() const { return ___hoopPrefab_10; }
	inline Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9 ** get_address_of_hoopPrefab_10() { return &___hoopPrefab_10; }
	inline void set_hoopPrefab_10(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9 * value)
	{
		___hoopPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hoopPrefab_10), (void*)value);
	}

	inline static int32_t get_offset_of_touchSensitivity_11() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___touchSensitivity_11)); }
	inline float get_touchSensitivity_11() const { return ___touchSensitivity_11; }
	inline float* get_address_of_touchSensitivity_11() { return &___touchSensitivity_11; }
	inline void set_touchSensitivity_11(float value)
	{
		___touchSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_touchSensitivityMobile_12() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___touchSensitivityMobile_12)); }
	inline float get_touchSensitivityMobile_12() const { return ___touchSensitivityMobile_12; }
	inline float* get_address_of_touchSensitivityMobile_12() { return &___touchSensitivityMobile_12; }
	inline void set_touchSensitivityMobile_12(float value)
	{
		___touchSensitivityMobile_12 = value;
	}

	inline static int32_t get_offset_of_availableSettings_13() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___availableSettings_13)); }
	inline List_1_tB9C3B2978F60CC833FA7D172B1F33BC2EE26A672 * get_availableSettings_13() const { return ___availableSettings_13; }
	inline List_1_tB9C3B2978F60CC833FA7D172B1F33BC2EE26A672 ** get_address_of_availableSettings_13() { return &___availableSettings_13; }
	inline void set_availableSettings_13(List_1_tB9C3B2978F60CC833FA7D172B1F33BC2EE26A672 * value)
	{
		___availableSettings_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___availableSettings_13), (void*)value);
	}

	inline static int32_t get_offset_of_groundSharedMaterial_14() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___groundSharedMaterial_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_groundSharedMaterial_14() const { return ___groundSharedMaterial_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_groundSharedMaterial_14() { return &___groundSharedMaterial_14; }
	inline void set_groundSharedMaterial_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___groundSharedMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groundSharedMaterial_14), (void*)value);
	}

	inline static int32_t get_offset_of_hoopsSharedMaterial_15() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___hoopsSharedMaterial_15)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_hoopsSharedMaterial_15() const { return ___hoopsSharedMaterial_15; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_hoopsSharedMaterial_15() { return &___hoopsSharedMaterial_15; }
	inline void set_hoopsSharedMaterial_15(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___hoopsSharedMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hoopsSharedMaterial_15), (void*)value);
	}

	inline static int32_t get_offset_of_coinSpawnChance_16() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___coinSpawnChance_16)); }
	inline float get_coinSpawnChance_16() const { return ___coinSpawnChance_16; }
	inline float* get_address_of_coinSpawnChance_16() { return &___coinSpawnChance_16; }
	inline void set_coinSpawnChance_16(float value)
	{
		___coinSpawnChance_16 = value;
	}

	inline static int32_t get_offset_of_swipeThreshold_17() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___swipeThreshold_17)); }
	inline float get_swipeThreshold_17() const { return ___swipeThreshold_17; }
	inline float* get_address_of_swipeThreshold_17() { return &___swipeThreshold_17; }
	inline void set_swipeThreshold_17(float value)
	{
		___swipeThreshold_17 = value;
	}

	inline static int32_t get_offset_of_platformsRoot_18() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___platformsRoot_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_platformsRoot_18() const { return ___platformsRoot_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_platformsRoot_18() { return &___platformsRoot_18; }
	inline void set_platformsRoot_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___platformsRoot_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___platformsRoot_18), (void*)value);
	}

	inline static int32_t get_offset_of_gameSave_19() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___gameSave_19)); }
	inline GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8 * get_gameSave_19() const { return ___gameSave_19; }
	inline GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8 ** get_address_of_gameSave_19() { return &___gameSave_19; }
	inline void set_gameSave_19(GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8 * value)
	{
		___gameSave_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameSave_19), (void*)value);
	}

	inline static int32_t get_offset_of_levelCoins_20() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___levelCoins_20)); }
	inline List_1_tB7E3B3F1DAA95D6323FBDC0C4B3A8AB00EAF2D2F * get_levelCoins_20() const { return ___levelCoins_20; }
	inline List_1_tB7E3B3F1DAA95D6323FBDC0C4B3A8AB00EAF2D2F ** get_address_of_levelCoins_20() { return &___levelCoins_20; }
	inline void set_levelCoins_20(List_1_tB7E3B3F1DAA95D6323FBDC0C4B3A8AB00EAF2D2F * value)
	{
		___levelCoins_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelCoins_20), (void*)value);
	}

	inline static int32_t get_offset_of_levelHoops_21() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___levelHoops_21)); }
	inline List_1_t4616D0C08FB7C88EEC62F447A5F661C4CE8ACF26 * get_levelHoops_21() const { return ___levelHoops_21; }
	inline List_1_t4616D0C08FB7C88EEC62F447A5F661C4CE8ACF26 ** get_address_of_levelHoops_21() { return &___levelHoops_21; }
	inline void set_levelHoops_21(List_1_t4616D0C08FB7C88EEC62F447A5F661C4CE8ACF26 * value)
	{
		___levelHoops_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelHoops_21), (void*)value);
	}

	inline static int32_t get_offset_of_levelPlatforms_22() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___levelPlatforms_22)); }
	inline List_1_tBA87FE584CEE77F9910FC9239505F539594377ED * get_levelPlatforms_22() const { return ___levelPlatforms_22; }
	inline List_1_tBA87FE584CEE77F9910FC9239505F539594377ED ** get_address_of_levelPlatforms_22() { return &___levelPlatforms_22; }
	inline void set_levelPlatforms_22(List_1_tBA87FE584CEE77F9910FC9239505F539594377ED * value)
	{
		___levelPlatforms_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelPlatforms_22), (void*)value);
	}

	inline static int32_t get_offset_of_currentSlime_23() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___currentSlime_23)); }
	inline SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * get_currentSlime_23() const { return ___currentSlime_23; }
	inline SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 ** get_address_of_currentSlime_23() { return &___currentSlime_23; }
	inline void set_currentSlime_23(SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421 * value)
	{
		___currentSlime_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentSlime_23), (void*)value);
	}

	inline static int32_t get_offset_of_lastPos_24() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___lastPos_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastPos_24() const { return ___lastPos_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastPos_24() { return &___lastPos_24; }
	inline void set_lastPos_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastPos_24 = value;
	}

	inline static int32_t get_offset_of_fogDensityTween_25() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___fogDensityTween_25)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_fogDensityTween_25() const { return ___fogDensityTween_25; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_fogDensityTween_25() { return &___fogDensityTween_25; }
	inline void set_fogDensityTween_25(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___fogDensityTween_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fogDensityTween_25), (void*)value);
	}

	inline static int32_t get_offset_of_timeScaleTween_26() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___timeScaleTween_26)); }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * get_timeScaleTween_26() const { return ___timeScaleTween_26; }
	inline Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 ** get_address_of_timeScaleTween_26() { return &___timeScaleTween_26; }
	inline void set_timeScaleTween_26(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437 * value)
	{
		___timeScaleTween_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timeScaleTween_26), (void*)value);
	}

	inline static int32_t get_offset_of_currentLevelSettings_27() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___currentLevelSettings_27)); }
	inline LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773 * get_currentLevelSettings_27() const { return ___currentLevelSettings_27; }
	inline LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773 ** get_address_of_currentLevelSettings_27() { return &___currentLevelSettings_27; }
	inline void set_currentLevelSettings_27(LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773 * value)
	{
		___currentLevelSettings_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentLevelSettings_27), (void*)value);
	}

	inline static int32_t get_offset_of_endPlatform_28() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___endPlatform_28)); }
	inline EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD * get_endPlatform_28() const { return ___endPlatform_28; }
	inline EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD ** get_address_of_endPlatform_28() { return &___endPlatform_28; }
	inline void set_endPlatform_28(EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD * value)
	{
		___endPlatform_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endPlatform_28), (void*)value);
	}

	inline static int32_t get_offset_of_isGameOver_29() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___isGameOver_29)); }
	inline bool get_isGameOver_29() const { return ___isGameOver_29; }
	inline bool* get_address_of_isGameOver_29() { return &___isGameOver_29; }
	inline void set_isGameOver_29(bool value)
	{
		___isGameOver_29 = value;
	}

	inline static int32_t get_offset_of_isGameStarted_30() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___isGameStarted_30)); }
	inline bool get_isGameStarted_30() const { return ___isGameStarted_30; }
	inline bool* get_address_of_isGameStarted_30() { return &___isGameStarted_30; }
	inline void set_isGameStarted_30(bool value)
	{
		___isGameStarted_30 = value;
	}

	inline static int32_t get_offset_of_isEndReached_31() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___isEndReached_31)); }
	inline bool get_isEndReached_31() const { return ___isEndReached_31; }
	inline bool* get_address_of_isEndReached_31() { return &___isEndReached_31; }
	inline void set_isEndReached_31(bool value)
	{
		___isEndReached_31 = value;
	}

	inline static int32_t get_offset_of_isInTutorial_32() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___isInTutorial_32)); }
	inline bool get_isInTutorial_32() const { return ___isInTutorial_32; }
	inline bool* get_address_of_isInTutorial_32() { return &___isInTutorial_32; }
	inline void set_isInTutorial_32(bool value)
	{
		___isInTutorial_32 = value;
	}

	inline static int32_t get_offset_of_maxJump_33() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___maxJump_33)); }
	inline int32_t get_maxJump_33() const { return ___maxJump_33; }
	inline int32_t* get_address_of_maxJump_33() { return &___maxJump_33; }
	inline void set_maxJump_33(int32_t value)
	{
		___maxJump_33 = value;
	}

	inline static int32_t get_offset_of_currentJump_34() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___currentJump_34)); }
	inline int32_t get_currentJump_34() const { return ___currentJump_34; }
	inline int32_t* get_address_of_currentJump_34() { return &___currentJump_34; }
	inline void set_currentJump_34(int32_t value)
	{
		___currentJump_34 = value;
	}

	inline static int32_t get_offset_of_startFogDensity_35() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___startFogDensity_35)); }
	inline float get_startFogDensity_35() const { return ___startFogDensity_35; }
	inline float* get_address_of_startFogDensity_35() { return &___startFogDensity_35; }
	inline void set_startFogDensity_35(float value)
	{
		___startFogDensity_35 = value;
	}
};


// InputManager
struct  InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE  : public SingletonGeneric_1_tAF48E5EF01B2CB89DBA790CE0CC7008F5458256E
{
public:
	// InputManager_TouchDelegate InputManager::OnTouchBegan
	TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * ___OnTouchBegan_5;
	// InputManager_TouchDelegate InputManager::OnTouchUpdate
	TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * ___OnTouchUpdate_6;
	// InputManager_TouchDelegate InputManager::OnTouchEnded
	TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * ___OnTouchEnded_7;
	// UnityEngine.Vector3 InputManager::lastTouchPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastTouchPosition_8;
	// UnityEngine.Vector3 InputManager::currentTouchPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentTouchPos_9;
	// UnityEngine.Vector3 InputManager::touchesInitialPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___touchesInitialPosition_10;
	// System.Boolean InputManager::isTouching
	bool ___isTouching_11;
	// System.Single InputManager::touchDuration
	float ___touchDuration_12;

public:
	inline static int32_t get_offset_of_OnTouchBegan_5() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___OnTouchBegan_5)); }
	inline TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * get_OnTouchBegan_5() const { return ___OnTouchBegan_5; }
	inline TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 ** get_address_of_OnTouchBegan_5() { return &___OnTouchBegan_5; }
	inline void set_OnTouchBegan_5(TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * value)
	{
		___OnTouchBegan_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTouchBegan_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnTouchUpdate_6() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___OnTouchUpdate_6)); }
	inline TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * get_OnTouchUpdate_6() const { return ___OnTouchUpdate_6; }
	inline TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 ** get_address_of_OnTouchUpdate_6() { return &___OnTouchUpdate_6; }
	inline void set_OnTouchUpdate_6(TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * value)
	{
		___OnTouchUpdate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTouchUpdate_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnTouchEnded_7() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___OnTouchEnded_7)); }
	inline TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * get_OnTouchEnded_7() const { return ___OnTouchEnded_7; }
	inline TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 ** get_address_of_OnTouchEnded_7() { return &___OnTouchEnded_7; }
	inline void set_OnTouchEnded_7(TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322 * value)
	{
		___OnTouchEnded_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTouchEnded_7), (void*)value);
	}

	inline static int32_t get_offset_of_lastTouchPosition_8() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___lastTouchPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastTouchPosition_8() const { return ___lastTouchPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastTouchPosition_8() { return &___lastTouchPosition_8; }
	inline void set_lastTouchPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastTouchPosition_8 = value;
	}

	inline static int32_t get_offset_of_currentTouchPos_9() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___currentTouchPos_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentTouchPos_9() const { return ___currentTouchPos_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentTouchPos_9() { return &___currentTouchPos_9; }
	inline void set_currentTouchPos_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentTouchPos_9 = value;
	}

	inline static int32_t get_offset_of_touchesInitialPosition_10() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___touchesInitialPosition_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_touchesInitialPosition_10() const { return ___touchesInitialPosition_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_touchesInitialPosition_10() { return &___touchesInitialPosition_10; }
	inline void set_touchesInitialPosition_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___touchesInitialPosition_10 = value;
	}

	inline static int32_t get_offset_of_isTouching_11() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___isTouching_11)); }
	inline bool get_isTouching_11() const { return ___isTouching_11; }
	inline bool* get_address_of_isTouching_11() { return &___isTouching_11; }
	inline void set_isTouching_11(bool value)
	{
		___isTouching_11 = value;
	}

	inline static int32_t get_offset_of_touchDuration_12() { return static_cast<int32_t>(offsetof(InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE, ___touchDuration_12)); }
	inline float get_touchDuration_12() const { return ___touchDuration_12; }
	inline float* get_address_of_touchDuration_12() { return &___touchDuration_12; }
	inline void set_touchDuration_12(float value)
	{
		___touchDuration_12 = value;
	}
};


// UIController
struct  UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE  : public SingletonGeneric_1_t2BD3193D28CB06B653FFB3ACC786BA907C4D4556
{
public:
	// UIScreen UIController::mainMenuScreen
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * ___mainMenuScreen_5;
	// UIScreen UIController::ingameScreen
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * ___ingameScreen_6;
	// UIScreen UIController::retryScreen
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * ___retryScreen_7;
	// UIScreen UIController::victoryScreen
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * ___victoryScreen_8;
	// UnityEngine.GameObject UIController::tutoDashPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___tutoDashPanel_9;
	// UnityEngine.UI.Slider UIController::levelProgression
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___levelProgression_10;
	// UnityEngine.UI.Text UIController::levelText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___levelText_11;
	// UnityEngine.UI.Text UIController::coinText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___coinText_12;
	// UnityEngine.UI.Text UIController::completionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___completionText_13;
	// UnityEngine.UI.Text UIController::maxJumpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___maxJumpText_14;
	// UnityEngine.UI.Text UIController::jumpText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___jumpText_15;

public:
	inline static int32_t get_offset_of_mainMenuScreen_5() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___mainMenuScreen_5)); }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * get_mainMenuScreen_5() const { return ___mainMenuScreen_5; }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 ** get_address_of_mainMenuScreen_5() { return &___mainMenuScreen_5; }
	inline void set_mainMenuScreen_5(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * value)
	{
		___mainMenuScreen_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainMenuScreen_5), (void*)value);
	}

	inline static int32_t get_offset_of_ingameScreen_6() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___ingameScreen_6)); }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * get_ingameScreen_6() const { return ___ingameScreen_6; }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 ** get_address_of_ingameScreen_6() { return &___ingameScreen_6; }
	inline void set_ingameScreen_6(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * value)
	{
		___ingameScreen_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ingameScreen_6), (void*)value);
	}

	inline static int32_t get_offset_of_retryScreen_7() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___retryScreen_7)); }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * get_retryScreen_7() const { return ___retryScreen_7; }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 ** get_address_of_retryScreen_7() { return &___retryScreen_7; }
	inline void set_retryScreen_7(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * value)
	{
		___retryScreen_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___retryScreen_7), (void*)value);
	}

	inline static int32_t get_offset_of_victoryScreen_8() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___victoryScreen_8)); }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * get_victoryScreen_8() const { return ___victoryScreen_8; }
	inline UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 ** get_address_of_victoryScreen_8() { return &___victoryScreen_8; }
	inline void set_victoryScreen_8(UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557 * value)
	{
		___victoryScreen_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___victoryScreen_8), (void*)value);
	}

	inline static int32_t get_offset_of_tutoDashPanel_9() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___tutoDashPanel_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_tutoDashPanel_9() const { return ___tutoDashPanel_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_tutoDashPanel_9() { return &___tutoDashPanel_9; }
	inline void set_tutoDashPanel_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___tutoDashPanel_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tutoDashPanel_9), (void*)value);
	}

	inline static int32_t get_offset_of_levelProgression_10() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___levelProgression_10)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_levelProgression_10() const { return ___levelProgression_10; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_levelProgression_10() { return &___levelProgression_10; }
	inline void set_levelProgression_10(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___levelProgression_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelProgression_10), (void*)value);
	}

	inline static int32_t get_offset_of_levelText_11() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___levelText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_levelText_11() const { return ___levelText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_levelText_11() { return &___levelText_11; }
	inline void set_levelText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___levelText_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelText_11), (void*)value);
	}

	inline static int32_t get_offset_of_coinText_12() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___coinText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_coinText_12() const { return ___coinText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_coinText_12() { return &___coinText_12; }
	inline void set_coinText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___coinText_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinText_12), (void*)value);
	}

	inline static int32_t get_offset_of_completionText_13() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___completionText_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_completionText_13() const { return ___completionText_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_completionText_13() { return &___completionText_13; }
	inline void set_completionText_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___completionText_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___completionText_13), (void*)value);
	}

	inline static int32_t get_offset_of_maxJumpText_14() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___maxJumpText_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_maxJumpText_14() const { return ___maxJumpText_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_maxJumpText_14() { return &___maxJumpText_14; }
	inline void set_maxJumpText_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___maxJumpText_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___maxJumpText_14), (void*)value);
	}

	inline static int32_t get_offset_of_jumpText_15() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___jumpText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_jumpText_15() const { return ___jumpText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_jumpText_15() { return &___jumpText_15; }
	inline void set_jumpText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___jumpText_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jumpText_15), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseInput
struct  BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:

public:
};


// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t1BB31D71CB8736DC35CCD9FFB9228FAFD427EB52 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_RaycastResultCache_4)); }
	inline List_1_t1BB31D71CB8736DC35CCD9FFB9228FAFD427EB52 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_t1BB31D71CB8736DC35CCD9FFB9228FAFD427EB52 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_t1BB31D71CB8736DC35CCD9FFB9228FAFD427EB52 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastResultCache_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_AxisEventData_5)); }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AxisEventData_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_EventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_BaseEventData_7)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BaseEventData_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_InputOverride_8)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputOverride_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_DefaultInput_9)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultInput_9), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.BaseRaycaster::m_RootRaycaster
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___m_RootRaycaster_4;

public:
	inline static int32_t get_offset_of_m_RootRaycaster_4() { return static_cast<int32_t>(offsetof(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966, ___m_RootRaycaster_4)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_m_RootRaycaster_4() const { return ___m_RootRaycaster_4; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_m_RootRaycaster_4() { return &___m_RootRaycaster_4; }
	inline void set_m_RootRaycaster_4(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___m_RootRaycaster_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RootRaycaster_4), (void*)value);
	}
};


// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t4FB5BF302DAD74D690156A022C4FA4D4081E9B26 * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_SystemInputModules_4)); }
	inline List_1_t4FB5BF302DAD74D690156A022C4FA4D4081E9B26 * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t4FB5BF302DAD74D690156A022C4FA4D4081E9B26 ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t4FB5BF302DAD74D690156A022C4FA4D4081E9B26 * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SystemInputModules_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentInputModule_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_FirstSelected_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstSelected_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_CurrentSelected_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentSelected_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_DummyData_13)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DummyData_13), (void*)value);
	}
};

struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t882412D5BE0B5BFC1900366319F8B2EB544BDD8B * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t32541D3F4C935BBA3800256BD21A7CA8148AAC13 * ___s_RaycastComparer_14;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___m_EventSystems_6)); }
	inline List_1_t882412D5BE0B5BFC1900366319F8B2EB544BDD8B * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_t882412D5BE0B5BFC1900366319F8B2EB544BDD8B ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_t882412D5BE0B5BFC1900366319F8B2EB544BDD8B * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystems_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t32541D3F4C935BBA3800256BD21A7CA8148AAC13 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t32541D3F4C935BBA3800256BD21A7CA8148AAC13 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t32541D3F4C935BBA3800256BD21A7CA8148AAC13 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastComparer_14), (void*)value);
	}
};


// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Graphic_4), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.RectMask2D
struct  RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.RectangularVertexClipper UnityEngine.UI.RectMask2D::m_VertexClipper
	RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91 * ___m_VertexClipper_4;
	// UnityEngine.RectTransform UnityEngine.UI.RectMask2D::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_5;
	// System.Collections.Generic.HashSet`1<UnityEngine.UI.MaskableGraphic> UnityEngine.UI.RectMask2D::m_MaskableTargets
	HashSet_1_tF035B0C2C7E1925B6966D73DB277DF70D4C48408 * ___m_MaskableTargets_6;
	// System.Collections.Generic.HashSet`1<UnityEngine.UI.IClippable> UnityEngine.UI.RectMask2D::m_ClipTargets
	HashSet_1_tAAE962DCA7E1BD56AD7B2C079CD4DBA3D0B231AD * ___m_ClipTargets_7;
	// System.Boolean UnityEngine.UI.RectMask2D::m_ShouldRecalculateClipRects
	bool ___m_ShouldRecalculateClipRects_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D> UnityEngine.UI.RectMask2D::m_Clippers
	List_1_t2BA894CDA977BF8C8AC7BD397B1F6602A63B92C4 * ___m_Clippers_9;
	// UnityEngine.Rect UnityEngine.UI.RectMask2D::m_LastClipRectCanvasSpace
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_LastClipRectCanvasSpace_10;
	// System.Boolean UnityEngine.UI.RectMask2D::m_ForceClip
	bool ___m_ForceClip_11;
	// UnityEngine.Canvas UnityEngine.UI.RectMask2D::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_12;
	// UnityEngine.Vector3[] UnityEngine.UI.RectMask2D::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_13;

public:
	inline static int32_t get_offset_of_m_VertexClipper_4() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_VertexClipper_4)); }
	inline RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91 * get_m_VertexClipper_4() const { return ___m_VertexClipper_4; }
	inline RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91 ** get_address_of_m_VertexClipper_4() { return &___m_VertexClipper_4; }
	inline void set_m_VertexClipper_4(RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91 * value)
	{
		___m_VertexClipper_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VertexClipper_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_RectTransform_5() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_RectTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_5() const { return ___m_RectTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_5() { return &___m_RectTransform_5; }
	inline void set_m_RectTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_MaskableTargets_6() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_MaskableTargets_6)); }
	inline HashSet_1_tF035B0C2C7E1925B6966D73DB277DF70D4C48408 * get_m_MaskableTargets_6() const { return ___m_MaskableTargets_6; }
	inline HashSet_1_tF035B0C2C7E1925B6966D73DB277DF70D4C48408 ** get_address_of_m_MaskableTargets_6() { return &___m_MaskableTargets_6; }
	inline void set_m_MaskableTargets_6(HashSet_1_tF035B0C2C7E1925B6966D73DB277DF70D4C48408 * value)
	{
		___m_MaskableTargets_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskableTargets_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_ClipTargets_7() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_ClipTargets_7)); }
	inline HashSet_1_tAAE962DCA7E1BD56AD7B2C079CD4DBA3D0B231AD * get_m_ClipTargets_7() const { return ___m_ClipTargets_7; }
	inline HashSet_1_tAAE962DCA7E1BD56AD7B2C079CD4DBA3D0B231AD ** get_address_of_m_ClipTargets_7() { return &___m_ClipTargets_7; }
	inline void set_m_ClipTargets_7(HashSet_1_tAAE962DCA7E1BD56AD7B2C079CD4DBA3D0B231AD * value)
	{
		___m_ClipTargets_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ClipTargets_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculateClipRects_8() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_ShouldRecalculateClipRects_8)); }
	inline bool get_m_ShouldRecalculateClipRects_8() const { return ___m_ShouldRecalculateClipRects_8; }
	inline bool* get_address_of_m_ShouldRecalculateClipRects_8() { return &___m_ShouldRecalculateClipRects_8; }
	inline void set_m_ShouldRecalculateClipRects_8(bool value)
	{
		___m_ShouldRecalculateClipRects_8 = value;
	}

	inline static int32_t get_offset_of_m_Clippers_9() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_Clippers_9)); }
	inline List_1_t2BA894CDA977BF8C8AC7BD397B1F6602A63B92C4 * get_m_Clippers_9() const { return ___m_Clippers_9; }
	inline List_1_t2BA894CDA977BF8C8AC7BD397B1F6602A63B92C4 ** get_address_of_m_Clippers_9() { return &___m_Clippers_9; }
	inline void set_m_Clippers_9(List_1_t2BA894CDA977BF8C8AC7BD397B1F6602A63B92C4 * value)
	{
		___m_Clippers_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Clippers_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_LastClipRectCanvasSpace_10() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_LastClipRectCanvasSpace_10)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_LastClipRectCanvasSpace_10() const { return ___m_LastClipRectCanvasSpace_10; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_LastClipRectCanvasSpace_10() { return &___m_LastClipRectCanvasSpace_10; }
	inline void set_m_LastClipRectCanvasSpace_10(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_LastClipRectCanvasSpace_10 = value;
	}

	inline static int32_t get_offset_of_m_ForceClip_11() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_ForceClip_11)); }
	inline bool get_m_ForceClip_11() const { return ___m_ForceClip_11; }
	inline bool* get_address_of_m_ForceClip_11() { return &___m_ForceClip_11; }
	inline void set_m_ForceClip_11(bool value)
	{
		___m_ForceClip_11 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_12() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_Canvas_12)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_12() const { return ___m_Canvas_12; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_12() { return &___m_Canvas_12; }
	inline void set_m_Canvas_12(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Corners_13() { return static_cast<int32_t>(offsetof(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B, ___m_Corners_13)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_13() const { return ___m_Corners_13; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_13() { return &___m_Corners_13; }
	inline void set_m_Corners_13(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_13), (void*)value);
	}
};


// UnityEngine.UI.ScrollRect
struct  ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect_MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect_ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect_ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect_ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Scrolling
	bool ___m_Scrolling_27;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PrevPosition_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_PrevContentBounds_29;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_PrevViewBounds_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_32;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_33;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_34;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HorizontalScrollbarRect_37;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_VerticalScrollbarRect_38;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_39;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_40;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Content_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Content_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Viewport_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Viewport_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalScrollbar_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalScrollbar_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ContentStartPosition_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ViewRect_22)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ViewRect_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ContentBounds_23)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ViewBounds_24)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Velocity_25)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_Scrolling_27() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Scrolling_27)); }
	inline bool get_m_Scrolling_27() const { return ___m_Scrolling_27; }
	inline bool* get_address_of_m_Scrolling_27() { return &___m_Scrolling_27; }
	inline void set_m_Scrolling_27(bool value)
	{
		___m_Scrolling_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_28() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PrevPosition_28)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PrevPosition_28() const { return ___m_PrevPosition_28; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PrevPosition_28() { return &___m_PrevPosition_28; }
	inline void set_m_PrevPosition_28(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PrevPosition_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PrevContentBounds_29)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_PrevContentBounds_29() const { return ___m_PrevContentBounds_29; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_PrevContentBounds_29() { return &___m_PrevContentBounds_29; }
	inline void set_m_PrevContentBounds_29(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_PrevContentBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_30() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PrevViewBounds_30)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_PrevViewBounds_30() const { return ___m_PrevViewBounds_30; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_PrevViewBounds_30() { return &___m_PrevViewBounds_30; }
	inline void set_m_PrevViewBounds_30(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_PrevViewBounds_30 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_31() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HasRebuiltLayout_31)); }
	inline bool get_m_HasRebuiltLayout_31() const { return ___m_HasRebuiltLayout_31; }
	inline bool* get_address_of_m_HasRebuiltLayout_31() { return &___m_HasRebuiltLayout_31; }
	inline void set_m_HasRebuiltLayout_31(bool value)
	{
		___m_HasRebuiltLayout_31 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HSliderExpand_32)); }
	inline bool get_m_HSliderExpand_32() const { return ___m_HSliderExpand_32; }
	inline bool* get_address_of_m_HSliderExpand_32() { return &___m_HSliderExpand_32; }
	inline void set_m_HSliderExpand_32(bool value)
	{
		___m_HSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_33() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VSliderExpand_33)); }
	inline bool get_m_VSliderExpand_33() const { return ___m_VSliderExpand_33; }
	inline bool* get_address_of_m_VSliderExpand_33() { return &___m_VSliderExpand_33; }
	inline void set_m_VSliderExpand_33(bool value)
	{
		___m_VSliderExpand_33 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_34() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HSliderHeight_34)); }
	inline float get_m_HSliderHeight_34() const { return ___m_HSliderHeight_34; }
	inline float* get_address_of_m_HSliderHeight_34() { return &___m_HSliderHeight_34; }
	inline void set_m_HSliderHeight_34(float value)
	{
		___m_HSliderHeight_34 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_35() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VSliderWidth_35)); }
	inline float get_m_VSliderWidth_35() const { return ___m_VSliderWidth_35; }
	inline float* get_address_of_m_VSliderWidth_35() { return &___m_VSliderWidth_35; }
	inline void set_m_VSliderWidth_35(float value)
	{
		___m_VSliderWidth_35 = value;
	}

	inline static int32_t get_offset_of_m_Rect_36() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Rect_36)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_36() const { return ___m_Rect_36; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_36() { return &___m_Rect_36; }
	inline void set_m_Rect_36(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rect_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbarRect_37)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HorizontalScrollbarRect_37() const { return ___m_HorizontalScrollbarRect_37; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HorizontalScrollbarRect_37() { return &___m_HorizontalScrollbarRect_37; }
	inline void set_m_HorizontalScrollbarRect_37(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HorizontalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalScrollbarRect_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_38() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbarRect_38)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_VerticalScrollbarRect_38() const { return ___m_VerticalScrollbarRect_38; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_VerticalScrollbarRect_38() { return &___m_VerticalScrollbarRect_38; }
	inline void set_m_VerticalScrollbarRect_38(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_VerticalScrollbarRect_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalScrollbarRect_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tracker_39() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Tracker_39)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_39() const { return ___m_Tracker_39; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_39() { return &___m_Tracker_39; }
	inline void set_m_Tracker_39(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_39 = value;
	}

	inline static int32_t get_offset_of_m_Corners_40() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Corners_40)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_40() const { return ___m_Corners_40; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_40() { return &___m_Corners_40; }
	inline void set_m_Corners_40(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_40), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_7;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Boolean UnityEngine.UI.Selectable::m_WillRemove
	bool ___m_WillRemove_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_7)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_9)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_10)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_13)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_WillRemove_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_WillRemove_15)); }
	inline bool get_m_WillRemove_15() const { return ___m_WillRemove_15; }
	inline bool* get_address_of_m_WillRemove_15() { return &___m_WillRemove_15; }
	inline void set_m_WillRemove_15(bool value)
	{
		___m_WillRemove_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_19)); }
	inline List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
	// System.Boolean UnityEngine.UI.Selectable::s_IsDirty
	bool ___s_IsDirty_6;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}

	inline static int32_t get_offset_of_s_IsDirty_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_IsDirty_6)); }
	inline bool get_s_IsDirty_6() const { return ___s_IsDirty_6; }
	inline bool* get_address_of_s_IsDirty_6() { return &___s_IsDirty_6; }
	inline void set_s_IsDirty_6(bool value)
	{
		___s_IsDirty_6 = value;
	}
};


// UnityEngine.UI.ToggleGroup
struct  ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean UnityEngine.UI.ToggleGroup::m_AllowSwitchOff
	bool ___m_AllowSwitchOff_4;
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::m_Toggles
	List_1_t5603D617415C67B322F898ABF659C3E2C8BD668C * ___m_Toggles_5;

public:
	inline static int32_t get_offset_of_m_AllowSwitchOff_4() { return static_cast<int32_t>(offsetof(ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786, ___m_AllowSwitchOff_4)); }
	inline bool get_m_AllowSwitchOff_4() const { return ___m_AllowSwitchOff_4; }
	inline bool* get_address_of_m_AllowSwitchOff_4() { return &___m_AllowSwitchOff_4; }
	inline void set_m_AllowSwitchOff_4(bool value)
	{
		___m_AllowSwitchOff_4 = value;
	}

	inline static int32_t get_offset_of_m_Toggles_5() { return static_cast<int32_t>(offsetof(ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786, ___m_Toggles_5)); }
	inline List_1_t5603D617415C67B322F898ABF659C3E2C8BD668C * get_m_Toggles_5() const { return ___m_Toggles_5; }
	inline List_1_t5603D617415C67B322F898ABF659C3E2C8BD668C ** get_address_of_m_Toggles_5() { return &___m_Toggles_5; }
	inline void set_m_Toggles_5(List_1_t5603D617415C67B322F898ABF659C3E2C8BD668C * value)
	{
		___m_Toggles_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Toggles_5), (void*)value);
	}
};


// iOSHapticFeedbackAdvanced
struct  iOSHapticFeedbackAdvanced_tEFFB9D9CABFE2130E9DFA20CD1C41E25E1D8728B  : public iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898
{
public:

public:
};


// UnityEngine.EventSystems.PhysicsRaycaster
struct  PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C  : public BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966
{
public:
	// UnityEngine.Camera UnityEngine.EventSystems.PhysicsRaycaster::m_EventCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_EventCamera_6;
	// UnityEngine.LayerMask UnityEngine.EventSystems.PhysicsRaycaster::m_EventMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___m_EventMask_7;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_MaxRayIntersections
	int32_t ___m_MaxRayIntersections_8;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_LastMaxRayIntersections
	int32_t ___m_LastMaxRayIntersections_9;
	// UnityEngine.RaycastHit[] UnityEngine.EventSystems.PhysicsRaycaster::m_Hits
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___m_Hits_10;

public:
	inline static int32_t get_offset_of_m_EventCamera_6() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C, ___m_EventCamera_6)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_EventCamera_6() const { return ___m_EventCamera_6; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_EventCamera_6() { return &___m_EventCamera_6; }
	inline void set_m_EventCamera_6(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_EventCamera_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventCamera_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventMask_7() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C, ___m_EventMask_7)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_m_EventMask_7() const { return ___m_EventMask_7; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_m_EventMask_7() { return &___m_EventMask_7; }
	inline void set_m_EventMask_7(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___m_EventMask_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxRayIntersections_8() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C, ___m_MaxRayIntersections_8)); }
	inline int32_t get_m_MaxRayIntersections_8() const { return ___m_MaxRayIntersections_8; }
	inline int32_t* get_address_of_m_MaxRayIntersections_8() { return &___m_MaxRayIntersections_8; }
	inline void set_m_MaxRayIntersections_8(int32_t value)
	{
		___m_MaxRayIntersections_8 = value;
	}

	inline static int32_t get_offset_of_m_LastMaxRayIntersections_9() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C, ___m_LastMaxRayIntersections_9)); }
	inline int32_t get_m_LastMaxRayIntersections_9() const { return ___m_LastMaxRayIntersections_9; }
	inline int32_t* get_address_of_m_LastMaxRayIntersections_9() { return &___m_LastMaxRayIntersections_9; }
	inline void set_m_LastMaxRayIntersections_9(int32_t value)
	{
		___m_LastMaxRayIntersections_9 = value;
	}

	inline static int32_t get_offset_of_m_Hits_10() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C, ___m_Hits_10)); }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* get_m_Hits_10() const { return ___m_Hits_10; }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57** get_address_of_m_Hits_10() { return &___m_Hits_10; }
	inline void set_m_Hits_10(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* value)
	{
		___m_Hits_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Hits_10), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C  : public BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t9F401E8FAE13945DFBD264E317248AB9CC9C36CA * ___m_PointerData_14;
	// UnityEngine.EventSystems.PointerInputModule_MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7 * ___m_MouseState_15;

public:
	inline static int32_t get_offset_of_m_PointerData_14() { return static_cast<int32_t>(offsetof(PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C, ___m_PointerData_14)); }
	inline Dictionary_2_t9F401E8FAE13945DFBD264E317248AB9CC9C36CA * get_m_PointerData_14() const { return ___m_PointerData_14; }
	inline Dictionary_2_t9F401E8FAE13945DFBD264E317248AB9CC9C36CA ** get_address_of_m_PointerData_14() { return &___m_PointerData_14; }
	inline void set_m_PointerData_14(Dictionary_2_t9F401E8FAE13945DFBD264E317248AB9CC9C36CA * value)
	{
		___m_PointerData_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerData_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_MouseState_15() { return static_cast<int32_t>(offsetof(PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C, ___m_MouseState_15)); }
	inline MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7 * get_m_MouseState_15() const { return ___m_MouseState_15; }
	inline MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7 ** get_address_of_m_MouseState_15() { return &___m_MouseState_15; }
	inline void set_m_MouseState_15(MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7 * value)
	{
		___m_MouseState_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MouseState_15), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_29;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_31;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_32;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_33;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_29)); }
	inline bool get_m_IncludeForMasking_29() const { return ___m_IncludeForMasking_29; }
	inline bool* get_address_of_m_IncludeForMasking_29() { return &___m_IncludeForMasking_29; }
	inline void set_m_IncludeForMasking_29(bool value)
	{
		___m_IncludeForMasking_29 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_30)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_30() const { return ___m_OnCullStateChanged_30; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_30() { return &___m_OnCullStateChanged_30; }
	inline void set_m_OnCullStateChanged_30(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_31)); }
	inline bool get_m_ShouldRecalculate_31() const { return ___m_ShouldRecalculate_31; }
	inline bool* get_address_of_m_ShouldRecalculate_31() { return &___m_ShouldRecalculate_31; }
	inline void set_m_ShouldRecalculate_31(bool value)
	{
		___m_ShouldRecalculate_31 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_32)); }
	inline int32_t get_m_StencilValue_32() const { return ___m_StencilValue_32; }
	inline int32_t* get_address_of_m_StencilValue_32() { return &___m_StencilValue_32; }
	inline void set_m_StencilValue_32(int32_t value)
	{
		___m_StencilValue_32 = value;
	}

	inline static int32_t get_offset_of_m_Corners_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_33)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_33() const { return ___m_Corners_33; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_33() { return &___m_Corners_33; }
	inline void set_m_Corners_33(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_33), (void*)value);
	}
};


// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};


// UnityEngine.UI.Scrollbar
struct  Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_HandleRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleRect_20;
	// UnityEngine.UI.Scrollbar_Direction UnityEngine.UI.Scrollbar::m_Direction
	int32_t ___m_Direction_21;
	// System.Single UnityEngine.UI.Scrollbar::m_Value
	float ___m_Value_22;
	// System.Single UnityEngine.UI.Scrollbar::m_Size
	float ___m_Size_23;
	// System.Int32 UnityEngine.UI.Scrollbar::m_NumberOfSteps
	int32_t ___m_NumberOfSteps_24;
	// UnityEngine.UI.Scrollbar_ScrollEvent UnityEngine.UI.Scrollbar::m_OnValueChanged
	ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7 * ___m_OnValueChanged_25;
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_ContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ContainerRect_26;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar::m_Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Offset_27;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Scrollbar::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_28;
	// UnityEngine.Coroutine UnityEngine.UI.Scrollbar::m_PointerDownRepeat
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___m_PointerDownRepeat_29;
	// System.Boolean UnityEngine.UI.Scrollbar::isPointerDownAndNotDragging
	bool ___isPointerDownAndNotDragging_30;
	// System.Boolean UnityEngine.UI.Scrollbar::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_31;

public:
	inline static int32_t get_offset_of_m_HandleRect_20() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_HandleRect_20)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleRect_20() const { return ___m_HandleRect_20; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleRect_20() { return &___m_HandleRect_20; }
	inline void set_m_HandleRect_20(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_21() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_Direction_21)); }
	inline int32_t get_m_Direction_21() const { return ___m_Direction_21; }
	inline int32_t* get_address_of_m_Direction_21() { return &___m_Direction_21; }
	inline void set_m_Direction_21(int32_t value)
	{
		___m_Direction_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_Size_23() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_Size_23)); }
	inline float get_m_Size_23() const { return ___m_Size_23; }
	inline float* get_address_of_m_Size_23() { return &___m_Size_23; }
	inline void set_m_Size_23(float value)
	{
		___m_Size_23 = value;
	}

	inline static int32_t get_offset_of_m_NumberOfSteps_24() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_NumberOfSteps_24)); }
	inline int32_t get_m_NumberOfSteps_24() const { return ___m_NumberOfSteps_24; }
	inline int32_t* get_address_of_m_NumberOfSteps_24() { return &___m_NumberOfSteps_24; }
	inline void set_m_NumberOfSteps_24(int32_t value)
	{
		___m_NumberOfSteps_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_OnValueChanged_25)); }
	inline ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7 * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7 ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7 * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContainerRect_26() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_ContainerRect_26)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ContainerRect_26() const { return ___m_ContainerRect_26; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ContainerRect_26() { return &___m_ContainerRect_26; }
	inline void set_m_ContainerRect_26(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ContainerRect_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_27() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_Offset_27)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Offset_27() const { return ___m_Offset_27; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Offset_27() { return &___m_Offset_27; }
	inline void set_m_Offset_27(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Offset_27 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_28() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_Tracker_28)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_28() const { return ___m_Tracker_28; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_28() { return &___m_Tracker_28; }
	inline void set_m_Tracker_28(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_28 = value;
	}

	inline static int32_t get_offset_of_m_PointerDownRepeat_29() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_PointerDownRepeat_29)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_m_PointerDownRepeat_29() const { return ___m_PointerDownRepeat_29; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_m_PointerDownRepeat_29() { return &___m_PointerDownRepeat_29; }
	inline void set_m_PointerDownRepeat_29(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___m_PointerDownRepeat_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerDownRepeat_29), (void*)value);
	}

	inline static int32_t get_offset_of_isPointerDownAndNotDragging_30() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___isPointerDownAndNotDragging_30)); }
	inline bool get_isPointerDownAndNotDragging_30() const { return ___isPointerDownAndNotDragging_30; }
	inline bool* get_address_of_isPointerDownAndNotDragging_30() { return &___isPointerDownAndNotDragging_30; }
	inline void set_isPointerDownAndNotDragging_30(bool value)
	{
		___isPointerDownAndNotDragging_30 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_31() { return static_cast<int32_t>(offsetof(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389, ___m_DelayedUpdateVisuals_31)); }
	inline bool get_m_DelayedUpdateVisuals_31() const { return ___m_DelayedUpdateVisuals_31; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_31() { return &___m_DelayedUpdateVisuals_31; }
	inline void set_m_DelayedUpdateVisuals_31(bool value)
	{
		___m_DelayedUpdateVisuals_31 = value;
	}
};


// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};


// UnityEngine.UI.Slider
struct  Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_FillRect_20;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleRect_21;
	// UnityEngine.UI.Slider_Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_22;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_23;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_24;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_25;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_26;
	// UnityEngine.UI.Slider_SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * ___m_OnValueChanged_27;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_FillImage_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_FillTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_FillContainerRect_30;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_HandleTransform_31;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleContainerRect_32;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Offset_33;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_34;
	// System.Boolean UnityEngine.UI.Slider::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_35;

public:
	inline static int32_t get_offset_of_m_FillRect_20() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillRect_20)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_FillRect_20() const { return ___m_FillRect_20; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_FillRect_20() { return &___m_FillRect_20; }
	inline void set_m_FillRect_20(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_FillRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleRect_21() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleRect_21)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleRect_21() const { return ___m_HandleRect_21; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleRect_21() { return &___m_HandleRect_21; }
	inline void set_m_HandleRect_21(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleRect_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_22() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Direction_22)); }
	inline int32_t get_m_Direction_22() const { return ___m_Direction_22; }
	inline int32_t* get_address_of_m_Direction_22() { return &___m_Direction_22; }
	inline void set_m_Direction_22(int32_t value)
	{
		___m_Direction_22 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_23() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_MinValue_23)); }
	inline float get_m_MinValue_23() const { return ___m_MinValue_23; }
	inline float* get_address_of_m_MinValue_23() { return &___m_MinValue_23; }
	inline void set_m_MinValue_23(float value)
	{
		___m_MinValue_23 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_24() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_MaxValue_24)); }
	inline float get_m_MaxValue_24() const { return ___m_MaxValue_24; }
	inline float* get_address_of_m_MaxValue_24() { return &___m_MaxValue_24; }
	inline void set_m_MaxValue_24(float value)
	{
		___m_MaxValue_24 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_25() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_WholeNumbers_25)); }
	inline bool get_m_WholeNumbers_25() const { return ___m_WholeNumbers_25; }
	inline bool* get_address_of_m_WholeNumbers_25() { return &___m_WholeNumbers_25; }
	inline void set_m_WholeNumbers_25(bool value)
	{
		___m_WholeNumbers_25 = value;
	}

	inline static int32_t get_offset_of_m_Value_26() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Value_26)); }
	inline float get_m_Value_26() const { return ___m_Value_26; }
	inline float* get_address_of_m_Value_26() { return &___m_Value_26; }
	inline void set_m_Value_26(float value)
	{
		___m_Value_26 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_OnValueChanged_27)); }
	inline SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillImage_28() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillImage_28)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_FillImage_28() const { return ___m_FillImage_28; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_FillImage_28() { return &___m_FillImage_28; }
	inline void set_m_FillImage_28(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_FillImage_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillImage_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillTransform_29() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillTransform_29)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_FillTransform_29() const { return ___m_FillTransform_29; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_FillTransform_29() { return &___m_FillTransform_29; }
	inline void set_m_FillTransform_29(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_FillTransform_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillTransform_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_FillContainerRect_30)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_FillContainerRect_30() const { return ___m_FillContainerRect_30; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_FillContainerRect_30() { return &___m_FillContainerRect_30; }
	inline void set_m_FillContainerRect_30(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_FillContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillContainerRect_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_31() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleTransform_31)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_HandleTransform_31() const { return ___m_HandleTransform_31; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_HandleTransform_31() { return &___m_HandleTransform_31; }
	inline void set_m_HandleTransform_31(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_HandleTransform_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleTransform_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_32() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_HandleContainerRect_32)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleContainerRect_32() const { return ___m_HandleContainerRect_32; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleContainerRect_32() { return &___m_HandleContainerRect_32; }
	inline void set_m_HandleContainerRect_32(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleContainerRect_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleContainerRect_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_33() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Offset_33)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Offset_33() const { return ___m_Offset_33; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Offset_33() { return &___m_Offset_33; }
	inline void set_m_Offset_33(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Offset_33 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_34() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_Tracker_34)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_34() const { return ___m_Tracker_34; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_34() { return &___m_Tracker_34; }
	inline void set_m_Tracker_34(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_34 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_35() { return static_cast<int32_t>(offsetof(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09, ___m_DelayedUpdateVisuals_35)); }
	inline bool get_m_DelayedUpdateVisuals_35() const { return ___m_DelayedUpdateVisuals_35; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_35() { return &___m_DelayedUpdateVisuals_35; }
	inline void set_m_DelayedUpdateVisuals_35(bool value)
	{
		___m_DelayedUpdateVisuals_35 = value;
	}
};


// UnityEngine.UI.Toggle
struct  Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Toggle_ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_20;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___graphic_21;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 * ___m_Group_22;
	// UnityEngine.UI.Toggle_ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 * ___onValueChanged_23;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_24;

public:
	inline static int32_t get_offset_of_toggleTransition_20() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___toggleTransition_20)); }
	inline int32_t get_toggleTransition_20() const { return ___toggleTransition_20; }
	inline int32_t* get_address_of_toggleTransition_20() { return &___toggleTransition_20; }
	inline void set_toggleTransition_20(int32_t value)
	{
		___toggleTransition_20 = value;
	}

	inline static int32_t get_offset_of_graphic_21() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___graphic_21)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_graphic_21() const { return ___graphic_21; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_graphic_21() { return &___graphic_21; }
	inline void set_graphic_21(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___graphic_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graphic_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Group_22() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___m_Group_22)); }
	inline ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 * get_m_Group_22() const { return ___m_Group_22; }
	inline ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 ** get_address_of_m_Group_22() { return &___m_Group_22; }
	inline void set_m_Group_22(ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786 * value)
	{
		___m_Group_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Group_22), (void*)value);
	}

	inline static int32_t get_offset_of_onValueChanged_23() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___onValueChanged_23)); }
	inline ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 * get_onValueChanged_23() const { return ___onValueChanged_23; }
	inline ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 ** get_address_of_onValueChanged_23() { return &___onValueChanged_23; }
	inline void set_onValueChanged_23(ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43 * value)
	{
		___onValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onValueChanged_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsOn_24() { return static_cast<int32_t>(offsetof(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106, ___m_IsOn_24)); }
	inline bool get_m_IsOn_24() const { return ___m_IsOn_24; }
	inline bool* get_address_of_m_IsOn_24() { return &___m_IsOn_24; }
	inline void set_m_IsOn_24(bool value)
	{
		___m_IsOn_24 = value;
	}
};


// UnityEngine.EventSystems.Physics2DRaycaster
struct  Physics2DRaycaster_t5D190F0825AA5F9E76892B852D6A5437D9981972  : public PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C
{
public:
	// UnityEngine.RaycastHit2D[] UnityEngine.EventSystems.Physics2DRaycaster::m_Hits
	RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165* ___m_Hits_11;

public:
	inline static int32_t get_offset_of_m_Hits_11() { return static_cast<int32_t>(offsetof(Physics2DRaycaster_t5D190F0825AA5F9E76892B852D6A5437D9981972, ___m_Hits_11)); }
	inline RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165* get_m_Hits_11() const { return ___m_Hits_11; }
	inline RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165** get_address_of_m_Hits_11() { return &___m_Hits_11; }
	inline void set_m_Hits_11(RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165* value)
	{
		___m_Hits_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Hits_11), (void*)value);
	}
};


// UnityEngine.EventSystems.StandaloneInputModule
struct  StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5  : public PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C
{
public:
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMoveVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_LastMoveVector_17;
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMousePosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_LastMousePosition_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_MousePosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_MousePosition_20;
	// UnityEngine.GameObject UnityEngine.EventSystems.StandaloneInputModule::m_CurrentFocusedGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_CurrentFocusedGameObject_21;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.StandaloneInputModule::m_InputPointerEvent
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___m_InputPointerEvent_22;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_23;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_24;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_25;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_26;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_27;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_28;
	// System.Boolean UnityEngine.EventSystems.StandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_29;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_16() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_PrevActionTime_16)); }
	inline float get_m_PrevActionTime_16() const { return ___m_PrevActionTime_16; }
	inline float* get_address_of_m_PrevActionTime_16() { return &___m_PrevActionTime_16; }
	inline void set_m_PrevActionTime_16(float value)
	{
		___m_PrevActionTime_16 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_17() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_LastMoveVector_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_LastMoveVector_17() const { return ___m_LastMoveVector_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_LastMoveVector_17() { return &___m_LastMoveVector_17; }
	inline void set_m_LastMoveVector_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_LastMoveVector_17 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_18() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_ConsecutiveMoveCount_18)); }
	inline int32_t get_m_ConsecutiveMoveCount_18() const { return ___m_ConsecutiveMoveCount_18; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_18() { return &___m_ConsecutiveMoveCount_18; }
	inline void set_m_ConsecutiveMoveCount_18(int32_t value)
	{
		___m_ConsecutiveMoveCount_18 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_19() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_LastMousePosition_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_LastMousePosition_19() const { return ___m_LastMousePosition_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_LastMousePosition_19() { return &___m_LastMousePosition_19; }
	inline void set_m_LastMousePosition_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_LastMousePosition_19 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_20() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_MousePosition_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_MousePosition_20() const { return ___m_MousePosition_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_MousePosition_20() { return &___m_MousePosition_20; }
	inline void set_m_MousePosition_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_MousePosition_20 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFocusedGameObject_21() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_CurrentFocusedGameObject_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_CurrentFocusedGameObject_21() const { return ___m_CurrentFocusedGameObject_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_CurrentFocusedGameObject_21() { return &___m_CurrentFocusedGameObject_21; }
	inline void set_m_CurrentFocusedGameObject_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_CurrentFocusedGameObject_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentFocusedGameObject_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_22() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_InputPointerEvent_22)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_m_InputPointerEvent_22() const { return ___m_InputPointerEvent_22; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_m_InputPointerEvent_22() { return &___m_InputPointerEvent_22; }
	inline void set_m_InputPointerEvent_22(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___m_InputPointerEvent_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputPointerEvent_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_23() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_HorizontalAxis_23)); }
	inline String_t* get_m_HorizontalAxis_23() const { return ___m_HorizontalAxis_23; }
	inline String_t** get_address_of_m_HorizontalAxis_23() { return &___m_HorizontalAxis_23; }
	inline void set_m_HorizontalAxis_23(String_t* value)
	{
		___m_HorizontalAxis_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalAxis_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_24() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_VerticalAxis_24)); }
	inline String_t* get_m_VerticalAxis_24() const { return ___m_VerticalAxis_24; }
	inline String_t** get_address_of_m_VerticalAxis_24() { return &___m_VerticalAxis_24; }
	inline void set_m_VerticalAxis_24(String_t* value)
	{
		___m_VerticalAxis_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalAxis_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_25() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_SubmitButton_25)); }
	inline String_t* get_m_SubmitButton_25() const { return ___m_SubmitButton_25; }
	inline String_t** get_address_of_m_SubmitButton_25() { return &___m_SubmitButton_25; }
	inline void set_m_SubmitButton_25(String_t* value)
	{
		___m_SubmitButton_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SubmitButton_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_CancelButton_26() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_CancelButton_26)); }
	inline String_t* get_m_CancelButton_26() const { return ___m_CancelButton_26; }
	inline String_t** get_address_of_m_CancelButton_26() { return &___m_CancelButton_26; }
	inline void set_m_CancelButton_26(String_t* value)
	{
		___m_CancelButton_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CancelButton_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_27() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_InputActionsPerSecond_27)); }
	inline float get_m_InputActionsPerSecond_27() const { return ___m_InputActionsPerSecond_27; }
	inline float* get_address_of_m_InputActionsPerSecond_27() { return &___m_InputActionsPerSecond_27; }
	inline void set_m_InputActionsPerSecond_27(float value)
	{
		___m_InputActionsPerSecond_27 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_28() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_RepeatDelay_28)); }
	inline float get_m_RepeatDelay_28() const { return ___m_RepeatDelay_28; }
	inline float* get_address_of_m_RepeatDelay_28() { return &___m_RepeatDelay_28; }
	inline void set_m_RepeatDelay_28(float value)
	{
		___m_RepeatDelay_28 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_29() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5, ___m_ForceModuleActive_29)); }
	inline bool get_m_ForceModuleActive_29() const { return ___m_ForceModuleActive_29; }
	inline bool* get_address_of_m_ForceModuleActive_29() { return &___m_ForceModuleActive_29; }
	inline void set_m_ForceModuleActive_29(bool value)
	{
		___m_ForceModuleActive_29 = value;
	}
};


// UnityEngine.EventSystems.TouchInputModule
struct  TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945  : public PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_LastMousePosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_LastMousePosition_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_MousePosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_MousePosition_17;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.TouchInputModule::m_InputPointerEvent
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___m_InputPointerEvent_18;
	// System.Boolean UnityEngine.EventSystems.TouchInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_19;

public:
	inline static int32_t get_offset_of_m_LastMousePosition_16() { return static_cast<int32_t>(offsetof(TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945, ___m_LastMousePosition_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_LastMousePosition_16() const { return ___m_LastMousePosition_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_LastMousePosition_16() { return &___m_LastMousePosition_16; }
	inline void set_m_LastMousePosition_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_LastMousePosition_16 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_17() { return static_cast<int32_t>(offsetof(TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945, ___m_MousePosition_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_MousePosition_17() const { return ___m_MousePosition_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_MousePosition_17() { return &___m_MousePosition_17; }
	inline void set_m_MousePosition_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_MousePosition_17 = value;
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_18() { return static_cast<int32_t>(offsetof(TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945, ___m_InputPointerEvent_18)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_m_InputPointerEvent_18() const { return ___m_InputPointerEvent_18; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_m_InputPointerEvent_18() { return &___m_InputPointerEvent_18; }
	inline void set_m_InputPointerEvent_18(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___m_InputPointerEvent_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputPointerEvent_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_19() { return static_cast<int32_t>(offsetof(TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945, ___m_ForceModuleActive_19)); }
	inline bool get_m_ForceModuleActive_19() const { return ___m_ForceModuleActive_19; }
	inline bool* get_address_of_m_ForceModuleActive_19() { return &___m_ForceModuleActive_19; }
	inline void set_m_ForceModuleActive_19(bool value)
	{
		___m_ForceModuleActive_19 = value;
	}
};


// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};


// UnityEngine.UI.RawImage
struct  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Texture_34;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_UVRect_35;

public:
	inline static int32_t get_offset_of_m_Texture_34() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_Texture_34)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Texture_34() const { return ___m_Texture_34; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Texture_34() { return &___m_Texture_34; }
	inline void set_m_Texture_34(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Texture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_35() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_UVRect_35)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_UVRect_35() const { return ___m_UVRect_35; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_UVRect_35() { return &___m_UVRect_35; }
	inline void set_m_UVRect_35(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_UVRect_35 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_34;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_35;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_36;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_37;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_39;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_40;

public:
	inline static int32_t get_offset_of_m_FontData_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_34)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_34() const { return ___m_FontData_34; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_34() { return &___m_FontData_34; }
	inline void set_m_FontData_34(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_35)); }
	inline String_t* get_m_Text_35() const { return ___m_Text_35; }
	inline String_t** get_address_of_m_Text_35() { return &___m_Text_35; }
	inline void set_m_Text_35(String_t* value)
	{
		___m_Text_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_36)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_36() const { return ___m_TextCache_36; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_36() { return &___m_TextCache_36; }
	inline void set_m_TextCache_36(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_37() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_37)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_37() const { return ___m_TextCacheForLayout_37; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_37() { return &___m_TextCacheForLayout_37; }
	inline void set_m_TextCacheForLayout_37(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_39() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_39)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_39() const { return ___m_DisableFontTextureRebuiltCallback_39; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_39() { return &___m_DisableFontTextureRebuiltCallback_39; }
	inline void set_m_DisableFontTextureRebuiltCallback_39(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_39 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_40() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_40)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_40() const { return ___m_TempVerts_40; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_40() { return &___m_TempVerts_40; }
	inline void set_m_TempVerts_40(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_40), (void*)value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_38;

public:
	inline static int32_t get_offset_of_s_DefaultText_38() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_38)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_38() const { return ___s_DefaultText_38; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_38() { return &___s_DefaultText_38; }
	inline void set_s_DefaultText_38(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_38), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4023[9] = 
{
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_ShouldRecalculateStencil_25(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_MaskMaterial_26(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_ParentMask_27(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_Maskable_28(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_IncludeForMasking_29(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_OnCullStateChanged_30(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_ShouldRecalculate_31(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_StencilValue_32(),
	MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F::get_offset_of_m_Corners_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (MaskUtilities_t28395C0AF1B83B3A798D76DC69B012BB303D9683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (Misc_t87057804A6479127307E42B6C83A4F3244521315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4028[5] = 
{
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4029[6] = 
{
	Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4030[2] = 
{
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8::get_offset_of_m_Texture_34(),
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8::get_offset_of_m_UVRect_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4031[10] = 
{
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_VertexClipper_4(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_RectTransform_5(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_MaskableTargets_6(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_ClipTargets_7(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_ShouldRecalculateClipRects_8(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_Clippers_9(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_LastClipRectCanvasSpace_10(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_ForceClip_11(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_Canvas_12(),
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B::get_offset_of_m_Corners_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4032[12] = 
{
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_HandleRect_20(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_Direction_21(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_Value_22(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_Size_23(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_NumberOfSteps_24(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_OnValueChanged_25(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_ContainerRect_26(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_Offset_27(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_Tracker_28(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_PointerDownRepeat_29(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_isPointerDownAndNotDragging_30(),
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389::get_offset_of_m_DelayedUpdateVisuals_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (Direction_t7DC57FCC1DB6C12E88B2227EEEE2FCEF3F1483FF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4033[5] = 
{
	Direction_t7DC57FCC1DB6C12E88B2227EEEE2FCEF3F1483FF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (ScrollEvent_t07B0FA266C69E36437A0083D5058B2952D151FF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (Axis_t5CC6D92E75113BD2F2816AFC44EF728126921DF7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4035[3] = 
{
	Axis_t5CC6D92E75113BD2F2816AFC44EF728126921DF7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4036[4] = 
{
	U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074::get_offset_of_U3CU3E1__state_0(),
	U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074::get_offset_of_U3CU3E2__current_1(),
	U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074::get_offset_of_U3CU3E4__this_2(),
	U3CClickRepeatU3Ed__57_t8AEFCC130793890A19566D24B454C47B2705B074::get_offset_of_eventData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4037[37] = 
{
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Content_4(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Horizontal_5(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Vertical_6(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_MovementType_7(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Elasticity_8(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Inertia_9(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_DecelerationRate_10(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_ScrollSensitivity_11(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Viewport_12(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HorizontalScrollbar_13(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_VerticalScrollbar_14(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HorizontalScrollbarVisibility_15(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_VerticalScrollbarVisibility_16(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HorizontalScrollbarSpacing_17(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_VerticalScrollbarSpacing_18(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_OnValueChanged_19(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_PointerStartLocalCursor_20(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_ContentStartPosition_21(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_ViewRect_22(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_ContentBounds_23(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_ViewBounds_24(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Velocity_25(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Dragging_26(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Scrolling_27(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_PrevPosition_28(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_PrevContentBounds_29(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_PrevViewBounds_30(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HasRebuiltLayout_31(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HSliderExpand_32(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_VSliderExpand_33(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HSliderHeight_34(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_VSliderWidth_35(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Rect_36(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_HorizontalScrollbarRect_37(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_VerticalScrollbarRect_38(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Tracker_39(),
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51::get_offset_of_m_Corners_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (MovementType_t78F2436465C40CA3C70631E1E5F088EA7A15C97A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4038[4] = 
{
	MovementType_t78F2436465C40CA3C70631E1E5F088EA7A15C97A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (ScrollbarVisibility_t4D6A5D8EF1681A91CED9F04283D0C882DCE1531F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4039[4] = 
{
	ScrollbarVisibility_t4D6A5D8EF1681A91CED9F04283D0C882DCE1531F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A), -1, sizeof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4041[16] = 
{
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields::get_offset_of_s_Selectables_4(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields::get_offset_of_s_SelectableCount_5(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields::get_offset_of_s_IsDirty_6(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_Navigation_7(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_Transition_8(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_Colors_9(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_SpriteState_10(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_AnimationTriggers_11(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_Interactable_12(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_TargetGraphic_13(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_GroupsAllowInteraction_14(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_WillRemove_15(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_U3CisPointerInsideU3Ek__BackingField_16(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_U3CisPointerDownU3Ek__BackingField_17(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_U3ChasSelectionU3Ek__BackingField_18(),
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A::get_offset_of_m_CanvasGroupCache_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (Transition_tA9261C608B54C52324084A0B080E7A3E0548A181)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4042[5] = 
{
	Transition_tA9261C608B54C52324084A0B080E7A3E0548A181::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (SelectionState_tF089B96B46A592693753CBF23C52A3887632D210)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4043[6] = 
{
	SelectionState_tF089B96B46A592693753CBF23C52A3887632D210::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (SetPropertyUtility_t20B3FC057E91FD49F7F71279C2DFAAD263E32DEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4045[16] = 
{
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_FillRect_20(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_HandleRect_21(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_Direction_22(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_MinValue_23(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_MaxValue_24(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_WholeNumbers_25(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_Value_26(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_OnValueChanged_27(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_FillImage_28(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_FillTransform_29(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_FillContainerRect_30(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_HandleTransform_31(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_HandleContainerRect_32(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_Offset_33(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_Tracker_34(),
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09::get_offset_of_m_DelayedUpdateVisuals_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4046[5] = 
{
	Direction_tAAEBCB52D43F1B8F5DBB1A6F1025F9D02852B67E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (SliderEvent_t64A824F56F80FC8E2F233F0A0FB0821702DF416C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (Axis_t5D4CE8029AAE120D6F7C8AC3FE1B1F46B2623830)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4048[3] = 
{
	Axis_t5D4CE8029AAE120D6F7C8AC3FE1B1F46B2623830::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4049[4] = 
{
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A::get_offset_of_m_SelectedSprite_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A::get_offset_of_m_DisabledSprite_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (StencilMaterial_t1DC5D1CDE53AF67E74925AC2F4083FD29810D974), -1, sizeof(StencilMaterial_t1DC5D1CDE53AF67E74925AC2F4083FD29810D974_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4050[1] = 
{
	StencilMaterial_t1DC5D1CDE53AF67E74925AC2F4083FD29810D974_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4051[10] = 
{
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_baseMat_0(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_customMat_1(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_count_2(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_stencilId_3(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_operation_4(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_compareFunction_5(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_readMask_6(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_writeMask_7(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_useAlphaClip_8(),
	MatEntry_t6D4406239BE26E2ED3F441944F6A047913DB6701::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030), -1, sizeof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4052[7] = 
{
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030::get_offset_of_m_FontData_34(),
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030::get_offset_of_m_Text_35(),
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030::get_offset_of_m_TextCache_36(),
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030::get_offset_of_m_TextCacheForLayout_37(),
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields::get_offset_of_s_DefaultText_38(),
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030::get_offset_of_m_DisableFontTextureRebuiltCallback_39(),
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030::get_offset_of_m_TempVerts_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4053[5] = 
{
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106::get_offset_of_toggleTransition_20(),
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106::get_offset_of_graphic_21(),
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106::get_offset_of_m_Group_22(),
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106::get_offset_of_onValueChanged_23(),
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106::get_offset_of_m_IsOn_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (ToggleTransition_t45980EB1352FF47B2D8D8EBC90385AB68939046D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4054[3] = 
{
	ToggleTransition_t45980EB1352FF47B2D8D8EBC90385AB68939046D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (ToggleEvent_t50D925F8E220FB47DA738411CEF9C57FF7E1DC43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4056[2] = 
{
	ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786::get_offset_of_m_AllowSwitchOff_4(),
	ToggleGroup_t11E2B254D3C968C7D0DA11C606CC06D7D7F0D786::get_offset_of_m_Toggles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E), -1, sizeof(U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4057[3] = 
{
	U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_t97EB3105DF6005EBB830B751CB253A590768A27E_StaticFields::get_offset_of_U3CU3E9__13_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4058[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4059[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4060[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4067[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4069[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4074[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4075[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4077[6] = 
{
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4078[4] = 
{
	ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4080[5] = 
{
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4082[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4083[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4084[2] = 
{
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4085[1] = 
{
	AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4086[1] = 
{
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4087[21] = 
{
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_m_PointerPress_3(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_hovered_9(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4088[4] = 
{
	InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4089[5] = 
{
	FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4090[3] = 
{
	EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77), -1, sizeof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4109[11] = 
{
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_SystemInputModules_4(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_CurrentInputModule_5(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_m_EventSystems_6(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_FirstSelected_7(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_sendNavigationEvents_8(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_DragThreshold_9(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_CurrentSelected_10(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_HasFocus_11(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_SelectionGuard_12(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_DummyData_13(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_s_RaycastComparer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4110[1] = 
{
	EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298::get_offset_of_m_Delegates_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { sizeof (TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4112[2] = 
{
	Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E::get_offset_of_eventID_0(),
	Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4113[18] = 
{
	EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985), -1, sizeof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4114[19] = 
{
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_InternalTransformList_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C), -1, sizeof(U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4116[1] = 
{
	U3CU3Ec_t91DA84DB86FD18E664B2FBDACCB1B7A5E2A0849C_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { sizeof (BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4118[6] = 
{
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_RaycastResultCache_4(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_AxisEventData_5(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_EventSystem_6(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_BaseEventData_7(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_InputOverride_8(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_DefaultInput_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { sizeof (PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4119[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C::get_offset_of_m_PointerData_14(),
	PointerInputModule_tE8CB9BDC38DAF3162843E22541093DADDE1BB19C::get_offset_of_m_MouseState_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (ButtonState_tCF0544E1131CD058FABBEE56FA1D0A4716A17F9D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4120[2] = 
{
	ButtonState_tCF0544E1131CD058FABBEE56FA1D0A4716A17F9D::get_offset_of_m_Button_0(),
	ButtonState_tCF0544E1131CD058FABBEE56FA1D0A4716A17F9D::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { sizeof (MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4121[1] = 
{
	MouseState_t4D6249AEF3F24542B7F13D49020EC1B8DC2F05D7::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { sizeof (MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4122[2] = 
{
	MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988::get_offset_of_buttonState_0(),
	MouseButtonEventData_tDD4D7A2BEE7C4674ADFD921AB2323FBFF7317988::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4123[14] = 
{
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_PrevActionTime_16(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_LastMoveVector_17(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_ConsecutiveMoveCount_18(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_LastMousePosition_19(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_MousePosition_20(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_CurrentFocusedGameObject_21(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_InputPointerEvent_22(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_HorizontalAxis_23(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_VerticalAxis_24(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_SubmitButton_25(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_CancelButton_26(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_InputActionsPerSecond_27(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_RepeatDelay_28(),
	StandaloneInputModule_tF3BDE3C0D374D1A0C87654254FA5E74F6B8C1EF5::get_offset_of_m_ForceModuleActive_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (InputMode_t6C81C4F84B743FC877C53380040470BE273BA79D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4124[3] = 
{
	InputMode_t6C81C4F84B743FC877C53380040470BE273BA79D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4125[4] = 
{
	TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945::get_offset_of_m_LastMousePosition_16(),
	TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945::get_offset_of_m_MousePosition_17(),
	TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945::get_offset_of_m_InputPointerEvent_18(),
	TouchInputModule_t9D8F03041D5F5C10102782C1FD3264794CF6F945::get_offset_of_m_ForceModuleActive_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4126[6] = 
{
	MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A), -1, sizeof(RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4127[1] = 
{
	RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4128[1] = 
{
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966::get_offset_of_m_RootRaycaster_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (Physics2DRaycaster_t5D190F0825AA5F9E76892B852D6A5437D9981972), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4129[1] = 
{
	Physics2DRaycaster_t5D190F0825AA5F9E76892B852D6A5437D9981972::get_offset_of_m_Hits_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { sizeof (PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4130[6] = 
{
	0,
	PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C::get_offset_of_m_EventCamera_6(),
	PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C::get_offset_of_m_EventMask_7(),
	PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C::get_offset_of_m_MaxRayIntersections_8(),
	PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C::get_offset_of_m_LastMaxRayIntersections_9(),
	PhysicsRaycaster_tA2270920B561715BFCB1BDF0D759889B5985826C::get_offset_of_m_Hits_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { sizeof (RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2), -1, sizeof(RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4131[1] = 
{
	RaycastHitComparer_t5465A53942AAC60F5716D514946F4AB6C263CFA2_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { sizeof (RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4132[10] = 
{
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { sizeof (UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4134[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (__StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4137[6] = 
{
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A::get_offset_of_feedbackType_4(),
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A::get_offset_of_scaleParent_5(),
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A::get_offset_of_bounceHeight_6(),
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A::get_offset_of_bounceDistance_7(),
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A::get_offset_of_squeezeSequence_8(),
	Bouncer_t269280960D444444322CB81F529ECAC25C6BF96A::get_offset_of_originalScale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB), -1, sizeof(CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4138[5] = 
{
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB_StaticFields::get_offset_of_currentCamera_5(),
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB::get_offset_of_cameraSpeed_6(),
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB::get_offset_of_offset_7(),
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB::get_offset_of_initalCameraPos_8(),
	CameraController_tC58777A506A1D4F1F1794E5D666E619C1DA073BB::get_offset_of_followTarget_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4139[3] = 
{
	Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59::get_offset_of_CanBeCollected_4(),
	Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59::get_offset_of_meshRenderer_5(),
	Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59::get_offset_of_particles_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (CoinSpawn_t2DF1091628ECF009A19AD074E884F22370447DED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4141[2] = 
{
	EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD::get_offset_of_particleSystems_4(),
	EndPlatform_t2B433EBCB22812CF2F1CA3F48C6EBD9CC23B38DD::get_offset_of_fireDelay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { sizeof (U3CU3Ec__DisplayClass2_0_t664CF92BE2A9F7882339520E787A188AA72FFC14), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4142[2] = 
{
	U3CU3Ec__DisplayClass2_0_t664CF92BE2A9F7882339520E787A188AA72FFC14::get_offset_of_unref_0(),
	U3CU3Ec__DisplayClass2_0_t664CF92BE2A9F7882339520E787A188AA72FFC14::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { sizeof (Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF), -1, sizeof(Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4143[4] = 
{
	Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields::get_offset_of_unityPlayer_0(),
	Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields::get_offset_of_currentActivity_1(),
	Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields::get_offset_of_vibrator_2(),
	Feedback_tCCF39051393D6F77DC47A069C3DA05736E0FBEEF_StaticFields::get_offset_of_androidFeedbacks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { sizeof (GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4144[31] = 
{
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_startPlatformPrefab_5(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_endPlatformPrefab_6(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_platformPrefabs_7(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_slimePrefab_8(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_coinPrefab_9(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_hoopPrefab_10(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_touchSensitivity_11(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_touchSensitivityMobile_12(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_availableSettings_13(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_groundSharedMaterial_14(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_hoopsSharedMaterial_15(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_coinSpawnChance_16(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_swipeThreshold_17(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_platformsRoot_18(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_gameSave_19(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_levelCoins_20(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_levelHoops_21(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_levelPlatforms_22(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_currentSlime_23(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_lastPos_24(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_fogDensityTween_25(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_timeScaleTween_26(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_currentLevelSettings_27(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_endPlatform_28(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_isGameOver_29(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_isGameStarted_30(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_isEndReached_31(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_isInTutorial_32(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_maxJump_33(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_currentJump_34(),
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3::get_offset_of_startFogDensity_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { sizeof (U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928), -1, sizeof(U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4145[3] = 
{
	U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields::get_offset_of_U3CU3E9__51_0_1(),
	U3CU3Ec_t05E039AEB268866B45BF37EDE5229C4D310CD928_StaticFields::get_offset_of_U3CU3E9__52_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { sizeof (GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4146[4] = 
{
	GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8::get_offset_of_currentLevel_0(),
	GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8::get_offset_of_coins_1(),
	GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8::get_offset_of_tutorialSeen_2(),
	GameSave_t5FC5C1B1B5DE425A0BCC4A3712390E713E25D1E8::get_offset_of_currentLevelSetting_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { sizeof (Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9), -1, sizeof(Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4147[6] = 
{
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9::get_offset_of_popupPrefab_4(),
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9::get_offset_of_particle_5(),
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9::get_offset_of_particle2_6(),
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9::get_offset_of_activated_7(),
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9_StaticFields::get_offset_of_rewardCombo_8(),
	Hoop_t023EC391368C9A149962227F4AC6D83DDF0E2ED9::get_offset_of_rotateTween_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { sizeof (U3CU3Ec__DisplayClass9_0_t57ACCCEDEA84808D0BDDE4C7D54AA49E19430A59), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4148[1] = 
{
	U3CU3Ec__DisplayClass9_0_t57ACCCEDEA84808D0BDDE4C7D54AA49E19430A59::get_offset_of_newPopup_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { sizeof (HoopsSpawner_tDB8A7B4D583F9D023EFD65E42D9DAAF5AADE3B18), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4149[2] = 
{
	HoopsSpawner_tDB8A7B4D583F9D023EFD65E42D9DAAF5AADE3B18::get_offset_of_connectedBouncer_4(),
	HoopsSpawner_tDB8A7B4D583F9D023EFD65E42D9DAAF5AADE3B18::get_offset_of_spawnChance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4150[8] = 
{
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_OnTouchBegan_5(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_OnTouchUpdate_6(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_OnTouchEnded_7(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_lastTouchPosition_8(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_currentTouchPos_9(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_touchesInitialPosition_10(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_isTouching_11(),
	InputManager_t208BBEDF18F7B320FCB945FC0F5D757BEBC45ACE::get_offset_of_touchDuration_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (TouchDelegate_tD22A35BAFED3D989E2B2C56C7955A955F0A14322), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (JSONUtils_t5A3C370C1B54A62088180AB38FE12D8AAB21616D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4153[3] = 
{
	LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773::get_offset_of_hoopsColor_4(),
	LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773::get_offset_of_groundColor_5(),
	LevelSetting_tF85BD5E3B967708BFCFAA687FF5F7662AB62F773::get_offset_of_groundTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (Platform_t32A27B4C009E21E1D2383495043A2E49A919F337), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4154[3] = 
{
	Platform_t32A27B4C009E21E1D2383495043A2E49A919F337::get_offset_of_marbleSpawnPos_4(),
	Platform_t32A27B4C009E21E1D2383495043A2E49A919F337::get_offset_of_Length_5(),
	Platform_t32A27B4C009E21E1D2383495043A2E49A919F337::get_offset_of_angleID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4155[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4156[25] = 
{
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_cubeScaleParent_4(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_cubeRotateParent_5(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_ballRigidbody_6(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_trailRenderer_7(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_particles_8(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_landingDuration_9(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_hopHeight_10(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_hopDistance_11(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_finalSlimeRot_12(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_originalPosition_13(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_stickOffset_14(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_jumpTween_15(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_scaleBounceTween_16(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_slimeRotateTween_17(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_moveTween_18(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_peakScale_19(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_landScale_20(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_isJumping_21(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_isSticked_22(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_isMovingLateraly_23(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_isTransitioning_24(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_inputApplied_25(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_stickDirection_26(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_initialLateralPosition_27(),
	SlimeController_tECB9A5A405A3E9CA868B39162654C4EA588AC421::get_offset_of_diffLatPos_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { sizeof (U3CU3Ec__DisplayClass43_0_t057190362D7BBE749EBD4B3F6325A76A58995F51), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4157[2] = 
{
	U3CU3Ec__DisplayClass43_0_t057190362D7BBE749EBD4B3F6325A76A58995F51::get_offset_of_checkGameOver_0(),
	U3CU3Ec__DisplayClass43_0_t057190362D7BBE749EBD4B3F6325A76A58995F51::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4158[11] = 
{
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_mainMenuScreen_5(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_ingameScreen_6(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_retryScreen_7(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_victoryScreen_8(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_tutoDashPanel_9(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_levelProgression_10(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_levelText_11(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_coinText_12(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_completionText_13(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_maxJumpText_14(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_jumpText_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4159[8] = 
{
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_header_4(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_footer_5(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_headerOriginalPos_6(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_footerOriginalPos_7(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_headerTransform_8(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_footerTransform_9(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_headerTween_10(),
	UIScreen_tFD2053C8DCC81382846B4D2D1102A726880E5557::get_offset_of_footerTween_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898), -1, sizeof(iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4160[5] = 
{
	iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898_StaticFields::get_offset_of__instance_4(),
	iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898::get_offset_of_usedFeedbackTypes_5(),
	iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898::get_offset_of_feedbackGeneratorsSetUp_6(),
	iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898::get_offset_of_debug_7(),
	iOSHapticFeedback_t536EE8C20B3D3B93F6A00A739E97AF64BF857898::get_offset_of__isEnabled_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4161[7] = 
{
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_SelectionChange_0(),
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_ImpactLight_1(),
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_ImpactMedium_2(),
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_ImpactHeavy_3(),
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_NotificationSuccess_4(),
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_NotificationWarning_5(),
	iOSFeedbackTypeSettings_t888214791606A42E3FE0C0F3E3B3DCFEF9CF86D1::get_offset_of_NotificationFailure_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (iOSFeedbackType_tCBEE4B633688224F2D9773B2AB0AE54C55D73A27)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4162[9] = 
{
	iOSFeedbackType_tCBEE4B633688224F2D9773B2AB0AE54C55D73A27::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (iOSHapticFeedbackAdvanced_tEFFB9D9CABFE2130E9DFA20CD1C41E25E1D8728B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (iOSHapticFeedbackExample_t53A765AEC3D85A09166BCE723079D28A37238970), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4164[1] = 
{
	iOSHapticFeedbackExample_t53A765AEC3D85A09166BCE723079D28A37238970::get_offset_of_supported_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (DOTweenModuleAudio_t769A1BF9E3697BFACC9815179E7A39085E56CE4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { sizeof (U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4166[1] = 
{
	U3CU3Ec__DisplayClass0_0_t72C207EC4FC022EB09F6F8D33004BEC6FBC66E6F::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { sizeof (U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4167[1] = 
{
	U3CU3Ec__DisplayClass1_0_t4A743D54E80E5072E25F38A09BE27414B88BC2DD::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4168[2] = 
{
	U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass2_0_t9C60BFCCE659FB7698FF542E41008F990B147ACC::get_offset_of_floatName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (DOTweenModulePhysics_t54AF484E9A4CEC236EE03303DDE8634FC383938E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { sizeof (U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4170[1] = 
{
	U3CU3Ec__DisplayClass0_0_t5C64C7513BD29248DFA2579BC02E0EB936F7BFE3::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { sizeof (U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4171[1] = 
{
	U3CU3Ec__DisplayClass1_0_tA56D94394389743477D8477AC85120BFC5536CC3::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { sizeof (U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4172[1] = 
{
	U3CU3Ec__DisplayClass2_0_t0C665D49874F06B1EA3D9C3145443B48B75733FF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { sizeof (U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4173[1] = 
{
	U3CU3Ec__DisplayClass3_0_tFD9A3293995CC5CFCD9996BB2B9DBECE1F2853EB::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4174[1] = 
{
	U3CU3Ec__DisplayClass4_0_t95C132B298C8FED300DCB1A2CF98CCC5CFBA9AE3::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4175[1] = 
{
	U3CU3Ec__DisplayClass5_0_t41A0920837839D6F1EBED888FCA9A88B12AC0F18::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { sizeof (U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4176[7] = 
{
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_startPosY_1(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_offsetYSet_2(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_offsetY_3(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_s_4(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_endValue_5(),
	U3CU3Ec__DisplayClass6_0_t43377B7F5E091119F288FC2F606BD0CCF9764CD5::get_offset_of_yTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { sizeof (U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4177[1] = 
{
	U3CU3Ec__DisplayClass7_0_t08A1ECA62C440E40725FC57E64C7EB336CECCC84::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { sizeof (U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4178[2] = 
{
	U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass8_0_tB9DE4506C721C59BC5C6FAC15C9250AA5BCAE2C1::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4179[1] = 
{
	U3CU3Ec__DisplayClass9_0_t96637B7BE8971D6AE57F08CD85C9BFF8E11C2B6F::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4180[2] = 
{
	U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass10_0_t41907D10A4F8492809D26E4C4B12EEF791CD791F::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (DOTweenModulePhysics2D_t31D337CBA76C3DFD0CF747008022FBBAB46343BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4182[1] = 
{
	U3CU3Ec__DisplayClass0_0_tAE598412087F01228B45541B5C42672EA61EC671::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4183[1] = 
{
	U3CU3Ec__DisplayClass1_0_t4A0577007A5B036CAFBBD362531FDA652965EF09::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4184[1] = 
{
	U3CU3Ec__DisplayClass2_0_t2674AB8293A4F3119319017A1FFF86A555F234A7::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4185[1] = 
{
	U3CU3Ec__DisplayClass3_0_t6012E00DCCD7C59473E11E70423E98FFAEAB126F::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4186[7] = 
{
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_startPosY_1(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_offsetYSet_2(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_offsetY_3(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_s_4(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_endValue_5(),
	U3CU3Ec__DisplayClass4_0_t7F406E4E06939C6DCA0E180DA0A06F4D928893A7::get_offset_of_yTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (DOTweenModuleSprite_tA07D646AA9234481CF1BA55D046BC7F824A818F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4188[1] = 
{
	U3CU3Ec__DisplayClass0_0_tF979E38F1CE3544D9E454AE0AF7ECB621C359998::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4189[1] = 
{
	U3CU3Ec__DisplayClass1_0_t3C1F8A4FCB252FA03523157E817DEDAD87EBA1C7::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { sizeof (U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4190[2] = 
{
	U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass3_0_t23377DB23DB8225608440DA6A6E4D7E8A7DDDE5B::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (DOTweenModuleUI_t39E58130742D7CFD84DE6B0EA1DC7ED3C0A839D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (Utils_tF7D730835163762D9317B6FB65E30C704BD3BF7F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4193[1] = 
{
	U3CU3Ec__DisplayClass0_0_t1CD861E6D228C2C19A057642CB25BC00977708E7::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4194[1] = 
{
	U3CU3Ec__DisplayClass1_0_t551B53D6928422D334300FB5FE36087E17A7199A::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195 = { sizeof (U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4195[1] = 
{
	U3CU3Ec__DisplayClass2_0_tE5F3CF3169FC68264B35D20D81204FFF0F52D2CF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196 = { sizeof (U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4196[1] = 
{
	U3CU3Ec__DisplayClass3_0_t8723E84900FA09817B34237CBECC97C7A463F288::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197 = { sizeof (U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4197[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3D7A8AD757769FD8E3E12FE825921D885FBAC98E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198 = { sizeof (U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4198[1] = 
{
	U3CU3Ec__DisplayClass5_0_t89CE27455F60C162B98656F385CDF2F8C83B3813::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199 = { sizeof (U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4199[1] = 
{
	U3CU3Ec__DisplayClass7_0_t1997C2BD49954A2A3D92FD767A822B8F1B4B95DB::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4200[1] = 
{
	U3CU3Ec__DisplayClass8_0_t4DCDDE3297E2F252B0DF9F420BC4BB15DA0F9A05::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4201[1] = 
{
	U3CU3Ec__DisplayClass9_0_tDEEEB22115E6297DC859D85A3AE0A653EE94057A::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4202[1] = 
{
	U3CU3Ec__DisplayClass10_0_tBF413496555AC2A272175AA8755DF59A98BCA0FA::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4203[1] = 
{
	U3CU3Ec__DisplayClass11_0_t8E94FE5CFCD1448F6DB9CD8458A96BE1E74A4647::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4204[1] = 
{
	U3CU3Ec__DisplayClass12_0_t790D7F2EB69070025BFD0B937F282076759B4A4B::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4205[1] = 
{
	U3CU3Ec__DisplayClass13_0_tDDBFBF8BD4E3C0A383A7F8EFAEAB4BC0B4E6C480::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4206[1] = 
{
	U3CU3Ec__DisplayClass14_0_t1B353457EF31AB21155411F660244D4145FCF128::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4207[1] = 
{
	U3CU3Ec__DisplayClass15_0_tCE5DA7ACE59AA075AE304A5A65C4BE6C21F076C4::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4208[1] = 
{
	U3CU3Ec__DisplayClass16_0_t706FEF5518F61907696FC3A310D73D37CD78B6A0::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4209[1] = 
{
	U3CU3Ec__DisplayClass17_0_tAE678224AB3EC50CFB472606215A68152D7F5AC1::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4210[1] = 
{
	U3CU3Ec__DisplayClass18_0_tA273C87FB6E798BAB978138FE380FF6FC9F4F138::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4211[1] = 
{
	U3CU3Ec__DisplayClass19_0_t1794A712D34E25EECD71BFB9CD23543FFC5F872E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4212[1] = 
{
	U3CU3Ec__DisplayClass20_0_t9F486EB363F7A1C17B3BD86CAA664C05A5C6A79E::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4213[1] = 
{
	U3CU3Ec__DisplayClass21_0_tCDC578ECDF8FE00993AE0D17FF67093DD95E9D57::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4214[1] = 
{
	U3CU3Ec__DisplayClass22_0_t81A251AB34D6CE62518C247A08DFFE3807D9E0AF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4215[1] = 
{
	U3CU3Ec__DisplayClass23_0_t45ED683E687C7262CE823E5F9A9DAEA89874C085::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4216[1] = 
{
	U3CU3Ec__DisplayClass24_0_t71F9474AA3C36F97F772BA59C25995156ECD66FC::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4217[1] = 
{
	U3CU3Ec__DisplayClass25_0_t975863A71A716224C6847B9A338C668DC6314E33::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4218[1] = 
{
	U3CU3Ec__DisplayClass26_0_t2A556C8F1165C48B48129CF97F4540C07A92C246::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4219[1] = 
{
	U3CU3Ec__DisplayClass27_0_t532D99F0F8DA6E0614982A36F088D1A92EE1A202::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4220[1] = 
{
	U3CU3Ec__DisplayClass28_0_t240F883CE61D32824C0A8AB45434C4814C8191B9::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4221[6] = 
{
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_startPosY_1(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_offsetYSet_2(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_offsetY_3(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_s_4(),
	U3CU3Ec__DisplayClass29_0_tC1CCB2D9235CC4DCAED9CB302BC4CB18F4A06417::get_offset_of_endValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4222[1] = 
{
	U3CU3Ec__DisplayClass30_0_t0EAE0CFBBE38A9125A218AEA8E7E3547FFD2E642::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4223[1] = 
{
	U3CU3Ec__DisplayClass31_0_tD33376E7AD1840C55C746AE041973A050EBC1E79::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4224[1] = 
{
	U3CU3Ec__DisplayClass32_0_tC0F866B0D751B9314266D288A9B0CAAF243044BF::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4225[1] = 
{
	U3CU3Ec__DisplayClass33_0_t842F7830A363757917266288415109DEB9756187::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4226[1] = 
{
	U3CU3Ec__DisplayClass34_0_t36ABF39E8C6A0425E8AE5433E6893AD82FC22C5D::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4227[1] = 
{
	U3CU3Ec__DisplayClass35_0_tD9FE4B782773B3477CCDDA2F594A59D8BECDC7D8::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4228[1] = 
{
	U3CU3Ec__DisplayClass36_0_t3AA43B8A77F27D4CD69D520B24592030974FC649::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4229[2] = 
{
	U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass37_0_t7A4101EB47B17C9335CA23259CDC3E76DE2576F7::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4230[2] = 
{
	U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass38_0_t22563C87AB743724B1D6E93DCB17C37C84EAF2A6::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4231[2] = 
{
	U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass39_0_t0C6FF7340BB553BCC49620701ED13F794930154B::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (DOTweenModuleUnityVersion_tD051D14A3CC840A947AF4E411FDA4A3E19E40CCE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4233[2] = 
{
	U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass8_0_tDA79EC3F83F3AFE07D58CAC1EF7751AE5A594943::get_offset_of_propertyID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4234[2] = 
{
	U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass9_0_t6760BEDD777B5BEE7F93B1F79679119F3F062BB6::get_offset_of_propertyID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (DOTweenCYInstruction_t99180052E9B15DB8365A25440477F7BE9B4D6847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4236[1] = 
{
	WaitForCompletion_t7E8309E6BD5D8BA1532973AC6730D960E0FD7A27::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4237[1] = 
{
	WaitForRewind_tA058A6E9E93DE243C93B1183D0ED12C6EAE43229::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4238[1] = 
{
	WaitForKill_t12674F16CDC05D7EE6ADA24C4733F47C9D0FD3CB::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4239[2] = 
{
	WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83::get_offset_of_t_0(),
	WaitForElapsedLoops_t8E663E2313D0C1BBBFE108212DDD75BE81144C83::get_offset_of_elapsedLoops_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4240[2] = 
{
	WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3::get_offset_of_t_0(),
	WaitForPosition_tC5AD605D856D031196E313FD950FE37FC99E4BC3::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4241[1] = 
{
	WaitForStart_t4BCF21589FA389F96FCED9ABE22111121CB5813C::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF), -1, sizeof(DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4242[1] = 
{
	DOTweenModuleUtils_tD2F0C6CC8F025ED71A5CD324BB82D12E6D7955AF_StaticFields::get_offset_of__initialized_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (Physics_tFF3A360D9F8EF2286D1343AA0E89E67FC909D59D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4244[2] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U331DCC8673FADE08890F573E7E9B161FFFC255F84_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U34EF93FCF8C8343821BE623E829DCE20851E64BD5_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t94E9375C899479358C941227FA08AB6B16D04315 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { sizeof (__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D48_tF32E0E0CF1CDF233FDDA0A19F0CAB73F393A7CB6 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4248[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (ConsoleMethodAttribute_t985BF70FAEB63DF31809C507803CD727E03ADE59), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4249[2] = 
{
	ConsoleMethodAttribute_t985BF70FAEB63DF31809C507803CD727E03ADE59::get_offset_of_m_command_0(),
	ConsoleMethodAttribute_t985BF70FAEB63DF31809C507803CD727E03ADE59::get_offset_of_m_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4250[4] = 
{
	ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65::get_offset_of_method_0(),
	ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65::get_offset_of_parameterTypes_1(),
	ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65::get_offset_of_instance_2(),
	ConsoleMethodInfo_t3EC97FD511941A4E1D7AF36CA7A6ADEB65A9DC65::get_offset_of_signature_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488), -1, sizeof(DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4251[5] = 
{
	DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields::get_offset_of_methods_0(),
	DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields::get_offset_of_parseFunctions_1(),
	DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields::get_offset_of_typeReadableNames_2(),
	DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields::get_offset_of_commandArguments_3(),
	DebugLogConsole_t840F94CA7BD7CA1D5DE04FF1ADEC73A5A9333488_StaticFields::get_offset_of_inputDelimiters_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (ParseFunction_tF3BA66F5AA42C4E5C2C99F8F4F6457E5BDFDFC00), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4253[7] = 
{
	0,
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8::get_offset_of_logString_1(),
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8::get_offset_of_stackTrace_2(),
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8::get_offset_of_completeLog_3(),
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8::get_offset_of_logTypeSpriteRepresentation_4(),
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8::get_offset_of_count_5(),
	DebugLogEntry_t691A2DF82E715A2A40A5A7686F162B21441FDCC8::get_offset_of_hashValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96)+ sizeof (RuntimeObject), sizeof(QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4254[3] = 
{
	QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96::get_offset_of_logString_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96::get_offset_of_stackTrace_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QueuedDebugLogEntry_t94F048D5EC04291E50EB41E9D0687E54E7FE5D96::get_offset_of_logType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { sizeof (DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4255[2] = 
{
	DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC::get_offset_of_indices_0(),
	DebugLogIndexList_tBB8F8C2195B991EBC2D26984189F6EF28C054DFC::get_offset_of_size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4256[9] = 
{
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_transformComponent_4(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_imageComponent_5(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_logText_6(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_logTypeImage_7(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_logCountParent_8(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_logCountText_9(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_logEntry_10(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_entryIndex_11(),
	DebugLogItem_tCCF74CA51BF32610AFF35E661151040817F68A51::get_offset_of_manager_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (DebugLogFilter_tF5E3C266EB271F87811192CBD62A660BAD0A66C9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4257[6] = 
{
	DebugLogFilter_tF5E3C266EB271F87811192CBD62A660BAD0A66C9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974), -1, sizeof(DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4258[53] = 
{
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974_StaticFields::get_offset_of_instance_4(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_singleton_5(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_minimumHeight_6(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_enablePopup_7(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_startInPopupMode_8(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_toggleWithKey_9(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_toggleKey_10(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_clearCommandAfterExecution_11(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_commandHistorySize_12(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_receiveLogcatLogsInAndroid_13(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logcatArguments_14(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logItemPrefab_15(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_infoLog_16(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_warningLog_17(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_errorLog_18(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logSpriteRepresentations_19(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_collapseButtonNormalColor_20(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_collapseButtonSelectedColor_21(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_filterButtonsNormalColor_22(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_filterButtonsSelectedColor_23(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logWindowTR_24(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_canvasTR_25(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logItemsContainer_26(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_commandInputField_27(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_collapseButton_28(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_filterInfoButton_29(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_filterWarningButton_30(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_filterErrorButton_31(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_infoEntryCountText_32(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_warningEntryCountText_33(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_errorEntryCountText_34(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_snapToBottomButton_35(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logWindowCanvasGroup_36(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_popupManager_37(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logItemsScrollRect_38(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_recycledListView_39(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_infoEntryCount_40(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_warningEntryCount_41(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_errorEntryCount_42(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_isLogWindowVisible_43(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_screenDimensionsChanged_44(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_isCollapseOn_45(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_logFilter_46(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_snapToBottom_47(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_collapsedLogEntries_48(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_collapsedLogEntriesMap_49(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_uncollapsedLogEntriesIndices_50(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_indicesOfListEntriesToShow_51(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_queuedLogs_52(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_pooledLogItems_53(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_commandHistory_54(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_commandHistoryIndex_55(),
	DebugLogManager_tB71954FFB28992F6115A38B632D6C95F6F564974::get_offset_of_nullPointerEventData_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4259[17] = 
{
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_popupTransform_4(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_halfSize_5(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_backgroundImage_6(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_canvasGroup_7(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_debugManager_8(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_newInfoCountText_9(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_newWarningCountText_10(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_newErrorCountText_11(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_alertColorInfo_12(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_alertColorWarning_13(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_alertColorError_14(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_newInfoCount_15(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_newWarningCount_16(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_newErrorCount_17(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_normalColor_18(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_isPopupBeingDragged_19(),
	DebugLogPopup_t055DFDAFD4646E4A32702E1211592F20F9B5B656::get_offset_of_moveToPosCoroutine_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4260[6] = 
{
	U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548::get_offset_of_U3CU3E1__state_0(),
	U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548::get_offset_of_U3CU3E2__current_1(),
	U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548::get_offset_of_U3CU3E4__this_2(),
	U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548::get_offset_of_targetPos_3(),
	U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548::get_offset_of_U3CmodifierU3E5__2_4(),
	U3CMoveToPosAnimationU3Ed__24_t1B873F89F686E17965DE986E4A670E8C1A9E4548::get_offset_of_U3CinitialPosU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4261[20] = 
{
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_transformComponent_4(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_viewportTransform_5(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_debugManager_6(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_logItemNormalColor1_7(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_logItemNormalColor2_8(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_logItemSelectedColor_9(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_manager_10(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_logItemHeight_11(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of__1OverLogItemHeight_12(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_viewportHeight_13(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_collapsedLogEntries_14(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_indicesOfEntriesToShow_15(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_indexOfSelectedLogEntry_16(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_positionOfSelectedLogEntry_17(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_heightOfSelectedLogEntry_18(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_deltaHeightOfSelectedLogEntry_19(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_logItemsAtIndices_20(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_isCollapseOn_21(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_currentTopIndex_22(),
	DebugLogRecycledListView_tAECBE39F9B9608EE821DEDCD0EAE0BA7584EB928::get_offset_of_currentBottomIndex_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (DebugsOnScrollListener_tF2CF1E9D609286A379D31C9A269461007847308E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4262[2] = 
{
	DebugsOnScrollListener_tF2CF1E9D609286A379D31C9A269461007847308E::get_offset_of_debugsScrollRect_4(),
	DebugsOnScrollListener_tF2CF1E9D609286A379D31C9A269461007847308E::get_offset_of_debugLogManager_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
