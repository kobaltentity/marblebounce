﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 ();
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000033 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003E TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000041 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000044 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000046 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000047 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000004B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004D System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004E System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004F System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000050 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000051 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000052 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000053 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000054 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000059 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000005A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000005C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000005D System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000005E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000060 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000061 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000062 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000064 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000065 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000067 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000069 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006B System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000006C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000006D System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000070 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000071 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000072 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000073 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000074 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000075 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000076 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000077 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000078 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000079 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007A System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000007B System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000007C System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000007D TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000007E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000007F System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000080 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000081 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000082 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000083 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000084 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000085 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000086 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000087 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000088 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000089 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000008A System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x0000008B System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x0000008C System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x0000008D TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008E System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000093 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000094 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000095 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000096 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000097 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000098 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000099 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000009A System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009B System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000009C System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x0000009D System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x0000009E System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x0000009F TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x000000A0 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A1 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A2 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000A3 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A4 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000A5 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000A6 System.Void System.Linq.Lookup`2::Resize()
// 0x000000A7 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000AB System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000AC System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000AD System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000AE System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000AF System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000B0 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000B1 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000B2 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000B3 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000B4 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000B5 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000B6 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x000000B7 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000B8 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000B9 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x000000BA TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000BB System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000BC System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000BD System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000BE System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000BF System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x000000C0 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000C1 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000C2 System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000C3 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000C4 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000C5 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000C6 System.Void System.Linq.Set`1::Resize()
// 0x000000C7 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000C8 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000C9 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000CA System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CB System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000CC System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000CD System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CE System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000CF System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000D0 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000D1 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000D2 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000D3 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000D4 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000D5 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000D6 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000D7 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000D8 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000D9 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000DA System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000DB System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000DC System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000DD System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000DE System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000DF System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000E0 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E4 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000E6 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000E8 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000E9 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000EA System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000EB System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000EC System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000ED System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000EE System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000EF System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000F0 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000F1 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000F2 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000F3 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000F4 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000F5 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000F6 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000F7 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000F8 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000F9 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000FA System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000FB T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000FC System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000FD System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[253] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[253] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	936,
	27,
	37,
	206,
	206,
	3,
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[62] = 
{
	{ 0x02000008, { 91, 4 } },
	{ 0x02000009, { 95, 9 } },
	{ 0x0200000A, { 106, 7 } },
	{ 0x0200000B, { 115, 10 } },
	{ 0x0200000C, { 127, 11 } },
	{ 0x0200000D, { 141, 9 } },
	{ 0x0200000E, { 153, 12 } },
	{ 0x0200000F, { 168, 1 } },
	{ 0x02000010, { 169, 2 } },
	{ 0x02000011, { 171, 12 } },
	{ 0x02000012, { 183, 12 } },
	{ 0x02000013, { 195, 6 } },
	{ 0x02000014, { 201, 2 } },
	{ 0x02000015, { 203, 4 } },
	{ 0x02000016, { 207, 3 } },
	{ 0x02000019, { 210, 17 } },
	{ 0x0200001A, { 231, 5 } },
	{ 0x0200001B, { 236, 1 } },
	{ 0x0200001D, { 237, 8 } },
	{ 0x0200001F, { 245, 4 } },
	{ 0x02000020, { 249, 3 } },
	{ 0x02000021, { 252, 5 } },
	{ 0x02000022, { 257, 7 } },
	{ 0x02000023, { 264, 3 } },
	{ 0x02000024, { 267, 7 } },
	{ 0x02000025, { 274, 4 } },
	{ 0x02000026, { 278, 21 } },
	{ 0x02000028, { 299, 2 } },
	{ 0x06000034, { 0, 10 } },
	{ 0x06000035, { 10, 10 } },
	{ 0x06000036, { 20, 5 } },
	{ 0x06000037, { 25, 5 } },
	{ 0x06000038, { 30, 1 } },
	{ 0x06000039, { 31, 2 } },
	{ 0x0600003A, { 33, 2 } },
	{ 0x0600003B, { 35, 4 } },
	{ 0x0600003C, { 39, 1 } },
	{ 0x0600003D, { 40, 2 } },
	{ 0x0600003E, { 42, 3 } },
	{ 0x0600003F, { 45, 2 } },
	{ 0x06000040, { 47, 1 } },
	{ 0x06000041, { 48, 7 } },
	{ 0x06000042, { 55, 2 } },
	{ 0x06000043, { 57, 2 } },
	{ 0x06000044, { 59, 4 } },
	{ 0x06000045, { 63, 4 } },
	{ 0x06000046, { 67, 3 } },
	{ 0x06000047, { 70, 4 } },
	{ 0x06000048, { 74, 4 } },
	{ 0x06000049, { 78, 3 } },
	{ 0x0600004A, { 81, 1 } },
	{ 0x0600004B, { 82, 1 } },
	{ 0x0600004C, { 83, 3 } },
	{ 0x0600004D, { 86, 3 } },
	{ 0x0600004E, { 89, 2 } },
	{ 0x0600005E, { 104, 2 } },
	{ 0x06000063, { 113, 2 } },
	{ 0x06000068, { 125, 2 } },
	{ 0x0600006E, { 138, 3 } },
	{ 0x06000073, { 150, 3 } },
	{ 0x06000078, { 165, 3 } },
	{ 0x060000A0, { 227, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[301] = 
{
	{ (Il2CppRGCTXDataType)2, 23197 },
	{ (Il2CppRGCTXDataType)3, 16236 },
	{ (Il2CppRGCTXDataType)2, 23198 },
	{ (Il2CppRGCTXDataType)2, 23199 },
	{ (Il2CppRGCTXDataType)3, 16237 },
	{ (Il2CppRGCTXDataType)2, 23200 },
	{ (Il2CppRGCTXDataType)2, 23201 },
	{ (Il2CppRGCTXDataType)3, 16238 },
	{ (Il2CppRGCTXDataType)2, 23202 },
	{ (Il2CppRGCTXDataType)3, 16239 },
	{ (Il2CppRGCTXDataType)2, 23203 },
	{ (Il2CppRGCTXDataType)3, 16240 },
	{ (Il2CppRGCTXDataType)2, 23204 },
	{ (Il2CppRGCTXDataType)2, 23205 },
	{ (Il2CppRGCTXDataType)3, 16241 },
	{ (Il2CppRGCTXDataType)2, 23206 },
	{ (Il2CppRGCTXDataType)2, 23207 },
	{ (Il2CppRGCTXDataType)3, 16242 },
	{ (Il2CppRGCTXDataType)2, 23208 },
	{ (Il2CppRGCTXDataType)3, 16243 },
	{ (Il2CppRGCTXDataType)2, 23209 },
	{ (Il2CppRGCTXDataType)3, 16244 },
	{ (Il2CppRGCTXDataType)3, 16245 },
	{ (Il2CppRGCTXDataType)2, 18269 },
	{ (Il2CppRGCTXDataType)3, 16246 },
	{ (Il2CppRGCTXDataType)2, 23210 },
	{ (Il2CppRGCTXDataType)3, 16247 },
	{ (Il2CppRGCTXDataType)3, 16248 },
	{ (Il2CppRGCTXDataType)2, 18276 },
	{ (Il2CppRGCTXDataType)3, 16249 },
	{ (Il2CppRGCTXDataType)3, 16250 },
	{ (Il2CppRGCTXDataType)2, 23211 },
	{ (Il2CppRGCTXDataType)3, 16251 },
	{ (Il2CppRGCTXDataType)2, 23212 },
	{ (Il2CppRGCTXDataType)3, 16252 },
	{ (Il2CppRGCTXDataType)3, 16253 },
	{ (Il2CppRGCTXDataType)2, 23213 },
	{ (Il2CppRGCTXDataType)2, 23214 },
	{ (Il2CppRGCTXDataType)3, 16254 },
	{ (Il2CppRGCTXDataType)3, 16255 },
	{ (Il2CppRGCTXDataType)2, 23215 },
	{ (Il2CppRGCTXDataType)3, 16256 },
	{ (Il2CppRGCTXDataType)2, 23216 },
	{ (Il2CppRGCTXDataType)3, 16257 },
	{ (Il2CppRGCTXDataType)3, 16258 },
	{ (Il2CppRGCTXDataType)2, 18308 },
	{ (Il2CppRGCTXDataType)3, 16259 },
	{ (Il2CppRGCTXDataType)3, 16260 },
	{ (Il2CppRGCTXDataType)2, 18323 },
	{ (Il2CppRGCTXDataType)3, 16261 },
	{ (Il2CppRGCTXDataType)2, 18316 },
	{ (Il2CppRGCTXDataType)2, 23217 },
	{ (Il2CppRGCTXDataType)3, 16262 },
	{ (Il2CppRGCTXDataType)3, 16263 },
	{ (Il2CppRGCTXDataType)3, 16264 },
	{ (Il2CppRGCTXDataType)2, 18324 },
	{ (Il2CppRGCTXDataType)3, 16265 },
	{ (Il2CppRGCTXDataType)2, 23218 },
	{ (Il2CppRGCTXDataType)3, 16266 },
	{ (Il2CppRGCTXDataType)2, 23219 },
	{ (Il2CppRGCTXDataType)2, 23220 },
	{ (Il2CppRGCTXDataType)2, 18328 },
	{ (Il2CppRGCTXDataType)2, 23221 },
	{ (Il2CppRGCTXDataType)2, 23222 },
	{ (Il2CppRGCTXDataType)2, 23223 },
	{ (Il2CppRGCTXDataType)2, 18330 },
	{ (Il2CppRGCTXDataType)2, 23224 },
	{ (Il2CppRGCTXDataType)2, 18332 },
	{ (Il2CppRGCTXDataType)2, 23225 },
	{ (Il2CppRGCTXDataType)3, 16267 },
	{ (Il2CppRGCTXDataType)2, 23226 },
	{ (Il2CppRGCTXDataType)2, 23227 },
	{ (Il2CppRGCTXDataType)2, 18335 },
	{ (Il2CppRGCTXDataType)2, 23228 },
	{ (Il2CppRGCTXDataType)2, 23229 },
	{ (Il2CppRGCTXDataType)2, 23230 },
	{ (Il2CppRGCTXDataType)2, 18337 },
	{ (Il2CppRGCTXDataType)2, 23231 },
	{ (Il2CppRGCTXDataType)2, 18339 },
	{ (Il2CppRGCTXDataType)2, 23232 },
	{ (Il2CppRGCTXDataType)3, 16268 },
	{ (Il2CppRGCTXDataType)2, 23233 },
	{ (Il2CppRGCTXDataType)2, 18344 },
	{ (Il2CppRGCTXDataType)2, 18346 },
	{ (Il2CppRGCTXDataType)2, 23234 },
	{ (Il2CppRGCTXDataType)3, 16269 },
	{ (Il2CppRGCTXDataType)2, 18349 },
	{ (Il2CppRGCTXDataType)2, 23235 },
	{ (Il2CppRGCTXDataType)3, 16270 },
	{ (Il2CppRGCTXDataType)2, 23236 },
	{ (Il2CppRGCTXDataType)2, 18352 },
	{ (Il2CppRGCTXDataType)3, 16271 },
	{ (Il2CppRGCTXDataType)3, 16272 },
	{ (Il2CppRGCTXDataType)2, 18356 },
	{ (Il2CppRGCTXDataType)3, 16273 },
	{ (Il2CppRGCTXDataType)3, 16274 },
	{ (Il2CppRGCTXDataType)2, 18368 },
	{ (Il2CppRGCTXDataType)2, 23237 },
	{ (Il2CppRGCTXDataType)3, 16275 },
	{ (Il2CppRGCTXDataType)3, 16276 },
	{ (Il2CppRGCTXDataType)2, 18370 },
	{ (Il2CppRGCTXDataType)2, 23071 },
	{ (Il2CppRGCTXDataType)3, 16277 },
	{ (Il2CppRGCTXDataType)3, 16278 },
	{ (Il2CppRGCTXDataType)2, 23238 },
	{ (Il2CppRGCTXDataType)3, 16279 },
	{ (Il2CppRGCTXDataType)3, 16280 },
	{ (Il2CppRGCTXDataType)2, 18380 },
	{ (Il2CppRGCTXDataType)2, 23239 },
	{ (Il2CppRGCTXDataType)3, 16281 },
	{ (Il2CppRGCTXDataType)3, 16282 },
	{ (Il2CppRGCTXDataType)3, 15643 },
	{ (Il2CppRGCTXDataType)3, 16283 },
	{ (Il2CppRGCTXDataType)2, 23240 },
	{ (Il2CppRGCTXDataType)3, 16284 },
	{ (Il2CppRGCTXDataType)3, 16285 },
	{ (Il2CppRGCTXDataType)2, 18392 },
	{ (Il2CppRGCTXDataType)2, 23241 },
	{ (Il2CppRGCTXDataType)3, 16286 },
	{ (Il2CppRGCTXDataType)3, 16287 },
	{ (Il2CppRGCTXDataType)3, 16288 },
	{ (Il2CppRGCTXDataType)3, 16289 },
	{ (Il2CppRGCTXDataType)3, 16290 },
	{ (Il2CppRGCTXDataType)3, 15649 },
	{ (Il2CppRGCTXDataType)3, 16291 },
	{ (Il2CppRGCTXDataType)2, 23242 },
	{ (Il2CppRGCTXDataType)3, 16292 },
	{ (Il2CppRGCTXDataType)3, 16293 },
	{ (Il2CppRGCTXDataType)2, 18405 },
	{ (Il2CppRGCTXDataType)2, 23243 },
	{ (Il2CppRGCTXDataType)3, 16294 },
	{ (Il2CppRGCTXDataType)3, 16295 },
	{ (Il2CppRGCTXDataType)2, 18407 },
	{ (Il2CppRGCTXDataType)2, 23244 },
	{ (Il2CppRGCTXDataType)3, 16296 },
	{ (Il2CppRGCTXDataType)3, 16297 },
	{ (Il2CppRGCTXDataType)2, 23245 },
	{ (Il2CppRGCTXDataType)3, 16298 },
	{ (Il2CppRGCTXDataType)3, 16299 },
	{ (Il2CppRGCTXDataType)2, 23246 },
	{ (Il2CppRGCTXDataType)3, 16300 },
	{ (Il2CppRGCTXDataType)3, 16301 },
	{ (Il2CppRGCTXDataType)2, 18422 },
	{ (Il2CppRGCTXDataType)2, 23247 },
	{ (Il2CppRGCTXDataType)3, 16302 },
	{ (Il2CppRGCTXDataType)3, 16303 },
	{ (Il2CppRGCTXDataType)3, 16304 },
	{ (Il2CppRGCTXDataType)3, 15660 },
	{ (Il2CppRGCTXDataType)2, 23248 },
	{ (Il2CppRGCTXDataType)3, 16305 },
	{ (Il2CppRGCTXDataType)3, 16306 },
	{ (Il2CppRGCTXDataType)2, 23249 },
	{ (Il2CppRGCTXDataType)3, 16307 },
	{ (Il2CppRGCTXDataType)3, 16308 },
	{ (Il2CppRGCTXDataType)2, 18438 },
	{ (Il2CppRGCTXDataType)2, 23250 },
	{ (Il2CppRGCTXDataType)3, 16309 },
	{ (Il2CppRGCTXDataType)3, 16310 },
	{ (Il2CppRGCTXDataType)3, 16311 },
	{ (Il2CppRGCTXDataType)3, 16312 },
	{ (Il2CppRGCTXDataType)3, 16313 },
	{ (Il2CppRGCTXDataType)3, 16314 },
	{ (Il2CppRGCTXDataType)3, 15666 },
	{ (Il2CppRGCTXDataType)2, 23251 },
	{ (Il2CppRGCTXDataType)3, 16315 },
	{ (Il2CppRGCTXDataType)3, 16316 },
	{ (Il2CppRGCTXDataType)2, 23252 },
	{ (Il2CppRGCTXDataType)3, 16317 },
	{ (Il2CppRGCTXDataType)3, 16318 },
	{ (Il2CppRGCTXDataType)3, 16319 },
	{ (Il2CppRGCTXDataType)3, 16320 },
	{ (Il2CppRGCTXDataType)3, 16321 },
	{ (Il2CppRGCTXDataType)3, 16322 },
	{ (Il2CppRGCTXDataType)2, 23253 },
	{ (Il2CppRGCTXDataType)2, 23254 },
	{ (Il2CppRGCTXDataType)3, 16323 },
	{ (Il2CppRGCTXDataType)2, 18473 },
	{ (Il2CppRGCTXDataType)2, 18467 },
	{ (Il2CppRGCTXDataType)3, 16324 },
	{ (Il2CppRGCTXDataType)2, 18466 },
	{ (Il2CppRGCTXDataType)2, 23255 },
	{ (Il2CppRGCTXDataType)3, 16325 },
	{ (Il2CppRGCTXDataType)3, 16326 },
	{ (Il2CppRGCTXDataType)3, 16327 },
	{ (Il2CppRGCTXDataType)3, 16328 },
	{ (Il2CppRGCTXDataType)2, 23256 },
	{ (Il2CppRGCTXDataType)3, 16329 },
	{ (Il2CppRGCTXDataType)2, 18489 },
	{ (Il2CppRGCTXDataType)2, 18481 },
	{ (Il2CppRGCTXDataType)3, 16330 },
	{ (Il2CppRGCTXDataType)3, 16331 },
	{ (Il2CppRGCTXDataType)2, 18480 },
	{ (Il2CppRGCTXDataType)2, 23257 },
	{ (Il2CppRGCTXDataType)3, 16332 },
	{ (Il2CppRGCTXDataType)3, 16333 },
	{ (Il2CppRGCTXDataType)3, 16334 },
	{ (Il2CppRGCTXDataType)2, 18493 },
	{ (Il2CppRGCTXDataType)3, 16335 },
	{ (Il2CppRGCTXDataType)2, 23258 },
	{ (Il2CppRGCTXDataType)3, 16336 },
	{ (Il2CppRGCTXDataType)3, 16337 },
	{ (Il2CppRGCTXDataType)2, 23259 },
	{ (Il2CppRGCTXDataType)2, 23260 },
	{ (Il2CppRGCTXDataType)2, 23261 },
	{ (Il2CppRGCTXDataType)3, 16338 },
	{ (Il2CppRGCTXDataType)2, 18505 },
	{ (Il2CppRGCTXDataType)3, 16339 },
	{ (Il2CppRGCTXDataType)2, 23262 },
	{ (Il2CppRGCTXDataType)3, 16340 },
	{ (Il2CppRGCTXDataType)2, 23262 },
	{ (Il2CppRGCTXDataType)2, 18529 },
	{ (Il2CppRGCTXDataType)3, 16341 },
	{ (Il2CppRGCTXDataType)3, 16342 },
	{ (Il2CppRGCTXDataType)3, 16343 },
	{ (Il2CppRGCTXDataType)3, 16344 },
	{ (Il2CppRGCTXDataType)2, 23263 },
	{ (Il2CppRGCTXDataType)2, 23264 },
	{ (Il2CppRGCTXDataType)2, 23265 },
	{ (Il2CppRGCTXDataType)3, 16345 },
	{ (Il2CppRGCTXDataType)3, 16346 },
	{ (Il2CppRGCTXDataType)2, 18525 },
	{ (Il2CppRGCTXDataType)2, 18528 },
	{ (Il2CppRGCTXDataType)3, 16347 },
	{ (Il2CppRGCTXDataType)3, 16348 },
	{ (Il2CppRGCTXDataType)2, 18532 },
	{ (Il2CppRGCTXDataType)3, 16349 },
	{ (Il2CppRGCTXDataType)2, 23266 },
	{ (Il2CppRGCTXDataType)2, 18522 },
	{ (Il2CppRGCTXDataType)2, 23267 },
	{ (Il2CppRGCTXDataType)3, 16350 },
	{ (Il2CppRGCTXDataType)3, 16351 },
	{ (Il2CppRGCTXDataType)3, 16352 },
	{ (Il2CppRGCTXDataType)2, 23268 },
	{ (Il2CppRGCTXDataType)3, 16353 },
	{ (Il2CppRGCTXDataType)3, 16354 },
	{ (Il2CppRGCTXDataType)3, 16355 },
	{ (Il2CppRGCTXDataType)2, 18547 },
	{ (Il2CppRGCTXDataType)3, 16356 },
	{ (Il2CppRGCTXDataType)2, 23269 },
	{ (Il2CppRGCTXDataType)2, 23270 },
	{ (Il2CppRGCTXDataType)3, 16357 },
	{ (Il2CppRGCTXDataType)3, 16358 },
	{ (Il2CppRGCTXDataType)2, 18568 },
	{ (Il2CppRGCTXDataType)3, 16359 },
	{ (Il2CppRGCTXDataType)2, 18569 },
	{ (Il2CppRGCTXDataType)3, 16360 },
	{ (Il2CppRGCTXDataType)2, 23271 },
	{ (Il2CppRGCTXDataType)3, 16361 },
	{ (Il2CppRGCTXDataType)3, 16362 },
	{ (Il2CppRGCTXDataType)2, 23272 },
	{ (Il2CppRGCTXDataType)3, 16363 },
	{ (Il2CppRGCTXDataType)3, 16364 },
	{ (Il2CppRGCTXDataType)2, 23273 },
	{ (Il2CppRGCTXDataType)3, 16365 },
	{ (Il2CppRGCTXDataType)3, 16366 },
	{ (Il2CppRGCTXDataType)3, 16367 },
	{ (Il2CppRGCTXDataType)2, 18600 },
	{ (Il2CppRGCTXDataType)3, 16368 },
	{ (Il2CppRGCTXDataType)2, 18609 },
	{ (Il2CppRGCTXDataType)3, 16369 },
	{ (Il2CppRGCTXDataType)2, 23274 },
	{ (Il2CppRGCTXDataType)2, 23275 },
	{ (Il2CppRGCTXDataType)3, 16370 },
	{ (Il2CppRGCTXDataType)3, 16371 },
	{ (Il2CppRGCTXDataType)3, 16372 },
	{ (Il2CppRGCTXDataType)3, 16373 },
	{ (Il2CppRGCTXDataType)3, 16374 },
	{ (Il2CppRGCTXDataType)3, 16375 },
	{ (Il2CppRGCTXDataType)2, 18625 },
	{ (Il2CppRGCTXDataType)2, 23276 },
	{ (Il2CppRGCTXDataType)3, 16376 },
	{ (Il2CppRGCTXDataType)3, 16377 },
	{ (Il2CppRGCTXDataType)2, 18629 },
	{ (Il2CppRGCTXDataType)3, 16378 },
	{ (Il2CppRGCTXDataType)2, 23277 },
	{ (Il2CppRGCTXDataType)2, 18639 },
	{ (Il2CppRGCTXDataType)2, 18637 },
	{ (Il2CppRGCTXDataType)2, 23278 },
	{ (Il2CppRGCTXDataType)3, 16379 },
	{ (Il2CppRGCTXDataType)2, 23279 },
	{ (Il2CppRGCTXDataType)3, 16380 },
	{ (Il2CppRGCTXDataType)3, 16381 },
	{ (Il2CppRGCTXDataType)3, 16382 },
	{ (Il2CppRGCTXDataType)2, 18642 },
	{ (Il2CppRGCTXDataType)3, 16383 },
	{ (Il2CppRGCTXDataType)3, 16384 },
	{ (Il2CppRGCTXDataType)2, 18645 },
	{ (Il2CppRGCTXDataType)3, 16385 },
	{ (Il2CppRGCTXDataType)1, 23280 },
	{ (Il2CppRGCTXDataType)2, 18644 },
	{ (Il2CppRGCTXDataType)3, 16386 },
	{ (Il2CppRGCTXDataType)1, 18644 },
	{ (Il2CppRGCTXDataType)1, 18642 },
	{ (Il2CppRGCTXDataType)2, 23281 },
	{ (Il2CppRGCTXDataType)2, 18644 },
	{ (Il2CppRGCTXDataType)3, 16387 },
	{ (Il2CppRGCTXDataType)3, 16388 },
	{ (Il2CppRGCTXDataType)3, 16389 },
	{ (Il2CppRGCTXDataType)2, 18643 },
	{ (Il2CppRGCTXDataType)3, 16390 },
	{ (Il2CppRGCTXDataType)2, 18656 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	253,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	62,
	s_rgctxIndices,
	301,
	s_rgctxValues,
	NULL,
};
