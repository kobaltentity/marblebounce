﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Bouncer::Start()
extern void Bouncer_Start_m39268BEDFA9A9EC977E4EFF8A21A8A130F63593A ();
// 0x00000002 System.Void Bouncer::Update()
extern void Bouncer_Update_m1D966B71CB415CBEB9058D2C65B36CABFFE910F1 ();
// 0x00000003 System.Void Bouncer::OnCollisionEnter(UnityEngine.Collision)
extern void Bouncer_OnCollisionEnter_mA4D7F1212B2F6C749025AC7370AA88D7135C2B4E ();
// 0x00000004 System.Void Bouncer::SqueezeAnimation()
extern void Bouncer_SqueezeAnimation_mAFBAA226BCBB9C003197AEDCD40469B37EDDEB50 ();
// 0x00000005 System.Void Bouncer::OnDrawGizmos()
extern void Bouncer_OnDrawGizmos_m31CF62556A9E9A2B0F3BCCEAADDE3A39F33D57F5 ();
// 0x00000006 UnityEngine.Vector3 Bouncer::GetPosOnJump(System.Single,System.Single,System.Single)
extern void Bouncer_GetPosOnJump_m88EC8EE9112EA029F2CD11BDFA6DCDA274FDBF7B ();
// 0x00000007 System.Void Bouncer::.ctor()
extern void Bouncer__ctor_m76C49EF18596BB1DE0133CD59BB420200895D55E ();
// 0x00000008 UnityEngine.Camera CameraController::get_CurrentCamera()
extern void CameraController_get_CurrentCamera_m3BD442F154210FE37DEEFD3F222E7AD5510C1DBA ();
// 0x00000009 System.Void CameraController::Start()
extern void CameraController_Start_m92C8EBBE34DA314CE0C730E07400A96E968264BB ();
// 0x0000000A System.Void CameraController::Update()
extern void CameraController_Update_mEFEBBAC80E781E93A04E53B8A6F10601CFDBC199 ();
// 0x0000000B System.Void CameraController::UpdateFollow()
extern void CameraController_UpdateFollow_m9A422CC8E3AB74404AB0E7628B811D0607A0A16D ();
// 0x0000000C System.Void CameraController::Follow(UnityEngine.Transform)
extern void CameraController_Follow_mCEB594A6675D2214BA5E44BA8ACE0DFC03223860 ();
// 0x0000000D System.Void CameraController::.ctor()
extern void CameraController__ctor_m6E88C7769B5C6E7CC68A3670C07050374E05F902 ();
// 0x0000000E System.Void Coin::Start()
extern void Coin_Start_m091891BDFF95DE209C1C086AA6E5AF2E8E7F2E01 ();
// 0x0000000F System.Void Coin::Update()
extern void Coin_Update_m3C113B6905A7E3512ADAB23C121BD0C6B1241037 ();
// 0x00000010 System.Void Coin::OnCollisionEnter(UnityEngine.Collision)
extern void Coin_OnCollisionEnter_m1943EAE75F5D21012509C6C9D39AEF09D9D81F12 ();
// 0x00000011 System.Void Coin::.ctor()
extern void Coin__ctor_m1BDB4DA53B3F8A81A2DCB8589DFF09BCAEEE892B ();
// 0x00000012 System.Void CoinSpawn::Start()
extern void CoinSpawn_Start_m6BA364DE32C9A2138A51E23ADFD24FF4DAB96111 ();
// 0x00000013 System.Void CoinSpawn::Update()
extern void CoinSpawn_Update_m77ADAF94713C7105EE932684F4B7A230B877C5BE ();
// 0x00000014 System.Void CoinSpawn::RollSpawn()
extern void CoinSpawn_RollSpawn_m4C2CDD48BA9200228147127DDA6927327956B551 ();
// 0x00000015 System.Void CoinSpawn::SpawnCoin()
extern void CoinSpawn_SpawnCoin_mA7EE51CFC5AB82B1753F4A3823AED0265BCF4ECB ();
// 0x00000016 System.Void CoinSpawn::OnDrawGizmos()
extern void CoinSpawn_OnDrawGizmos_m7A01AE6CEB7E857D42CD7396790F3DB46E0D84CE ();
// 0x00000017 System.Void CoinSpawn::.ctor()
extern void CoinSpawn__ctor_m7BAA28B1228766072EC7B5E7964651B95F8911FB ();
// 0x00000018 System.Void EndPlatform::FireParticles()
extern void EndPlatform_FireParticles_m418BF0F3EC9F69F94D22A86B245CB14350DA52A4 ();
// 0x00000019 System.Void EndPlatform::.ctor()
extern void EndPlatform__ctor_m8E9CF4FCAEC7868D221DF1BBD5A488D8F0279003 ();
// 0x0000001A System.Void Feedback::InitAndroidFeedback()
extern void Feedback_InitAndroidFeedback_mFE3C9B79BCB2791A7BE36BB557DC4DD72E9B5945 ();
// 0x0000001B System.Void Feedback::SendFeedback(iOSHapticFeedback_iOSFeedbackType)
extern void Feedback_SendFeedback_m87636D90C0C1890D94617CC5BBBA78C50CA88F33 ();
// 0x0000001C System.Void Feedback::Vibrate(System.Int64[],System.Int32)
extern void Feedback_Vibrate_m4473BEADCA8C8607F7D21B4865517E44B21A9B77 ();
// 0x0000001D System.Boolean Feedback::HasVibrator()
extern void Feedback_HasVibrator_m12E862E6D16004A9983EB0C41AB2A5BD18F67F87 ();
// 0x0000001E System.Void Feedback::Cancel()
extern void Feedback_Cancel_m196A68B39027F0893004DE86A23FDAEE04F23043 ();
// 0x0000001F System.Boolean Feedback::isAndroid()
extern void Feedback_isAndroid_m434A881E55A5F2C670E4BC0F1FA5A50B2C2544D1 ();
// 0x00000020 System.Void GameController::Awake()
extern void GameController_Awake_mDE56BB9F9E51A4C075AB2CF5BF2C04A39476578E ();
// 0x00000021 System.Void GameController::Start()
extern void GameController_Start_mF709714337C920D2A7F640A97C3CAEF788E3358F ();
// 0x00000022 System.Void GameController::Update()
extern void GameController_Update_mBCFBA31999BD51BFC0E3CACA495249FF8FB4E225 ();
// 0x00000023 System.Void GameController::SlimeJump()
extern void GameController_SlimeJump_mBB245CE14A6EF47C8882D2635761A90C1DAFE399 ();
// 0x00000024 System.Void GameController::CollectCoin(Coin)
extern void GameController_CollectCoin_mF7672976D2B17D940254ED1F9315A65292F11823 ();
// 0x00000025 System.Void GameController::CoinSpawned(Coin)
extern void GameController_CoinSpawned_m8686E28DB8D7144BFC62733778856C2746D5E156 ();
// 0x00000026 System.Void GameController::HoopSpawned(Hoop)
extern void GameController_HoopSpawned_m7AD84F7ECBC982AA3843E839AD5603C1B9366D90 ();
// 0x00000027 System.Void GameController::AddJump(System.Int32)
extern void GameController_AddJump_m98D81A8224307C790333A55DE4BE5195D7AC6D65 ();
// 0x00000028 System.Void GameController::PlayerOnPlatform(Platform)
extern void GameController_PlayerOnPlatform_m5A574C7F930BB97437D81472F54FC58560702839 ();
// 0x00000029 System.Void GameController::GameOver(System.Boolean)
extern void GameController_GameOver_mB9729C42B008269F8D21E7AE025025377D3FA8FF ();
// 0x0000002A System.Void GameController::Victory()
extern void GameController_Victory_m24F379BA38D5FB2C4105035435EF8779C76DA331 ();
// 0x0000002B System.Void GameController::Defeat()
extern void GameController_Defeat_mACB7101A5B79220B16162120145F26962AFFDE13 ();
// 0x0000002C System.Single GameController::GetDifficulty()
extern void GameController_GetDifficulty_m58580BC96DAC23BEE55ACF12CBAE01223C953191 ();
// 0x0000002D System.Void GameController::EnterTutorial()
extern void GameController_EnterTutorial_m0FB1952EB51A5AE1AB0E3FA60F1C392F00D9FFDA ();
// 0x0000002E System.Void GameController::ExitTutorial()
extern void GameController_ExitTutorial_m30D26445D828BDA266DC3BAA773A9705E3C2E2F1 ();
// 0x0000002F System.Void GameController::ApplyLevelSettings(LevelSetting)
extern void GameController_ApplyLevelSettings_m2E1DFECE7696D7AFB6AAFB0EE3707DB1FF799C45 ();
// 0x00000030 Platform GameController::ContinueLevel(Platform,System.Boolean)
extern void GameController_ContinueLevel_m8BADE47A45C9A5D2655E6D49504A55B0FEDE3D20 ();
// 0x00000031 System.Void GameController::InitLevel()
extern void GameController_InitLevel_m16E7B56885CC13161C8FE8533B3B23D5B7CF1640 ();
// 0x00000032 System.Void GameController::ClearLevel()
extern void GameController_ClearLevel_m9871AF20A96C221C4500A5BDD18F9F2C653CD691 ();
// 0x00000033 Platform GameController::CreatePlatform(UnityEngine.Vector3&,Platform)
extern void GameController_CreatePlatform_m6AA94D88E7E40D4E38B1405061728572A3B149D4 ();
// 0x00000034 System.Void GameController::ChangeFog(System.Single,System.Single)
extern void GameController_ChangeFog_mEC816DD94B4B56EA21FC6C4040912B76C14669FE ();
// 0x00000035 System.Void GameController::ChangeTimeScale(System.Single,System.Single)
extern void GameController_ChangeTimeScale_mC61BBC6197B1FEF734DE4F80B713AAC87FD6F7F8 ();
// 0x00000036 System.Void GameController::SpawnPlayer()
extern void GameController_SpawnPlayer_m591000ADF6B1AA3624BF175A7A1A97A6B94FD000 ();
// 0x00000037 System.Void GameController::StartGame()
extern void GameController_StartGame_mEAC9289B743077F253A9E4C4BEE50A2FF052A001 ();
// 0x00000038 System.Void GameController::ResetGame()
extern void GameController_ResetGame_m3E2B8A9F774A6F81E647F1F9130F0D60E02E3CF7 ();
// 0x00000039 System.Void GameController::OnTouchBegan()
extern void GameController_OnTouchBegan_m5FADF218919FF40D209C908E80080A1E4BC81CC6 ();
// 0x0000003A System.Void GameController::OnTouchUpdate()
extern void GameController_OnTouchUpdate_mF8DF086EAFC280571557CC59BFEA30D0375F9AD5 ();
// 0x0000003B System.Void GameController::OnTouchEnded()
extern void GameController_OnTouchEnded_m901D7E67FD464A9A534FF5F667D71223635DFAE3 ();
// 0x0000003C System.Void GameController::.ctor()
extern void GameController__ctor_m6C90AFC15A0F46BF2901D879C39FE7E50AE7CDB3 ();
// 0x0000003D System.Void GameController::<Victory>b__41_0()
extern void GameController_U3CVictoryU3Eb__41_0_mF7491F461218A9E45D54259E71C7E910B93223DF ();
// 0x0000003E System.Void GameSave::.ctor()
extern void GameSave__ctor_mC908542ED039B572AC9E45103DDAA56FDB5E6300 ();
// 0x0000003F System.Void GameSave::ResetSave(GameSave&)
extern void GameSave_ResetSave_m26E65BDED3EA935E15CB05EAD44D8E16E3D952C0 ();
// 0x00000040 GameSave GameSave::LoadSave()
extern void GameSave_LoadSave_mDEB036EB4B5FD2E362BDB58E3D15511F071E53D2 ();
// 0x00000041 System.Void GameSave::Save(GameSave)
extern void GameSave_Save_mA14E3029C505C1A780BA50CD10D9ED8A05005270 ();
// 0x00000042 System.Void Hoop::Start()
extern void Hoop_Start_mBD3C497CB640451A2256E1940E2289C8B408D572 ();
// 0x00000043 System.Void Hoop::Update()
extern void Hoop_Update_m15C768CE69A0BADAA11F7052CCAABB1E3F500BA2 ();
// 0x00000044 System.Void Hoop::OnHitSide(UnityEngine.Vector3)
extern void Hoop_OnHitSide_mB2082AD006706E4BCF506F4771E3099A7EFCDFE0 ();
// 0x00000045 System.Void Hoop::OnHitCenter()
extern void Hoop_OnHitCenter_m2943A90D045456B2627FA4D0E50845A6083E8D86 ();
// 0x00000046 System.Void Hoop::.ctor()
extern void Hoop__ctor_m1FA4DC2D4B74722F95D7EF7FB2676869F79F6662 ();
// 0x00000047 System.Void Hoop::.cctor()
extern void Hoop__cctor_mA60060B5123E4D18A4CDFB4A123252A3E177074C ();
// 0x00000048 System.Void HoopsSpawner::Start()
extern void HoopsSpawner_Start_m4458A3AEFE099F7595060E7B4820B27D7747AB8D ();
// 0x00000049 System.Void HoopsSpawner::RollSpawn()
extern void HoopsSpawner_RollSpawn_mAB773B5AE073386E016E661A70EEFBA5B1F2AB85 ();
// 0x0000004A System.Void HoopsSpawner::SpawnHoop()
extern void HoopsSpawner_SpawnHoop_m53D02B492CDE782637FEC9B8AB0F467E5F62DEF8 ();
// 0x0000004B System.Void HoopsSpawner::OnDrawGizmos()
extern void HoopsSpawner_OnDrawGizmos_m002DE1A3F8A999D8A0270DABCE257456343CEB64 ();
// 0x0000004C System.Void HoopsSpawner::.ctor()
extern void HoopsSpawner__ctor_mD83829D70A41F4044A456813CBDC7E07C30AA9F9 ();
// 0x0000004D System.Void InputManager::Start()
extern void InputManager_Start_m9EAB4933E74781D59CB95E9FCD6580F9A1EDA14E ();
// 0x0000004E System.Void InputManager::Update()
extern void InputManager_Update_m813F65B6A0D8A18ACFAFEFF8CB3F7E1FE116BCA7 ();
// 0x0000004F System.Single InputManager::GetTouchMagnitude()
extern void InputManager_GetTouchMagnitude_m65971F7EB06227F2516DC0653EC6C14E432E3BBC ();
// 0x00000050 System.Single InputManager::GetTouchDuration()
extern void InputManager_GetTouchDuration_m551F3871D9A7D70EF6CEB48238D711F5B86E44B9 ();
// 0x00000051 UnityEngine.Vector2 InputManager::GetTouchVector()
extern void InputManager_GetTouchVector_m61D15307CB7423C1CC326A9284F2301528FE23DE ();
// 0x00000052 UnityEngine.Vector2 InputManager::GetTouchDifference()
extern void InputManager_GetTouchDifference_mDCE38284AAB3638486479B8D64DE66113AE9259A ();
// 0x00000053 System.Void InputManager::ResetTouchVector()
extern void InputManager_ResetTouchVector_m71EDC3BC10E6F475F38071816EB7BD3B9F71671D ();
// 0x00000054 System.Void InputManager::.ctor()
extern void InputManager__ctor_m92E656DB5CA317F9006F3CD8CE443B7D15EE2A47 ();
// 0x00000055 System.Void JSONUtils::SaveJson(System.Object,System.String)
extern void JSONUtils_SaveJson_m0551F2D84063CB16D36C1636079ACC8E6245B7B3 ();
// 0x00000056 T JSONUtils::LoadJson(System.String)
// 0x00000057 T JSONUtils::FromJson(System.String)
// 0x00000058 System.Void JSONUtils::.ctor()
extern void JSONUtils__ctor_m36BC1B678914D83708BF2A849FAFD0E15D75B18A ();
// 0x00000059 System.Void LevelSetting::.ctor()
extern void LevelSetting__ctor_m626FD1130FE5EDC031605883BE876F06E7B13F91 ();
// 0x0000005A System.Void Platform::Start()
extern void Platform_Start_mF33B833267B9B2BF65D6562E4E5F4CB252C2E9BF ();
// 0x0000005B System.Void Platform::Update()
extern void Platform_Update_mE2C277250260163AEA6CABB4F729BF2AA2A3ED0E ();
// 0x0000005C UnityEngine.Vector3 Platform::GetMarbleSpawnPos()
extern void Platform_GetMarbleSpawnPos_m2F14D7A7191E3422A4744E10CC75603F755A72B3 ();
// 0x0000005D System.Void Platform::OnDrawGizmosSelected()
extern void Platform_OnDrawGizmosSelected_m8889652814A311EB7EE11F378CCD4BC4A5038847 ();
// 0x0000005E System.Void Platform::.ctor()
extern void Platform__ctor_m4380C762ACE2D5A5EBAFC8C223D6A9AF70257704 ();
// 0x0000005F T SingletonGeneric`1::get_Instance()
// 0x00000060 System.Void SingletonGeneric`1::Awake()
// 0x00000061 System.Void SingletonGeneric`1::.ctor()
// 0x00000062 System.Void SlimeController::Awake()
extern void SlimeController_Awake_m05BDE545A4DB031D71E69020F09E8F46824FDDC4 ();
// 0x00000063 System.Void SlimeController::Start()
extern void SlimeController_Start_m1E8CE60383615868F5BF5D3A2A1943A2FE56034F ();
// 0x00000064 System.Void SlimeController::Update()
extern void SlimeController_Update_m520B7CCEC6B6F0EA7F8B9460488B343CC0384FBA ();
// 0x00000065 System.Void SlimeController::CheckTutorial()
extern void SlimeController_CheckTutorial_mBC661D6199A3FE0661DABBEFC33FF2F6F8B10B88 ();
// 0x00000066 System.Void SlimeController::InputDirection(System.Single)
extern void SlimeController_InputDirection_m64DCDEFE205FA9D80E09F3B55E3AFF3D7F404389 ();
// 0x00000067 System.Void SlimeController::SetLateralPosition(System.Single)
extern void SlimeController_SetLateralPosition_m0F03095E088D829185E97D7420319C77AB486854 ();
// 0x00000068 System.Void SlimeController::ApplyLateralPosition()
extern void SlimeController_ApplyLateralPosition_m3C87E592A3743ECC015043FAED3440EAE4F75AC9 ();
// 0x00000069 System.Void SlimeController::UnStick()
extern void SlimeController_UnStick_m08112A44183F496E68E8CC50F5DE8FC00A4AD199 ();
// 0x0000006A System.Void SlimeController::Dash(System.Int32)
extern void SlimeController_Dash_mFB6EDA4F0C655A44F119AB5AF0023A2130A73D07 ();
// 0x0000006B System.Boolean SlimeController::CanDash(System.Int32)
extern void SlimeController_CanDash_m5FE58EEEED3CABD11B4FD8F34CDFBDFE12502E52 ();
// 0x0000006C System.Void SlimeController::OnLanded()
extern void SlimeController_OnLanded_m53DC8A7FF574D031246EA4E8DF0D9B5C2846581C ();
// 0x0000006D System.Void SlimeController::StartBouncing()
extern void SlimeController_StartBouncing_m482650A043C07BE78186840F698D2FBCEC2CEE60 ();
// 0x0000006E System.Void SlimeController::NextMove()
extern void SlimeController_NextMove_m5E38AC0C458FAD052B24A01D3F81AC28E879A9D2 ();
// 0x0000006F Bouncer SlimeController::GetBumper()
extern void SlimeController_GetBumper_m90AD2F3E9C4E8C9E4D117A815E5BE761FBEC7665 ();
// 0x00000070 System.Void SlimeController::AnimationJump(System.Single,System.Single,DG.Tweening.TweenCallback)
extern void SlimeController_AnimationJump_m86BE1305BA9D4C54330A822FAEF847ABF5045319 ();
// 0x00000071 System.Void SlimeController::AnimationScale(System.Single,DG.Tweening.TweenCallback)
extern void SlimeController_AnimationScale_m7526B90B90886AC1460CB188DA6563AD53C6E3A4 ();
// 0x00000072 System.Single SlimeController::GetDurationFromJumpHeight(System.Single)
extern void SlimeController_GetDurationFromJumpHeight_mD1461679FFBF1E80E223A9AD1EAB1294B1FE741B ();
// 0x00000073 System.Void SlimeController::AnimationRotateSlime(System.Single)
extern void SlimeController_AnimationRotateSlime_m1579A4911A4F5D6805AB0B8BA071BC37875983FC ();
// 0x00000074 System.Void SlimeController::MoveForward(System.Single,System.Single,System.Boolean)
extern void SlimeController_MoveForward_mDC3574888ED78F672B08234A4650B5BF5D719184 ();
// 0x00000075 System.Void SlimeController::CheckGameOver()
extern void SlimeController_CheckGameOver_m5D1C5C8E076A3F1F1E34D3FACF9855C65F789B77 ();
// 0x00000076 System.Boolean SlimeController::CheckCollision()
extern void SlimeController_CheckCollision_m98B7025B88A2AEFFEBAD0BB37FDDDAD47F5CC682 ();
// 0x00000077 System.Void SlimeController::CheckHoops()
extern void SlimeController_CheckHoops_mDE5C9970B7236953BD3F42C123B6DA3463D7FD01 ();
// 0x00000078 System.Void SlimeController::Fall()
extern void SlimeController_Fall_m4C52B273540328307832111F3EAAAB3DB2566EFF ();
// 0x00000079 System.Void SlimeController::CeaseMovement()
extern void SlimeController_CeaseMovement_m70733515895C84702107E05C30D70BBDC67DC842 ();
// 0x0000007A System.Void SlimeController::.ctor()
extern void SlimeController__ctor_m6A10C1799C5D4B338AAF4C12EBD45563C0539B0D ();
// 0x0000007B System.Void SlimeController::<UnStick>b__32_0()
extern void SlimeController_U3CUnStickU3Eb__32_0_m876E2D808D2B16A079EFC785884688F1C31300D9 ();
// 0x0000007C System.Void SlimeController::<UnStick>b__32_1()
extern void SlimeController_U3CUnStickU3Eb__32_1_m7B34DFEA74F28D5CE088B7A60E5C3687F874D7A9 ();
// 0x0000007D System.Void SlimeController::<Dash>b__33_0()
extern void SlimeController_U3CDashU3Eb__33_0_mA949116D7F2CEE33E46AC9B6BF448D3ED5F607E0 ();
// 0x0000007E System.Void SlimeController::<AnimationJump>b__39_0()
extern void SlimeController_U3CAnimationJumpU3Eb__39_0_m551EFBBFDE9E3CB5C68105868CB9CA35352A91E4 ();
// 0x0000007F System.Void SlimeController::<CheckCollision>b__45_0()
extern void SlimeController_U3CCheckCollisionU3Eb__45_0_mD141524431F8B5B85466181D5BFA94B257070463 ();
// 0x00000080 System.Void UIController::Start()
extern void UIController_Start_m15E91565B003DEB2B4898F1FF002EDD1B7554D9B ();
// 0x00000081 System.Void UIController::Update()
extern void UIController_Update_m418FDF444584AAE6D1F04C2509D0E935A4330F1D ();
// 0x00000082 System.Void UIController::OnPlay()
extern void UIController_OnPlay_m6B8CDE66DF0CBB12AED81C310F25174B937DCE5C ();
// 0x00000083 System.Void UIController::SetJumpAmount(System.Int32,System.Int32)
extern void UIController_SetJumpAmount_m4212BCEF6891DA31604C1DE8D56EFB5CEADB8792 ();
// 0x00000084 System.Void UIController::ToggleTutoDashPanel(System.Boolean)
extern void UIController_ToggleTutoDashPanel_m368F7AF0AF278C0623D3986D1BDF4853C93B3919 ();
// 0x00000085 System.Void UIController::EndScreenCompletion(System.Single,System.Boolean)
extern void UIController_EndScreenCompletion_mA9B7E3E8E41AF085A2FA8CF7917642E3530671AA ();
// 0x00000086 System.Void UIController::SetCurrentLevel(System.Int32)
extern void UIController_SetCurrentLevel_m09F7A9CA5D79B543FC09418E6D284916BE099351 ();
// 0x00000087 System.Void UIController::SetCoinAmount(System.Int32)
extern void UIController_SetCoinAmount_m3EB8222F83C7F9C7DE2407E1AE0914065441503A ();
// 0x00000088 System.Void UIController::BackToMainMenu()
extern void UIController_BackToMainMenu_m566A10B2D4279648C5571817A698374B7C4E252D ();
// 0x00000089 System.Void UIController::.ctor()
extern void UIController__ctor_m2957FB6041970C1305D4DED3E11D4D800E932486 ();
// 0x0000008A System.Void UIScreen::Start()
extern void UIScreen_Start_mCF30C23A5161CCA9A2C720B9061AE2A3F2E4B995 ();
// 0x0000008B System.Void UIScreen::SetEnable(System.Boolean,System.Single)
extern void UIScreen_SetEnable_m6FCDA1E814AD709ACABE9F8912AB9D5668845BB4 ();
// 0x0000008C System.Void UIScreen::KillTweens()
extern void UIScreen_KillTweens_m709DC3EBF51AC5AA937990F4692168BDC570CA0C ();
// 0x0000008D System.Void UIScreen::.ctor()
extern void UIScreen__ctor_mECFB4FA666EDFD74496FDF2177AAA212DB25C024 ();
// 0x0000008E iOSHapticFeedback iOSHapticFeedback::get_Instance()
extern void iOSHapticFeedback_get_Instance_mB7F77E6A6747AC5A5B31B011F7C6E76D0C528563 ();
// 0x0000008F System.Void iOSHapticFeedback::Awake()
extern void iOSHapticFeedback_Awake_m2B3AD9EA7D99E6B97C8EE2E386C274F49D902BA5 ();
// 0x00000090 System.Void iOSHapticFeedback::OnDestroy()
extern void iOSHapticFeedback_OnDestroy_m926230A2B3DC73404ACECA5E9F96C79631E18674 ();
// 0x00000091 System.Boolean iOSHapticFeedback::FeedbackIdSet(System.Int32)
extern void iOSHapticFeedback_FeedbackIdSet_mC6951619D58597EFACB8A9EB1C0262DAD6E07FA6 ();
// 0x00000092 System.Void iOSHapticFeedback::_instantiateFeedbackGenerator(System.Int32)
extern void iOSHapticFeedback__instantiateFeedbackGenerator_m485B13D16526BEE1E535DC98122E3E90C1D444CE ();
// 0x00000093 System.Void iOSHapticFeedback::_prepareFeedbackGenerator(System.Int32)
extern void iOSHapticFeedback__prepareFeedbackGenerator_m20D315216C35EE20D0EBAF13C8A336827A976793 ();
// 0x00000094 System.Void iOSHapticFeedback::_triggerFeedbackGenerator(System.Int32,System.Boolean)
extern void iOSHapticFeedback__triggerFeedbackGenerator_m92F1B1CE0892A4076A40F0D2042F1324BD69DF6A ();
// 0x00000095 System.Void iOSHapticFeedback::_releaseFeedbackGenerator(System.Int32)
extern void iOSHapticFeedback__releaseFeedbackGenerator_m7933EC6328135286DE847B0B60849FDC915D564B ();
// 0x00000096 System.Void iOSHapticFeedback::InstantiateFeedbackGenerator(System.Int32)
extern void iOSHapticFeedback_InstantiateFeedbackGenerator_m5086A4AD681ACB44A2D44BA120CA515CFCEE6341 ();
// 0x00000097 System.Void iOSHapticFeedback::PrepareFeedbackGenerator(System.Int32)
extern void iOSHapticFeedback_PrepareFeedbackGenerator_m94FB54E3FF19057F0AFB4B2DB45AA5651598F5B6 ();
// 0x00000098 System.Void iOSHapticFeedback::TriggerFeedbackGenerator(System.Int32,System.Boolean)
extern void iOSHapticFeedback_TriggerFeedbackGenerator_mEDFD54D523DE0AC8BE7AFEB3769180EACF60B419 ();
// 0x00000099 System.Void iOSHapticFeedback::ReleaseFeedbackGenerator(System.Int32)
extern void iOSHapticFeedback_ReleaseFeedbackGenerator_m748BBFF7110A3E507BB0E9730025226A73EEDBA9 ();
// 0x0000009A System.Void iOSHapticFeedback::set_IsEnabled(System.Boolean)
extern void iOSHapticFeedback_set_IsEnabled_m3D3E4E00FB4A577E66E41D7074C3A6DB506A1A7A ();
// 0x0000009B System.Boolean iOSHapticFeedback::get_IsEnabled()
extern void iOSHapticFeedback_get_IsEnabled_m6778DE6BAD2680971785BA0FEA9DE38DA3BCEBC2 ();
// 0x0000009C System.Void iOSHapticFeedback::Trigger(iOSHapticFeedback_iOSFeedbackType)
extern void iOSHapticFeedback_Trigger_m20779B1B65C4AB79C6B8CC375ADDF412CF43C62F ();
// 0x0000009D System.Boolean iOSHapticFeedback::IsSupported()
extern void iOSHapticFeedback_IsSupported_m943DF4786E61C0359A79361ADAEAA38A903597FF ();
// 0x0000009E System.Void iOSHapticFeedback::.ctor()
extern void iOSHapticFeedback__ctor_m0E95DC12A646136D1516359171A7082421C8BEDA ();
// 0x0000009F System.Void iOSHapticFeedbackAdvanced::Awake()
extern void iOSHapticFeedbackAdvanced_Awake_m54080BFF6E392AE392B89937FF40B8593A74ABFB ();
// 0x000000A0 System.Void iOSHapticFeedbackAdvanced::Trigger(iOSHapticFeedback_iOSFeedbackType)
extern void iOSHapticFeedbackAdvanced_Trigger_mA455D72E0EB6A27DF674F5EA0E8E8D8FD3AAA40C ();
// 0x000000A1 System.Void iOSHapticFeedbackAdvanced::InstantiateFeedbackGenerator(iOSHapticFeedback_iOSFeedbackType)
extern void iOSHapticFeedbackAdvanced_InstantiateFeedbackGenerator_m591ECFAD58AA363551B1ADFFA1A54B7E96BC9E16 ();
// 0x000000A2 System.Void iOSHapticFeedbackAdvanced::PrepareFeedbackGenerator(iOSHapticFeedback_iOSFeedbackType)
extern void iOSHapticFeedbackAdvanced_PrepareFeedbackGenerator_mDA48F96DFBF2C65AF6E91F0DA9E816D6257A902E ();
// 0x000000A3 System.Void iOSHapticFeedbackAdvanced::TriggerFeedbackGenerator(iOSHapticFeedback_iOSFeedbackType)
extern void iOSHapticFeedbackAdvanced_TriggerFeedbackGenerator_m69A0EF4CBE3A99DD7E1199A7FED7069A07952462 ();
// 0x000000A4 System.Void iOSHapticFeedbackAdvanced::ReleaseFeedbackGenerator(iOSHapticFeedback_iOSFeedbackType)
extern void iOSHapticFeedbackAdvanced_ReleaseFeedbackGenerator_mFA1EBEC4AF6646BD5EB9207B99167E12AB06266A ();
// 0x000000A5 System.Void iOSHapticFeedbackAdvanced::.ctor()
extern void iOSHapticFeedbackAdvanced__ctor_mB57B4FC651F2B5260C142D65605088ACF8EA0B9D ();
// 0x000000A6 System.Void iOSHapticFeedbackExample::Start()
extern void iOSHapticFeedbackExample_Start_mD21E55539E0838F57E9727ECF3BEBDBCD0C36CE7 ();
// 0x000000A7 System.Void iOSHapticFeedbackExample::OnGUI()
extern void iOSHapticFeedbackExample_OnGUI_mAC4803959151DC16C198C5625B5C3E46F1ABFDF4 ();
// 0x000000A8 System.Void iOSHapticFeedbackExample::.ctor()
extern void iOSHapticFeedbackExample__ctor_m0EA255CD27C9B5DA3F0CFFC3DF176CF6E32B6A50 ();
// 0x000000A9 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_mB895E6623DB3DAE93D395FD5F885451249738498 ();
// 0x000000AA DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m17470F82E3757914C9F0B77F5B812555AECCBD60 ();
// 0x000000AB DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_mEFC74DBA5F02D4CAB1CB545F37AD3198ECF94B03 ();
// 0x000000AC System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_m8A094AC09079A9F5CAE8527E8022F194351068BD ();
// 0x000000AD System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_m4B31EA97960FD9FF2BABC3C29C7B32B5C44D08B5 ();
// 0x000000AE System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_mA847EA9957D83FFD582BB51F5DCB2F3809398678 ();
// 0x000000AF System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_mC314805229AB8931AF1677599FF847354113BC6A ();
// 0x000000B0 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_mC28F1284009712995255B7A90403C1DA50A1903C ();
// 0x000000B1 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m57722E0D66F4CBA2D5AC2BE166D3F987A34C1A53 ();
// 0x000000B2 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_m7DB5FA484E3AF8741F590DD8F4F3523DDB1BAF76 ();
// 0x000000B3 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m87F1773D251CCA2094CC79576F141EF098AB3FFB ();
// 0x000000B4 System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_mAD32A3398DC4FC3D24FE6527FBEFEB9D180FECD2 ();
// 0x000000B5 System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_mE2B095AF13DE79B9A9B91FEA6A117F5F3DB75772 ();
// 0x000000B6 System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_m8F7868428006B0ACC315E87192D4CEA145110A0D ();
// 0x000000B7 System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_m6A9131F6CF8CB82FF3AED0AB0A831B2537ACB492 ();
// 0x000000B8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_m04DC92B1D5473A3B74F00EA5FB7BD483EE61368A ();
// 0x000000B9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_mE1712777B02AE15A8856F510D99E4D8B2D29BE69 ();
// 0x000000BA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_m0C7A58A9F9A2C64472B136F042233E693FCE5A4B ();
// 0x000000BB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_m51E2D9D00889980A6F3069E30F1B3AECE9A6F56B ();
// 0x000000BC DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_mA7DF8F0F38CEE91C8E22EBDD08779349B22EB2E9 ();
// 0x000000BD DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m6DE60A979D3F8FFC0BCE231438B81B60E21118E2 ();
// 0x000000BE DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_mA4C40AD22FDF758374978E6AA9444B8AF5E28968 ();
// 0x000000BF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_m76DAA5B3B088E540C09412C4BA00E73977BA72B0 ();
// 0x000000C0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_m8F810CF905D7C16FCFDB2DDE6747D417D2A1276E ();
// 0x000000C1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m7E33EB3323693ECFE3478860191F90EFA89BEFEB ();
// 0x000000C2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_mED288BC25D9DB70757E2841CCFF079857EF7E616 ();
// 0x000000C3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_m7DA7D6F281AD23631C31951A782D109672BC6084 ();
// 0x000000C4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_mBB34A371FE4800F1592211FE14691DDBB25A7149 ();
// 0x000000C5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_m22C5E230A66BD275BBA274D57212CF301197CBAE ();
// 0x000000C6 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_mFD2622F30699A2D839E059003BD0B6F80D320134 ();
// 0x000000C7 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m1A08125A11636A168F3D5A95DF552D654C3ACFF9 ();
// 0x000000C8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_m722030927677D8D607C4C955F66250EB478B59C7 ();
// 0x000000C9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_m3B5756EC0481C06E0571536C740FE5A6C2A2FD84 ();
// 0x000000CA DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m22EB92798058929D14E5EB0CB534C5C0344CD947 ();
// 0x000000CB DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_mAC0C5C5596B2AF63F4A57623F211838CD2C1B597 ();
// 0x000000CC DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m9263D65172482504B1A046F5E511490C181A866D ();
// 0x000000CD DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m758BBB01C103927CFE361F9BD19DFD0E1BB23B2E ();
// 0x000000CE DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mDC20440A77656C4E43F78479151649345858500C ();
// 0x000000CF DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m63BDFF1F39C303626D0F0DA3E9FE1A213CD90711 ();
// 0x000000D0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m38A550C7A51BC3891DE26F8952B2F7916B984EFC ();
// 0x000000D1 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_m82803C7BF3430F67DCFCDC208DBED116414217DA ();
// 0x000000D2 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_m3B2EA7BE2B53D8EDEC23A8EE53D10D496BACE503 ();
// 0x000000D3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_m80D6AEFA43E147F6C4234B41EA6406C78BA18E9B ();
// 0x000000D4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_mE96A5A92FE347C318D5B6A59F0ED27D98A32CA67 ();
// 0x000000D5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_m540CE78577237D1BE6FB99AB9F8143BDECE08432 ();
// 0x000000D6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m226D8AC396F10ABCC171E1E9ED878D62441A1816 ();
// 0x000000D7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m61F3E3A436DA82F5E4D85264C80E50F0E2A93943 ();
// 0x000000D8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_m3F492F86DA293CDA9A27823F49FD50936D9F84E1 ();
// 0x000000D9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_m4B7DAECB0E1FD429435C2FF8612CFF7E0F5717E0 ();
// 0x000000DA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_mC0CA1A85842B3C1E4EF2EB6514B9AB551368CE9F ();
// 0x000000DB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_mEC0CDA9E3828E1D703A144620A6187CF950A2D69 ();
// 0x000000DC DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m6B937277DF2B389D169B89AE08947544C5FA4924 ();
// 0x000000DD DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_m4FD8BFCB31904082D7470D0D19D475310D71AF15 ();
// 0x000000DE DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_m1D8B7CF1E41562D3C8B0960FC28ED77054D2000A ();
// 0x000000DF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m876869289951806B828EF9A0644CAEE20FB72AFC ();
// 0x000000E0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_mC7791F2E0F55FF48FBF696839E5D45E9021C36C4 ();
// 0x000000E1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_mD85F4855FB8989FEB6A75C08E0C86B71A073DC32 ();
// 0x000000E2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_mBEE2FC4EB705017C440341F0F5ABAD41A9D1E6E7 ();
// 0x000000E3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mECA1AF29FBAF77B3FC310E8FCC0CC96838F7BA99 ();
// 0x000000E4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m4148FAE93214CB89245428CDD8AB69F3062D75E0 ();
// 0x000000E5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_m238A4EC18672EF8B9D4B236E5B34BCCC37C4F354 ();
// 0x000000E6 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_m82CE87E5ED9596485B8EC26743E6760D498D582C ();
// 0x000000E7 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m1BC1F1222951F602046B9265C9C5389DE8E8E190 ();
// 0x000000E8 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m75E868D2A4EACA8E9A94DEEEE842FA79EA120108 ();
// 0x000000E9 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m69F7916D909CFEF1171DCCF294AF1D7ACFE5AEE4 ();
// 0x000000EA DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_m30FC5B6D303C26D4299C80BCC149BE64A380B6D6 ();
// 0x000000EB DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_m970CAE4EEEB626E2C95547AA3C9485F924576C17 ();
// 0x000000EC DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_mD853402020AEC35415931A321C0E33B294A58E66 ();
// 0x000000ED DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_m1CD78CEB8F29CDD7C28CA0D4ED6407CFB95F4A91 ();
// 0x000000EE DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mC5895F9A92931B09FED80AD3CBAC42C13A01CB83 ();
// 0x000000EF DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m4BC24CDB20DA7FB142F8CC1759978F1073267025 ();
// 0x000000F0 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mFC30460ADF17ACFF852662753B4FFFA0CDB5FB0D ();
// 0x000000F1 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m4305297962324398C6A0170D689EB10EB8D805A1 ();
// 0x000000F2 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m370BD2A767C8A6BD46561ACA8B32487BD48D42CD ();
// 0x000000F3 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m98F474F73DBDEE43363853C8AE86CD9A74E0BF5A ();
// 0x000000F4 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m0101EE36EC8ECF73C0A6F30AA16A69C2ACD7E1FE ();
// 0x000000F5 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mB4E72995236B85B6B1AF56D0F365507DAB1B6EA5 ();
// 0x000000F6 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_mBFB806E7A92BD3176C7F3C7481C2F16724B69E9D ();
// 0x000000F7 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mCB4E8C1B6691BCC003E09C905F03CE3AA0DB1BE2 ();
// 0x000000F8 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m6DEAF0B60254A2E4DC457DFD820DC701CDD6084E ();
// 0x000000F9 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_mA6045B988E998B9113509A6D0C508142727D9347 ();
// 0x000000FA UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_mDABE7C2BD065537A4D28300F4A4C1E835B8B8332 ();
// 0x000000FB UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_m8B396ABEABA9DD73328B263095CB0884A03CFE38 ();
// 0x000000FC DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_mBBABF79C7EC8FB51F312A3A9CD3E49CEF1DF2B6E ();
// 0x000000FD DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_m14C9D0A3D9AD487687474B0F243C9656E20DC697 ();
// 0x000000FE System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_m4A1880D2D4CAB505023229810019E6117C546C77 ();
// 0x000000FF System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mC0D1235B1B75C39513919D943AB88D5EDF24E7A8 ();
// 0x00000100 System.Void EndPlatform_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m0AB73C0AB407F20CAA2302E8B0526707B3762DBD ();
// 0x00000101 System.Void EndPlatform_<>c__DisplayClass2_0::<FireParticles>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CFireParticlesU3Eb__0_m5F9813A924F8C77F70F38E5279ACBE3FBBE85500 ();
// 0x00000102 System.Void GameController_<>c::.cctor()
extern void U3CU3Ec__cctor_m62FEF400139913C37ABFC52C1E1A374FC9958CC9 ();
// 0x00000103 System.Void GameController_<>c::.ctor()
extern void U3CU3Ec__ctor_m28478222F1AA4BE5804FB2D3488A3897F47EE76C ();
// 0x00000104 System.Void GameController_<>c::<ChangeFog>b__51_0(System.Single)
extern void U3CU3Ec_U3CChangeFogU3Eb__51_0_m60EA28D4836EA64DA416067D47DF893594CF53CC ();
// 0x00000105 System.Void GameController_<>c::<ChangeTimeScale>b__52_0(System.Single)
extern void U3CU3Ec_U3CChangeTimeScaleU3Eb__52_0_m8F336F46262B49AD040B7EFFC3B5B6A9868D0175 ();
// 0x00000106 System.Void Hoop_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m7D90E3D6D47268EA1C979E7CC6EBD26D17523290 ();
// 0x00000107 System.Void Hoop_<>c__DisplayClass9_0::<OnHitCenter>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3COnHitCenterU3Eb__0_mD151406F87F1F730A181FA63F42852DF097C6B83 ();
// 0x00000108 System.Void InputManager_TouchDelegate::.ctor(System.Object,System.IntPtr)
extern void TouchDelegate__ctor_mEBE98E56AA1183D5F1053FA131FFC467D001733C ();
// 0x00000109 System.Void InputManager_TouchDelegate::Invoke()
extern void TouchDelegate_Invoke_m714BEEC32B3A0A0200ACB1402329C99EC7D18F68 ();
// 0x0000010A System.IAsyncResult InputManager_TouchDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void TouchDelegate_BeginInvoke_m203840362C924FB41611E3E79BE6CF41CEC2D7F9 ();
// 0x0000010B System.Void InputManager_TouchDelegate::EndInvoke(System.IAsyncResult)
extern void TouchDelegate_EndInvoke_mCFDCC3D286F47107CF2D7AE480070B77CE7873FF ();
// 0x0000010C System.Void SlimeController_<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m106DF84FBAD9A192E82B54A8FB5B8F50334F61DC ();
// 0x0000010D System.Void SlimeController_<>c__DisplayClass43_0::<MoveForward>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CMoveForwardU3Eb__0_mE615767DF57E7747D0873EF58043B2C76D4D13F3 ();
// 0x0000010E System.Boolean iOSHapticFeedback_iOSFeedbackTypeSettings::get_Notifications()
extern void iOSFeedbackTypeSettings_get_Notifications_m207B452FE7C525C9114388B2F4D248BC1C806CF5 ();
// 0x0000010F System.Void iOSHapticFeedback_iOSFeedbackTypeSettings::.ctor()
extern void iOSFeedbackTypeSettings__ctor_mCC383DA9AF3D04EBCCA175B446FF1C354A48ECA4 ();
// 0x00000110 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m410489F815730C05633CB198EAF29184C61E5DD8 ();
// 0x00000111 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m553B5BF47DC863789E86A5BA9C58219CBFCCAE7D ();
// 0x00000112 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mA503D22951DFB1756D2093D25A534F4DE527EA1A ();
// 0x00000113 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m668929D1037563C4BA05DC1C6D49208EFB41B5FF ();
// 0x00000114 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_mFEC0334FA56515F8E10FB4B573547AE339A7F3E6 ();
// 0x00000115 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mFB24EA119BE93AD9F7BD52CCA50955AFAD91E1FD ();
// 0x00000116 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m0427EA7A432602D7490FF082CB68BCAD71A86189 ();
// 0x00000117 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_mF3065D336F54345A67A41AB553F91B12E56A41FF ();
// 0x00000118 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_m7E51751C14C21C8E8A1AC3BB06E95F16EDF13535 ();
// 0x00000119 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m80ADE6AD0A6AE24E88C1763E70FD33C833B502B6 ();
// 0x0000011A UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mBDD978F428BA10922F372EAB21DE234526590F98 ();
// 0x0000011B System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m61766A5644E878F1E5F0B033A602BA12FE6B249D ();
// 0x0000011C UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m7CA25B5D3253D1A1BB83175835E6F06B37E6D3CA ();
// 0x0000011D System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mB8B190685AB0F803C260CE1CB8A4500980441B7E ();
// 0x0000011E UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m475A4066BB6D47109DBDA7711674E92F8B26F80F ();
// 0x0000011F System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m53D2E1E6F2D6D8372B286983781D03A34E557CE0 ();
// 0x00000120 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mFAD55624F73C8C7678B40AE2B544A4E21440093C ();
// 0x00000121 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m877BF0BB29B71DB544629A0FBAB3A16BC819E75D ();
// 0x00000122 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_mCA650E955F3C9B985D9781B4E88F9EC8E22B4DF9 ();
// 0x00000123 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m9310E069CC7950951613CFC194B0C3906C92A052 ();
// 0x00000124 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_mBD3D65C2B6DAE37DB5CBA1BBF01D666A2908E540 ();
// 0x00000125 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m4B17E82EF353958EDF46FD0D37B988F0A41BA5BF ();
// 0x00000126 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m04813310411D22F519E3142909FCF08E4A83DB05 ();
// 0x00000127 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_mCB70FEE3ADA2EC1B74A0A7BB60CD8274F9838C02 ();
// 0x00000128 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mB47776ADD5831D4ED18EDE2DDE38A858AC92A9C9 ();
// 0x00000129 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m67EF6E7E97403AB9053D7811A551A6D052D90CFD ();
// 0x0000012A System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m287B8EED4C4A9357AD78642E3F2914D8FABE2805 ();
// 0x0000012B System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mA561D07841A4F7CAEBBD7114F3ED4151EDD788E0 ();
// 0x0000012C UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_mC71BA0A22CF6F7A1ACEC09FC99AAC8B963F653F8 ();
// 0x0000012D System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m8AF8F5129417528E73EBEDE7BA21211B3C38E5F0 ();
// 0x0000012E UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_m9C60CDEB2CFE741DF9088E45428A7881F1E7E398 ();
// 0x0000012F System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m91D6C114DE18B6EA58C6C7813E906870095F035E ();
// 0x00000130 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m8B993A11861859EDAC694573BEFD4FC35DA13044 ();
// 0x00000131 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mF111BF5DF70FA94EDC3C3A6F0D8A299272BC3262 ();
// 0x00000132 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mD7B65E3231BBB518F764D92874504A6E817A3A09 ();
// 0x00000133 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_mBC510690793F98CC7A4CEC293AA1C1420D77B006 ();
// 0x00000134 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mEA0F5B946589BAA3FCB20D2E150440CE5294E6F3 ();
// 0x00000135 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m76BE70D5858C4B19FFDF20DA088F62D45CEFBDCB ();
// 0x00000136 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m7AAE48D11CDA351B8BD9555E638AA2AD3B967DDF ();
// 0x00000137 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mD8CAC14A3F48F66250EDA54950744A54CE2430B0 ();
// 0x00000138 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m6317E8FB8A070BB91F81C94666F3CDA20A2E43E5 ();
// 0x00000139 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mF9F03ED121A568F712D5265B86BC8D836F5D712E ();
// 0x0000013A UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4DB120064C1A7D8A47A52371A6E532DACA6C73A6 ();
// 0x0000013B System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m45136E0EBA7276A3B440B3F1C0FB5969A8F10EBF ();
// 0x0000013C System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_m738875FC6E09B036EECFE32ABE5F31BBB6399E52 ();
// 0x0000013D System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mBC3F126EB212356DA892532E6FEEB0E144ECF221 ();
// 0x0000013E UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m1D63D0BA78EB39F89A1B1E066912FFE44287B21B ();
// 0x0000013F System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m05BFB77CECAA68F50F7F6AC593F37CBD3697B170 ();
// 0x00000140 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_m312338665037BB1F402A90991F221C28C1BAE39B ();
// 0x00000141 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_m9B07E2AACF2C5AE01FF2BF681C2CB7D99007B0BD ();
// 0x00000142 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_mC9A5921C18BDFE2191C1841EB12300FBB4A71A18 ();
// 0x00000143 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m1C478D59001D4FE6DE43383B960DF92F0BC99182 ();
// 0x00000144 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mF60CDB37F0CFBB33DA3BE248538EFFC167065618 ();
// 0x00000145 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m19FA6E77EF091166B632107C37CF96158B3CE288 ();
// 0x00000146 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m956569E1E9AEA53B62AEEDA64CB966B74E83A499 ();
// 0x00000147 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m52C917C39DD1186239C950566F8F09210E71F4CD ();
// 0x00000148 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m493943ED4559E6518634D0901230AAC5808BFEDD ();
// 0x00000149 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m83A9CE643617C1B0EEA1F7AC43247148DD77A124 ();
// 0x0000014A System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m6BB3107242E9DAEDE2A970A9842DBC6997F0A934 ();
// 0x0000014B UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_mEA5E8386E195FF3054C4AC1F7C46FCFA2E3095F5 ();
// 0x0000014C System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m5E38544B8C65CDD9BC4A612E4672CFAA22344B6E ();
// 0x0000014D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m953E8B35B59142D580B1EC5A3CB48163D94FE270 ();
// 0x0000014E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m4E16EB0B8FD5FBE168DFC737535ACACA42FC93D2 ();
// 0x0000014F System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m926D478A89E47B6AB847D5FE75BB68C92BC64B5A ();
// 0x00000150 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1C04B14892E7626ECA44D1777458053AB2B3E7F0 ();
// 0x00000151 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m2F7C7DD6D0645D565EB083D9B9346E65C2D8847E ();
// 0x00000152 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mD5A08FA687B4F3D4ACDE03C5F4747ADD5A85ACE8 ();
// 0x00000153 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m0D02D00E3A1DE8AB6AC3B91D071603A4A5449181 ();
// 0x00000154 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m1414476DA1A717B266BFBC3A676501101EB18CFE ();
// 0x00000155 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m96E8793AEB0F5454A98699F2A492CD0C6A6F29D9 ();
// 0x00000156 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m7EAF02255761CC601956430B1E80C4EEBD932ABF ();
// 0x00000157 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mC63FBDC1D1E28C1067D7FB39AE7DB4A86CEE9CCC ();
// 0x00000158 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mD60F568B3218FB775B0F2F472DEF66F12F240EB8 ();
// 0x00000159 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m3EFC6D8D38659950BD09A97C858526829F838657 ();
// 0x0000015A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m0773FFC61452C361ED706D7A144A12E29C5AE7C0 ();
// 0x0000015B UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m2ABDE00A2CE387D94979FF4772233C32E0433002 ();
// 0x0000015C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m6C458B0BB259B641E486624D57F7ADB3CE77DBDD ();
// 0x0000015D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mB4AA8029171F09FB5B569FE0064569575122F320 ();
// 0x0000015E System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m8284CEE2126386F18071835A8EF76549F57C802F ();
// 0x0000015F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m8F2913B41E7748055C6CF28FED759CD593F72D9F ();
// 0x00000160 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m77C731D6C164BD65C9879E2113BD8385855E8A69 ();
// 0x00000161 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mFA007EE23041AC3DB626E202537641F7CCC10895 ();
// 0x00000162 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_mDAB5785D80675BB3E337A7F835A8E506CD18DBE7 ();
// 0x00000163 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mF74EAB9037A10D32AA8A2E62C76AD529AA2F939F ();
// 0x00000164 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m5AADB9E424374D66A929CAE4FE7C91FAAD9C1F92 ();
// 0x00000165 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_mC28A35FB645B9F98D92597606CC43BC0C762EAE5 ();
// 0x00000166 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m61D4ABCFF239F5E7E8C3D839AA726D2DC94F90AD ();
// 0x00000167 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF3EEFDE0596A75FA4C9B5E3CBB8D27B37286B7D7 ();
// 0x00000168 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_mE48AFB91AA0459196E8D673F66FFA6603733BD9E ();
// 0x00000169 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mDF8853F408C48F0DEE4BDB520B12086A4A96584E ();
// 0x0000016A UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mD23FC3413F649B7175F76650D99C969D080A52B6 ();
// 0x0000016B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m622D4E29805ED0018C2F07663A12F884A3867E80 ();
// 0x0000016C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mD30DE6D8FAF4F2CEEA758C16C34081ABB19A1FE5 ();
// 0x0000016D UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m97CEC28FB18E8EC25DE68CBBC9363C1969199D92 ();
// 0x0000016E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m9C8D55E54BB459893598BBA3A01FAE16F19707F2 ();
// 0x0000016F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mE8231F7A5382654DAC80084A0615F0D62D59B80C ();
// 0x00000170 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mD4ACC050241FAF74718A82BAA2FAD799B221D52F ();
// 0x00000171 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m3CE46E157F858D6666E562E24732ACCD51286D3A ();
// 0x00000172 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mBC215B2133A5003258CA482F6BE1FD938062EBF8 ();
// 0x00000173 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mEC1BFCBC158066EE11442276BAF4904FC8167F78 ();
// 0x00000174 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m8D2760C43F03B2EBB991BDCC324BE7DB9DF90349 ();
// 0x00000175 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m34FD9996E6407C44D5B51B824B5E51B37B802273 ();
// 0x00000176 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m946B10547ACAFC05288900D6C8AE8BE29FA8E769 ();
// 0x00000177 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_mD44EDB300AE68F3CF9F9C05A04736FEBC36A62C9 ();
// 0x00000178 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m155F27D8B139E97FC6CD758489ACC4006C876F3C ();
// 0x00000179 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE74B0E96F68D07C8FB2B95F696DDB3F3EF90633E ();
// 0x0000017A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_mD2751A8D2B91C8628463F8817C56481A58ECCCF6 ();
// 0x0000017B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mCA56D7F89996A8B5FF1B0720AFE3142B501CFB1F ();
// 0x0000017C UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m54AA9D5E75139ECB7FB5D0BF9518849E9258416F ();
// 0x0000017D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mC8E507C07915B5AA9C4D033EED58F7DD384CEEB4 ();
// 0x0000017E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mF0C8F175B7992C1F263828E8951B3D7CA8EEDE95 ();
// 0x0000017F UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m7C05989ECA103CD60B5CF731D6C7920B1FC985E1 ();
// 0x00000180 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m082B6F826D56C521A15A1C2AC2270EECD5494B4B ();
// 0x00000181 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mB41226EF2155BF4759D574341D00D48FA8147C1E ();
// 0x00000182 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m9D5E91C0F8BF3135CA706B2D09504A99E384E3F6 ();
// 0x00000183 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mB6CA72A3DB84D4A8054678A74B9F90E1D2F4D7C6 ();
// 0x00000184 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m6F95C61CB46CCE9AD6E7F2C68828F7E2B3F8F76F ();
// 0x00000185 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mCE069FE82ADB466CBC0DAFC700BC15C8613B138A ();
// 0x00000186 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mA99BE8EF43FD3FDB2C2048472124B8E9535C8D21 ();
// 0x00000187 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m37A420AB4F75727ED565429F5255310B227A4793 ();
// 0x00000188 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m0D36293A6361A326C0853382366377CE10796BC9 ();
// 0x00000189 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m2363ACA3A36A35D8F816953CFCFA314AB44BF64E ();
// 0x0000018A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m3C4DC31D280101BD1F83B7AB6847D855F3D08554 ();
// 0x0000018B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD66DD96927DDAF369C64AE446E76D6F0F03AE0F5 ();
// 0x0000018C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m87EC5E0454FFFFCCA1F39624496EC189D9C20DF9 ();
// 0x0000018D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m0F878681308922D788307AC39714D59D3CC00000 ();
// 0x0000018E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m7DC62E4EE195C844218B14EB4EF6E5AED8989D0D ();
// 0x0000018F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mCD92689E52DE74483FF9CEF188D080AEA98FB48B ();
// 0x00000190 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mCDC8B6E3F2F9175E0A55357DD876C0CCF4641FD7 ();
// 0x00000191 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mF72A07116DE5256A691DBAFB766E637F55C4EF45 ();
// 0x00000192 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_mF7F854224FB363EAC67951EA7DC121032840607C ();
// 0x00000193 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mA365D90EA16C25D6D26531196AFC1C4BE0390854 ();
// 0x00000194 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_mADD1E688B9294ECD2A7CCD79F78CA0CD68E055B4 ();
// 0x00000195 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mFAB6BC6C0DF65DE9FA072F42567FFB24A1D3C154 ();
// 0x00000196 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m393618009F4E2F97836536F9AB76EEFD8A68FD36 ();
// 0x00000197 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m036AB1FD8ACAAB27669C6F82B807258A7CF95161 ();
// 0x00000198 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m08257CEDA26D13E5A8633A6BAE8C6D6C3554F606 ();
// 0x00000199 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m28F1AD8814B9CBC6855A48A00E2E8C2743C7540F ();
// 0x0000019A UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mA1CDB05B94AFD8D48E1B1DB4B6F2F53FDDD274EC ();
// 0x0000019B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m8D001C76DD8E98541DB03FF4C6B9D60AB1773F48 ();
// 0x0000019C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m009781B788D8BF90494B3AAA73704BD4E17B4622 ();
// 0x0000019D UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m23ADE1EDABB1FF436DCB15B4D2956341A403F86F ();
// 0x0000019E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mD71967EF4D17CF7059F992D8790F8AB52F83972E ();
// 0x0000019F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m7EA6439CC95C5AFA962A1F2B6692EF6AC08D2FDE ();
// 0x000001A0 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mBD11B2BA0B8A07753156C1F9CA22B33A041F6A28 ();
// 0x000001A1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m157A05E0EE65A300C5AE80828B38F95123127F44 ();
// 0x000001A2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m46346E5D03CEBEFD04ADE8670C5A9A3D2212ECDC ();
// 0x000001A3 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m3336D670200C9F13D96D7F52F123920132234850 ();
// 0x000001A4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m40CA655FF5E56CE5405C95B10113A9C4AB18DDF3 ();
// 0x000001A5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_m0DD9D7FEE1DB03692260F3B293B431E729C2213D ();
// 0x000001A6 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m0D9E7219678950432D0E913F31C5A3E3B3D1F449 ();
// 0x000001A7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_mA0D6B7FA51A3D9BAF9A71FF8C94B458A56B88E03 ();
// 0x000001A8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_mACE1C1FC22CB19EA9BD92589C0AD1D45CBF5E9C2 ();
// 0x000001A9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mE46F8688A15AA59EAB6E8BAF0BA3DD844691F1F3 ();
// 0x000001AA UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_mE69AF155BCC63288298C03F1FDE8264894DC5112 ();
// 0x000001AB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m6E5FB0266BC13BCB695DD7B72DC74C19CCF0F3AD ();
// 0x000001AC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mFB9DA92206594AD9D941461DEDFE8484C3068A2C ();
// 0x000001AD System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m5FB527945F0FF1ED7B9DCC177766E747CCFA63D4 ();
// 0x000001AE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_m26811C4F5F41671445F1A02B0BD88A3FA396D990 ();
// 0x000001AF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m70919EB25B6ED305D785B69422AD8C7B22AC867B ();
// 0x000001B0 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m23B9440FE647ED7E52A734B8B3971D2DE0A2B276 ();
// 0x000001B1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m86FAFD38DD8654C5ED6F11C838E7FBC33AED9361 ();
// 0x000001B2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m65100B9ABF5D6F5D6391265FBEF999F71DB4F89E ();
// 0x000001B3 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mDEE07FC2B98C7E5BFE645D4CD8E6E99E95265F70 ();
// 0x000001B4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_m5130DE7DC7811A77FAECE6D41B69BE332B0FE638 ();
// 0x000001B5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mAE1DE5261C2A864B7AC53DCDEFFED3AB330EC2B2 ();
// 0x000001B6 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_mC0172F2115741150E114BCEB8C6366E8DC05F0FE ();
// 0x000001B7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m62239333E0B0AE84382D45A8C2BDDB73DF94212A ();
// 0x000001B8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_mEBAE6DC4503B8E19743BBBDD12FFFBF036CD1029 ();
// 0x000001B9 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m7AEADCAF76E9378EF16998AF06C981E238317A14 ();
// 0x000001BA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_mF1633BA0B0E3A39E320BDFFD1D2AC6DDAD08D0B0 ();
// 0x000001BB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m527FD2EC7D877B6441B750B1F8A0315EFD095D66 ();
// 0x000001BC System.String DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m9CF7A71E7C6B7F3D963E8151E8B19AF7BF38D765 ();
// 0x000001BD System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m8EE1D16A9753A612F4F2D0660BD6B035092E918E ();
// 0x000001BE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m57780CD760FA32CFF80E9DE9B259D652AE0E538A ();
// 0x000001BF UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m5C210BC6DB5E81E37C5DA7F0ED05DCEBF88A838F ();
// 0x000001C0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_mD1F0C265C9C64AB81DDB9E6A3F2924B7C01F5EDD ();
// 0x000001C1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m0CB071B9A6D2A3B18B3313D2FC19584BC9DACD37 ();
// 0x000001C2 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mD045F88560E9C38E36C045962A24AEEE7E220146 ();
// 0x000001C3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m1815F9D5038BEECB23F5DD48BCE7092C8B789DE2 ();
// 0x000001C4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m95A2D9F9240BB4DBDF145D090616BCC0AA6BE30E ();
// 0x000001C5 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m769FD067F1DD156E6737065978D723626F684DD4 ();
// 0x000001C6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m806AD3EE1B7A414688748F18E0046359989CB5CC ();
// 0x000001C7 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m825753EB38BCD235FAA91B4EF503347E3A72C6FC ();
// 0x000001C8 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8CB04FB886F55DF914E092EAB3E88CADA5E4BFEB ();
// 0x000001C9 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m64C942C0D9E832DAA71AA441F1C8EC739F01BD06 ();
// 0x000001CA System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mF144DEE04A5A997D037F570C7FFCB312A4553C5B ();
// 0x000001CB UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m45117726269A56BEFE7511E185A5EB5DC6614C08 ();
// 0x000001CC System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m35E74CFE50BCF6B890BA0F6E12021B724333EBBE ();
// 0x000001CD System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mD664D3B152C13FA9775F88258EEA369A4F9FE40F ();
// 0x000001CE System.Void DG.Tweening.DOTweenCYInstruction_WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_mA5238710C0ADAA5121265E2E659A2F2D4266CB23 ();
// 0x000001CF System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_m23B6817979AAC4F440AC30142185C5FE053112B8 ();
// 0x000001D0 System.Void DG.Tweening.DOTweenCYInstruction_WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m98C421AD48771BD13EE0BE37799B6876F43591F5 ();
// 0x000001D1 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m44BE0063D80EC43815DB16A08852A27C61B01C0A ();
// 0x000001D2 System.Void DG.Tweening.DOTweenCYInstruction_WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_mFA1D55A21E90BE45AC7F19BCF615F962BC08B96D ();
// 0x000001D3 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_mFCD43EB01C4ABA9A3FA10EE1594C1D16208DBE74 ();
// 0x000001D4 System.Void DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_m1A5F9AB280D57365E4E4329DD228940F0F5DFA67 ();
// 0x000001D5 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m0C2BF58FE1A0E6C2EF64D1DED1F81EF350B66CDD ();
// 0x000001D6 System.Void DG.Tweening.DOTweenCYInstruction_WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_m11EA62EAE04560732E3912B35D3E026FE4DD9F61 ();
// 0x000001D7 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_mA0282A1018BFCFFBD5899BBA541E6D3A808CA92F ();
// 0x000001D8 System.Void DG.Tweening.DOTweenCYInstruction_WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_m288D6407017386F53EAA2B8F3810796A7D0A10AD ();
// 0x000001D9 System.Void DG.Tweening.DOTweenModuleUtils_Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mB01AB481CE9E6801C5772FE15904B4BDB7BE8BBB ();
// 0x000001DA System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_m1EE9EEFA66372C5C4E06F4F9F6AE82D649FFEC12 ();
// 0x000001DB System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m949FC0527A713F7423101765DCA1A6FC003F7A49 ();
// 0x000001DC DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils_Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_mC39AFF330B4681C341DF44E5690D774C1402504A ();
static Il2CppMethodPointer s_methodPointers[476] = 
{
	Bouncer_Start_m39268BEDFA9A9EC977E4EFF8A21A8A130F63593A,
	Bouncer_Update_m1D966B71CB415CBEB9058D2C65B36CABFFE910F1,
	Bouncer_OnCollisionEnter_mA4D7F1212B2F6C749025AC7370AA88D7135C2B4E,
	Bouncer_SqueezeAnimation_mAFBAA226BCBB9C003197AEDCD40469B37EDDEB50,
	Bouncer_OnDrawGizmos_m31CF62556A9E9A2B0F3BCCEAADDE3A39F33D57F5,
	Bouncer_GetPosOnJump_m88EC8EE9112EA029F2CD11BDFA6DCDA274FDBF7B,
	Bouncer__ctor_m76C49EF18596BB1DE0133CD59BB420200895D55E,
	CameraController_get_CurrentCamera_m3BD442F154210FE37DEEFD3F222E7AD5510C1DBA,
	CameraController_Start_m92C8EBBE34DA314CE0C730E07400A96E968264BB,
	CameraController_Update_mEFEBBAC80E781E93A04E53B8A6F10601CFDBC199,
	CameraController_UpdateFollow_m9A422CC8E3AB74404AB0E7628B811D0607A0A16D,
	CameraController_Follow_mCEB594A6675D2214BA5E44BA8ACE0DFC03223860,
	CameraController__ctor_m6E88C7769B5C6E7CC68A3670C07050374E05F902,
	Coin_Start_m091891BDFF95DE209C1C086AA6E5AF2E8E7F2E01,
	Coin_Update_m3C113B6905A7E3512ADAB23C121BD0C6B1241037,
	Coin_OnCollisionEnter_m1943EAE75F5D21012509C6C9D39AEF09D9D81F12,
	Coin__ctor_m1BDB4DA53B3F8A81A2DCB8589DFF09BCAEEE892B,
	CoinSpawn_Start_m6BA364DE32C9A2138A51E23ADFD24FF4DAB96111,
	CoinSpawn_Update_m77ADAF94713C7105EE932684F4B7A230B877C5BE,
	CoinSpawn_RollSpawn_m4C2CDD48BA9200228147127DDA6927327956B551,
	CoinSpawn_SpawnCoin_mA7EE51CFC5AB82B1753F4A3823AED0265BCF4ECB,
	CoinSpawn_OnDrawGizmos_m7A01AE6CEB7E857D42CD7396790F3DB46E0D84CE,
	CoinSpawn__ctor_m7BAA28B1228766072EC7B5E7964651B95F8911FB,
	EndPlatform_FireParticles_m418BF0F3EC9F69F94D22A86B245CB14350DA52A4,
	EndPlatform__ctor_m8E9CF4FCAEC7868D221DF1BBD5A488D8F0279003,
	Feedback_InitAndroidFeedback_mFE3C9B79BCB2791A7BE36BB557DC4DD72E9B5945,
	Feedback_SendFeedback_m87636D90C0C1890D94617CC5BBBA78C50CA88F33,
	Feedback_Vibrate_m4473BEADCA8C8607F7D21B4865517E44B21A9B77,
	Feedback_HasVibrator_m12E862E6D16004A9983EB0C41AB2A5BD18F67F87,
	Feedback_Cancel_m196A68B39027F0893004DE86A23FDAEE04F23043,
	Feedback_isAndroid_m434A881E55A5F2C670E4BC0F1FA5A50B2C2544D1,
	GameController_Awake_mDE56BB9F9E51A4C075AB2CF5BF2C04A39476578E,
	GameController_Start_mF709714337C920D2A7F640A97C3CAEF788E3358F,
	GameController_Update_mBCFBA31999BD51BFC0E3CACA495249FF8FB4E225,
	GameController_SlimeJump_mBB245CE14A6EF47C8882D2635761A90C1DAFE399,
	GameController_CollectCoin_mF7672976D2B17D940254ED1F9315A65292F11823,
	GameController_CoinSpawned_m8686E28DB8D7144BFC62733778856C2746D5E156,
	GameController_HoopSpawned_m7AD84F7ECBC982AA3843E839AD5603C1B9366D90,
	GameController_AddJump_m98D81A8224307C790333A55DE4BE5195D7AC6D65,
	GameController_PlayerOnPlatform_m5A574C7F930BB97437D81472F54FC58560702839,
	GameController_GameOver_mB9729C42B008269F8D21E7AE025025377D3FA8FF,
	GameController_Victory_m24F379BA38D5FB2C4105035435EF8779C76DA331,
	GameController_Defeat_mACB7101A5B79220B16162120145F26962AFFDE13,
	GameController_GetDifficulty_m58580BC96DAC23BEE55ACF12CBAE01223C953191,
	GameController_EnterTutorial_m0FB1952EB51A5AE1AB0E3FA60F1C392F00D9FFDA,
	GameController_ExitTutorial_m30D26445D828BDA266DC3BAA773A9705E3C2E2F1,
	GameController_ApplyLevelSettings_m2E1DFECE7696D7AFB6AAFB0EE3707DB1FF799C45,
	GameController_ContinueLevel_m8BADE47A45C9A5D2655E6D49504A55B0FEDE3D20,
	GameController_InitLevel_m16E7B56885CC13161C8FE8533B3B23D5B7CF1640,
	GameController_ClearLevel_m9871AF20A96C221C4500A5BDD18F9F2C653CD691,
	GameController_CreatePlatform_m6AA94D88E7E40D4E38B1405061728572A3B149D4,
	GameController_ChangeFog_mEC816DD94B4B56EA21FC6C4040912B76C14669FE,
	GameController_ChangeTimeScale_mC61BBC6197B1FEF734DE4F80B713AAC87FD6F7F8,
	GameController_SpawnPlayer_m591000ADF6B1AA3624BF175A7A1A97A6B94FD000,
	GameController_StartGame_mEAC9289B743077F253A9E4C4BEE50A2FF052A001,
	GameController_ResetGame_m3E2B8A9F774A6F81E647F1F9130F0D60E02E3CF7,
	GameController_OnTouchBegan_m5FADF218919FF40D209C908E80080A1E4BC81CC6,
	GameController_OnTouchUpdate_mF8DF086EAFC280571557CC59BFEA30D0375F9AD5,
	GameController_OnTouchEnded_m901D7E67FD464A9A534FF5F667D71223635DFAE3,
	GameController__ctor_m6C90AFC15A0F46BF2901D879C39FE7E50AE7CDB3,
	GameController_U3CVictoryU3Eb__41_0_mF7491F461218A9E45D54259E71C7E910B93223DF,
	GameSave__ctor_mC908542ED039B572AC9E45103DDAA56FDB5E6300,
	GameSave_ResetSave_m26E65BDED3EA935E15CB05EAD44D8E16E3D952C0,
	GameSave_LoadSave_mDEB036EB4B5FD2E362BDB58E3D15511F071E53D2,
	GameSave_Save_mA14E3029C505C1A780BA50CD10D9ED8A05005270,
	Hoop_Start_mBD3C497CB640451A2256E1940E2289C8B408D572,
	Hoop_Update_m15C768CE69A0BADAA11F7052CCAABB1E3F500BA2,
	Hoop_OnHitSide_mB2082AD006706E4BCF506F4771E3099A7EFCDFE0,
	Hoop_OnHitCenter_m2943A90D045456B2627FA4D0E50845A6083E8D86,
	Hoop__ctor_m1FA4DC2D4B74722F95D7EF7FB2676869F79F6662,
	Hoop__cctor_mA60060B5123E4D18A4CDFB4A123252A3E177074C,
	HoopsSpawner_Start_m4458A3AEFE099F7595060E7B4820B27D7747AB8D,
	HoopsSpawner_RollSpawn_mAB773B5AE073386E016E661A70EEFBA5B1F2AB85,
	HoopsSpawner_SpawnHoop_m53D02B492CDE782637FEC9B8AB0F467E5F62DEF8,
	HoopsSpawner_OnDrawGizmos_m002DE1A3F8A999D8A0270DABCE257456343CEB64,
	HoopsSpawner__ctor_mD83829D70A41F4044A456813CBDC7E07C30AA9F9,
	InputManager_Start_m9EAB4933E74781D59CB95E9FCD6580F9A1EDA14E,
	InputManager_Update_m813F65B6A0D8A18ACFAFEFF8CB3F7E1FE116BCA7,
	InputManager_GetTouchMagnitude_m65971F7EB06227F2516DC0653EC6C14E432E3BBC,
	InputManager_GetTouchDuration_m551F3871D9A7D70EF6CEB48238D711F5B86E44B9,
	InputManager_GetTouchVector_m61D15307CB7423C1CC326A9284F2301528FE23DE,
	InputManager_GetTouchDifference_mDCE38284AAB3638486479B8D64DE66113AE9259A,
	InputManager_ResetTouchVector_m71EDC3BC10E6F475F38071816EB7BD3B9F71671D,
	InputManager__ctor_m92E656DB5CA317F9006F3CD8CE443B7D15EE2A47,
	JSONUtils_SaveJson_m0551F2D84063CB16D36C1636079ACC8E6245B7B3,
	NULL,
	NULL,
	JSONUtils__ctor_m36BC1B678914D83708BF2A849FAFD0E15D75B18A,
	LevelSetting__ctor_m626FD1130FE5EDC031605883BE876F06E7B13F91,
	Platform_Start_mF33B833267B9B2BF65D6562E4E5F4CB252C2E9BF,
	Platform_Update_mE2C277250260163AEA6CABB4F729BF2AA2A3ED0E,
	Platform_GetMarbleSpawnPos_m2F14D7A7191E3422A4744E10CC75603F755A72B3,
	Platform_OnDrawGizmosSelected_m8889652814A311EB7EE11F378CCD4BC4A5038847,
	Platform__ctor_m4380C762ACE2D5A5EBAFC8C223D6A9AF70257704,
	NULL,
	NULL,
	NULL,
	SlimeController_Awake_m05BDE545A4DB031D71E69020F09E8F46824FDDC4,
	SlimeController_Start_m1E8CE60383615868F5BF5D3A2A1943A2FE56034F,
	SlimeController_Update_m520B7CCEC6B6F0EA7F8B9460488B343CC0384FBA,
	SlimeController_CheckTutorial_mBC661D6199A3FE0661DABBEFC33FF2F6F8B10B88,
	SlimeController_InputDirection_m64DCDEFE205FA9D80E09F3B55E3AFF3D7F404389,
	SlimeController_SetLateralPosition_m0F03095E088D829185E97D7420319C77AB486854,
	SlimeController_ApplyLateralPosition_m3C87E592A3743ECC015043FAED3440EAE4F75AC9,
	SlimeController_UnStick_m08112A44183F496E68E8CC50F5DE8FC00A4AD199,
	SlimeController_Dash_mFB6EDA4F0C655A44F119AB5AF0023A2130A73D07,
	SlimeController_CanDash_m5FE58EEEED3CABD11B4FD8F34CDFBDFE12502E52,
	SlimeController_OnLanded_m53DC8A7FF574D031246EA4E8DF0D9B5C2846581C,
	SlimeController_StartBouncing_m482650A043C07BE78186840F698D2FBCEC2CEE60,
	SlimeController_NextMove_m5E38AC0C458FAD052B24A01D3F81AC28E879A9D2,
	SlimeController_GetBumper_m90AD2F3E9C4E8C9E4D117A815E5BE761FBEC7665,
	SlimeController_AnimationJump_m86BE1305BA9D4C54330A822FAEF847ABF5045319,
	SlimeController_AnimationScale_m7526B90B90886AC1460CB188DA6563AD53C6E3A4,
	SlimeController_GetDurationFromJumpHeight_mD1461679FFBF1E80E223A9AD1EAB1294B1FE741B,
	SlimeController_AnimationRotateSlime_m1579A4911A4F5D6805AB0B8BA071BC37875983FC,
	SlimeController_MoveForward_mDC3574888ED78F672B08234A4650B5BF5D719184,
	SlimeController_CheckGameOver_m5D1C5C8E076A3F1F1E34D3FACF9855C65F789B77,
	SlimeController_CheckCollision_m98B7025B88A2AEFFEBAD0BB37FDDDAD47F5CC682,
	SlimeController_CheckHoops_mDE5C9970B7236953BD3F42C123B6DA3463D7FD01,
	SlimeController_Fall_m4C52B273540328307832111F3EAAAB3DB2566EFF,
	SlimeController_CeaseMovement_m70733515895C84702107E05C30D70BBDC67DC842,
	SlimeController__ctor_m6A10C1799C5D4B338AAF4C12EBD45563C0539B0D,
	SlimeController_U3CUnStickU3Eb__32_0_m876E2D808D2B16A079EFC785884688F1C31300D9,
	SlimeController_U3CUnStickU3Eb__32_1_m7B34DFEA74F28D5CE088B7A60E5C3687F874D7A9,
	SlimeController_U3CDashU3Eb__33_0_mA949116D7F2CEE33E46AC9B6BF448D3ED5F607E0,
	SlimeController_U3CAnimationJumpU3Eb__39_0_m551EFBBFDE9E3CB5C68105868CB9CA35352A91E4,
	SlimeController_U3CCheckCollisionU3Eb__45_0_mD141524431F8B5B85466181D5BFA94B257070463,
	UIController_Start_m15E91565B003DEB2B4898F1FF002EDD1B7554D9B,
	UIController_Update_m418FDF444584AAE6D1F04C2509D0E935A4330F1D,
	UIController_OnPlay_m6B8CDE66DF0CBB12AED81C310F25174B937DCE5C,
	UIController_SetJumpAmount_m4212BCEF6891DA31604C1DE8D56EFB5CEADB8792,
	UIController_ToggleTutoDashPanel_m368F7AF0AF278C0623D3986D1BDF4853C93B3919,
	UIController_EndScreenCompletion_mA9B7E3E8E41AF085A2FA8CF7917642E3530671AA,
	UIController_SetCurrentLevel_m09F7A9CA5D79B543FC09418E6D284916BE099351,
	UIController_SetCoinAmount_m3EB8222F83C7F9C7DE2407E1AE0914065441503A,
	UIController_BackToMainMenu_m566A10B2D4279648C5571817A698374B7C4E252D,
	UIController__ctor_m2957FB6041970C1305D4DED3E11D4D800E932486,
	UIScreen_Start_mCF30C23A5161CCA9A2C720B9061AE2A3F2E4B995,
	UIScreen_SetEnable_m6FCDA1E814AD709ACABE9F8912AB9D5668845BB4,
	UIScreen_KillTweens_m709DC3EBF51AC5AA937990F4692168BDC570CA0C,
	UIScreen__ctor_mECFB4FA666EDFD74496FDF2177AAA212DB25C024,
	iOSHapticFeedback_get_Instance_mB7F77E6A6747AC5A5B31B011F7C6E76D0C528563,
	iOSHapticFeedback_Awake_m2B3AD9EA7D99E6B97C8EE2E386C274F49D902BA5,
	iOSHapticFeedback_OnDestroy_m926230A2B3DC73404ACECA5E9F96C79631E18674,
	iOSHapticFeedback_FeedbackIdSet_mC6951619D58597EFACB8A9EB1C0262DAD6E07FA6,
	iOSHapticFeedback__instantiateFeedbackGenerator_m485B13D16526BEE1E535DC98122E3E90C1D444CE,
	iOSHapticFeedback__prepareFeedbackGenerator_m20D315216C35EE20D0EBAF13C8A336827A976793,
	iOSHapticFeedback__triggerFeedbackGenerator_m92F1B1CE0892A4076A40F0D2042F1324BD69DF6A,
	iOSHapticFeedback__releaseFeedbackGenerator_m7933EC6328135286DE847B0B60849FDC915D564B,
	iOSHapticFeedback_InstantiateFeedbackGenerator_m5086A4AD681ACB44A2D44BA120CA515CFCEE6341,
	iOSHapticFeedback_PrepareFeedbackGenerator_m94FB54E3FF19057F0AFB4B2DB45AA5651598F5B6,
	iOSHapticFeedback_TriggerFeedbackGenerator_mEDFD54D523DE0AC8BE7AFEB3769180EACF60B419,
	iOSHapticFeedback_ReleaseFeedbackGenerator_m748BBFF7110A3E507BB0E9730025226A73EEDBA9,
	iOSHapticFeedback_set_IsEnabled_m3D3E4E00FB4A577E66E41D7074C3A6DB506A1A7A,
	iOSHapticFeedback_get_IsEnabled_m6778DE6BAD2680971785BA0FEA9DE38DA3BCEBC2,
	iOSHapticFeedback_Trigger_m20779B1B65C4AB79C6B8CC375ADDF412CF43C62F,
	iOSHapticFeedback_IsSupported_m943DF4786E61C0359A79361ADAEAA38A903597FF,
	iOSHapticFeedback__ctor_m0E95DC12A646136D1516359171A7082421C8BEDA,
	iOSHapticFeedbackAdvanced_Awake_m54080BFF6E392AE392B89937FF40B8593A74ABFB,
	iOSHapticFeedbackAdvanced_Trigger_mA455D72E0EB6A27DF674F5EA0E8E8D8FD3AAA40C,
	iOSHapticFeedbackAdvanced_InstantiateFeedbackGenerator_m591ECFAD58AA363551B1ADFFA1A54B7E96BC9E16,
	iOSHapticFeedbackAdvanced_PrepareFeedbackGenerator_mDA48F96DFBF2C65AF6E91F0DA9E816D6257A902E,
	iOSHapticFeedbackAdvanced_TriggerFeedbackGenerator_m69A0EF4CBE3A99DD7E1199A7FED7069A07952462,
	iOSHapticFeedbackAdvanced_ReleaseFeedbackGenerator_mFA1EBEC4AF6646BD5EB9207B99167E12AB06266A,
	iOSHapticFeedbackAdvanced__ctor_mB57B4FC651F2B5260C142D65605088ACF8EA0B9D,
	iOSHapticFeedbackExample_Start_mD21E55539E0838F57E9727ECF3BEBDBCD0C36CE7,
	iOSHapticFeedbackExample_OnGUI_mAC4803959151DC16C198C5625B5C3E46F1ABFDF4,
	iOSHapticFeedbackExample__ctor_m0EA255CD27C9B5DA3F0CFFC3DF176CF6E32B6A50,
	DOTweenModuleAudio_DOFade_mB895E6623DB3DAE93D395FD5F885451249738498,
	DOTweenModuleAudio_DOPitch_m17470F82E3757914C9F0B77F5B812555AECCBD60,
	DOTweenModuleAudio_DOSetFloat_mEFC74DBA5F02D4CAB1CB545F37AD3198ECF94B03,
	DOTweenModuleAudio_DOComplete_m8A094AC09079A9F5CAE8527E8022F194351068BD,
	DOTweenModuleAudio_DOKill_m4B31EA97960FD9FF2BABC3C29C7B32B5C44D08B5,
	DOTweenModuleAudio_DOFlip_mA847EA9957D83FFD582BB51F5DCB2F3809398678,
	DOTweenModuleAudio_DOGoto_mC314805229AB8931AF1677599FF847354113BC6A,
	DOTweenModuleAudio_DOPause_mC28F1284009712995255B7A90403C1DA50A1903C,
	DOTweenModuleAudio_DOPlay_m57722E0D66F4CBA2D5AC2BE166D3F987A34C1A53,
	DOTweenModuleAudio_DOPlayBackwards_m7DB5FA484E3AF8741F590DD8F4F3523DDB1BAF76,
	DOTweenModuleAudio_DOPlayForward_m87F1773D251CCA2094CC79576F141EF098AB3FFB,
	DOTweenModuleAudio_DORestart_mAD32A3398DC4FC3D24FE6527FBEFEB9D180FECD2,
	DOTweenModuleAudio_DORewind_mE2B095AF13DE79B9A9B91FEA6A117F5F3DB75772,
	DOTweenModuleAudio_DOSmoothRewind_m8F7868428006B0ACC315E87192D4CEA145110A0D,
	DOTweenModuleAudio_DOTogglePause_m6A9131F6CF8CB82FF3AED0AB0A831B2537ACB492,
	DOTweenModulePhysics_DOMove_m04DC92B1D5473A3B74F00EA5FB7BD483EE61368A,
	DOTweenModulePhysics_DOMoveX_mE1712777B02AE15A8856F510D99E4D8B2D29BE69,
	DOTweenModulePhysics_DOMoveY_m0C7A58A9F9A2C64472B136F042233E693FCE5A4B,
	DOTweenModulePhysics_DOMoveZ_m51E2D9D00889980A6F3069E30F1B3AECE9A6F56B,
	DOTweenModulePhysics_DORotate_mA7DF8F0F38CEE91C8E22EBDD08779349B22EB2E9,
	DOTweenModulePhysics_DOLookAt_m6DE60A979D3F8FFC0BCE231438B81B60E21118E2,
	DOTweenModulePhysics_DOJump_mA4C40AD22FDF758374978E6AA9444B8AF5E28968,
	DOTweenModulePhysics_DOPath_m76DAA5B3B088E540C09412C4BA00E73977BA72B0,
	DOTweenModulePhysics_DOLocalPath_m8F810CF905D7C16FCFDB2DDE6747D417D2A1276E,
	DOTweenModulePhysics_DOPath_m7E33EB3323693ECFE3478860191F90EFA89BEFEB,
	DOTweenModulePhysics_DOLocalPath_mED288BC25D9DB70757E2841CCFF079857EF7E616,
	DOTweenModulePhysics2D_DOMove_m7DA7D6F281AD23631C31951A782D109672BC6084,
	DOTweenModulePhysics2D_DOMoveX_mBB34A371FE4800F1592211FE14691DDBB25A7149,
	DOTweenModulePhysics2D_DOMoveY_m22C5E230A66BD275BBA274D57212CF301197CBAE,
	DOTweenModulePhysics2D_DORotate_mFD2622F30699A2D839E059003BD0B6F80D320134,
	DOTweenModulePhysics2D_DOJump_m1A08125A11636A168F3D5A95DF552D654C3ACFF9,
	DOTweenModuleSprite_DOColor_m722030927677D8D607C4C955F66250EB478B59C7,
	DOTweenModuleSprite_DOFade_m3B5756EC0481C06E0571536C740FE5A6C2A2FD84,
	DOTweenModuleSprite_DOGradientColor_m22EB92798058929D14E5EB0CB534C5C0344CD947,
	DOTweenModuleSprite_DOBlendableColor_mAC0C5C5596B2AF63F4A57623F211838CD2C1B597,
	DOTweenModuleUI_DOFade_m9263D65172482504B1A046F5E511490C181A866D,
	DOTweenModuleUI_DOColor_m758BBB01C103927CFE361F9BD19DFD0E1BB23B2E,
	DOTweenModuleUI_DOFade_mDC20440A77656C4E43F78479151649345858500C,
	DOTweenModuleUI_DOColor_m63BDFF1F39C303626D0F0DA3E9FE1A213CD90711,
	DOTweenModuleUI_DOFade_m38A550C7A51BC3891DE26F8952B2F7916B984EFC,
	DOTweenModuleUI_DOFillAmount_m82803C7BF3430F67DCFCDC208DBED116414217DA,
	DOTweenModuleUI_DOGradientColor_m3B2EA7BE2B53D8EDEC23A8EE53D10D496BACE503,
	DOTweenModuleUI_DOFlexibleSize_m80D6AEFA43E147F6C4234B41EA6406C78BA18E9B,
	DOTweenModuleUI_DOMinSize_mE96A5A92FE347C318D5B6A59F0ED27D98A32CA67,
	DOTweenModuleUI_DOPreferredSize_m540CE78577237D1BE6FB99AB9F8143BDECE08432,
	DOTweenModuleUI_DOColor_m226D8AC396F10ABCC171E1E9ED878D62441A1816,
	DOTweenModuleUI_DOFade_m61F3E3A436DA82F5E4D85264C80E50F0E2A93943,
	DOTweenModuleUI_DOScale_m3F492F86DA293CDA9A27823F49FD50936D9F84E1,
	DOTweenModuleUI_DOAnchorPos_m4B7DAECB0E1FD429435C2FF8612CFF7E0F5717E0,
	DOTweenModuleUI_DOAnchorPosX_mC0CA1A85842B3C1E4EF2EB6514B9AB551368CE9F,
	DOTweenModuleUI_DOAnchorPosY_mEC0CDA9E3828E1D703A144620A6187CF950A2D69,
	DOTweenModuleUI_DOAnchorPos3D_m6B937277DF2B389D169B89AE08947544C5FA4924,
	DOTweenModuleUI_DOAnchorPos3DX_m4FD8BFCB31904082D7470D0D19D475310D71AF15,
	DOTweenModuleUI_DOAnchorPos3DY_m1D8B7CF1E41562D3C8B0960FC28ED77054D2000A,
	DOTweenModuleUI_DOAnchorPos3DZ_m876869289951806B828EF9A0644CAEE20FB72AFC,
	DOTweenModuleUI_DOAnchorMax_mC7791F2E0F55FF48FBF696839E5D45E9021C36C4,
	DOTweenModuleUI_DOAnchorMin_mD85F4855FB8989FEB6A75C08E0C86B71A073DC32,
	DOTweenModuleUI_DOPivot_mBEE2FC4EB705017C440341F0F5ABAD41A9D1E6E7,
	DOTweenModuleUI_DOPivotX_mECA1AF29FBAF77B3FC310E8FCC0CC96838F7BA99,
	DOTweenModuleUI_DOPivotY_m4148FAE93214CB89245428CDD8AB69F3062D75E0,
	DOTweenModuleUI_DOSizeDelta_m238A4EC18672EF8B9D4B236E5B34BCCC37C4F354,
	DOTweenModuleUI_DOPunchAnchorPos_m82CE87E5ED9596485B8EC26743E6760D498D582C,
	DOTweenModuleUI_DOShakeAnchorPos_m1BC1F1222951F602046B9265C9C5389DE8E8E190,
	DOTweenModuleUI_DOShakeAnchorPos_m75E868D2A4EACA8E9A94DEEEE842FA79EA120108,
	DOTweenModuleUI_DOJumpAnchorPos_m69F7916D909CFEF1171DCCF294AF1D7ACFE5AEE4,
	DOTweenModuleUI_DONormalizedPos_m30FC5B6D303C26D4299C80BCC149BE64A380B6D6,
	DOTweenModuleUI_DOHorizontalNormalizedPos_m970CAE4EEEB626E2C95547AA3C9485F924576C17,
	DOTweenModuleUI_DOVerticalNormalizedPos_mD853402020AEC35415931A321C0E33B294A58E66,
	DOTweenModuleUI_DOValue_m1CD78CEB8F29CDD7C28CA0D4ED6407CFB95F4A91,
	DOTweenModuleUI_DOColor_mC5895F9A92931B09FED80AD3CBAC42C13A01CB83,
	DOTweenModuleUI_DOFade_m4BC24CDB20DA7FB142F8CC1759978F1073267025,
	DOTweenModuleUI_DOText_mFC30460ADF17ACFF852662753B4FFFA0CDB5FB0D,
	DOTweenModuleUI_DOBlendableColor_m4305297962324398C6A0170D689EB10EB8D805A1,
	DOTweenModuleUI_DOBlendableColor_m370BD2A767C8A6BD46561ACA8B32487BD48D42CD,
	DOTweenModuleUI_DOBlendableColor_m98F474F73DBDEE43363853C8AE86CD9A74E0BF5A,
	DOTweenModuleUnityVersion_DOGradientColor_m0101EE36EC8ECF73C0A6F30AA16A69C2ACD7E1FE,
	DOTweenModuleUnityVersion_DOGradientColor_mB4E72995236B85B6B1AF56D0F365507DAB1B6EA5,
	DOTweenModuleUnityVersion_WaitForCompletion_mBFB806E7A92BD3176C7F3C7481C2F16724B69E9D,
	DOTweenModuleUnityVersion_WaitForRewind_mCB4E8C1B6691BCC003E09C905F03CE3AA0DB1BE2,
	DOTweenModuleUnityVersion_WaitForKill_m6DEAF0B60254A2E4DC457DFD820DC701CDD6084E,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_mA6045B988E998B9113509A6D0C508142727D9347,
	DOTweenModuleUnityVersion_WaitForPosition_mDABE7C2BD065537A4D28300F4A4C1E835B8B8332,
	DOTweenModuleUnityVersion_WaitForStart_m8B396ABEABA9DD73328B263095CB0884A03CFE38,
	DOTweenModuleUnityVersion_DOOffset_mBBABF79C7EC8FB51F312A3A9CD3E49CEF1DF2B6E,
	DOTweenModuleUnityVersion_DOTiling_m14C9D0A3D9AD487687474B0F243C9656E20DC697,
	DOTweenModuleUtils_Init_m4A1880D2D4CAB505023229810019E6117C546C77,
	DOTweenModuleUtils_Preserver_mC0D1235B1B75C39513919D943AB88D5EDF24E7A8,
	U3CU3Ec__DisplayClass2_0__ctor_m0AB73C0AB407F20CAA2302E8B0526707B3762DBD,
	U3CU3Ec__DisplayClass2_0_U3CFireParticlesU3Eb__0_m5F9813A924F8C77F70F38E5279ACBE3FBBE85500,
	U3CU3Ec__cctor_m62FEF400139913C37ABFC52C1E1A374FC9958CC9,
	U3CU3Ec__ctor_m28478222F1AA4BE5804FB2D3488A3897F47EE76C,
	U3CU3Ec_U3CChangeFogU3Eb__51_0_m60EA28D4836EA64DA416067D47DF893594CF53CC,
	U3CU3Ec_U3CChangeTimeScaleU3Eb__52_0_m8F336F46262B49AD040B7EFFC3B5B6A9868D0175,
	U3CU3Ec__DisplayClass9_0__ctor_m7D90E3D6D47268EA1C979E7CC6EBD26D17523290,
	U3CU3Ec__DisplayClass9_0_U3COnHitCenterU3Eb__0_mD151406F87F1F730A181FA63F42852DF097C6B83,
	TouchDelegate__ctor_mEBE98E56AA1183D5F1053FA131FFC467D001733C,
	TouchDelegate_Invoke_m714BEEC32B3A0A0200ACB1402329C99EC7D18F68,
	TouchDelegate_BeginInvoke_m203840362C924FB41611E3E79BE6CF41CEC2D7F9,
	TouchDelegate_EndInvoke_mCFDCC3D286F47107CF2D7AE480070B77CE7873FF,
	U3CU3Ec__DisplayClass43_0__ctor_m106DF84FBAD9A192E82B54A8FB5B8F50334F61DC,
	U3CU3Ec__DisplayClass43_0_U3CMoveForwardU3Eb__0_mE615767DF57E7747D0873EF58043B2C76D4D13F3,
	iOSFeedbackTypeSettings_get_Notifications_m207B452FE7C525C9114388B2F4D248BC1C806CF5,
	iOSFeedbackTypeSettings__ctor_mCC383DA9AF3D04EBCCA175B446FF1C354A48ECA4,
	U3CU3Ec__DisplayClass0_0__ctor_m410489F815730C05633CB198EAF29184C61E5DD8,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m553B5BF47DC863789E86A5BA9C58219CBFCCAE7D,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mA503D22951DFB1756D2093D25A534F4DE527EA1A,
	U3CU3Ec__DisplayClass1_0__ctor_m668929D1037563C4BA05DC1C6D49208EFB41B5FF,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_mFEC0334FA56515F8E10FB4B573547AE339A7F3E6,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mFB24EA119BE93AD9F7BD52CCA50955AFAD91E1FD,
	U3CU3Ec__DisplayClass2_0__ctor_m0427EA7A432602D7490FF082CB68BCAD71A86189,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_mF3065D336F54345A67A41AB553F91B12E56A41FF,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_m7E51751C14C21C8E8A1AC3BB06E95F16EDF13535,
	U3CU3Ec__DisplayClass0_0__ctor_m80ADE6AD0A6AE24E88C1763E70FD33C833B502B6,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mBDD978F428BA10922F372EAB21DE234526590F98,
	U3CU3Ec__DisplayClass1_0__ctor_m61766A5644E878F1E5F0B033A602BA12FE6B249D,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m7CA25B5D3253D1A1BB83175835E6F06B37E6D3CA,
	U3CU3Ec__DisplayClass2_0__ctor_mB8B190685AB0F803C260CE1CB8A4500980441B7E,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m475A4066BB6D47109DBDA7711674E92F8B26F80F,
	U3CU3Ec__DisplayClass3_0__ctor_m53D2E1E6F2D6D8372B286983781D03A34E557CE0,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mFAD55624F73C8C7678B40AE2B544A4E21440093C,
	U3CU3Ec__DisplayClass4_0__ctor_m877BF0BB29B71DB544629A0FBAB3A16BC819E75D,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_mCA650E955F3C9B985D9781B4E88F9EC8E22B4DF9,
	U3CU3Ec__DisplayClass5_0__ctor_m9310E069CC7950951613CFC194B0C3906C92A052,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_mBD3D65C2B6DAE37DB5CBA1BBF01D666A2908E540,
	U3CU3Ec__DisplayClass6_0__ctor_m4B17E82EF353958EDF46FD0D37B988F0A41BA5BF,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m04813310411D22F519E3142909FCF08E4A83DB05,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_mCB70FEE3ADA2EC1B74A0A7BB60CD8274F9838C02,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mB47776ADD5831D4ED18EDE2DDE38A858AC92A9C9,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m67EF6E7E97403AB9053D7811A551A6D052D90CFD,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m287B8EED4C4A9357AD78642E3F2914D8FABE2805,
	U3CU3Ec__DisplayClass7_0__ctor_mA561D07841A4F7CAEBBD7114F3ED4151EDD788E0,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_mC71BA0A22CF6F7A1ACEC09FC99AAC8B963F653F8,
	U3CU3Ec__DisplayClass8_0__ctor_m8AF8F5129417528E73EBEDE7BA21211B3C38E5F0,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_m9C60CDEB2CFE741DF9088E45428A7881F1E7E398,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m91D6C114DE18B6EA58C6C7813E906870095F035E,
	U3CU3Ec__DisplayClass9_0__ctor_m8B993A11861859EDAC694573BEFD4FC35DA13044,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mF111BF5DF70FA94EDC3C3A6F0D8A299272BC3262,
	U3CU3Ec__DisplayClass10_0__ctor_mD7B65E3231BBB518F764D92874504A6E817A3A09,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_mBC510690793F98CC7A4CEC293AA1C1420D77B006,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mEA0F5B946589BAA3FCB20D2E150440CE5294E6F3,
	U3CU3Ec__DisplayClass0_0__ctor_m76BE70D5858C4B19FFDF20DA088F62D45CEFBDCB,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m7AAE48D11CDA351B8BD9555E638AA2AD3B967DDF,
	U3CU3Ec__DisplayClass1_0__ctor_mD8CAC14A3F48F66250EDA54950744A54CE2430B0,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m6317E8FB8A070BB91F81C94666F3CDA20A2E43E5,
	U3CU3Ec__DisplayClass2_0__ctor_mF9F03ED121A568F712D5265B86BC8D836F5D712E,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4DB120064C1A7D8A47A52371A6E532DACA6C73A6,
	U3CU3Ec__DisplayClass3_0__ctor_m45136E0EBA7276A3B440B3F1C0FB5969A8F10EBF,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_m738875FC6E09B036EECFE32ABE5F31BBB6399E52,
	U3CU3Ec__DisplayClass4_0__ctor_mBC3F126EB212356DA892532E6FEEB0E144ECF221,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m1D63D0BA78EB39F89A1B1E066912FFE44287B21B,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m05BFB77CECAA68F50F7F6AC593F37CBD3697B170,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_m312338665037BB1F402A90991F221C28C1BAE39B,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_m9B07E2AACF2C5AE01FF2BF681C2CB7D99007B0BD,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_mC9A5921C18BDFE2191C1841EB12300FBB4A71A18,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m1C478D59001D4FE6DE43383B960DF92F0BC99182,
	U3CU3Ec__DisplayClass0_0__ctor_mF60CDB37F0CFBB33DA3BE248538EFFC167065618,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m19FA6E77EF091166B632107C37CF96158B3CE288,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m956569E1E9AEA53B62AEEDA64CB966B74E83A499,
	U3CU3Ec__DisplayClass1_0__ctor_m52C917C39DD1186239C950566F8F09210E71F4CD,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m493943ED4559E6518634D0901230AAC5808BFEDD,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m83A9CE643617C1B0EEA1F7AC43247148DD77A124,
	U3CU3Ec__DisplayClass3_0__ctor_m6BB3107242E9DAEDE2A970A9842DBC6997F0A934,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_mEA5E8386E195FF3054C4AC1F7C46FCFA2E3095F5,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m5E38544B8C65CDD9BC4A612E4672CFAA22344B6E,
	Utils_SwitchToRectTransform_m953E8B35B59142D580B1EC5A3CB48163D94FE270,
	U3CU3Ec__DisplayClass0_0__ctor_m4E16EB0B8FD5FBE168DFC737535ACACA42FC93D2,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m926D478A89E47B6AB847D5FE75BB68C92BC64B5A,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1C04B14892E7626ECA44D1777458053AB2B3E7F0,
	U3CU3Ec__DisplayClass1_0__ctor_m2F7C7DD6D0645D565EB083D9B9346E65C2D8847E,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mD5A08FA687B4F3D4ACDE03C5F4747ADD5A85ACE8,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m0D02D00E3A1DE8AB6AC3B91D071603A4A5449181,
	U3CU3Ec__DisplayClass2_0__ctor_m1414476DA1A717B266BFBC3A676501101EB18CFE,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m96E8793AEB0F5454A98699F2A492CD0C6A6F29D9,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m7EAF02255761CC601956430B1E80C4EEBD932ABF,
	U3CU3Ec__DisplayClass3_0__ctor_mC63FBDC1D1E28C1067D7FB39AE7DB4A86CEE9CCC,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mD60F568B3218FB775B0F2F472DEF66F12F240EB8,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m3EFC6D8D38659950BD09A97C858526829F838657,
	U3CU3Ec__DisplayClass4_0__ctor_m0773FFC61452C361ED706D7A144A12E29C5AE7C0,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m2ABDE00A2CE387D94979FF4772233C32E0433002,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m6C458B0BB259B641E486624D57F7ADB3CE77DBDD,
	U3CU3Ec__DisplayClass5_0__ctor_mB4AA8029171F09FB5B569FE0064569575122F320,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m8284CEE2126386F18071835A8EF76549F57C802F,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m8F2913B41E7748055C6CF28FED759CD593F72D9F,
	U3CU3Ec__DisplayClass7_0__ctor_m77C731D6C164BD65C9879E2113BD8385855E8A69,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mFA007EE23041AC3DB626E202537641F7CCC10895,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_mDAB5785D80675BB3E337A7F835A8E506CD18DBE7,
	U3CU3Ec__DisplayClass8_0__ctor_mF74EAB9037A10D32AA8A2E62C76AD529AA2F939F,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m5AADB9E424374D66A929CAE4FE7C91FAAD9C1F92,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_mC28A35FB645B9F98D92597606CC43BC0C762EAE5,
	U3CU3Ec__DisplayClass9_0__ctor_m61D4ABCFF239F5E7E8C3D839AA726D2DC94F90AD,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF3EEFDE0596A75FA4C9B5E3CBB8D27B37286B7D7,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_mE48AFB91AA0459196E8D673F66FFA6603733BD9E,
	U3CU3Ec__DisplayClass10_0__ctor_mDF8853F408C48F0DEE4BDB520B12086A4A96584E,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mD23FC3413F649B7175F76650D99C969D080A52B6,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m622D4E29805ED0018C2F07663A12F884A3867E80,
	U3CU3Ec__DisplayClass11_0__ctor_mD30DE6D8FAF4F2CEEA758C16C34081ABB19A1FE5,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m97CEC28FB18E8EC25DE68CBBC9363C1969199D92,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m9C8D55E54BB459893598BBA3A01FAE16F19707F2,
	U3CU3Ec__DisplayClass12_0__ctor_mE8231F7A5382654DAC80084A0615F0D62D59B80C,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mD4ACC050241FAF74718A82BAA2FAD799B221D52F,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m3CE46E157F858D6666E562E24732ACCD51286D3A,
	U3CU3Ec__DisplayClass13_0__ctor_mBC215B2133A5003258CA482F6BE1FD938062EBF8,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mEC1BFCBC158066EE11442276BAF4904FC8167F78,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m8D2760C43F03B2EBB991BDCC324BE7DB9DF90349,
	U3CU3Ec__DisplayClass14_0__ctor_m34FD9996E6407C44D5B51B824B5E51B37B802273,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m946B10547ACAFC05288900D6C8AE8BE29FA8E769,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_mD44EDB300AE68F3CF9F9C05A04736FEBC36A62C9,
	U3CU3Ec__DisplayClass15_0__ctor_m155F27D8B139E97FC6CD758489ACC4006C876F3C,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE74B0E96F68D07C8FB2B95F696DDB3F3EF90633E,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_mD2751A8D2B91C8628463F8817C56481A58ECCCF6,
	U3CU3Ec__DisplayClass16_0__ctor_mCA56D7F89996A8B5FF1B0720AFE3142B501CFB1F,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m54AA9D5E75139ECB7FB5D0BF9518849E9258416F,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mC8E507C07915B5AA9C4D033EED58F7DD384CEEB4,
	U3CU3Ec__DisplayClass17_0__ctor_mF0C8F175B7992C1F263828E8951B3D7CA8EEDE95,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m7C05989ECA103CD60B5CF731D6C7920B1FC985E1,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m082B6F826D56C521A15A1C2AC2270EECD5494B4B,
	U3CU3Ec__DisplayClass18_0__ctor_mB41226EF2155BF4759D574341D00D48FA8147C1E,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m9D5E91C0F8BF3135CA706B2D09504A99E384E3F6,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mB6CA72A3DB84D4A8054678A74B9F90E1D2F4D7C6,
	U3CU3Ec__DisplayClass19_0__ctor_m6F95C61CB46CCE9AD6E7F2C68828F7E2B3F8F76F,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mCE069FE82ADB466CBC0DAFC700BC15C8613B138A,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mA99BE8EF43FD3FDB2C2048472124B8E9535C8D21,
	U3CU3Ec__DisplayClass20_0__ctor_m37A420AB4F75727ED565429F5255310B227A4793,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m0D36293A6361A326C0853382366377CE10796BC9,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m2363ACA3A36A35D8F816953CFCFA314AB44BF64E,
	U3CU3Ec__DisplayClass21_0__ctor_m3C4DC31D280101BD1F83B7AB6847D855F3D08554,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD66DD96927DDAF369C64AE446E76D6F0F03AE0F5,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m87EC5E0454FFFFCCA1F39624496EC189D9C20DF9,
	U3CU3Ec__DisplayClass22_0__ctor_m0F878681308922D788307AC39714D59D3CC00000,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m7DC62E4EE195C844218B14EB4EF6E5AED8989D0D,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mCD92689E52DE74483FF9CEF188D080AEA98FB48B,
	U3CU3Ec__DisplayClass23_0__ctor_mCDC8B6E3F2F9175E0A55357DD876C0CCF4641FD7,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mF72A07116DE5256A691DBAFB766E637F55C4EF45,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_mF7F854224FB363EAC67951EA7DC121032840607C,
	U3CU3Ec__DisplayClass24_0__ctor_mA365D90EA16C25D6D26531196AFC1C4BE0390854,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_mADD1E688B9294ECD2A7CCD79F78CA0CD68E055B4,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mFAB6BC6C0DF65DE9FA072F42567FFB24A1D3C154,
	U3CU3Ec__DisplayClass25_0__ctor_m393618009F4E2F97836536F9AB76EEFD8A68FD36,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m036AB1FD8ACAAB27669C6F82B807258A7CF95161,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m08257CEDA26D13E5A8633A6BAE8C6D6C3554F606,
	U3CU3Ec__DisplayClass26_0__ctor_m28F1AD8814B9CBC6855A48A00E2E8C2743C7540F,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mA1CDB05B94AFD8D48E1B1DB4B6F2F53FDDD274EC,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m8D001C76DD8E98541DB03FF4C6B9D60AB1773F48,
	U3CU3Ec__DisplayClass27_0__ctor_m009781B788D8BF90494B3AAA73704BD4E17B4622,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m23ADE1EDABB1FF436DCB15B4D2956341A403F86F,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mD71967EF4D17CF7059F992D8790F8AB52F83972E,
	U3CU3Ec__DisplayClass28_0__ctor_m7EA6439CC95C5AFA962A1F2B6692EF6AC08D2FDE,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mBD11B2BA0B8A07753156C1F9CA22B33A041F6A28,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m157A05E0EE65A300C5AE80828B38F95123127F44,
	U3CU3Ec__DisplayClass29_0__ctor_m46346E5D03CEBEFD04ADE8670C5A9A3D2212ECDC,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m3336D670200C9F13D96D7F52F123920132234850,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m40CA655FF5E56CE5405C95B10113A9C4AB18DDF3,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_m0DD9D7FEE1DB03692260F3B293B431E729C2213D,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m0D9E7219678950432D0E913F31C5A3E3B3D1F449,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_mA0D6B7FA51A3D9BAF9A71FF8C94B458A56B88E03,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_mACE1C1FC22CB19EA9BD92589C0AD1D45CBF5E9C2,
	U3CU3Ec__DisplayClass30_0__ctor_mE46F8688A15AA59EAB6E8BAF0BA3DD844691F1F3,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_mE69AF155BCC63288298C03F1FDE8264894DC5112,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m6E5FB0266BC13BCB695DD7B72DC74C19CCF0F3AD,
	U3CU3Ec__DisplayClass31_0__ctor_mFB9DA92206594AD9D941461DEDFE8484C3068A2C,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m5FB527945F0FF1ED7B9DCC177766E747CCFA63D4,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_m26811C4F5F41671445F1A02B0BD88A3FA396D990,
	U3CU3Ec__DisplayClass32_0__ctor_m70919EB25B6ED305D785B69422AD8C7B22AC867B,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m23B9440FE647ED7E52A734B8B3971D2DE0A2B276,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m86FAFD38DD8654C5ED6F11C838E7FBC33AED9361,
	U3CU3Ec__DisplayClass33_0__ctor_m65100B9ABF5D6F5D6391265FBEF999F71DB4F89E,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mDEE07FC2B98C7E5BFE645D4CD8E6E99E95265F70,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_m5130DE7DC7811A77FAECE6D41B69BE332B0FE638,
	U3CU3Ec__DisplayClass34_0__ctor_mAE1DE5261C2A864B7AC53DCDEFFED3AB330EC2B2,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_mC0172F2115741150E114BCEB8C6366E8DC05F0FE,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m62239333E0B0AE84382D45A8C2BDDB73DF94212A,
	U3CU3Ec__DisplayClass35_0__ctor_mEBAE6DC4503B8E19743BBBDD12FFFBF036CD1029,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m7AEADCAF76E9378EF16998AF06C981E238317A14,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_mF1633BA0B0E3A39E320BDFFD1D2AC6DDAD08D0B0,
	U3CU3Ec__DisplayClass36_0__ctor_m527FD2EC7D877B6441B750B1F8A0315EFD095D66,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m9CF7A71E7C6B7F3D963E8151E8B19AF7BF38D765,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m8EE1D16A9753A612F4F2D0660BD6B035092E918E,
	U3CU3Ec__DisplayClass37_0__ctor_m57780CD760FA32CFF80E9DE9B259D652AE0E538A,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m5C210BC6DB5E81E37C5DA7F0ED05DCEBF88A838F,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_mD1F0C265C9C64AB81DDB9E6A3F2924B7C01F5EDD,
	U3CU3Ec__DisplayClass38_0__ctor_m0CB071B9A6D2A3B18B3313D2FC19584BC9DACD37,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mD045F88560E9C38E36C045962A24AEEE7E220146,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m1815F9D5038BEECB23F5DD48BCE7092C8B789DE2,
	U3CU3Ec__DisplayClass39_0__ctor_m95A2D9F9240BB4DBDF145D090616BCC0AA6BE30E,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m769FD067F1DD156E6737065978D723626F684DD4,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m806AD3EE1B7A414688748F18E0046359989CB5CC,
	U3CU3Ec__DisplayClass8_0__ctor_m825753EB38BCD235FAA91B4EF503347E3A72C6FC,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8CB04FB886F55DF914E092EAB3E88CADA5E4BFEB,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m64C942C0D9E832DAA71AA441F1C8EC739F01BD06,
	U3CU3Ec__DisplayClass9_0__ctor_mF144DEE04A5A997D037F570C7FFCB312A4553C5B,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m45117726269A56BEFE7511E185A5EB5DC6614C08,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m35E74CFE50BCF6B890BA0F6E12021B724333EBBE,
	WaitForCompletion_get_keepWaiting_mD664D3B152C13FA9775F88258EEA369A4F9FE40F,
	WaitForCompletion__ctor_mA5238710C0ADAA5121265E2E659A2F2D4266CB23,
	WaitForRewind_get_keepWaiting_m23B6817979AAC4F440AC30142185C5FE053112B8,
	WaitForRewind__ctor_m98C421AD48771BD13EE0BE37799B6876F43591F5,
	WaitForKill_get_keepWaiting_m44BE0063D80EC43815DB16A08852A27C61B01C0A,
	WaitForKill__ctor_mFA1D55A21E90BE45AC7F19BCF615F962BC08B96D,
	WaitForElapsedLoops_get_keepWaiting_mFCD43EB01C4ABA9A3FA10EE1594C1D16208DBE74,
	WaitForElapsedLoops__ctor_m1A5F9AB280D57365E4E4329DD228940F0F5DFA67,
	WaitForPosition_get_keepWaiting_m0C2BF58FE1A0E6C2EF64D1DED1F81EF350B66CDD,
	WaitForPosition__ctor_m11EA62EAE04560732E3912B35D3E026FE4DD9F61,
	WaitForStart_get_keepWaiting_mA0282A1018BFCFFBD5899BBA541E6D3A808CA92F,
	WaitForStart__ctor_m288D6407017386F53EAA2B8F3810796A7D0A10AD,
	Physics_SetOrientationOnPath_mB01AB481CE9E6801C5772FE15904B4BDB7BE8BBB,
	Physics_HasRigidbody2D_m1EE9EEFA66372C5C4E06F4F9F6AE82D649FFEC12,
	Physics_HasRigidbody_m949FC0527A713F7423101765DCA1A6FC003F7A49,
	Physics_CreateDOTweenPathTween_mC39AFF330B4681C341DF44E5690D774C1402504A,
};
static const int32_t s_InvokerIndices[476] = 
{
	23,
	23,
	26,
	23,
	23,
	2234,
	23,
	4,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	173,
	383,
	49,
	3,
	49,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	32,
	26,
	31,
	23,
	23,
	744,
	23,
	23,
	26,
	157,
	23,
	23,
	1431,
	1603,
	1603,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	17,
	4,
	163,
	23,
	23,
	1526,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	744,
	744,
	1533,
	1533,
	23,
	23,
	137,
	-1,
	-1,
	23,
	23,
	23,
	23,
	1525,
	23,
	23,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	347,
	347,
	23,
	23,
	32,
	30,
	23,
	23,
	23,
	14,
	2235,
	2004,
	1476,
	347,
	2160,
	23,
	89,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	129,
	31,
	2185,
	32,
	32,
	23,
	23,
	23,
	2236,
	23,
	23,
	4,
	23,
	23,
	30,
	173,
	173,
	921,
	173,
	32,
	32,
	133,
	32,
	31,
	89,
	32,
	89,
	23,
	23,
	32,
	32,
	32,
	32,
	32,
	23,
	23,
	23,
	23,
	1897,
	1897,
	1857,
	1874,
	1874,
	94,
	1876,
	94,
	94,
	94,
	94,
	94,
	94,
	94,
	94,
	1911,
	1912,
	1912,
	1912,
	1913,
	1916,
	1917,
	1921,
	1921,
	1922,
	1922,
	2237,
	1912,
	1912,
	1897,
	2238,
	1898,
	1897,
	1893,
	1898,
	1897,
	1898,
	1897,
	1898,
	1897,
	1897,
	1893,
	2237,
	2237,
	2237,
	1898,
	1897,
	1907,
	2237,
	1912,
	1912,
	1911,
	1912,
	1912,
	1912,
	2237,
	2237,
	1907,
	1897,
	1897,
	2237,
	2238,
	1919,
	2239,
	2238,
	2237,
	1912,
	1912,
	1912,
	1898,
	1897,
	2240,
	1898,
	1898,
	1898,
	1893,
	1861,
	162,
	162,
	162,
	712,
	1928,
	162,
	2242,
	2242,
	3,
	3,
	23,
	23,
	3,
	23,
	347,
	347,
	23,
	23,
	124,
	23,
	105,
	26,
	23,
	23,
	89,
	23,
	23,
	744,
	347,
	23,
	744,
	347,
	23,
	744,
	347,
	23,
	1525,
	23,
	1525,
	23,
	1525,
	23,
	1525,
	23,
	1674,
	23,
	1674,
	23,
	1525,
	23,
	1525,
	1525,
	23,
	23,
	1525,
	23,
	1525,
	1526,
	23,
	1525,
	23,
	1525,
	1526,
	23,
	1533,
	23,
	1533,
	23,
	1533,
	23,
	744,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	2241,
	23,
	744,
	347,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	744,
	347,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1525,
	1526,
	23,
	1525,
	1526,
	23,
	1525,
	1526,
	23,
	1525,
	1526,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	1525,
	1526,
	23,
	1525,
	1526,
	23,
	1525,
	1526,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	23,
	23,
	1533,
	1550,
	23,
	744,
	347,
	23,
	744,
	347,
	23,
	744,
	347,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	14,
	26,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	1478,
	1479,
	23,
	1533,
	1550,
	23,
	1533,
	1550,
	89,
	26,
	89,
	26,
	89,
	26,
	89,
	130,
	89,
	953,
	89,
	26,
	2003,
	114,
	114,
	2243,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000010, { 4, 6 } },
	{ 0x06000056, { 0, 2 } },
	{ 0x06000057, { 2, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[10] = 
{
	{ (Il2CppRGCTXDataType)1, 22752 },
	{ (Il2CppRGCTXDataType)2, 22752 },
	{ (Il2CppRGCTXDataType)1, 22753 },
	{ (Il2CppRGCTXDataType)2, 22753 },
	{ (Il2CppRGCTXDataType)2, 23361 },
	{ (Il2CppRGCTXDataType)2, 22759 },
	{ (Il2CppRGCTXDataType)3, 16638 },
	{ (Il2CppRGCTXDataType)3, 16639 },
	{ (Il2CppRGCTXDataType)1, 22759 },
	{ (Il2CppRGCTXDataType)3, 16640 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	476,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	10,
	s_rgctxValues,
	NULL,
};
