﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 T IngameDebugConsole.CircularBuffer`1::get_Item(System.Int32)
// 0x00000002 System.Int32 IngameDebugConsole.CircularBuffer`1::get_Count()
// 0x00000003 System.Void IngameDebugConsole.CircularBuffer`1::set_Count(System.Int32)
// 0x00000004 System.Void IngameDebugConsole.CircularBuffer`1::.ctor(System.Int32)
// 0x00000005 System.Void IngameDebugConsole.CircularBuffer`1::Add(T)
// 0x00000006 System.String IngameDebugConsole.ConsoleMethodAttribute::get_Command()
extern void ConsoleMethodAttribute_get_Command_mD478B9BA79012AF7FDC068C6A54A184C28EFA671 ();
// 0x00000007 System.String IngameDebugConsole.ConsoleMethodAttribute::get_Description()
extern void ConsoleMethodAttribute_get_Description_mD9DA52A8BBE9A7FFEE604562F866A48CC08B335D ();
// 0x00000008 System.Void IngameDebugConsole.ConsoleMethodAttribute::.ctor(System.String,System.String)
extern void ConsoleMethodAttribute__ctor_mA48C7EE74469E92DBF944931E2DEE8EF5B82CC66 ();
// 0x00000009 System.Void IngameDebugConsole.ConsoleMethodInfo::.ctor(System.Reflection.MethodInfo,System.Type[],System.Object,System.String)
extern void ConsoleMethodInfo__ctor_m56D8A5C993EFB57B695B987AB03BC6D8139E7BC2 ();
// 0x0000000A System.Boolean IngameDebugConsole.ConsoleMethodInfo::IsValid()
extern void ConsoleMethodInfo_IsValid_mFE20AC2FCFBC16D822976C0F220AAF106D109DC5 ();
// 0x0000000B System.Void IngameDebugConsole.DebugLogConsole::.cctor()
extern void DebugLogConsole__cctor_m862D7C15B597711F2793FC46D1C536B8310CE05C ();
// 0x0000000C System.Void IngameDebugConsole.DebugLogConsole::LogAllCommands()
extern void DebugLogConsole_LogAllCommands_mA15A1A60E794C045C5C42FF5FB2AE78C3F553190 ();
// 0x0000000D System.Void IngameDebugConsole.DebugLogConsole::LogSystemInfo()
extern void DebugLogConsole_LogSystemInfo_mEF61B6440FEE95413D798944A1334741167E9F49 ();
// 0x0000000E System.Text.StringBuilder IngameDebugConsole.DebugLogConsole::AppendSysInfoIfPresent(System.Text.StringBuilder,System.String,System.String)
extern void DebugLogConsole_AppendSysInfoIfPresent_mCBE5C57DC83EEFF57E846BC059FAA97BCDD06378 ();
// 0x0000000F System.Text.StringBuilder IngameDebugConsole.DebugLogConsole::AppendSysInfoIfPresent(System.Text.StringBuilder,System.Int32,System.String)
extern void DebugLogConsole_AppendSysInfoIfPresent_m6D66F685B0AD7F81FC8DD0CFA729F6705952BF03 ();
// 0x00000010 System.Void IngameDebugConsole.DebugLogConsole::AddCommandInstance(System.String,System.String,System.String,System.Object)
extern void DebugLogConsole_AddCommandInstance_mD14D9BE5C2D3BF790F6E8E5F0846E6999CDEB25A ();
// 0x00000011 System.Void IngameDebugConsole.DebugLogConsole::AddCommandStatic(System.String,System.String,System.String,System.Type)
extern void DebugLogConsole_AddCommandStatic_m09A7A47E17442393F682AED759D30FBEFE5F2609 ();
// 0x00000012 System.Void IngameDebugConsole.DebugLogConsole::RemoveCommand(System.String)
extern void DebugLogConsole_RemoveCommand_m0DE6EB24F413AA5329C471557C668E492C67A611 ();
// 0x00000013 System.String IngameDebugConsole.DebugLogConsole::GetAutoCompleteCommand(System.String)
extern void DebugLogConsole_GetAutoCompleteCommand_m5B139884D00EC67B427CA9B1D6E955929C02EB60 ();
// 0x00000014 System.Void IngameDebugConsole.DebugLogConsole::AddCommand(System.String,System.String,System.String,System.Type,System.Object)
extern void DebugLogConsole_AddCommand_m6064690D84CAB754D1F3B29FB3A23B09570022F2 ();
// 0x00000015 System.Void IngameDebugConsole.DebugLogConsole::AddCommand(System.String,System.String,System.Reflection.MethodInfo,System.Object)
extern void DebugLogConsole_AddCommand_m066E213957CBA833046584B47B5DE13B5236CC0E ();
// 0x00000016 System.Void IngameDebugConsole.DebugLogConsole::ExecuteCommand(System.String)
extern void DebugLogConsole_ExecuteCommand_m11069FC3C049AFCC921DD00DC0883EB53E2471E8 ();
// 0x00000017 System.Int32 IngameDebugConsole.DebugLogConsole::IndexOfDelimiter(System.Char)
extern void DebugLogConsole_IndexOfDelimiter_m36E23C50486B76B35E5473F32440CD24F9894C63 ();
// 0x00000018 System.Int32 IngameDebugConsole.DebugLogConsole::IndexOfChar(System.String,System.Char,System.Int32)
extern void DebugLogConsole_IndexOfChar_mAD6247DD81CF5634442D5235921E4A9CE9385FA0 ();
// 0x00000019 System.Boolean IngameDebugConsole.DebugLogConsole::ParseString(System.String,System.Object&)
extern void DebugLogConsole_ParseString_mB674603BBFFE765960DE7433879A0883652277B0 ();
// 0x0000001A System.Boolean IngameDebugConsole.DebugLogConsole::ParseBool(System.String,System.Object&)
extern void DebugLogConsole_ParseBool_mF313FA39C7CEF722544518C84DD4D1D0E266F048 ();
// 0x0000001B System.Boolean IngameDebugConsole.DebugLogConsole::ParseInt(System.String,System.Object&)
extern void DebugLogConsole_ParseInt_m5E0E81E29A0BD677AE1DBDE0F8CE571A4E257901 ();
// 0x0000001C System.Boolean IngameDebugConsole.DebugLogConsole::ParseUInt(System.String,System.Object&)
extern void DebugLogConsole_ParseUInt_m298E9FB540E9D14381494AF588192FE2FAA5E2EE ();
// 0x0000001D System.Boolean IngameDebugConsole.DebugLogConsole::ParseLong(System.String,System.Object&)
extern void DebugLogConsole_ParseLong_mE03FB79579B6C7F0389B838DB5F44D1B187CD5D9 ();
// 0x0000001E System.Boolean IngameDebugConsole.DebugLogConsole::ParseULong(System.String,System.Object&)
extern void DebugLogConsole_ParseULong_m20A891807E85A59F29C2C2CDE0FCC7FA7CB8331A ();
// 0x0000001F System.Boolean IngameDebugConsole.DebugLogConsole::ParseByte(System.String,System.Object&)
extern void DebugLogConsole_ParseByte_mA8A14A97D7256304BDA923CF317305AA04DD94AD ();
// 0x00000020 System.Boolean IngameDebugConsole.DebugLogConsole::ParseSByte(System.String,System.Object&)
extern void DebugLogConsole_ParseSByte_m68829FEE1AEE94991B9707322F77CC88983FA2EF ();
// 0x00000021 System.Boolean IngameDebugConsole.DebugLogConsole::ParseShort(System.String,System.Object&)
extern void DebugLogConsole_ParseShort_m4F05D60CB83F051D996C4F575F8157A4D60FE0CA ();
// 0x00000022 System.Boolean IngameDebugConsole.DebugLogConsole::ParseUShort(System.String,System.Object&)
extern void DebugLogConsole_ParseUShort_m4DD8C02FA34D21A85A413A0C7AC6839A75FBAF34 ();
// 0x00000023 System.Boolean IngameDebugConsole.DebugLogConsole::ParseChar(System.String,System.Object&)
extern void DebugLogConsole_ParseChar_mFE7EBC77E7E1B77705D7D74F40F0AF8254EF825B ();
// 0x00000024 System.Boolean IngameDebugConsole.DebugLogConsole::ParseFloat(System.String,System.Object&)
extern void DebugLogConsole_ParseFloat_m34D72DDAE08BE6365E85CD89A203B23A857D024B ();
// 0x00000025 System.Boolean IngameDebugConsole.DebugLogConsole::ParseDouble(System.String,System.Object&)
extern void DebugLogConsole_ParseDouble_m43BA9F7E9E6055743ADC5A5A95CCEBD63D5B0477 ();
// 0x00000026 System.Boolean IngameDebugConsole.DebugLogConsole::ParseDecimal(System.String,System.Object&)
extern void DebugLogConsole_ParseDecimal_m00E66D127C6645EF6812AC47AB46B1AEFAE4CFC0 ();
// 0x00000027 System.Boolean IngameDebugConsole.DebugLogConsole::ParseVector2(System.String,System.Object&)
extern void DebugLogConsole_ParseVector2_mFD8F51B2006463DB8C197705D91F13B1CEDCFD9A ();
// 0x00000028 System.Boolean IngameDebugConsole.DebugLogConsole::ParseVector3(System.String,System.Object&)
extern void DebugLogConsole_ParseVector3_mB021CD42A3990C62847EFFFF06247BE0AEBDC797 ();
// 0x00000029 System.Boolean IngameDebugConsole.DebugLogConsole::ParseVector4(System.String,System.Object&)
extern void DebugLogConsole_ParseVector4_m80534B06E86F87A23AA155B1BDF35A729CBD7822 ();
// 0x0000002A System.Boolean IngameDebugConsole.DebugLogConsole::ParseGameObject(System.String,System.Object&)
extern void DebugLogConsole_ParseGameObject_mC85B1E807C2E6DA9A3E401069A5DECFDCBB9B651 ();
// 0x0000002B System.Boolean IngameDebugConsole.DebugLogConsole::CreateVectorFromInput(System.String,System.Type,System.Object&)
extern void DebugLogConsole_CreateVectorFromInput_m543312F03E5911E883D931EA752BF4B86F3CD9C7 ();
// 0x0000002C System.Void IngameDebugConsole.DebugLogEntry::.ctor(System.String,System.String,UnityEngine.Sprite)
extern void DebugLogEntry__ctor_m581783CEE991B53B76A9CE1EF2A7554A486CD57D ();
// 0x0000002D System.Boolean IngameDebugConsole.DebugLogEntry::Equals(IngameDebugConsole.DebugLogEntry)
extern void DebugLogEntry_Equals_mEE22620D35682F9CB090499D11B90CEB8A2E2BAC ();
// 0x0000002E System.String IngameDebugConsole.DebugLogEntry::ToString()
extern void DebugLogEntry_ToString_mE34567888499EBBF8FA125C56111CA18A569EB5D ();
// 0x0000002F System.Int32 IngameDebugConsole.DebugLogEntry::GetHashCode()
extern void DebugLogEntry_GetHashCode_mB3152939F9B48B4B5C9569BA5C52BDC79D2AC563 ();
// 0x00000030 System.Void IngameDebugConsole.QueuedDebugLogEntry::.ctor(System.String,System.String,UnityEngine.LogType)
extern void QueuedDebugLogEntry__ctor_m2ABD374C862ECFED663AEB6D83EE53AC7672448B_AdjustorThunk ();
// 0x00000031 System.Int32 IngameDebugConsole.DebugLogIndexList::get_Count()
extern void DebugLogIndexList_get_Count_m9AF3D6E984DE3BBD8B59F7C8DDAC7F614ECF579B ();
// 0x00000032 System.Int32 IngameDebugConsole.DebugLogIndexList::get_Item(System.Int32)
extern void DebugLogIndexList_get_Item_m875B10664E1C94292ACB48EE00D21B3A82F6FF9D ();
// 0x00000033 System.Void IngameDebugConsole.DebugLogIndexList::.ctor()
extern void DebugLogIndexList__ctor_m0265B1D8E02A02A937CD0A1E18AA552AF3605AC3 ();
// 0x00000034 System.Void IngameDebugConsole.DebugLogIndexList::Add(System.Int32)
extern void DebugLogIndexList_Add_mCD11B341A9FDA87E1482A0474ED03064EC79360B ();
// 0x00000035 System.Void IngameDebugConsole.DebugLogIndexList::Clear()
extern void DebugLogIndexList_Clear_mA9914DF775A01469F304F3BEC3F04FFD7CADCFD7 ();
// 0x00000036 UnityEngine.RectTransform IngameDebugConsole.DebugLogItem::get_Transform()
extern void DebugLogItem_get_Transform_mF60358BC5F73D95931C0169AF5A264040D1D96B6 ();
// 0x00000037 UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::get_Image()
extern void DebugLogItem_get_Image_m312418E2D986F4AEAF17D1C2203EC093CEC093BF ();
// 0x00000038 System.Int32 IngameDebugConsole.DebugLogItem::get_Index()
extern void DebugLogItem_get_Index_mF9717DDF3D342CDCC33434EA852674F2887699B1 ();
// 0x00000039 System.Void IngameDebugConsole.DebugLogItem::Initialize(IngameDebugConsole.DebugLogRecycledListView)
extern void DebugLogItem_Initialize_m92E6DAE0DDDBD4F18C8140FACEADA92C3F4FB93D ();
// 0x0000003A System.Void IngameDebugConsole.DebugLogItem::SetContent(IngameDebugConsole.DebugLogEntry,System.Int32,System.Boolean)
extern void DebugLogItem_SetContent_m2B76D7772380DB1AD3035FB7254F53EE3D77A60B ();
// 0x0000003B System.Void IngameDebugConsole.DebugLogItem::ShowCount()
extern void DebugLogItem_ShowCount_m661D4DFA40139427083BB417E94A77550F693CF5 ();
// 0x0000003C System.Void IngameDebugConsole.DebugLogItem::HideCount()
extern void DebugLogItem_HideCount_m3FF2E65C9AA833D3445EB2A9BFC7147794326656 ();
// 0x0000003D System.Void IngameDebugConsole.DebugLogItem::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DebugLogItem_OnPointerClick_m3EDD2E51AAA0510C6FE1547C9124D8895E33CA0F ();
// 0x0000003E System.Single IngameDebugConsole.DebugLogItem::CalculateExpandedHeight(System.String)
extern void DebugLogItem_CalculateExpandedHeight_m60708ECA9F68C6124C49CDA1F2E94F2665110AE9 ();
// 0x0000003F System.String IngameDebugConsole.DebugLogItem::ToString()
extern void DebugLogItem_ToString_m4208F63B133E9774295B17A624E95AF20F196AB7 ();
// 0x00000040 System.Void IngameDebugConsole.DebugLogItem::.ctor()
extern void DebugLogItem__ctor_mF0C9B0B66A383B6FCF91E539E00F5D4667FA8A1F ();
// 0x00000041 System.Void IngameDebugConsole.DebugLogManager::Awake()
extern void DebugLogManager_Awake_mAD4BFA6D0064C55721789C2A23FC268F56094C06 ();
// 0x00000042 System.Void IngameDebugConsole.DebugLogManager::OnEnable()
extern void DebugLogManager_OnEnable_mFAED8F3F2415D35FAAB1175B34B96578D102C71A ();
// 0x00000043 System.Void IngameDebugConsole.DebugLogManager::OnDisable()
extern void DebugLogManager_OnDisable_m79C1ECB4D9BABEC9753D2E0932FCADC9AA34C8F1 ();
// 0x00000044 System.Void IngameDebugConsole.DebugLogManager::Start()
extern void DebugLogManager_Start_mD061B4205755EA53A66F0F97222B192A181F525A ();
// 0x00000045 System.Void IngameDebugConsole.DebugLogManager::OnRectTransformDimensionsChange()
extern void DebugLogManager_OnRectTransformDimensionsChange_mBF9B04AD38345A1C504F634E3EE5E8B9F92E3071 ();
// 0x00000046 System.Void IngameDebugConsole.DebugLogManager::LateUpdate()
extern void DebugLogManager_LateUpdate_m5CC2A5B4CA833DDB114866ECE9B692DB459E3AD8 ();
// 0x00000047 System.Void IngameDebugConsole.DebugLogManager::ShowLogWindow()
extern void DebugLogManager_ShowLogWindow_m060BFCDA1B9DF0881794A7BC560DB1EACCB260C0 ();
// 0x00000048 System.Void IngameDebugConsole.DebugLogManager::ShowPopup()
extern void DebugLogManager_ShowPopup_m3A711C76659ECD2129977A10EA948556367A7A35 ();
// 0x00000049 System.Char IngameDebugConsole.DebugLogManager::OnValidateCommand(System.String,System.Int32,System.Char)
extern void DebugLogManager_OnValidateCommand_m3A902BC00C9E4BED8163A1A511606E5D264B5333 ();
// 0x0000004A System.Void IngameDebugConsole.DebugLogManager::ReceivedLog(System.String,System.String,UnityEngine.LogType)
extern void DebugLogManager_ReceivedLog_m1401A0C0FAC1F208C1E0984C2AD536D0948C783E ();
// 0x0000004B System.Void IngameDebugConsole.DebugLogManager::SetSnapToBottom(System.Boolean)
extern void DebugLogManager_SetSnapToBottom_m51372C5BB900B7A83227ED38028097EBB70AE2F0 ();
// 0x0000004C System.Void IngameDebugConsole.DebugLogManager::ValidateScrollPosition()
extern void DebugLogManager_ValidateScrollPosition_mBE75DB575D3386DE1B4C57976295CAD8021C1ABA ();
// 0x0000004D System.Void IngameDebugConsole.DebugLogManager::HideButtonPressed()
extern void DebugLogManager_HideButtonPressed_m20CD1A98FA84BF4F75BD764273D72BF1471DE831 ();
// 0x0000004E System.Void IngameDebugConsole.DebugLogManager::ClearButtonPressed()
extern void DebugLogManager_ClearButtonPressed_mA7C6CCE7D1F32C6BC180FB295A7C2ACA748759C6 ();
// 0x0000004F System.Void IngameDebugConsole.DebugLogManager::CollapseButtonPressed()
extern void DebugLogManager_CollapseButtonPressed_m3BAF06CE6C25ECA00454F727DA32AD6B6E1A9DAC ();
// 0x00000050 System.Void IngameDebugConsole.DebugLogManager::FilterLogButtonPressed()
extern void DebugLogManager_FilterLogButtonPressed_m0EFF40E19A2D5BB9400A3CCFE2F01D99C24FEB73 ();
// 0x00000051 System.Void IngameDebugConsole.DebugLogManager::FilterWarningButtonPressed()
extern void DebugLogManager_FilterWarningButtonPressed_m55D402FD91000803DEB4CF1CAD1F2A518EA29527 ();
// 0x00000052 System.Void IngameDebugConsole.DebugLogManager::FilterErrorButtonPressed()
extern void DebugLogManager_FilterErrorButtonPressed_m551C6D21AB78DC4C6A8D3EC216E7A8E1941CB407 ();
// 0x00000053 System.Void IngameDebugConsole.DebugLogManager::Resize(UnityEngine.EventSystems.BaseEventData)
extern void DebugLogManager_Resize_mA2DBC20DDDB336C4D3359FD9B69E895095D72350 ();
// 0x00000054 System.Void IngameDebugConsole.DebugLogManager::FilterLogs()
extern void DebugLogManager_FilterLogs_mE75AE832B1F8265AF4B85D28E7D3FD6E05266FAE ();
// 0x00000055 System.String IngameDebugConsole.DebugLogManager::GetAllLogs()
extern void DebugLogManager_GetAllLogs_m6A115BF24EBAB6A9EA53F43A09F7DFA3DF7847E2 ();
// 0x00000056 System.Void IngameDebugConsole.DebugLogManager::SaveLogsToFile()
extern void DebugLogManager_SaveLogsToFile_m811FDE13C398386ECA9B89FF0195D66742401F41 ();
// 0x00000057 System.Void IngameDebugConsole.DebugLogManager::PoolLogItem(IngameDebugConsole.DebugLogItem)
extern void DebugLogManager_PoolLogItem_m98D90CF2C96AAF3253600F4F00D00A6A9B8B0BE0 ();
// 0x00000058 IngameDebugConsole.DebugLogItem IngameDebugConsole.DebugLogManager::PopLogItem()
extern void DebugLogManager_PopLogItem_m50DA8E833282F6ABDDF8659536FBE2E3BE4A2B7B ();
// 0x00000059 System.Void IngameDebugConsole.DebugLogManager::.ctor()
extern void DebugLogManager__ctor_mE3452522F9794C7564022EA14B65DC485E57ECC7 ();
// 0x0000005A System.Void IngameDebugConsole.DebugLogManager::.cctor()
extern void DebugLogManager__cctor_m28A88642D332115382F715E0E68078F34AB3C043 ();
// 0x0000005B System.Void IngameDebugConsole.DebugLogPopup::Awake()
extern void DebugLogPopup_Awake_mBB4C67422454CB4F3B3D042A2843004B4A84CA0C ();
// 0x0000005C System.Void IngameDebugConsole.DebugLogPopup::Start()
extern void DebugLogPopup_Start_mA0738E813EC36BD23FE333C0A5AEC9C2C4FE9C6B ();
// 0x0000005D System.Void IngameDebugConsole.DebugLogPopup::OnViewportDimensionsChanged()
extern void DebugLogPopup_OnViewportDimensionsChanged_m245ADE4DEFD7CBC182A5360A54241B7A65EC0055 ();
// 0x0000005E System.Void IngameDebugConsole.DebugLogPopup::NewInfoLogArrived()
extern void DebugLogPopup_NewInfoLogArrived_mC7C19B37B958A4A2BFA9D46F8613128AD71922CA ();
// 0x0000005F System.Void IngameDebugConsole.DebugLogPopup::NewWarningLogArrived()
extern void DebugLogPopup_NewWarningLogArrived_m97FF525B3698296B0521CC62DC169F54DBB3C5B1 ();
// 0x00000060 System.Void IngameDebugConsole.DebugLogPopup::NewErrorLogArrived()
extern void DebugLogPopup_NewErrorLogArrived_m246A4AB394DDFB6747E9B5214037D557058C96B3 ();
// 0x00000061 System.Void IngameDebugConsole.DebugLogPopup::Reset()
extern void DebugLogPopup_Reset_mD25A151791705F4D97B3795D12C79FDCF00BCC52 ();
// 0x00000062 System.Collections.IEnumerator IngameDebugConsole.DebugLogPopup::MoveToPosAnimation(UnityEngine.Vector3)
extern void DebugLogPopup_MoveToPosAnimation_mCA53B9C222FF8675DDE473F4C033D863A697A46E ();
// 0x00000063 System.Void IngameDebugConsole.DebugLogPopup::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DebugLogPopup_OnPointerClick_m6E0DEFD42498D32DBF6A5F319EFDD60FBC6EC088 ();
// 0x00000064 System.Void IngameDebugConsole.DebugLogPopup::Show()
extern void DebugLogPopup_Show_m4EC0D40B6A626943EC154948F46577E23B451965 ();
// 0x00000065 System.Void IngameDebugConsole.DebugLogPopup::Hide()
extern void DebugLogPopup_Hide_mC323970669906549C4FCC2B31776B7BC953FCD98 ();
// 0x00000066 System.Void IngameDebugConsole.DebugLogPopup::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DebugLogPopup_OnBeginDrag_m7794F43AB92D4214F6A32BA5CC11288AA4BB1ED5 ();
// 0x00000067 System.Void IngameDebugConsole.DebugLogPopup::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DebugLogPopup_OnDrag_m95DA5AFBA316E34C0451638DE055E16789C00CD8 ();
// 0x00000068 System.Void IngameDebugConsole.DebugLogPopup::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DebugLogPopup_OnEndDrag_mF7E1B96A0D75180A2BFDCEC49DF891DD4C2DA820 ();
// 0x00000069 System.Void IngameDebugConsole.DebugLogPopup::.ctor()
extern void DebugLogPopup__ctor_m0595073891BE91A06D77312AC669CE2F6D5C6758 ();
// 0x0000006A System.Single IngameDebugConsole.DebugLogRecycledListView::get_ItemHeight()
extern void DebugLogRecycledListView_get_ItemHeight_mA7E5BECF7090F5CE5377703F681C638D9473DDFB ();
// 0x0000006B System.Single IngameDebugConsole.DebugLogRecycledListView::get_SelectedItemHeight()
extern void DebugLogRecycledListView_get_SelectedItemHeight_mFE30C4D4CFAB6C69485098A33BBE69071E060569 ();
// 0x0000006C System.Void IngameDebugConsole.DebugLogRecycledListView::Awake()
extern void DebugLogRecycledListView_Awake_m984EF641EE8AF6AEE8FB5376C2AD1A099AD472E6 ();
// 0x0000006D System.Void IngameDebugConsole.DebugLogRecycledListView::Initialize(IngameDebugConsole.DebugLogManager,System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry>,IngameDebugConsole.DebugLogIndexList,System.Single)
extern void DebugLogRecycledListView_Initialize_m08EED897F152D8E35F8BFF1A4747B2AE013952E6 ();
// 0x0000006E System.Void IngameDebugConsole.DebugLogRecycledListView::SetCollapseMode(System.Boolean)
extern void DebugLogRecycledListView_SetCollapseMode_mBA2B1B3CF6DB85CDE30EE5FABED2A62A18547B16 ();
// 0x0000006F System.Void IngameDebugConsole.DebugLogRecycledListView::OnLogItemClicked(IngameDebugConsole.DebugLogItem)
extern void DebugLogRecycledListView_OnLogItemClicked_mB664C57F918520D6F229BB586E3F18D577AFFB9C ();
// 0x00000070 System.Void IngameDebugConsole.DebugLogRecycledListView::DeselectSelectedLogItem()
extern void DebugLogRecycledListView_DeselectSelectedLogItem_m127BB831F60A9041E61E08F765140B9665E2946F ();
// 0x00000071 System.Void IngameDebugConsole.DebugLogRecycledListView::OnLogEntriesUpdated(System.Boolean)
extern void DebugLogRecycledListView_OnLogEntriesUpdated_m193428818B1E608C24BE67382DA6987146F26F8D ();
// 0x00000072 System.Void IngameDebugConsole.DebugLogRecycledListView::OnCollapsedLogEntryAtIndexUpdated(System.Int32)
extern void DebugLogRecycledListView_OnCollapsedLogEntryAtIndexUpdated_m48443297DAECFFC767DC96106FF01753817A8FFE ();
// 0x00000073 System.Void IngameDebugConsole.DebugLogRecycledListView::OnViewportDimensionsChanged()
extern void DebugLogRecycledListView_OnViewportDimensionsChanged_mD9117460E50F1CDFFC3F1EF7B7D2D1DD97ED750D ();
// 0x00000074 System.Void IngameDebugConsole.DebugLogRecycledListView::HardResetItems()
extern void DebugLogRecycledListView_HardResetItems_m7727F063EC09741978151CFA7B5F136D9C696144 ();
// 0x00000075 System.Void IngameDebugConsole.DebugLogRecycledListView::CalculateContentHeight()
extern void DebugLogRecycledListView_CalculateContentHeight_m8F0A11E1C834A27A66B5C66D96365FD028E7248C ();
// 0x00000076 System.Void IngameDebugConsole.DebugLogRecycledListView::UpdateItemsInTheList(System.Boolean)
extern void DebugLogRecycledListView_UpdateItemsInTheList_mDD945483FC27513B005306193BDD849EF9E810E2 ();
// 0x00000077 System.Void IngameDebugConsole.DebugLogRecycledListView::CreateLogItemsBetweenIndices(System.Int32,System.Int32)
extern void DebugLogRecycledListView_CreateLogItemsBetweenIndices_m1845292669AD2457D9B812AA4DA4A26D3C01F0EB ();
// 0x00000078 System.Void IngameDebugConsole.DebugLogRecycledListView::CreateLogItemAtIndex(System.Int32)
extern void DebugLogRecycledListView_CreateLogItemAtIndex_mE31DA83F79BE1A443127D82F21E7161620A80E12 ();
// 0x00000079 System.Void IngameDebugConsole.DebugLogRecycledListView::DestroyLogItemsBetweenIndices(System.Int32,System.Int32)
extern void DebugLogRecycledListView_DestroyLogItemsBetweenIndices_m9395A3ED8884418B4975474363400F83F7AC39C8 ();
// 0x0000007A System.Void IngameDebugConsole.DebugLogRecycledListView::UpdateLogItemContentsBetweenIndices(System.Int32,System.Int32)
extern void DebugLogRecycledListView_UpdateLogItemContentsBetweenIndices_mCF1B19149C374E8F1C23DE55CE488DE76400518F ();
// 0x0000007B System.Void IngameDebugConsole.DebugLogRecycledListView::ColorLogItem(IngameDebugConsole.DebugLogItem,System.Int32)
extern void DebugLogRecycledListView_ColorLogItem_mF24EBB590D95016F4C362CD13BE5484F28799DBE ();
// 0x0000007C System.Void IngameDebugConsole.DebugLogRecycledListView::.ctor()
extern void DebugLogRecycledListView__ctor_mD6A065A041E2ACD44690DFC76207FB56C3042A36 ();
// 0x0000007D System.Void IngameDebugConsole.DebugsOnScrollListener::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern void DebugsOnScrollListener_OnScroll_m87B88B7819EDDA1F6C0CD9CEB780B0EFBFB72E3D ();
// 0x0000007E System.Void IngameDebugConsole.DebugsOnScrollListener::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DebugsOnScrollListener_OnBeginDrag_m51051903100DD6D67ABF959001F2EBFBD9356726 ();
// 0x0000007F System.Void IngameDebugConsole.DebugsOnScrollListener::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DebugsOnScrollListener_OnEndDrag_m89C9E630E4C402A1E1C46402A0A4E3D163FAB817 ();
// 0x00000080 System.Void IngameDebugConsole.DebugsOnScrollListener::OnScrollbarDragStart(UnityEngine.EventSystems.BaseEventData)
extern void DebugsOnScrollListener_OnScrollbarDragStart_mB47C67D677A478B14AB43D032338E208B0DB6F85 ();
// 0x00000081 System.Void IngameDebugConsole.DebugsOnScrollListener::OnScrollbarDragEnd(UnityEngine.EventSystems.BaseEventData)
extern void DebugsOnScrollListener_OnScrollbarDragEnd_m82D655C329A2F2A6FF7C4640E0A7E9A48D0A1AF0 ();
// 0x00000082 System.Boolean IngameDebugConsole.DebugsOnScrollListener::IsScrollbarAtBottom()
extern void DebugsOnScrollListener_IsScrollbarAtBottom_m2B74EA76407C00937EE6274239A776E2BBE3AACD ();
// 0x00000083 System.Void IngameDebugConsole.DebugsOnScrollListener::.ctor()
extern void DebugsOnScrollListener__ctor_m0005EF85F59F05EA3D3D3FA2BDDE874B1C6B8FD7 ();
// 0x00000084 System.Void IngameDebugConsole.DebugLogConsole_ParseFunction::.ctor(System.Object,System.IntPtr)
extern void ParseFunction__ctor_m51A1AAF3620F9C4E77F8E1F1AEBDCA7C95ED5A32 ();
// 0x00000085 System.Boolean IngameDebugConsole.DebugLogConsole_ParseFunction::Invoke(System.String,System.Object&)
extern void ParseFunction_Invoke_mE7BF52A57105CF934D4080687EE887DFEE67E979 ();
// 0x00000086 System.IAsyncResult IngameDebugConsole.DebugLogConsole_ParseFunction::BeginInvoke(System.String,System.Object&,System.AsyncCallback,System.Object)
extern void ParseFunction_BeginInvoke_m42C2BB91F78E574C2B562ED488A5CE0A3ECA20B7 ();
// 0x00000087 System.Boolean IngameDebugConsole.DebugLogConsole_ParseFunction::EndInvoke(System.Object&,System.IAsyncResult)
extern void ParseFunction_EndInvoke_m16DAFD9D510AC0F701D5E5A50645E108FCD13C65 ();
// 0x00000088 System.Void IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::.ctor(System.Int32)
extern void U3CMoveToPosAnimationU3Ed__24__ctor_m132A6E3F0203341A56DA574368658DE5F8C0A38B ();
// 0x00000089 System.Void IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::System.IDisposable.Dispose()
extern void U3CMoveToPosAnimationU3Ed__24_System_IDisposable_Dispose_m5CD9B9D7D15B111338F5B82D01AB2320890A8254 ();
// 0x0000008A System.Boolean IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::MoveNext()
extern void U3CMoveToPosAnimationU3Ed__24_MoveNext_mC39BD7A41E8E766064B0066002C0C8EBB1FDA3E1 ();
// 0x0000008B System.Object IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveToPosAnimationU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0961C16BF4C62E5E7917AC2D17DCEA4A38EF147 ();
// 0x0000008C System.Void IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::System.Collections.IEnumerator.Reset()
extern void U3CMoveToPosAnimationU3Ed__24_System_Collections_IEnumerator_Reset_mC67D97AA98E1C299A5DFAA08EBBD2150393B1AAA ();
// 0x0000008D System.Object IngameDebugConsole.DebugLogPopup_<MoveToPosAnimation>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CMoveToPosAnimationU3Ed__24_System_Collections_IEnumerator_get_Current_mBDE5661940C8A56FFA3BCC615EC3B852B2ADAE41 ();
static Il2CppMethodPointer s_methodPointers[141] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ConsoleMethodAttribute_get_Command_mD478B9BA79012AF7FDC068C6A54A184C28EFA671,
	ConsoleMethodAttribute_get_Description_mD9DA52A8BBE9A7FFEE604562F866A48CC08B335D,
	ConsoleMethodAttribute__ctor_mA48C7EE74469E92DBF944931E2DEE8EF5B82CC66,
	ConsoleMethodInfo__ctor_m56D8A5C993EFB57B695B987AB03BC6D8139E7BC2,
	ConsoleMethodInfo_IsValid_mFE20AC2FCFBC16D822976C0F220AAF106D109DC5,
	DebugLogConsole__cctor_m862D7C15B597711F2793FC46D1C536B8310CE05C,
	DebugLogConsole_LogAllCommands_mA15A1A60E794C045C5C42FF5FB2AE78C3F553190,
	DebugLogConsole_LogSystemInfo_mEF61B6440FEE95413D798944A1334741167E9F49,
	DebugLogConsole_AppendSysInfoIfPresent_mCBE5C57DC83EEFF57E846BC059FAA97BCDD06378,
	DebugLogConsole_AppendSysInfoIfPresent_m6D66F685B0AD7F81FC8DD0CFA729F6705952BF03,
	DebugLogConsole_AddCommandInstance_mD14D9BE5C2D3BF790F6E8E5F0846E6999CDEB25A,
	DebugLogConsole_AddCommandStatic_m09A7A47E17442393F682AED759D30FBEFE5F2609,
	DebugLogConsole_RemoveCommand_m0DE6EB24F413AA5329C471557C668E492C67A611,
	DebugLogConsole_GetAutoCompleteCommand_m5B139884D00EC67B427CA9B1D6E955929C02EB60,
	DebugLogConsole_AddCommand_m6064690D84CAB754D1F3B29FB3A23B09570022F2,
	DebugLogConsole_AddCommand_m066E213957CBA833046584B47B5DE13B5236CC0E,
	DebugLogConsole_ExecuteCommand_m11069FC3C049AFCC921DD00DC0883EB53E2471E8,
	DebugLogConsole_IndexOfDelimiter_m36E23C50486B76B35E5473F32440CD24F9894C63,
	DebugLogConsole_IndexOfChar_mAD6247DD81CF5634442D5235921E4A9CE9385FA0,
	DebugLogConsole_ParseString_mB674603BBFFE765960DE7433879A0883652277B0,
	DebugLogConsole_ParseBool_mF313FA39C7CEF722544518C84DD4D1D0E266F048,
	DebugLogConsole_ParseInt_m5E0E81E29A0BD677AE1DBDE0F8CE571A4E257901,
	DebugLogConsole_ParseUInt_m298E9FB540E9D14381494AF588192FE2FAA5E2EE,
	DebugLogConsole_ParseLong_mE03FB79579B6C7F0389B838DB5F44D1B187CD5D9,
	DebugLogConsole_ParseULong_m20A891807E85A59F29C2C2CDE0FCC7FA7CB8331A,
	DebugLogConsole_ParseByte_mA8A14A97D7256304BDA923CF317305AA04DD94AD,
	DebugLogConsole_ParseSByte_m68829FEE1AEE94991B9707322F77CC88983FA2EF,
	DebugLogConsole_ParseShort_m4F05D60CB83F051D996C4F575F8157A4D60FE0CA,
	DebugLogConsole_ParseUShort_m4DD8C02FA34D21A85A413A0C7AC6839A75FBAF34,
	DebugLogConsole_ParseChar_mFE7EBC77E7E1B77705D7D74F40F0AF8254EF825B,
	DebugLogConsole_ParseFloat_m34D72DDAE08BE6365E85CD89A203B23A857D024B,
	DebugLogConsole_ParseDouble_m43BA9F7E9E6055743ADC5A5A95CCEBD63D5B0477,
	DebugLogConsole_ParseDecimal_m00E66D127C6645EF6812AC47AB46B1AEFAE4CFC0,
	DebugLogConsole_ParseVector2_mFD8F51B2006463DB8C197705D91F13B1CEDCFD9A,
	DebugLogConsole_ParseVector3_mB021CD42A3990C62847EFFFF06247BE0AEBDC797,
	DebugLogConsole_ParseVector4_m80534B06E86F87A23AA155B1BDF35A729CBD7822,
	DebugLogConsole_ParseGameObject_mC85B1E807C2E6DA9A3E401069A5DECFDCBB9B651,
	DebugLogConsole_CreateVectorFromInput_m543312F03E5911E883D931EA752BF4B86F3CD9C7,
	DebugLogEntry__ctor_m581783CEE991B53B76A9CE1EF2A7554A486CD57D,
	DebugLogEntry_Equals_mEE22620D35682F9CB090499D11B90CEB8A2E2BAC,
	DebugLogEntry_ToString_mE34567888499EBBF8FA125C56111CA18A569EB5D,
	DebugLogEntry_GetHashCode_mB3152939F9B48B4B5C9569BA5C52BDC79D2AC563,
	QueuedDebugLogEntry__ctor_m2ABD374C862ECFED663AEB6D83EE53AC7672448B_AdjustorThunk,
	DebugLogIndexList_get_Count_m9AF3D6E984DE3BBD8B59F7C8DDAC7F614ECF579B,
	DebugLogIndexList_get_Item_m875B10664E1C94292ACB48EE00D21B3A82F6FF9D,
	DebugLogIndexList__ctor_m0265B1D8E02A02A937CD0A1E18AA552AF3605AC3,
	DebugLogIndexList_Add_mCD11B341A9FDA87E1482A0474ED03064EC79360B,
	DebugLogIndexList_Clear_mA9914DF775A01469F304F3BEC3F04FFD7CADCFD7,
	DebugLogItem_get_Transform_mF60358BC5F73D95931C0169AF5A264040D1D96B6,
	DebugLogItem_get_Image_m312418E2D986F4AEAF17D1C2203EC093CEC093BF,
	DebugLogItem_get_Index_mF9717DDF3D342CDCC33434EA852674F2887699B1,
	DebugLogItem_Initialize_m92E6DAE0DDDBD4F18C8140FACEADA92C3F4FB93D,
	DebugLogItem_SetContent_m2B76D7772380DB1AD3035FB7254F53EE3D77A60B,
	DebugLogItem_ShowCount_m661D4DFA40139427083BB417E94A77550F693CF5,
	DebugLogItem_HideCount_m3FF2E65C9AA833D3445EB2A9BFC7147794326656,
	DebugLogItem_OnPointerClick_m3EDD2E51AAA0510C6FE1547C9124D8895E33CA0F,
	DebugLogItem_CalculateExpandedHeight_m60708ECA9F68C6124C49CDA1F2E94F2665110AE9,
	DebugLogItem_ToString_m4208F63B133E9774295B17A624E95AF20F196AB7,
	DebugLogItem__ctor_mF0C9B0B66A383B6FCF91E539E00F5D4667FA8A1F,
	DebugLogManager_Awake_mAD4BFA6D0064C55721789C2A23FC268F56094C06,
	DebugLogManager_OnEnable_mFAED8F3F2415D35FAAB1175B34B96578D102C71A,
	DebugLogManager_OnDisable_m79C1ECB4D9BABEC9753D2E0932FCADC9AA34C8F1,
	DebugLogManager_Start_mD061B4205755EA53A66F0F97222B192A181F525A,
	DebugLogManager_OnRectTransformDimensionsChange_mBF9B04AD38345A1C504F634E3EE5E8B9F92E3071,
	DebugLogManager_LateUpdate_m5CC2A5B4CA833DDB114866ECE9B692DB459E3AD8,
	DebugLogManager_ShowLogWindow_m060BFCDA1B9DF0881794A7BC560DB1EACCB260C0,
	DebugLogManager_ShowPopup_m3A711C76659ECD2129977A10EA948556367A7A35,
	DebugLogManager_OnValidateCommand_m3A902BC00C9E4BED8163A1A511606E5D264B5333,
	DebugLogManager_ReceivedLog_m1401A0C0FAC1F208C1E0984C2AD536D0948C783E,
	DebugLogManager_SetSnapToBottom_m51372C5BB900B7A83227ED38028097EBB70AE2F0,
	DebugLogManager_ValidateScrollPosition_mBE75DB575D3386DE1B4C57976295CAD8021C1ABA,
	DebugLogManager_HideButtonPressed_m20CD1A98FA84BF4F75BD764273D72BF1471DE831,
	DebugLogManager_ClearButtonPressed_mA7C6CCE7D1F32C6BC180FB295A7C2ACA748759C6,
	DebugLogManager_CollapseButtonPressed_m3BAF06CE6C25ECA00454F727DA32AD6B6E1A9DAC,
	DebugLogManager_FilterLogButtonPressed_m0EFF40E19A2D5BB9400A3CCFE2F01D99C24FEB73,
	DebugLogManager_FilterWarningButtonPressed_m55D402FD91000803DEB4CF1CAD1F2A518EA29527,
	DebugLogManager_FilterErrorButtonPressed_m551C6D21AB78DC4C6A8D3EC216E7A8E1941CB407,
	DebugLogManager_Resize_mA2DBC20DDDB336C4D3359FD9B69E895095D72350,
	DebugLogManager_FilterLogs_mE75AE832B1F8265AF4B85D28E7D3FD6E05266FAE,
	DebugLogManager_GetAllLogs_m6A115BF24EBAB6A9EA53F43A09F7DFA3DF7847E2,
	DebugLogManager_SaveLogsToFile_m811FDE13C398386ECA9B89FF0195D66742401F41,
	DebugLogManager_PoolLogItem_m98D90CF2C96AAF3253600F4F00D00A6A9B8B0BE0,
	DebugLogManager_PopLogItem_m50DA8E833282F6ABDDF8659536FBE2E3BE4A2B7B,
	DebugLogManager__ctor_mE3452522F9794C7564022EA14B65DC485E57ECC7,
	DebugLogManager__cctor_m28A88642D332115382F715E0E68078F34AB3C043,
	DebugLogPopup_Awake_mBB4C67422454CB4F3B3D042A2843004B4A84CA0C,
	DebugLogPopup_Start_mA0738E813EC36BD23FE333C0A5AEC9C2C4FE9C6B,
	DebugLogPopup_OnViewportDimensionsChanged_m245ADE4DEFD7CBC182A5360A54241B7A65EC0055,
	DebugLogPopup_NewInfoLogArrived_mC7C19B37B958A4A2BFA9D46F8613128AD71922CA,
	DebugLogPopup_NewWarningLogArrived_m97FF525B3698296B0521CC62DC169F54DBB3C5B1,
	DebugLogPopup_NewErrorLogArrived_m246A4AB394DDFB6747E9B5214037D557058C96B3,
	DebugLogPopup_Reset_mD25A151791705F4D97B3795D12C79FDCF00BCC52,
	DebugLogPopup_MoveToPosAnimation_mCA53B9C222FF8675DDE473F4C033D863A697A46E,
	DebugLogPopup_OnPointerClick_m6E0DEFD42498D32DBF6A5F319EFDD60FBC6EC088,
	DebugLogPopup_Show_m4EC0D40B6A626943EC154948F46577E23B451965,
	DebugLogPopup_Hide_mC323970669906549C4FCC2B31776B7BC953FCD98,
	DebugLogPopup_OnBeginDrag_m7794F43AB92D4214F6A32BA5CC11288AA4BB1ED5,
	DebugLogPopup_OnDrag_m95DA5AFBA316E34C0451638DE055E16789C00CD8,
	DebugLogPopup_OnEndDrag_mF7E1B96A0D75180A2BFDCEC49DF891DD4C2DA820,
	DebugLogPopup__ctor_m0595073891BE91A06D77312AC669CE2F6D5C6758,
	DebugLogRecycledListView_get_ItemHeight_mA7E5BECF7090F5CE5377703F681C638D9473DDFB,
	DebugLogRecycledListView_get_SelectedItemHeight_mFE30C4D4CFAB6C69485098A33BBE69071E060569,
	DebugLogRecycledListView_Awake_m984EF641EE8AF6AEE8FB5376C2AD1A099AD472E6,
	DebugLogRecycledListView_Initialize_m08EED897F152D8E35F8BFF1A4747B2AE013952E6,
	DebugLogRecycledListView_SetCollapseMode_mBA2B1B3CF6DB85CDE30EE5FABED2A62A18547B16,
	DebugLogRecycledListView_OnLogItemClicked_mB664C57F918520D6F229BB586E3F18D577AFFB9C,
	DebugLogRecycledListView_DeselectSelectedLogItem_m127BB831F60A9041E61E08F765140B9665E2946F,
	DebugLogRecycledListView_OnLogEntriesUpdated_m193428818B1E608C24BE67382DA6987146F26F8D,
	DebugLogRecycledListView_OnCollapsedLogEntryAtIndexUpdated_m48443297DAECFFC767DC96106FF01753817A8FFE,
	DebugLogRecycledListView_OnViewportDimensionsChanged_mD9117460E50F1CDFFC3F1EF7B7D2D1DD97ED750D,
	DebugLogRecycledListView_HardResetItems_m7727F063EC09741978151CFA7B5F136D9C696144,
	DebugLogRecycledListView_CalculateContentHeight_m8F0A11E1C834A27A66B5C66D96365FD028E7248C,
	DebugLogRecycledListView_UpdateItemsInTheList_mDD945483FC27513B005306193BDD849EF9E810E2,
	DebugLogRecycledListView_CreateLogItemsBetweenIndices_m1845292669AD2457D9B812AA4DA4A26D3C01F0EB,
	DebugLogRecycledListView_CreateLogItemAtIndex_mE31DA83F79BE1A443127D82F21E7161620A80E12,
	DebugLogRecycledListView_DestroyLogItemsBetweenIndices_m9395A3ED8884418B4975474363400F83F7AC39C8,
	DebugLogRecycledListView_UpdateLogItemContentsBetweenIndices_mCF1B19149C374E8F1C23DE55CE488DE76400518F,
	DebugLogRecycledListView_ColorLogItem_mF24EBB590D95016F4C362CD13BE5484F28799DBE,
	DebugLogRecycledListView__ctor_mD6A065A041E2ACD44690DFC76207FB56C3042A36,
	DebugsOnScrollListener_OnScroll_m87B88B7819EDDA1F6C0CD9CEB780B0EFBFB72E3D,
	DebugsOnScrollListener_OnBeginDrag_m51051903100DD6D67ABF959001F2EBFBD9356726,
	DebugsOnScrollListener_OnEndDrag_m89C9E630E4C402A1E1C46402A0A4E3D163FAB817,
	DebugsOnScrollListener_OnScrollbarDragStart_mB47C67D677A478B14AB43D032338E208B0DB6F85,
	DebugsOnScrollListener_OnScrollbarDragEnd_m82D655C329A2F2A6FF7C4640E0A7E9A48D0A1AF0,
	DebugsOnScrollListener_IsScrollbarAtBottom_m2B74EA76407C00937EE6274239A776E2BBE3AACD,
	DebugsOnScrollListener__ctor_m0005EF85F59F05EA3D3D3FA2BDDE874B1C6B8FD7,
	ParseFunction__ctor_m51A1AAF3620F9C4E77F8E1F1AEBDCA7C95ED5A32,
	ParseFunction_Invoke_mE7BF52A57105CF934D4080687EE887DFEE67E979,
	ParseFunction_BeginInvoke_m42C2BB91F78E574C2B562ED488A5CE0A3ECA20B7,
	ParseFunction_EndInvoke_m16DAFD9D510AC0F701D5E5A50645E108FCD13C65,
	U3CMoveToPosAnimationU3Ed__24__ctor_m132A6E3F0203341A56DA574368658DE5F8C0A38B,
	U3CMoveToPosAnimationU3Ed__24_System_IDisposable_Dispose_m5CD9B9D7D15B111338F5B82D01AB2320890A8254,
	U3CMoveToPosAnimationU3Ed__24_MoveNext_mC39BD7A41E8E766064B0066002C0C8EBB1FDA3E1,
	U3CMoveToPosAnimationU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0961C16BF4C62E5E7917AC2D17DCEA4A38EF147,
	U3CMoveToPosAnimationU3Ed__24_System_Collections_IEnumerator_Reset_mC67D97AA98E1C299A5DFAA08EBBD2150393B1AAA,
	U3CMoveToPosAnimationU3Ed__24_System_Collections_IEnumerator_get_Current_mBDE5661940C8A56FFA3BCC615EC3B852B2ADAE41,
};
static const int32_t s_InvokerIndices[141] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	27,
	455,
	89,
	3,
	3,
	3,
	2,
	250,
	1427,
	1427,
	163,
	0,
	2244,
	1427,
	163,
	239,
	2245,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	225,
	401,
	206,
	9,
	14,
	10,
	118,
	10,
	37,
	23,
	32,
	23,
	14,
	14,
	10,
	26,
	371,
	23,
	23,
	26,
	228,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2172,
	118,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	23,
	26,
	14,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1587,
	26,
	23,
	23,
	26,
	26,
	26,
	23,
	744,
	744,
	23,
	2247,
	31,
	26,
	23,
	31,
	32,
	23,
	23,
	23,
	31,
	129,
	32,
	129,
	129,
	130,
	23,
	26,
	26,
	26,
	26,
	26,
	89,
	23,
	124,
	815,
	2246,
	2208,
	32,
	23,
	89,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000002, { 0, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)2, 23362 },
	{ (Il2CppRGCTXDataType)3, 16641 },
	{ (Il2CppRGCTXDataType)3, 16642 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	141,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
};
